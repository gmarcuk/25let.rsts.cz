import fs from 'fs';
import path from 'path';

export default function getFiles(dir, filter) {
    let files = fs.readdirSync(dir).filter(file => fs.statSync(path.join(dir, file)).isFile());

    if (filter) {
        files = files.filter(file => file.match(new RegExp(filter)));
    }

    return files;
}