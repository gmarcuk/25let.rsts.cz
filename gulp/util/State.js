let _state = {};

const State = {

    get: key => {
        if (key in _state) {
            return _state[key];
        }
        return null;
    },

    set: (key, value) => {
        _state[key] = value;
        return value;
    },

    getAll: () => {
        return Object.assign({}, _state);
    }
}

export default State;