import gutil from 'gulp-util';
import notifier from 'node-notifier';

const statsLog = {
    colors: true,
    chunks: false
}

function onError(error, callback) {
    let formatedError = new gutil.PluginError('webpack', error);

    notifier.notify({
        title: `Error: ${formatedError.plugin}`,
        message: formatedError.message
    });

    callback(formatedError);
}

function onSuccess(info) {
    gutil.log('[webpack]', info);
}

export default function handleStats(error, stats, callback) {
    if (error) {
        onError(error, callback)
    } else if (stats.hasErrors()) {
        onError(stats.toString(statsLog), callback);
    } else {
        onSuccess(stats.toString(statsLog));
    }
}