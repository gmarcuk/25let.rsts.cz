import fs from 'fs';

export default function folderExists(filePath) {
    try
    {
        return fs.statSync(filePath).isDirectory();
    }
    catch (err)
    {
        return false;
    }
}