import webpack from 'webpack';
import path from 'path';
import analyzer from 'webpack-bundle-analyzer';
import config from './config';

let plugins = [
    new webpack.ProvidePlugin({
        'window.jQuery': 'jquery',
        jQuery: 'jquery',
        $: 'jquery'
    }),
];

if (config.options.analyze) {
    plugins.push(new analyzer.BundleAnalyzerPlugin());
}

export default {
    devtool: 'source-map',
    module: {
        rules: [
            {
                test: /\.js$/,
                loader: 'babel-loader',
                exclude: [
                    path.resolve(__dirname, '../node_modules'),
                    path.resolve(__dirname, '../bower_components')
                ],
                options: {
                    presets: ['es2015', 'stage-0'],
                    plugins: [
                        'transform-function-bind',
                        'transform-decorators-legacy'
                    ]
                }
            }
        ]
    },
    plugins: plugins
}