import gulp         from  'gulp';
import gutil        from  'gulp-util';
import rename       from  'gulp-rename';
import debug        from  'gulp-debug';
import fs           from  'fs';
import path         from  'path';
import mergeStream  from  'merge-stream';
import runSequence  from  'run-sequence';
import del          from  'del';
import config       from  '../config';

import getFolders   from  '../util/getFolders.js';
import getFiles     from  '../util/getFiles.js';
import folderExists from  '../util/folderExists.js';

const src = './src';

gulp.task('upgrade:image-raw', callback => {
    let tasks = [];

    let targets = getFolders('./src');
    tasks = targets
        .map(target => {
            const folder = path.join(src, target, 'img-raw');
            const imageFolder = path.join(src, target, 'img');

            if (folderExists(folder)) {
                return gulp.src(folder + '/**/*')
                    .pipe(rename(path => {
                        // skip folders
                        if (path.extname) {
                            path.basename += '@';
                        }
                    }))
                    .pipe(gulp.dest(imageFolder))
                    .on('end', () => {
                        del(folder);
                        gutil.log('[upgrade:image-raw]', gutil.colors.yellow('Moved'), gutil.colors.blue(folder));
                    });
            }

            return null;
        })
        .filter(task => task);

    if (tasks.length) {
        return mergeStream(tasks);
    } else {
        gutil.log('[upgrade:image-raw]', gutil.colors.green('Nothing to upgrade'));
        callback();
    }
});

gulp.task('upgrade:image-raw:config', callback => {
    if (!('rawSrc' in config.images)) {
        gutil.log('[upgrade:image-raw:config]', gutil.colors.green('Nothing to upgrade'));
        callback();
    } else {
        const file = './gulp/config.js';
        fs.readFile(file, 'utf8', function(err, data) {
            data = data.replace(/\s*rawSrc[^\n]*/, '');
            fs.writeFile(file, data);
            gutil.log('[upgrade:image-raw:config]', gutil.colors.yellow('Config updated'));
            callback();
        });
    }
});

gulp.task('upgrade', callback => runSequence('upgrade:image-raw', 'upgrade:image-raw:config', callback));