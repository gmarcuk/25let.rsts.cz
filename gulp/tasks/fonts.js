import gulp    from 'gulp';
import changed from 'gulp-changed';
import mainConfig  from '../config';

const config = mainConfig.fonts;

gulp.task('fonts', () => {
    return gulp.src(config.src)
        .pipe(changed(config.dest))
        .pipe(gulp.dest(config.dest));
});