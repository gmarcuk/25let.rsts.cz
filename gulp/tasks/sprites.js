import gulp         from 'gulp';
import spritesmith  from 'gulp.spritesmith';
import rename       from 'gulp-rename';
import path         from 'path';
import fs           from 'fs';
import mergeStream  from 'merge-stream';
import getFiles     from '../util/getFiles';
import getFolders   from '../util/getFolders';
import folderExists from '../util/folderExists';
import fileExists   from '../util/fileExists';
import mainConfig   from '../config';

const config = mainConfig.sprites;

function cleanupImages(sourceFolder, optimization) {
    if (folderExists(sourceFolder)) {
        let images = getFiles(sourceFolder);
        images.forEach(file => {
            const folder = path.join(config.src, path.basename(file.replace('@2x',''), '.png'));
            const image = path.join(sourceFolder, file);

            if (optimization) {
                if (!folderExists(folder) && !folderExists(folder + '@')) {
                    fs.unlinkSync(image);
                }
            } else {
                if (!folderExists(folder)) {
                    fs.unlinkSync(image);
                }
            }

        });
    }
}

gulp.task('sprites:clean', callback => {

    cleanupImages(config.img, false);
    cleanupImages(config.dest, true);

    // css cleanup
    if (folderExists(config.css)) {
        let styles = getFiles(config.css, 'sprite');
        styles.forEach(file => {
            const name = path.basename(file, '.styl').replace('sprite-', '');
            const folder = path.join(config.src, name);
            const style = path.join(config.css, file);

            if (!folderExists(folder) && !folderExists(folder + '@')) {
                fs.unlinkSync(style);
            }
        });
    }

    callback();
});

gulp.task('sprites', ['sprites:clean'], callback => {
    let folders = getFolders(config.src);
    let tasks = folders
        .filter(folder => {
            // optimization flag
            const folderName = folder.replace(/@$/, '');

            const source = path.join(config.src, folder);
            const image = path.join(config.img, folder + '.png');
            const style = path.join(config.css, 'sprite-' + folderName + '.styl');

            return !fs.existsSync(image) || !fs.existsSync(style) || fs.statSync(source).mtime > fs.statSync(image).mtime;
        })
        .map(folder => {
            // optimization flag
            const folderName = folder.replace(/@$/, '');
            const retinaFiles = getFiles(path.join(config.src, folder), '@2x');

            const spriteOptions = {
                imgName: folder + '.png',
                imgPath: 'sprites/' + folderName + '.png',
                algorithm: 'binary-tree',
                cssName: 'sprite-' + folderName + '.styl',
                cssTemplate: config.template,
                cssOpts: {
                    'functions': true,
                    'name': folderName,
                    'retina': false
                }
            };

            if (retinaFiles.length) {
                spriteOptions.retinaSrcFilter = path.join(config.src, folder, '/*@2x.png');
                spriteOptions.retinaImgName = folderName + '@2x' + (folderName !== folder ? '@' : '') + '.png';
                spriteOptions.retinaImgPath = 'sprites/' + folderName + '@2x.png';
                spriteOptions.cssOpts.retina = true;
            }

            var spriteData = gulp.src(path.join(config.src, folder, '/*.png'))
                .pipe(spritesmith(spriteOptions));

            spriteData.img.pipe(gulp.dest(config.img));
            spriteData.css.pipe(gulp.dest(config.css));

            return spriteData;
        });

    if (tasks.length) {
        return mergeStream(tasks);
    } else {
        callback();
    }
});