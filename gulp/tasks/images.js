import gulp        from 'gulp';
import gutil       from 'gulp-util';
import changed     from 'gulp-changed';
import rename      from 'gulp-rename';
import filter      from 'gulp-filter';
import imagemin    from 'gulp-imagemin';
import pngquant    from 'imagemin-pngquant';
import mozjpeg     from 'imagemin-mozjpeg';
import mainConfig  from '../config';
import debug       from 'gulp-debug';

const config = mainConfig.images;

gulp.task('images', () => {

    if ('rawSrc' in config) {
        gutil.log(gutil.colors.yellow('Složka img-raw není již podporovaná. Spusťe gulp upgrade.'));
    }

    var optimizationFilter = filter(file => /[^@]\.[^\.]*$/.test(file.path), { restore: true });
    var renameFilter = filter(file => /@\.[^\.]*$/.test(file.path), { restore: true });

    return gulp.src(config.src)
        .pipe(changed(config.dest))
        .pipe(optimizationFilter)
        .pipe(
            imagemin([
                imagemin.gifsicle({interlaced: true}),
                imagemin.svgo({plugins: [{removeViewBox: true}]}),
                pngquant({
                    quality: '70-90',
                    speed: 4
                }),
                mozjpeg({
                    progressive: true,
                    quality: 90
                })
            ])
        )
        .pipe(optimizationFilter.restore)
        .pipe(renameFilter)
        .pipe(rename((path) => {
            path.basename = path.basename.replace(/@$/, '');
        }))
        .pipe(renameFilter.restore)
        .pipe(gulp.dest(config.dest));
});