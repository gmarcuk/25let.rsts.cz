import gulp     from 'gulp';
import uglify   from 'gulp-uglify';
import rename   from 'gulp-rename';
import gulpif   from 'gulp-if';
import gutil    from 'gulp-util';
import changed  from 'gulp-changed';
import lazypipe from 'lazypipe';
import webpack  from 'webpack';

import handleStats from '../util/handleStats';
import State from '../util/State';

import config from '../config';
import webpackConfig from '../webpack.config';
Object.assign(webpackConfig, config.webpack);

const { options, scriptsCopy } = config;

const copyBuild = lazypipe()
    .pipe(uglify)
    .pipe(rename, {extname: '.min.js'})
    .pipe(gulp.dest, scriptsCopy.dest);

gulp.task('scripts:copy', () => {
     return gulp.src(scriptsCopy.src)
        .pipe(gulpif(!options.production, changed(scriptsCopy.dest)))
        .pipe(gulp.dest(scriptsCopy.dest))
        .pipe(gulpif(options.production, copyBuild()));
});

// used for production build
function webpackBuild(config, callback) {
    config.plugins = config.plugins.concat([
        new webpack.DefinePlugin({
            "process.env": {
                "NODE_ENV": JSON.stringify("production")
            }
        }),
        new webpack.optimize.UglifyJsPlugin({
            sourceMap: true,
            compress: {
                warnings: true
            }
        })
    ]);
    config.output.filename = '[name].min.js';

    webpack(config, (err, stats) => {
        handleStats(err, stats, callback);
        callback();
    });
}

gulp.task('scripts', ['scripts:copy'], callback => {
    // skip on watch or serve
    if (State.get('task') === 'watch' || State.get('task') === 'serve') {
        callback();
    } else {
        webpack(webpackConfig, (err, stats) => {
            handleStats(err, stats, callback);

            if (options.production) {
                webpackBuild(webpackConfig, callback);
            } else {
                callback();
            }
        });
    }
});

gulp.task('scripts:watch', callback => {
    webpackConfig.watch = true;
    webpack(webpackConfig,  (err, stats) => {
        handleStats(err, stats, callback);
    });
});