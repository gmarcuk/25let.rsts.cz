import gulp    from 'gulp';
import config  from '../config';
import runSequence from 'run-sequence';
import State from '../util/State';

gulp.task('watch:tasksConfig', callback => {
    let tasks = [];
    if (State.get('task') !== 'serve') {
        State.set('task', 'watch');
        tasks.push('scripts:watch');
        runSequence('build', tasks, callback);
    } else {
        runSequence('build', callback);
    }
});

gulp.task('watch', ['watch:tasksConfig'], (callback) => {
    gulp.watch(config.styles.watch, ['styles']);
    gulp.watch(config.images.watch, () => runSequence('images', 'styles'));
    gulp.watch(config.iconfont.src, ['iconfont']);
    gulp.watch(config.fonts.src, ['fonts']);
    gulp.watch(config.sprites.watch, ['sprites']);
    gulp.watch(config.scriptsCopy.watch, ['scripts:copy']);
    callback();
});