import gulp        from 'gulp';
import iconfont    from 'gulp-iconfont';
import consolidate from 'gulp-consolidate';
import lodash      from 'lodash';
import rename      from 'gulp-rename';
import mainConfig  from '../config';

const config = mainConfig.iconfont;

gulp.task('iconfont', () => {
    return gulp.src(config.src)
        .pipe(iconfont({
            fontName: config.name,
            normalize: true,
            appendUnicode: false,
            formats: ['ttf', 'eot', 'woff', 'woff2']
        }))
        .on('glyphs', (glyphs, options) => {
            gulp.src(config.template)
                .pipe(consolidate('lodash', {
                    glyphs: glyphs,
                    fontName: config.name,
                    fontPath: config.path
                }))
                .pipe(rename({extname: '.styl'}))
                .pipe(gulp.dest(config.css));
        })
        .pipe(gulp.dest(config.dest));
});