import gulp            from 'gulp';
import stylus          from 'gulp-stylus';
import postcss         from 'gulp-postcss';
import gulpif          from 'gulp-if';
import rename          from 'gulp-rename';
import lazypipe        from 'lazypipe';
import path            from 'path';
import handleErrors    from '../util/handleErrors';
import comparemq       from 'compare-media-queries';
import mainConfig      from '../config';
import debug           from 'gulp-debug';
import fs              from 'fs';

const config = mainConfig.styles;
const options = mainConfig.options;

/**
 * Postcss modules
 */
import postcssPixrem from 'pixrem';
import postcssAutoprefixer from 'autoprefixer';
import postcssMqpacker from 'css-mqpacker';
import postcssUrl from 'postcss-url';
import csso from 'postcss-csso';

let pixrem = postcssPixrem({
    atrules: true
});

let autoprefixer = postcssAutoprefixer();

let mqpacker = postcssMqpacker({
    sort: comparemq
});

let url = postcssUrl({
    url: (urlObject, postObject) => {
        let url = urlObject.url;
        const imagePath = path.resolve(config.dest, url);
        let time = false;
        if (fs.existsSync(imagePath)) {
            const index = url.lastIndexOf('.');
            time = Math.round((new Date(fs.statSync(imagePath).mtime)).getTime()/1000) + '';
            url = url.substring(0, index) + '.' + time + url.substring(index);
        }
        return url;
    }
});

/**
 * Paths
 */
const includePaths = [path.resolve(__dirname, '../../', config.paths)];
const imagePath = [path.resolve(__dirname, '../../', config.dest) + '/'];

const build = lazypipe()
    .pipe(postcss, [csso, pixrem, url])
    .pipe(rename, {extname: '.min.css'})
    .pipe(gulp.dest, config.dest);

gulp.task('styles', () => {
    return gulp.src(config.src)
        .pipe(stylus({
            silent: false,
            'include css': true,
            define: {
                '$image-root': config.imgRoot
            },
            paths: includePaths,
            url: {
                name: 'inline-url',
                paths: imagePath,
                limit: false
            }
        }))
        .on('error', handleErrors)
        .pipe(postcss([
            pixrem,
            autoprefixer,
            mqpacker
        ]))
        .pipe(gulp.dest(config.dest))
        .pipe(gulpif(options.production, build()));
});