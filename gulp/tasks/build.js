import gulp from 'gulp';
import runSequence from 'run-sequence';

gulp.task('build', callback => {
    runSequence(['iconfont', 'sprites'], ['fonts', 'images'], ['styles', 'scripts'], callback);
});