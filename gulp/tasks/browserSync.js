import browserSync from 'browser-sync';
import gulp        from 'gulp';
import config from '../config';

import webpack from 'webpack';
import defaultWebpackConfig from '../webpack.config';

import handleStats from '../util/handleStats';

gulp.task('browserSync', callback => {

    let webpackConfig = Object.create(defaultWebpackConfig);
    Object.assign(webpackConfig, config.webpack);

    var bundler = webpack(webpackConfig);
    bundler.plugin('done', stats => {
        if (server && server.active) {
            server.reload();
        }
    });

    bundler.watch({}, (err, stats) => {
        handleStats(err, stats, callback);
    });

    var server = browserSync(config.browserSync);
});