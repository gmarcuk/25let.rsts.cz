import gulp    from 'gulp';
import runSequence from 'run-sequence';
import State from '../util/State';

gulp.task('serve:flag', callback => {
    State.set('task', 'serve');
    callback();
});

gulp.task('serve', ['serve:flag'], callback => runSequence('watch', 'browserSync', callback));