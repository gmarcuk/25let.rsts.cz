import pkg from '../package.json';
import minimist from 'minimist';
import path from 'path';
import webpack from 'webpack';

const argv = minimist(process.argv.slice(2), {
    alias: {
        p: 'production',
        t: 'target',
        a: 'analyze'
    }
});

const options = {
    production: argv.production,
    target: argv.target || 'frontend',
    analyze: argv.analyze || false
};

const dest = 'public/assets/' + options.target;
const src = 'src/' + options.target;
const templates = 'gulp/templates';

export default {
    options: options,
    browserSync: {
        proxy: pkg.name + '.loc',
        files: [
            dest + '/css/**',
            '!' + dest + '/**.map'
        ]
    },
    styles: {
        src: [src + '/css/*.styl', '!' + src + '/css/imports.styl'],
        dest: dest + '/css',
        watch: [src + '/css/**/*.styl', '!' + src + '/css/_exports/sprite*.styl'],
        imgRoot: '../img/',
        paths: dest + '/img/'
    },
    images: {
        src: src + '/img/**/*',
        watch: [src + '/img/**/*'],
        dest: dest + '/img'
    },
    iconfont: {
        name: 'icons',
        src: src + '/iconfont/*.svg',
        dest: dest + '/fonts/icons',
        template: templates + '/iconfont.lodash',
        path: '../fonts/icons/',
        css: src + '/css/_exports'
    },
    fonts: {
        src: src + '/fonts/**/*',
        dest: dest + '/fonts'
    },
    sprites: {
        src: src + '/sprites',
        watch: src + '/sprites/**/*.png',
        css: src + '/css/_exports',
        img: src + '/img/sprites',
        dest: dest + '/img/sprites',
        template: templates + '/sprite-template.handlebars'
    },
    scriptsCopy: {
        src: src + '/js/vendor/**/*.js',
        dest: dest + '/js/vendor',
        watch: src + '/js/vendor/**/*'
    },
    webpack: {
        context: path.join(__dirname, '../', src, 'js'),
        entry: {
            app: ['babel-regenerator-runtime', './app.js']
        },
        output: {
            path: path.join(__dirname, '../', dest + '/js/'),
            publicPath: '/assets/' + options.target + '/js/',
            filename: '[name].js',
            chunkFilename: '[name].[chunkhash].js'
        },
        resolve: {
            alias: {
                MorphSVGPlugin: path.resolve(__dirname,  '../src/frontend/js/gsap/plugins/MorphSVGPlugin.js'),
                TweenLite: path.resolve(__dirname,  '../src/frontend/js/gsap/TweenLite.js'),
                TimelineLite: path.resolve(__dirname,  '../src/frontend/js/gsap/TimelineLite.js'),
                EasePack: path.resolve(__dirname,  '../src/frontend/js/gsap/easing/EasePack.js'),
                CustomEase: path.resolve(__dirname,  '../src/frontend/js/gsap/easing/CustomEase.js'),
                SplitText: path.resolve(__dirname,  '../src/frontend/js/gsap/utils/SplitText.js'),
                CSSPlugin: path.resolve(__dirname,  '../src/frontend/js/gsap/plugins/CSSPlugin.js'),
            }
        },
    },
    webpackProduction: {
        output: {
            filename: '[name].min.js',
            chunkFilename: '[name].[chunkhash].min.js'
        }
    }
};