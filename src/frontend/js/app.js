let basepath = document.documentElement.getAttribute('data-basepath');
__webpack_public_path__ = basepath ? basepath  + '/assets/frontend/js/' : '/assets/frontend/js/';

import 'console-polyfill';
import './modules/modernizr';
//import './modules/path-data-polyfill';
import './modules/scroll-restoration';
import './modules/object-foreach-polyfill';
import './modules/dataset-polyfill';
import './modules/string-includes-polyfill';
import './modules/promise-polyfill';
import './modules/custom-event-polyfill';
//import './modules/hidpi-canvas-polyfill';

// GTM swup
document.addEventListener('swup:pageView', event => {
    if (typeof dataLayer !== 'undefined') {
        dataLayer.push({
            'event': 'VirtualPageview',
            'virtualPageURL': window.location.href,
            'virtualPageTitle': document.title
        });
    } else {
        console.warn('GTM is not loaded.')
    }
});

import './core/swup';

import FastClick from 'fastclick';

import components from './components/';
import { loadComponents } from './core/componentUtils';

window.app = {
    components: components
};

FastClick.attach(document.body);
loadComponents();

document.documentElement.classList.add('is-ready');
