document.addEventListener('swup:clickLink', event => {
    if (window.location.pathname == '/' || window.location.pathname == '/25let' || window.location.pathname == '/25let/') {
        window.savedScrollRestorationAmount = window.pageYOffset;
    }
});

document.addEventListener('swup:popState', event => {
    setTimeout(() => {
        if (window.location.pathname == '/' || window.location.pathname == '/25let' || window.location.pathname == '/25let/') {
            if (window.savedScrollRestorationAmount != null) {
                window.scrollTo(0, window.savedScrollRestorationAmount);
            }
        }
    }, 100);
});