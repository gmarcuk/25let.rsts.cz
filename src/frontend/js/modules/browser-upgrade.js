(function() {

    var scope = document.getElementById('browser-upgrade');

    if (scope) {
        var hidden = document.cookie.indexOf('browserUpgradeHidden') > -1;
        var message = UPGRADE_BROWSER_MESSAGE || '';
        if (!hidden && message) {
            scope.innerHTML = message;

            var button = scope.querySelector('.browser-upgrade__close');

            var close = function() {
                scope.style.display = 'none';
                document.cookie = 'browserUpgradeHidden=1';
            }

            if (button.attachEvent) {
                button.attachEvent('onclick', function(event) {
                    close();
                    return false;
                });
            } else {
                button.addEventListener('click', function(event) {
                    event.preventDefault();
                    close();
                }, false);
            }
        }


    }

})();