import Swupjs from 'swupjs';
import Component from './Component';
import scrollTo from '../services/scrollTo';
import { loadComponents, removeComponents } from './componentUtils';

import TweenLite from  'TweenLite';
import 'CSSPlugin';
import 'EasePack';

class CustomSwupjs extends Swupjs {
    constructor(setOptions) {
        super(setOptions)

        this.getPage = function(location, callback) {
            var request = new XMLHttpRequest();

            request.onreadystatechange = function() {
                if (request.readyState === 4) {
                    if (request.status !== 500) {
                        callback(request.responseText, request);
                    }
                    else {
                        callback(null, request);
                    }
                }
            }

            request.open("GET", location, true);
            request.setRequestHeader("X-Requested-With", "XMLHttpRequest");
            request.setRequestHeader("accept", "application/json");
            request.send(null);
            return request;
        }

        this.getDataFromHtml = function (c) {
            let j = JSON.parse(c);

            let fakeDom = document.createElement('div')
            let fakeDomLogo = document.createElement('div')
            fakeDom.innerHTML = j.snippets['snippet--content'];
            fakeDomLogo.innerHTML = j.snippets['snippet--logo'];
            fakeDom.querySelector('#swup').dataset.swup = '0';
            fakeDomLogo.querySelector('#logo').dataset.swup = '1';

            var json = {
                title: j.snippets['snippet--title'],
                pageClass: j.snippets['snippet--pageClass'],
                originalContent: null,
                blocks: [fakeDom.innerHTML, fakeDomLogo.innerHTML]
            }
            return json;
        }

        this.markSwupElements = function(element) {
            let blocks = 0;

            for (var i = 0; i < this.options.elements.length; i++) {
                if (element.querySelector(this.options.elements[i]) == null) {
                    console.warn(`Element ${this.options.elements[i]} is not in current page.`)
                } else {
                    [].forEach.call(document.body.querySelectorAll(this.options.elements[i]), (item, index) => {
                        element.querySelectorAll(this.options.elements[i])[index].dataset.swup = blocks;
                        element.querySelectorAll(this.options.elements[i])[index].setAttribute('data-swup', blocks);
                        blocks++;
                    });
                }
            }
        }

        this.scrollTo = (element, to, duration) => {
            if (duration == 0) {
                window.scrollTo(0, 0)
            } else {
                scrollTo(to);
            }
        };

        // this.popStateHandler = (event) => {
        //     var link = new Link()
        //     link.setPath(event.state ? event.state.url : window.location.pathname)
        //     if (link.getHash() != '') {
        //         this.scrollToElement = link.getHash()
        //     } else {
        //         console.log('!!!!!!!!!!prevent')
        //         event.preventDefault()
        //     }
        //     this.triggerEvent('popState')
        //     // hot fix for scroll restoration
        //     setTimeout(() => {
        //         this.loadPage(link.getAddress(), event)
        //     }, 10)
        // }
    }
}

let xhrElement = document.querySelector('#swup');
let fadeElement = document.querySelector('#fade');
let menu;

setTimeout(() => {
    const menuElement = document.querySelector('[data-component="Nav"]');
    menu = Component.getFromElement(menuElement);
}, 10)

// fade out
function fadeOut(next){
    fadeElement.style.opacity = 1;


    TweenLite.to(fadeElement, .5, {
        opacity: 0,
        onComplete: next
    });
}

// fade in
function fadeIn(next){
    fadeElement.style.opacity = 0;

    TweenLite.to(fadeElement, .5, {
        opacity: 1,
        onComplete: next
    });
}

let animations = {
    '*': {
        in: fadeIn,
        out: fadeOut
    },
    'homepage>*': {
        in: fadeIn,
        out: function (next) {
            TweenLite.to(document.getElementById('slideshow-bg'), 0.5, {
                transform: "scale(2)",
                opacity: 0
            });
            fadeOut(next)
        }
    }
}

let options = {
    debugMode: true,
    animations: animations,
    preload: true,
    elements: ['#swup', '#logo']
}
const swupjs = new CustomSwupjs(options);

document.addEventListener('swup:pageView', event => {
    swupjs.markSwupElements(document.documentElement);
});
swupjs.markSwupElements(document.documentElement);

document.addEventListener('swup:clickLink', event => {
    menu.close();
});

document.addEventListener('swup:contentReplaced', event => {
    xhrElement = document.querySelector('#swup');
    fadeElement = document.querySelector('#fade');
    //fadeElement.style.opacity = 0;

    setTimeout(() => {
        document.querySelectorAll('[data-swup]').forEach(element => {
            loadComponents(element)
        });
    }, 10);
    setTimeout(() => {
        loadComponents(document.getElementById('footer-nav'));
    }, 100);
});

document.addEventListener('swup:animationOutDone', event => {
    document.querySelectorAll('[data-swup]').forEach(element => {
        removeComponents(element);
    });
    removeComponents(document.getElementById('footer-nav'));
});

document.addEventListener('swup:animationInDone', event => {
    // remove "to-{page}" classes
    document.documentElement.classList.remove('is-changing')
    document.documentElement.classList.remove('is-rendering')
});