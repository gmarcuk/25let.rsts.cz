import Component from '../core/Component';
import TweenLite from  'TweenLite';
import throttle from  'lodash/throttle';
import { queries } from  '../core/config';
import 'CSSPlugin';
import 'EasePack';

export default class Nav extends Component {
    constructor(element) {
        super(element);
        this.resize();
        setTimeout(() =>{
            this.resize();
        }, 100)

        let options = {};
        if (this.element.dataset.options != null) {
            options = JSON.parse(this.element.dataset.options);
        }
        let defaults = {
            activeNext: false
        }

        this.options = {
            ...defaults,
            ...options
        }

        this.originalPath = this.ref.path.getAttribute('d');
        this.innerWidth = this.ref.scrollable.offsetWidth;
        this.mobile = true;
    }

    prepare() {
        if (window.innerWidth >= 1200) {
            this.mouseMoveHandler = throttle(this.mouseMove);
            this.resizeHandler = throttle(this.resize);

            this.element.addEventListener('mousemove', ::this.mouseMoveHandler);
            window.addEventListener('resize', ::this.resizeHandler);

            this.ref.item.forEach(item => {
                item.addEventListener('mouseover', ::this.showPath);
            })
            this.ref.item.forEach(item => {
                item.addEventListener('mouseleave', ::this.hidePath);
            })
        }

        if (window.innerWidth < 1200) {
            this.menuScrollHandler = throttle(this.menuScroll);

            this.ref.item.forEach(item => {
                this.showPath({currentTarget: item}, true);
            });
            this.ref.menu.addEventListener('scroll', ::this.menuScroll);

            // mobile next to active in viewport
            if (this.options.activeNext) {
                setTimeout(() => {
                    let path = window.location.pathname;
                    let scrollAmount = 0;
                    this.ref.item.forEach((item, index) => {
                        //console.log(path == item.parentNode.pathname && index+1 != this.ref.item.length);
                        if (path == item.parentNode.pathname && index+1 != this.ref.item.length) {
                            scrollAmount = this.ref.item[index+1].offsetLeft + (this.ref.item[0].offsetWidth/2) - (this.windowWidth/2);
                        }
                    });
                    this.ref.menu.scrollLeft = scrollAmount;
                }, 100);
            }
        }
    }

    open() {
        document.documentElement.classList.add('menu-is-opened');
        if (!this.mobile) {
            setTimeout(() => {
                this.goTo(0.9);
            }, 500);
        }
    }

    close() {
        document.documentElement.classList.remove('menu-is-opened');
        if (!this.mobile) {
            this.goTo(0.1);
        }
    }

    destroy() {
        if (window.innerWidth >= 1200) {
            this.element.removeEventListener('mousemove', ::this.mouseMoveHandler);
            window.removeEventListener('resize', ::this.resizeHandler);
        }
        if (window.innerWidth < 1200) {
            this.ref.menu.removeEventListener('scroll', ::this.menuScroll);
        }
    }

    mouseMove(event) {
        this.percent = (event.clientX)/(this.windowWidth);
        this.goTo(((this.percent-0.5)*1.3)+0.5);
    }

    resize(event) {
        this.windowWidth = window.innerWidth
    }

    showPath(event, halfOnly = false) {
        if (!event.currentTarget.classList.contains('is-active')) {
            let parent = event.currentTarget.parentNode.parentNode;

            let path = event.currentTarget.querySelector('[data-ref="path"]');
            let finalPath = event.currentTarget.querySelector('[data-ref="finalPath"]');

            event.currentTarget.classList.add('is-active')
            TweenLite.to(path, 0.7, {
                morphSVG: finalPath.getAttribute('d'),
                transform: (halfOnly == true) ? "translate3d(25px, 25px, 0) scale(.5)" : "translate3d(-300px, -450px, 0) scale(2.7)",
            });
            TweenLite.to(path, 0.1, {
                opacity: "1",
            });
            TweenLite.set(path, {
                opacity: "1",
                delay: .3
            });
        }
    }

    hidePath(event) {
        if (event.currentTarget.classList.contains('is-active')) {
            let path = event.currentTarget.querySelector('[data-ref="path"]');

            event.currentTarget.classList.remove('is-active')
            TweenLite.to(path, 0.7, {
                morphSVG: this.originalPath,
                transform: "translate3d(60px, 60px, 0) scale(1)"
            });
            TweenLite.to(path, 0.1, {
                opacity: "0",
                delay: 0.6
            });
        }
    }

    menuScroll(event) {
        let percent = (event.currentTarget.scrollLeft)/(this.innerWidth - this.windowWidth);
        let amountScrollbar = Math.round((-50+180) * percent * 100) / 100;

        TweenLite.to(this.ref.scrollbar, .8, {
            transform: `translate3d(${amountScrollbar}px, 0, 0)`,
            ease: Back.easeOut.config(1.1)
        });
    }

    goTo(percent) {
        let amount = Math.round((-this.innerWidth+this.windowWidth) * percent * 100) / 100;
        let amountScrollbar = Math.round((-50+180) * percent * 100) / 100;

        TweenLite.to(this.ref.scrollable, .8, {
            transform: `translate3d(${amount}px, 0, 0)`,
            ease: Back.easeOut.config(1.1)
        });
        TweenLite.to(this.ref.scrollbar, .8, {
            transform: `translate3d(${amountScrollbar}px, 0, 0)`,
            ease: Back.easeOut.config(1.1)
        });
    }
}