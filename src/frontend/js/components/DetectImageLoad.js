import Component from '../core/Component';

export default class DetectImageLoad extends Component {
    constructor(element) {
        super(element);
    }

    prepare() {
        let promises = [];

        this.ref.image.forEach(item => {
            let img = document.createElement("img");
            if (item.tagName == "IMG") {
                img.src = item.getAttribute('src');
            } else {
                let src = window.getComputedStyle( item ,null).getPropertyValue('background-image');
                src = src.replace('url("', '');
                src = src.replace('")', '');
                src = src.replace('url(', '');
                src = src.replace(')', '');
                img.src = src;
            }
            let promise = new Promise(function (resolve) {
                img.onload = function() {
                    resolve();
                };
            });
            promises.push(promise);
        });

        Promise
            .all(promises)
            .then(() => {
                this.element.classList.add('is-loaded');
            });
    }
}