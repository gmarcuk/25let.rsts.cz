import Component from '../core/Component';
import TweenLite from  'TweenLite';
import 'CSSPlugin';
import 'EasePack';

export default class Magnetic extends Component {

    constructor(element) {
        super(element);
        this.getRect();

        this.ref = {};
        this.ref.area = this.element.querySelector('[data-ref="area"]');
        this.ref.content = this.element.querySelector('[data-ref="content"]');
    }

    prepare() {
        if (window.innerWidth >= 1200) {
            window.addEventListener('resize', ::this.getRect);

            this.ref.area.addEventListener('mousemove', event => {
                let x = event.clientX - this.rect.left - this.element.offsetWidth/2;
                let y = event.clientY - this.rect.top - this.element.offsetHeight/2;

                TweenLite.to(this.ref.content, .6, {
                    transform: `translate3d(${x}px, ${y}px, 0)`
                })
            });

            this.ref.area.addEventListener('mouseleave', ::this.reset);
            this.ref.area.addEventListener('click', event => {
                this.ref.content.querySelector('a, button').click();
            });
        }
    }

    destroy() {
        if (window.innerWidth >= 1200) {
            window.removeEventListener('resize', ::this.getRect);
        }
    }

    getRect() {
        this.rect = this.element.getBoundingClientRect();
    }

    reset() {
        TweenLite.to(this.ref.content, 0.8, {
            transform: `translate3d(0, 0, 0)`,
            ease: Back.easeOut.config(1.2)
        })
    }
}