import Component from '../core/Component'
import throttle from 'lodash/throttle'
import TweenLite from  'TweenLite';

export default class Parallax extends Component {
    constructor($element) {
        super($element);

        let options = {};
        if (this.element.dataset.options != null) {
             options = JSON.parse(this.element.dataset.options);
        }
        let defaults = {
            move: 500
        }

        this.options = {
            ...defaults,
            ...options
        }
        this.resize();
    }

    prepare() {
        this.scrollHandler = throttle(::this.scroll);
        this.resizeHandler = throttle(::this.resize);

        if (window.innerWidth >= 1200) {
            window.addEventListener('scroll', this.scrollHandler, {passive: true});
            window.addEventListener('resize', this.resizeHandler);
            this.scrollHandler();
        }
    }

    destroy() {
        if (window.innerWidth >= 1200) {
            window.removeEventListener('scroll', this.scrollHandler);
            window.removeEventListener('resize', this.resizeHandler);
        }
    }

    resize(event) {
        this.windowHeight = window.innerHeight;
        let rect = this.element.getBoundingClientRect();
        this.top = rect.top + window.scrollY;
        this.topFull = this.top - this.options.move;
        this.bottom = rect.top + this.element.offsetHeight + window.scrollY;
        this.bottomFull = this.bottom + this.options.move;
    }

    scroll() {
        let scroll = window.pageYOffset;

        if (this.topFull < scroll + this.windowHeight && this.bottomFull > scroll) {
            let amount = Math.round(((this.top-scroll) / (this.windowHeight) - 0.5) * this.options.move/2);
            //this.element.style.transform = `translate3d(0, ${amount}px, 0)`;
            TweenLite.set(this.element, {
                transform: `translate3d(0, ${amount}px, 0)`
            });
        }
    }
}
