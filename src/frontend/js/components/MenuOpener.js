import Component from '../core/Component';

export default class MenuOpener extends Component {

    constructor($element) {
        super($element);
    }

    prepare() {
        setTimeout(() => {
            let nav = document.querySelector('[data-component="Nav"]');
            let menu = Component.getFromElement(nav);

            this.element.addEventListener('click', event => {
                if (document.documentElement.classList.contains('menu-is-opened')) {
                    menu.close();
                } else {
                    menu.open();
                }
            })

            document.addEventListener('keydown' ,function(event) {
                if (event.keyCode == 27 && document.documentElement.classList.contains('menu-is-opened')) {
                    menu.close();
                }
            });
        }, 100)
    }
}