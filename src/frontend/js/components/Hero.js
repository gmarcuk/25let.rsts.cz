import Component from '../core/Component';
import TweenLite from  'TweenLite';
import 'MorphSVGPlugin';
import 'CustomEase';

export default class Slideshow extends Component {
    constructor(element) {
        super(element);

        this.ease = CustomEase.create("custom", "M0,0,C0.128,0.572,0.257,0.926,0.512,1,0.672,1.046,0.838,1,1,1");
    }

    prepare() {
        if (window.innerWidth >= 1200) {
            this.originalPath = this.ref.background.getAttribute('d');
            this.smallPath = "M1004.3-301.4c-88.3,28.3,38.7,67.1-32,109.8c-71.4,42.8-80.7-47.5-129.1,16.8c-35.5,46.5-59.6-10.3-66.3,29.2 " +
                "c-6.6,39.2,8.1,1.1,22,36.2c28,70.9,46.5-40.8,89.4-13.3c43,29.1-11.5,40.9,38,41.4c152.4,53.6,4.3-23.3,110-81.6 " +
                "c139.4-72.7-32.6,37.7,93.3,44.3c62.6,2.4-6.3-88.3,57-108.3c63.6-20.3,119.5,91.7,167.4,7c26.1-43.4-59.3,9.2-64.9-57.7 " +
                "c-7-66.5-79.7,45.8-123.7-12c-43.9-57.8,99.7-68.2-23.2-76C1022.6-374.2,1163.6-354.8,1004.3-301.4z";


            this.ref.background.setAttribute('d', this.smallPath);
            this.ref.clip.setAttribute('d', this.smallPath);

            this.element.style.opacity = "1";

            TweenLite.to(this.ref.background, 1, {
                morphSVG: this.originalPath,
                delay: 0.1,
                ease: this.ease
            });
            TweenLite.to(this.ref.clip, 1, {
                morphSVG: this.originalPath,
                delay: 0.1,
                ease: this.ease
            });
        }
    }
}