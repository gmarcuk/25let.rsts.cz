import Component from '../core/Component';

export default class Carousel extends Component {
    constructor($element) {
        super($element);

        this.done = false;
        this.correct = this.element.querySelector('[data-correct]');
    }

    prepare() {
        this.ref.button.forEach(item => {
            item.removeAttribute('data-correct');
            item.addEventListener('click', ::this.handleClick);
        });
    }

    handleClick(event) {
        if (!this.done) { //<div class="Question-tag Question-tag--correct">Správně</div>

            let element = document.createElement('div');
            element.classList.add('Question-tag');
            element.classList.add('Question-tag--correct');
            element.innerHTML = "Správně";

            let errorElement = document.createElement('div');
            errorElement.classList.add('Question-tag');
            errorElement.classList.add('Question-tag--wrong');
            errorElement.innerHTML = "Špatně";

            if (event.currentTarget === this.correct) {
                event.currentTarget.parentNode.innerHTML = element.outerHTML + event.currentTarget.parentNode.innerHTML;
            } else {
                event.currentTarget.parentNode.innerHTML =  errorElement.outerHTML + event.currentTarget.parentNode.innerHTML;
                this.correct.parentNode.innerHTML = element.outerHTML + this.correct.parentNode.innerHTML;
            }
            this.done = true;
        }

    }
}