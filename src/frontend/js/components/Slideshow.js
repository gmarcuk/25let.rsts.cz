import Component from '../core/Component';
import TweenLite from  'TweenLite';
import throttle from  'lodash/throttle';
import scrollTo from  '../services/scrollTo';
import 'CSSPlugin';
import 'CustomEase';
import 'MorphSVGPlugin';

export default class Slideshow extends Component {
    constructor(element) {
        super(element);
        this.prev = 0;
        this.loop = null;
        this.indexOfSlide = 0;
        this.indexOfShape = 1;
        this.offset = {x: 0, y: 0};
        this.canUseCanvas = false;

        if (typeof Path2D !== 'undefined' && window.innerWidth >= 768 && window.navigator.userAgent.indexOf("Edge") == -1) { // because fucking edge does support path2D, but not really...
            this.canUseCanvas = true;
        }

        this.windowWidth = window.innerWidth;
        this.windowHeight = window.innerHeight;

        if (this.canUseCanvas) {
            this.useCanvas();
        }
        this.resize();

        setTimeout(() => {
            this.resize();
        }, 20)
        this.ease = CustomEase.create("custom", "M0,0 C0.128,0.572 0.275,1.109 0.528,1.026 0.904,0.902 0.838,1 1,1");
        //this.ease = CustomEase.create("custom", "M0,0 C0.096,0.291 0.22,1.1 0.42,1.074 0.542,1.058 0.546,0.94 0.638,0.94 0.73,0.94 0.739,1.042 0.806,1.028 0.862,1.016 0.87,0.984 0.908,0.984 0.944,0.984 0.924,1 1,1");
    }

    prepare() {
        this.mouseMoveHandler = throttle(this.mouseMove);
        this.resizeHandler = throttle(this.resize);
        this.scrollHandler = throttle(this.scroll);

        //this.element.addEventListener('mousemove', ::this.mouseMoveHandler);
        window.addEventListener('resize', ::this.resizeHandler);
        window.addEventListener('scroll', ::this.scrollHandler, {passive: true});
        document.addEventListener('keydown' , ::this.keypressHandler);
        if (this.canUseCanvas) {
            this.loop = setInterval(::this.float, 2000);
        };
        this.scrollHandler();
    }

    destroy() {
        window.removeEventListener('resize', ::this.resizeHandler);
        window.removeEventListener('scroll', ::this.scrollHandler);
        document.removeEventListener('keydown' , ::this.keypressHandler);
    }

    mouseMove(event) {
        this.percent = document.innerHeight;
    }

    resize(event) {
        let body = document.body,
            html = document.documentElement;

        // window/document
        this.windowWidth = window.innerWidth;
        this.windowHeight = window.innerHeight;
        this.docHeight = Math.max( body.scrollHeight, body.offsetHeight, html.clientHeight, html.scrollHeight, html.offsetHeight );

        //items
        this.borders = [];
        this.ref.item.forEach(item => {
            let rect = item.getBoundingClientRect();
            this.borders.push(rect.top + window.pageYOffset);
        });

        if (this.canUseCanvas) {
            this.canvasResize();
        }
    }

    getActive() {
        let i = 0;
        this.borders.forEach((item, index) => {
            if (item < window.pageYOffset + this.windowHeight/2) {
                i = index;
                return 0;
            }
        });
        return i;
    }

    scroll(event) {
        let currentIndex = this.getActive();

        if (this.prev != currentIndex) {
            this.indexOfSlide = currentIndex;
            //this.resetLoop();

            // reset nav
            this.ref.navItem.forEach(item => {
               item.classList.remove('is-active');
            });
            if (currentIndex != 0) {
                this.ref.navItem[currentIndex-1].classList.add('is-active');
            }

            // set number
            TweenLite.to(this.ref.num, .8, {
                transform: `translate3d(-${currentIndex*40}px, 0, 0)`
            });

            // morph
            this.prev = currentIndex;
            this.morphTo(currentIndex);

            if (currentIndex == 0) {
                this.element.classList.remove('is-scrolled');
            } else {
                this.element.classList.add('is-scrolled');
            }
        }
    }

    morphTo(index) {
        let position = this.getPosition(index);

        TweenLite.to(this.ref.bgPath, 2.4, {
            morphSVG: this.ref.path[index].getAttribute('d'),
            ease: this.ease,
            transform: position,
        });
        TweenLite.to(this.ref.bgPath, 0.6, {
            fill: this.ref.path[index].getAttribute('fill') 
        });


        TweenLite.to(this.ref.bgPath2, 2.4, {
            morphSVG: this.ref.path2[index].getAttribute('d'),
            ease: this.ease,
            transform: position
        });

        TweenLite.to(this.ref.bgPath3, 2.4, {
            morphSVG: this.ref.path3[index].getAttribute('d'),
            ease: this.ease,
            transform: position
        });

        let x = this.windowWidth/2 - this.ref.svgs[index].getAttribute('width').replace('px', '')/2
        let y = this.windowHeight/2 - this.ref.svgs[index].getAttribute('height').replace('px', '')/2

        TweenLite.to(this.offset, 2.4, {
            x: x,
            y: y,
            ease: this.ease
        });
    }

    getPosition(i) {
        let index = "translate3d(0, 0, 0) scale(1)";
        switch(i) {
            case 0:
                break;
            case 1:
                index = "translate3d(-60px, 0, 0) scale(0.8)";
                break;
            case 2:
                index = "translate3d(-60px, 120px, 0) scale(0.7)";
                break;
            case 3:
                index = "translate3d(-60px, 60px, 0) scale(0.8)";
                break;
            case 4:
                index = "translate3d(-60px, 60px, 0) scale(0.7)";
                break;
            case 5:
                index = "translate3d(0, 0, 0) scale(0.7)";
                break;
            case 6:
                index = "translate3d(-60px, 0, 0) scale(0.8)";
                break;
            case 7:
                index = "translate3d(-120px, 60px, 0) scale(0.8)";
                break;
            case 8:
                index = "translate3d(0, 0, 0) scale(0.7)";
                break;
            case 9:
                index = "translate3d(0, 0, 0) scale(0.7)";
                break;
            case 10:
                index = "translate3d(-60px, 0, 0) scale(0.7)";
                break;
        }

        return index;
    }

    getCanvasPosition(i) {
        let index = {x: 0, y: 0};
        switch(i) {
            case 0:
                break;
            case 1:
                index = {x: -500, y: 0};
                break;
            case 2:
                index = {x: -60, y: 120};
                break;
            case 3:
                index = {x: -60, y: 60};
                break;
            case 4:
                index = {x: -60, y: 60};
                break;
            case 5:
                index = {x: 0, y: 0};
                break;
            case 6:
                index = {x: -60, y: 0};
                break;
            case 7:
                index = {x: -120, y: 60};
                break;
            case 8:
                index = {x: 0, y: 0};
                break;
            case 9:
                index = {x: 0, y: 0};
                break;
            case 10:
                index = {x: -60, y: 0};
                break;
        }

        return index;
    }

    keypressHandler(event) {
        if (event.keyCode == 37 || event.keyCode == 38) {
            event.preventDefault();
            let index = this.getActive()-1;
            if (index >= 0) {
                scrollTo(this.borders[index]);
            }
        } else if (event.keyCode == 39 || event.keyCode == 40) {
            event.preventDefault();
            let index = this.getActive()+1;
            if (index < this.borders.length) {
                scrollTo(this.borders[index]);
            }
        }
    }

    resetLoop() {
        clearInterval(this.loop);
        this.indexOfShape = 1;
        this.loop = setInterval(::this.float, 2000)
    }

    float() {
        let index1 = this.indexOfShape;
        let index2 = this.indexOfShape + 1;
        let index3 = this.indexOfShape - 1;
        if (index2 == 3) {
            index2 = 0;
        }
        if (index3 == -1) {
            index3 = 2;
        }

        let name = index1 == 0 ? 'path' : 'path' + (1 + index1);
        let name2 = index2 == 0 ? 'path' : 'path' + (1 + index2);
        let name3 = index3 == 0 ? 'path' : 'path' + (1 + index3);

        TweenLite.to(this.ref.bgPath, 3, {
            morphSVG: this.ref[name][this.indexOfSlide].getAttribute('d'),
            ease:this.ease
        });
        TweenLite.to(this.ref.bgPath2, 3, {
            morphSVG: this.ref[name2][this.indexOfSlide].getAttribute('d'),
            ease:this.ease
        });
        TweenLite.to(this.ref.bgPath3, 3, {
            morphSVG: this.ref[name3][this.indexOfSlide].getAttribute('d'),
            ease:this.ease
        });

        this.indexOfShape++;
        if (this.indexOfShape == 3) {
            this.indexOfShape = 0;
        }
    }

    useCanvas() {
        let canvas = this.ref.canvas = document.createElement('canvas');
        this.ref.svg.parentNode.removeChild(this.ref.svg);
        this.ref.bg.appendChild(canvas);
        let context = this.context = this.ref.canvas.getContext('2d');

        let svg = this.ref.svg;
        let pathOffsetX = this.windowWidth/2*.5;
        let pathOffsetY = this.windowHeight/2*.3;

        this.offset.x = this.windowWidth/2 - this.ref.svgs[0].getAttribute('width').replace('px', '')/2;
        this.offset.y = this.windowHeight/2 - this.ref.svgs[0].getAttribute('height').replace('px', '')/2;

        let draw = (timestamp) => {
           // clear canvas
           context.setTransform(1, 0, 0, 1, 0, 0);
           context.clearRect(0, 0, this.ref.canvas.width, this.ref.canvas.height);
           //this.ref.canvas.width = this.ref.canvas.width;
           //let pos = this.getCanvasPosition(this.currentIndex);
           context.translate(this.offset.x, this.offset.y);
           context.fillStyle = this.ref.bgPath.style.fill;
           context.strokeStyle = "rgba(24, 36, 82, 0.1)";

           // draw main shape
           let path = new Path2D(this.ref.bgPath.getAttribute('d'));
           context.fill(path);

           // draw other shapes
           path = new Path2D(this.ref.bgPath2.getAttribute('d'), this.offset.x, this.offset.y);
           context.stroke(path);

           path = new Path2D(this.ref.bgPath3.getAttribute('d'), this.offset.x, this.offset.y);
           context.stroke(path);

           window.requestAnimationFrame(draw);
        }
        window.requestAnimationFrame(draw);
    }

    canvasResize() {
        this.context.canvas.width  = window.innerWidth;
        this.context.canvas.height = window.innerHeight;
        this.offset.x = this.windowWidth/2 - this.ref.svgs[this.indexOfSlide].getAttribute('width').replace('px', '')/2;
        this.offset.y = this.windowHeight/2 - this.ref.svgs[this.indexOfSlide].getAttribute('height').replace('px', '')/2;
    }
}