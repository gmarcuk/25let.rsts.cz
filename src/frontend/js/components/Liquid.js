import Component from '../core/Component';
import TimelineLite from 'TimelineLite';
import 'MorphSVGPlugin';

export default class SampleComponent extends Component {

    constructor($element) {
        super($element);

        // let options = {};
        // if (this.element.dataset.options != null) {
        //     options = JSON.parse(this.element.dataset.options);
        // }
        // let defaults = {
        //     range: 100,
        //     time: 3
        // }
        //
        // this.options = {
        //     ...defaults,
        //     ...options
        // }
        //
        // this.path = this.element;
        // this.originalData = Object.assign({}, this.path.getPathData());
    }

    prepare() {
        //this.setLoop();
    }

    setLoop() {
        this.generateData();
        setInterval(::this.generateData, this.options.time*1000);
    }

    generateData() {
        let newData = {};
        Object.assign({}, this.originalData);

        let prevRand = 0;

        Object.keys(this.originalData).forEach(key => {
            newData[key] = Object.assign({}, this.originalData[key]);
            newData[key].values = Object.assign({}, this.originalData[key].values);

            if (newData[key].type == 'C' || newData[key].type == 'c') {
                Object.keys(newData[key].values).forEach(subKey => {
                    let rand = Math.random()*this.options.range;
                    if (newData[key].values[subKey] - rand > 0) {
                        rand = -1*rand;
                    }

                    if (subKey == 0 || subKey == 1) {
                        newData[key].values[subKey] = parseInt(newData[key].values[subKey]) + prevRand;
                    } else {
                        newData[key].values[subKey] = parseInt(newData[key].values[subKey]) + rand;
                    }
                    prevRand = rand;
                })
            }
        });
        newData[0].values[0] = parseInt(newData[0].values[0]) + prevRand;
        newData[0].values[1] = parseInt(newData[0].values[1]) + prevRand;
        newData[1].values[0] = parseInt(newData[0].values[0]) + prevRand;
        newData[1].values[1] = parseInt(newData[0].values[1]) + prevRand;

        let compiledData = "";
        let prevType = null;
        Object.keys(newData).forEach(key => {
            if (prevType != newData[key].type) {
                compiledData += newData[key].type;
                prevType = newData[key].type;
            }

            Object.keys(newData[key].values).forEach(subKey => {
                if (Object.keys(newData[key].values).length-1 != subKey) {
                    compiledData += newData[key].values[subKey] + ",";
                } else {
                    compiledData += newData[key].values[subKey] + " ";
                }
            })
        })

        let tl = new TimelineLite();
        tl.to(this.path, this.options.time, {
            morphSVG: compiledData,
            ease: Linear.easeNone
        })

        //this.path.setAttribute("d", compiledData);
    }

}