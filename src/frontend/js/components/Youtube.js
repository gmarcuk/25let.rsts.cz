import Component from '../core/Component'
import TweenLite from  'TweenLite';

export default class Youtube extends Component {
    constructor($element) {
        super($element);

        this.blocked = false;

        let options = {};
        if (this.element.dataset.options != null) {
             options = JSON.parse(this.element.dataset.options);
        }
        let defaults = {
            move: 500
        }

        this.options = {
            ...defaults,
            ...options
        }
    }

    prepare() {
        this.element.addEventListener('click', ::this.append);
    }

    append() {
        //TweenLite.set(this.element, {
        //    transform: `translate3d(0, ${amount}px, 0)`
        //});
        if (!this.blocked) {
            this.element.classList.add('is-played');
            this.element.innerHTML += `<div class="Image-iframe"><iframe src="${this.options.url}&autoplay=1" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe></div>`
            this.blocked = true;
        }
    }
}
