import Cards from './Cards';
import Carousel from './Carousel';
import DetectImageLoad from './DetectImageLoad';
import Explode from './Explode';
import Hero from './Hero';
// import Gallery from './Gallery';
//import Liquid from './Liquid';
import Magnetic from './Magnetic';
import MenuOpener from './MenuOpener';
import Nav from './Nav';
import Parallax from './Parallax';
import Slideshow from './Slideshow';
import Youtube from './Youtube';
import Question from './Question';

const Components = {
    Cards: Cards,
    Carousel: Carousel,
    DetectImageLoad: DetectImageLoad,
    Explode: Explode,
    Hero: Hero,
    //Gallery: Gallery,
    //Liquid: Liquid,
    Magnetic: Magnetic,
    MenuOpener: MenuOpener,
    Nav: Nav,
    Parallax: Parallax,
    Slideshow: Slideshow,
    Youtube: Youtube,
    Question: Question,
};

export default Components;