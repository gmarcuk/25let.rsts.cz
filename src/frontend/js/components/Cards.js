import Component from '../core/Component';

export default class Carousel extends Component {
    constructor($element) {
        super($element);

        this.ref = {};
        this.ref.item = this.element.querySelectorAll('[data-ref="item"]');
        this.ref.navItem = this.element.querySelectorAll('[data-ref="navItem"]');
        this.ref.arrowTop = this.element.querySelector('[data-ref="arrowTop"]');
        this.ref.arrowBottom = this.element.querySelector('[data-ref="arrowBottom"]');

        this.id = 0;
        this.count = this.ref.item.length;

        let options = {};
        if (this.element.dataset.options != null) {
            options = JSON.parse(this.element.dataset.options);
        }

        let defaults = {

        }

        this.options = {
            ...defaults,
            ...options
        }
    }

    prepare() {
        // previous
        this.ref.arrowTop.addEventListener('click', event => {
            event.preventDefault();
            this.previous();
        });

        // next
        this.ref.arrowBottom.addEventListener('click', event => {
            event.preventDefault();
            this.next();
        });

        this.ref.navItem.forEach((element, i) => {
            element.addEventListener('click', event => {
                event.preventDefault();

                this.ref.navItem.forEach((item, i) => {
                    item.classList.remove('is-active');
                });
                element.classList.add('is-active');

                let index = this.count - i - 1;

                this.ref.item.forEach((item, subIndex) => {
                    item.classList.remove('is-active');
                    item.classList.remove('is-before');
                    item.classList.remove('is-after');
                    item.classList.remove('is-displayed');

                    if (subIndex < index) {
                        item.classList.add('is-after');
                        if (subIndex == index-2 || subIndex == index-1) {
                            item.classList.add('is-displayed');
                        }
                    } else if (subIndex == index) {
                        item.classList.add('is-active');
                    } else {
                        item.classList.add('is-before');
                    }
                })
             });
        });

        this.ref.navItem[0].click();
    }

    next() {
        this.ref.navItem.forEach((element, i) => {
            if (element.classList.contains('is-active')) {
                 if (this.ref.navItem[parseInt(i)+1] != null) {
                     setTimeout(() => {
                         this.ref.navItem[parseInt(i)+1].click();
                         return false;
                     }, 10)
                 }
            }
        });
    }

    previous() {
        this.ref.navItem.forEach((element, i) => {
            if (element.classList.contains('is-active')) {
                if (this.ref.navItem[i-1] != null) {
                    this.ref.navItem[i - 1].click();
                    return false;
                }
            }
        });
    }
}