import Component from '../core/Component'
import photoswipe from '../plugins/photoswipe'

export default class Explode extends Component {
    constructor(element) {
        super(element)

        // options
        let options = {};
        if (this.element.dataset.options != null) {
            options = JSON.parse(this.element.dataset.options);
        }
        let defaults = {

        }

        this.options = {
            ...defaults,
            ...options
        }
    }

    prepare() {

    }

    destroy() {

    }
}