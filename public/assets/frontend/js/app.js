/******/ (function(modules) { // webpackBootstrap
/******/ 	// install a JSONP callback for chunk loading
/******/ 	var parentJsonpFunction = window["webpackJsonp"];
/******/ 	window["webpackJsonp"] = function webpackJsonpCallback(chunkIds, moreModules, executeModules) {
/******/ 		// add "moreModules" to the modules object,
/******/ 		// then flag all "chunkIds" as loaded and fire callback
/******/ 		var moduleId, chunkId, i = 0, resolves = [], result;
/******/ 		for(;i < chunkIds.length; i++) {
/******/ 			chunkId = chunkIds[i];
/******/ 			if(installedChunks[chunkId]) {
/******/ 				resolves.push(installedChunks[chunkId][0]);
/******/ 			}
/******/ 			installedChunks[chunkId] = 0;
/******/ 		}
/******/ 		for(moduleId in moreModules) {
/******/ 			if(Object.prototype.hasOwnProperty.call(moreModules, moduleId)) {
/******/ 				modules[moduleId] = moreModules[moduleId];
/******/ 			}
/******/ 		}
/******/ 		if(parentJsonpFunction) parentJsonpFunction(chunkIds, moreModules, executeModules);
/******/ 		while(resolves.length) {
/******/ 			resolves.shift()();
/******/ 		}
/******/
/******/ 	};
/******/
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// objects to store loaded and loading chunks
/******/ 	var installedChunks = {
/******/ 		1: 0
/******/ 	};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/ 	// This file contains only the entry chunk.
/******/ 	// The chunk loading function for additional chunks
/******/ 	__webpack_require__.e = function requireEnsure(chunkId) {
/******/ 		var installedChunkData = installedChunks[chunkId];
/******/ 		if(installedChunkData === 0) {
/******/ 			return new Promise(function(resolve) { resolve(); });
/******/ 		}
/******/
/******/ 		// a Promise means "currently loading".
/******/ 		if(installedChunkData) {
/******/ 			return installedChunkData[2];
/******/ 		}
/******/
/******/ 		// setup Promise in chunk cache
/******/ 		var promise = new Promise(function(resolve, reject) {
/******/ 			installedChunkData = installedChunks[chunkId] = [resolve, reject];
/******/ 		});
/******/ 		installedChunkData[2] = promise;
/******/
/******/ 		// start chunk loading
/******/ 		var head = document.getElementsByTagName('head')[0];
/******/ 		var script = document.createElement('script');
/******/ 		script.type = 'text/javascript';
/******/ 		script.charset = 'utf-8';
/******/ 		script.async = true;
/******/ 		script.timeout = 120000;
/******/
/******/ 		if (__webpack_require__.nc) {
/******/ 			script.setAttribute("nonce", __webpack_require__.nc);
/******/ 		}
/******/ 		script.src = __webpack_require__.p + "" + ({}[chunkId]||chunkId) + "." + {"0":"bd59b9d83b37bfd8bbcf"}[chunkId] + ".js";
/******/ 		var timeout = setTimeout(onScriptComplete, 120000);
/******/ 		script.onerror = script.onload = onScriptComplete;
/******/ 		function onScriptComplete() {
/******/ 			// avoid mem leaks in IE.
/******/ 			script.onerror = script.onload = null;
/******/ 			clearTimeout(timeout);
/******/ 			var chunk = installedChunks[chunkId];
/******/ 			if(chunk !== 0) {
/******/ 				if(chunk) {
/******/ 					chunk[1](new Error('Loading chunk ' + chunkId + ' failed.'));
/******/ 				}
/******/ 				installedChunks[chunkId] = undefined;
/******/ 			}
/******/ 		};
/******/ 		head.appendChild(script);
/******/
/******/ 		return promise;
/******/ 	};
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/assets/frontend/js/";
/******/
/******/ 	// on error function for async loading
/******/ 	__webpack_require__.oe = function(err) { console.error(err); throw err; };
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 16);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/* WEBPACK VAR INJECTION */(function(global) {var __WEBPACK_AMD_DEFINE_ARRAY__, __WEBPACK_AMD_DEFINE_RESULT__;

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

/*!
 * VERSION: 1.20.3
 * DATE: 2017-10-02
 * UPDATES AND DOCS AT: http://greensock.com
 *
 * @license Copyright (c) 2008-2017, GreenSock. All rights reserved.
 * This work is subject to the terms at http://greensock.com/standard-license or for
 * Club GreenSock members, the software agreement that was issued with your membership.
 * 
 * @author: Jack Doyle, jack@greensock.com
 */
(function (window, moduleName) {

	"use strict";

	var _exports = {},
	    _doc = window.document,
	    _globals = window.GreenSockGlobals = window.GreenSockGlobals || window;
	if (_globals.TweenLite) {
		return; //in case the core set of classes is already loaded, don't instantiate twice.
	}
	var _namespace = function _namespace(ns) {
		var a = ns.split("."),
		    p = _globals,
		    i;
		for (i = 0; i < a.length; i++) {
			p[a[i]] = p = p[a[i]] || {};
		}
		return p;
	},
	    gs = _namespace("com.greensock"),
	    _tinyNum = 0.0000000001,
	    _slice = function _slice(a) {
		//don't use Array.prototype.slice.call(target, 0) because that doesn't work in IE8 with a NodeList that's returned by querySelectorAll()
		var b = [],
		    l = a.length,
		    i;
		for (i = 0; i !== l; b.push(a[i++])) {}
		return b;
	},
	    _emptyFunc = function _emptyFunc() {},
	    _isArray = function () {
		//works around issues in iframe environments where the Array global isn't shared, thus if the object originates in a different window/iframe, "(obj instanceof Array)" will evaluate false. We added some speed optimizations to avoid Object.prototype.toString.call() unless it's absolutely necessary because it's VERY slow (like 20x slower)
		var toString = Object.prototype.toString,
		    array = toString.call([]);
		return function (obj) {
			return obj != null && (obj instanceof Array || (typeof obj === "undefined" ? "undefined" : _typeof(obj)) === "object" && !!obj.push && toString.call(obj) === array);
		};
	}(),
	    a,
	    i,
	    p,
	    _ticker,
	    _tickerActive,
	    _defLookup = {},


	/**
  * @constructor
  * Defines a GreenSock class, optionally with an array of dependencies that must be instantiated first and passed into the definition.
  * This allows users to load GreenSock JS files in any order even if they have interdependencies (like CSSPlugin extends TweenPlugin which is
  * inside TweenLite.js, but if CSSPlugin is loaded first, it should wait to run its code until TweenLite.js loads and instantiates TweenPlugin
  * and then pass TweenPlugin to CSSPlugin's definition). This is all done automatically and internally.
  *
  * Every definition will be added to a "com.greensock" global object (typically window, but if a window.GreenSockGlobals object is found,
  * it will go there as of v1.7). For example, TweenLite will be found at window.com.greensock.TweenLite and since it's a global class that should be available anywhere,
  * it is ALSO referenced at window.TweenLite. However some classes aren't considered global, like the base com.greensock.core.Animation class, so
  * those will only be at the package like window.com.greensock.core.Animation. Again, if you define a GreenSockGlobals object on the window, everything
  * gets tucked neatly inside there instead of on the window directly. This allows you to do advanced things like load multiple versions of GreenSock
  * files and put them into distinct objects (imagine a banner ad uses a newer version but the main site uses an older one). In that case, you could
  * sandbox the banner one like:
  *
  * <script>
  *     var gs = window.GreenSockGlobals = {}; //the newer version we're about to load could now be referenced in a "gs" object, like gs.TweenLite.to(...). Use whatever alias you want as long as it's unique, "gs" or "banner" or whatever.
  * </script>
  * <script src="js/greensock/v1.7/TweenMax.js"></script>
  * <script>
  *     window.GreenSockGlobals = window._gsQueue = window._gsDefine = null; //reset it back to null (along with the special _gsQueue variable) so that the next load of TweenMax affects the window and we can reference things directly like TweenLite.to(...)
  * </script>
  * <script src="js/greensock/v1.6/TweenMax.js"></script>
  * <script>
  *     gs.TweenLite.to(...); //would use v1.7
  *     TweenLite.to(...); //would use v1.6
  * </script>
  *
  * @param {!string} ns The namespace of the class definition, leaving off "com.greensock." as that's assumed. For example, "TweenLite" or "plugins.CSSPlugin" or "easing.Back".
  * @param {!Array.<string>} dependencies An array of dependencies (described as their namespaces minus "com.greensock." prefix). For example ["TweenLite","plugins.TweenPlugin","core.Animation"]
  * @param {!function():Object} func The function that should be called and passed the resolved dependencies which will return the actual class for this definition.
  * @param {boolean=} global If true, the class will be added to the global scope (typically window unless you define a window.GreenSockGlobals object)
  */
	Definition = function Definition(ns, dependencies, func, global) {
		this.sc = _defLookup[ns] ? _defLookup[ns].sc : []; //subclasses
		_defLookup[ns] = this;
		this.gsClass = null;
		this.func = func;
		var _classes = [];
		this.check = function (init) {
			var i = dependencies.length,
			    missing = i,
			    cur,
			    a,
			    n,
			    cl;
			while (--i > -1) {
				if ((cur = _defLookup[dependencies[i]] || new Definition(dependencies[i], [])).gsClass) {
					_classes[i] = cur.gsClass;
					missing--;
				} else if (init) {
					cur.sc.push(this);
				}
			}
			if (missing === 0 && func) {
				a = ("com.greensock." + ns).split(".");
				n = a.pop();
				cl = _namespace(a.join("."))[n] = this.gsClass = func.apply(func, _classes);

				//exports to multiple environments
				if (global) {
					_globals[n] = _exports[n] = cl; //provides a way to avoid global namespace pollution. By default, the main classes like TweenLite, Power1, Strong, etc. are added to window unless a GreenSockGlobals is defined. So if you want to have things added to a custom object instead, just do something like window.GreenSockGlobals = {} before loading any GreenSock files. You can even set up an alias like window.GreenSockGlobals = windows.gs = {} so that you can access everything like gs.TweenLite. Also remember that ALL classes are added to the window.com.greensock object (in their respective packages, like com.greensock.easing.Power1, com.greensock.TweenLite, etc.)
					if (typeof module !== "undefined" && module.exports) {
						//node
						if (ns === moduleName) {
							module.exports = _exports[moduleName] = cl;
							for (i in _exports) {
								cl[i] = _exports[i];
							}
						} else if (_exports[moduleName]) {
							_exports[moduleName][n] = cl;
						}
					} else if (true) {
						//AMD
						!(__WEBPACK_AMD_DEFINE_ARRAY__ = [], __WEBPACK_AMD_DEFINE_RESULT__ = (function () {
							return cl;
						}).apply(exports, __WEBPACK_AMD_DEFINE_ARRAY__),
				__WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));
					}
				}
				for (i = 0; i < this.sc.length; i++) {
					this.sc[i].check();
				}
			}
		};
		this.check(true);
	},


	//used to create Definition instances (which basically registers a class that has dependencies).
	_gsDefine = window._gsDefine = function (ns, dependencies, func, global) {
		return new Definition(ns, dependencies, func, global);
	},


	//a quick way to create a class that doesn't have any dependencies. Returns the class, but first registers it in the GreenSock namespace so that other classes can grab it (other classes might be dependent on the class).
	_class = gs._class = function (ns, func, global) {
		func = func || function () {};
		_gsDefine(ns, [], function () {
			return func;
		}, global);
		return func;
	};

	_gsDefine.globals = _globals;

	/*
  * ----------------------------------------------------------------
  * Ease
  * ----------------------------------------------------------------
  */
	var _baseParams = [0, 0, 1, 1],
	    Ease = _class("easing.Ease", function (func, extraParams, type, power) {
		this._func = func;
		this._type = type || 0;
		this._power = power || 0;
		this._params = extraParams ? _baseParams.concat(extraParams) : _baseParams;
	}, true),
	    _easeMap = Ease.map = {},
	    _easeReg = Ease.register = function (ease, names, types, create) {
		var na = names.split(","),
		    i = na.length,
		    ta = (types || "easeIn,easeOut,easeInOut").split(","),
		    e,
		    name,
		    j,
		    type;
		while (--i > -1) {
			name = na[i];
			e = create ? _class("easing." + name, null, true) : gs.easing[name] || {};
			j = ta.length;
			while (--j > -1) {
				type = ta[j];
				_easeMap[name + "." + type] = _easeMap[type + name] = e[type] = ease.getRatio ? ease : ease[type] || new ease();
			}
		}
	};

	p = Ease.prototype;
	p._calcEnd = false;
	p.getRatio = function (p) {
		if (this._func) {
			this._params[0] = p;
			return this._func.apply(null, this._params);
		}
		var t = this._type,
		    pw = this._power,
		    r = t === 1 ? 1 - p : t === 2 ? p : p < 0.5 ? p * 2 : (1 - p) * 2;
		if (pw === 1) {
			r *= r;
		} else if (pw === 2) {
			r *= r * r;
		} else if (pw === 3) {
			r *= r * r * r;
		} else if (pw === 4) {
			r *= r * r * r * r;
		}
		return t === 1 ? 1 - r : t === 2 ? r : p < 0.5 ? r / 2 : 1 - r / 2;
	};

	//create all the standard eases like Linear, Quad, Cubic, Quart, Quint, Strong, Power0, Power1, Power2, Power3, and Power4 (each with easeIn, easeOut, and easeInOut)
	a = ["Linear", "Quad", "Cubic", "Quart", "Quint,Strong"];
	i = a.length;
	while (--i > -1) {
		p = a[i] + ",Power" + i;
		_easeReg(new Ease(null, null, 1, i), p, "easeOut", true);
		_easeReg(new Ease(null, null, 2, i), p, "easeIn" + (i === 0 ? ",easeNone" : ""));
		_easeReg(new Ease(null, null, 3, i), p, "easeInOut");
	}
	_easeMap.linear = gs.easing.Linear.easeIn;
	_easeMap.swing = gs.easing.Quad.easeInOut; //for jQuery folks


	/*
  * ----------------------------------------------------------------
  * EventDispatcher
  * ----------------------------------------------------------------
  */
	var EventDispatcher = _class("events.EventDispatcher", function (target) {
		this._listeners = {};
		this._eventTarget = target || this;
	});
	p = EventDispatcher.prototype;

	p.addEventListener = function (type, callback, scope, useParam, priority) {
		priority = priority || 0;
		var list = this._listeners[type],
		    index = 0,
		    listener,
		    i;
		if (this === _ticker && !_tickerActive) {
			_ticker.wake();
		}
		if (list == null) {
			this._listeners[type] = list = [];
		}
		i = list.length;
		while (--i > -1) {
			listener = list[i];
			if (listener.c === callback && listener.s === scope) {
				list.splice(i, 1);
			} else if (index === 0 && listener.pr < priority) {
				index = i + 1;
			}
		}
		list.splice(index, 0, { c: callback, s: scope, up: useParam, pr: priority });
	};

	p.removeEventListener = function (type, callback) {
		var list = this._listeners[type],
		    i;
		if (list) {
			i = list.length;
			while (--i > -1) {
				if (list[i].c === callback) {
					list.splice(i, 1);
					return;
				}
			}
		}
	};

	p.dispatchEvent = function (type) {
		var list = this._listeners[type],
		    i,
		    t,
		    listener;
		if (list) {
			i = list.length;
			if (i > 1) {
				list = list.slice(0); //in case addEventListener() is called from within a listener/callback (otherwise the index could change, resulting in a skip)
			}
			t = this._eventTarget;
			while (--i > -1) {
				listener = list[i];
				if (listener) {
					if (listener.up) {
						listener.c.call(listener.s || t, { type: type, target: t });
					} else {
						listener.c.call(listener.s || t);
					}
				}
			}
		}
	};

	/*
  * ----------------------------------------------------------------
  * Ticker
  * ----------------------------------------------------------------
  */
	var _reqAnimFrame = window.requestAnimationFrame,
	    _cancelAnimFrame = window.cancelAnimationFrame,
	    _getTime = Date.now || function () {
		return new Date().getTime();
	},
	    _lastUpdate = _getTime();

	//now try to determine the requestAnimationFrame and cancelAnimationFrame functions and if none are found, we'll use a setTimeout()/clearTimeout() polyfill.
	a = ["ms", "moz", "webkit", "o"];
	i = a.length;
	while (--i > -1 && !_reqAnimFrame) {
		_reqAnimFrame = window[a[i] + "RequestAnimationFrame"];
		_cancelAnimFrame = window[a[i] + "CancelAnimationFrame"] || window[a[i] + "CancelRequestAnimationFrame"];
	}

	_class("Ticker", function (fps, useRAF) {
		var _self = this,
		    _startTime = _getTime(),
		    _useRAF = useRAF !== false && _reqAnimFrame ? "auto" : false,
		    _lagThreshold = 500,
		    _adjustedLag = 33,
		    _tickWord = "tick",
		    //helps reduce gc burden
		_fps,
		    _req,
		    _id,
		    _gap,
		    _nextTime,
		    _tick = function _tick(manual) {
			var elapsed = _getTime() - _lastUpdate,
			    overlap,
			    dispatch;
			if (elapsed > _lagThreshold) {
				_startTime += elapsed - _adjustedLag;
			}
			_lastUpdate += elapsed;
			_self.time = (_lastUpdate - _startTime) / 1000;
			overlap = _self.time - _nextTime;
			if (!_fps || overlap > 0 || manual === true) {
				_self.frame++;
				_nextTime += overlap + (overlap >= _gap ? 0.004 : _gap - overlap);
				dispatch = true;
			}
			if (manual !== true) {
				//make sure the request is made before we dispatch the "tick" event so that timing is maintained. Otherwise, if processing the "tick" requires a bunch of time (like 15ms) and we're using a setTimeout() that's based on 16.7ms, it'd technically take 31.7ms between frames otherwise.
				_id = _req(_tick);
			}
			if (dispatch) {
				_self.dispatchEvent(_tickWord);
			}
		};

		EventDispatcher.call(_self);
		_self.time = _self.frame = 0;
		_self.tick = function () {
			_tick(true);
		};

		_self.lagSmoothing = function (threshold, adjustedLag) {
			if (!arguments.length) {
				//if lagSmoothing() is called with no arguments, treat it like a getter that returns a boolean indicating if it's enabled or not. This is purposely undocumented and is for internal use.
				return _lagThreshold < 1 / _tinyNum;
			}
			_lagThreshold = threshold || 1 / _tinyNum; //zero should be interpreted as basically unlimited
			_adjustedLag = Math.min(adjustedLag, _lagThreshold, 0);
		};

		_self.sleep = function () {
			if (_id == null) {
				return;
			}
			if (!_useRAF || !_cancelAnimFrame) {
				clearTimeout(_id);
			} else {
				_cancelAnimFrame(_id);
			}
			_req = _emptyFunc;
			_id = null;
			if (_self === _ticker) {
				_tickerActive = false;
			}
		};

		_self.wake = function (seamless) {
			if (_id !== null) {
				_self.sleep();
			} else if (seamless) {
				_startTime += -_lastUpdate + (_lastUpdate = _getTime());
			} else if (_self.frame > 10) {
				//don't trigger lagSmoothing if we're just waking up, and make sure that at least 10 frames have elapsed because of the iOS bug that we work around below with the 1.5-second setTimout().
				_lastUpdate = _getTime() - _lagThreshold + 5;
			}
			_req = _fps === 0 ? _emptyFunc : !_useRAF || !_reqAnimFrame ? function (f) {
				return setTimeout(f, (_nextTime - _self.time) * 1000 + 1 | 0);
			} : _reqAnimFrame;
			if (_self === _ticker) {
				_tickerActive = true;
			}
			_tick(2);
		};

		_self.fps = function (value) {
			if (!arguments.length) {
				return _fps;
			}
			_fps = value;
			_gap = 1 / (_fps || 60);
			_nextTime = this.time + _gap;
			_self.wake();
		};

		_self.useRAF = function (value) {
			if (!arguments.length) {
				return _useRAF;
			}
			_self.sleep();
			_useRAF = value;
			_self.fps(_fps);
		};
		_self.fps(fps);

		//a bug in iOS 6 Safari occasionally prevents the requestAnimationFrame from working initially, so we use a 1.5-second timeout that automatically falls back to setTimeout() if it senses this condition.
		setTimeout(function () {
			if (_useRAF === "auto" && _self.frame < 5 && _doc.visibilityState !== "hidden") {
				_self.useRAF(false);
			}
		}, 1500);
	});

	p = gs.Ticker.prototype = new gs.events.EventDispatcher();
	p.constructor = gs.Ticker;

	/*
  * ----------------------------------------------------------------
  * Animation
  * ----------------------------------------------------------------
  */
	var Animation = _class("core.Animation", function (duration, vars) {
		this.vars = vars = vars || {};
		this._duration = this._totalDuration = duration || 0;
		this._delay = Number(vars.delay) || 0;
		this._timeScale = 1;
		this._active = vars.immediateRender === true;
		this.data = vars.data;
		this._reversed = vars.reversed === true;

		if (!_rootTimeline) {
			return;
		}
		if (!_tickerActive) {
			//some browsers (like iOS 6 Safari) shut down JavaScript execution when the tab is disabled and they [occasionally] neglect to start up requestAnimationFrame again when returning - this code ensures that the engine starts up again properly.
			_ticker.wake();
		}

		var tl = this.vars.useFrames ? _rootFramesTimeline : _rootTimeline;
		tl.add(this, tl._time);

		if (this.vars.paused) {
			this.paused(true);
		}
	});

	_ticker = Animation.ticker = new gs.Ticker();
	p = Animation.prototype;
	p._dirty = p._gc = p._initted = p._paused = false;
	p._totalTime = p._time = 0;
	p._rawPrevTime = -1;
	p._next = p._last = p._onUpdate = p._timeline = p.timeline = null;
	p._paused = false;

	//some browsers (like iOS) occasionally drop the requestAnimationFrame event when the user switches to a different tab and then comes back again, so we use a 2-second setTimeout() to sense if/when that condition occurs and then wake() the ticker.
	var _checkTimeout = function _checkTimeout() {
		if (_tickerActive && _getTime() - _lastUpdate > 2000 && (_doc.visibilityState !== "hidden" || !_ticker.lagSmoothing())) {
			//note: if the tab is hidden, we should still wake if lagSmoothing has been disabled.
			_ticker.wake();
		}
		var t = setTimeout(_checkTimeout, 2000);
		if (t.unref) {
			// allows a node process to exit even if the timeout’s callback hasn't been invoked. Without it, the node process could hang as this function is called every two seconds.
			t.unref();
		}
	};
	_checkTimeout();

	p.play = function (from, suppressEvents) {
		if (from != null) {
			this.seek(from, suppressEvents);
		}
		return this.reversed(false).paused(false);
	};

	p.pause = function (atTime, suppressEvents) {
		if (atTime != null) {
			this.seek(atTime, suppressEvents);
		}
		return this.paused(true);
	};

	p.resume = function (from, suppressEvents) {
		if (from != null) {
			this.seek(from, suppressEvents);
		}
		return this.paused(false);
	};

	p.seek = function (time, suppressEvents) {
		return this.totalTime(Number(time), suppressEvents !== false);
	};

	p.restart = function (includeDelay, suppressEvents) {
		return this.reversed(false).paused(false).totalTime(includeDelay ? -this._delay : 0, suppressEvents !== false, true);
	};

	p.reverse = function (from, suppressEvents) {
		if (from != null) {
			this.seek(from || this.totalDuration(), suppressEvents);
		}
		return this.reversed(true).paused(false);
	};

	p.render = function (time, suppressEvents, force) {
		//stub - we override this method in subclasses.
	};

	p.invalidate = function () {
		this._time = this._totalTime = 0;
		this._initted = this._gc = false;
		this._rawPrevTime = -1;
		if (this._gc || !this.timeline) {
			this._enabled(true);
		}
		return this;
	};

	p.isActive = function () {
		var tl = this._timeline,
		    //the 2 root timelines won't have a _timeline; they're always active.
		startTime = this._startTime,
		    rawTime;
		return !tl || !this._gc && !this._paused && tl.isActive() && (rawTime = tl.rawTime(true)) >= startTime && rawTime < startTime + this.totalDuration() / this._timeScale - 0.0000001;
	};

	p._enabled = function (enabled, ignoreTimeline) {
		if (!_tickerActive) {
			_ticker.wake();
		}
		this._gc = !enabled;
		this._active = this.isActive();
		if (ignoreTimeline !== true) {
			if (enabled && !this.timeline) {
				this._timeline.add(this, this._startTime - this._delay);
			} else if (!enabled && this.timeline) {
				this._timeline._remove(this, true);
			}
		}
		return false;
	};

	p._kill = function (vars, target) {
		return this._enabled(false, false);
	};

	p.kill = function (vars, target) {
		this._kill(vars, target);
		return this;
	};

	p._uncache = function (includeSelf) {
		var tween = includeSelf ? this : this.timeline;
		while (tween) {
			tween._dirty = true;
			tween = tween.timeline;
		}
		return this;
	};

	p._swapSelfInParams = function (params) {
		var i = params.length,
		    copy = params.concat();
		while (--i > -1) {
			if (params[i] === "{self}") {
				copy[i] = this;
			}
		}
		return copy;
	};

	p._callback = function (type) {
		var v = this.vars,
		    callback = v[type],
		    params = v[type + "Params"],
		    scope = v[type + "Scope"] || v.callbackScope || this,
		    l = params ? params.length : 0;
		switch (l) {//speed optimization; call() is faster than apply() so use it when there are only a few parameters (which is by far most common). Previously we simply did var v = this.vars; v[type].apply(v[type + "Scope"] || v.callbackScope || this, v[type + "Params"] || _blankArray);
			case 0:
				callback.call(scope);break;
			case 1:
				callback.call(scope, params[0]);break;
			case 2:
				callback.call(scope, params[0], params[1]);break;
			default:
				callback.apply(scope, params);
		}
	};

	//----Animation getters/setters --------------------------------------------------------

	p.eventCallback = function (type, callback, params, scope) {
		if ((type || "").substr(0, 2) === "on") {
			var v = this.vars;
			if (arguments.length === 1) {
				return v[type];
			}
			if (callback == null) {
				delete v[type];
			} else {
				v[type] = callback;
				v[type + "Params"] = _isArray(params) && params.join("").indexOf("{self}") !== -1 ? this._swapSelfInParams(params) : params;
				v[type + "Scope"] = scope;
			}
			if (type === "onUpdate") {
				this._onUpdate = callback;
			}
		}
		return this;
	};

	p.delay = function (value) {
		if (!arguments.length) {
			return this._delay;
		}
		if (this._timeline.smoothChildTiming) {
			this.startTime(this._startTime + value - this._delay);
		}
		this._delay = value;
		return this;
	};

	p.duration = function (value) {
		if (!arguments.length) {
			this._dirty = false;
			return this._duration;
		}
		this._duration = this._totalDuration = value;
		this._uncache(true); //true in case it's a TweenMax or TimelineMax that has a repeat - we'll need to refresh the totalDuration.
		if (this._timeline.smoothChildTiming) if (this._time > 0) if (this._time < this._duration) if (value !== 0) {
			this.totalTime(this._totalTime * (value / this._duration), true);
		}
		return this;
	};

	p.totalDuration = function (value) {
		this._dirty = false;
		return !arguments.length ? this._totalDuration : this.duration(value);
	};

	p.time = function (value, suppressEvents) {
		if (!arguments.length) {
			return this._time;
		}
		if (this._dirty) {
			this.totalDuration();
		}
		return this.totalTime(value > this._duration ? this._duration : value, suppressEvents);
	};

	p.totalTime = function (time, suppressEvents, uncapped) {
		if (!_tickerActive) {
			_ticker.wake();
		}
		if (!arguments.length) {
			return this._totalTime;
		}
		if (this._timeline) {
			if (time < 0 && !uncapped) {
				time += this.totalDuration();
			}
			if (this._timeline.smoothChildTiming) {
				if (this._dirty) {
					this.totalDuration();
				}
				var totalDuration = this._totalDuration,
				    tl = this._timeline;
				if (time > totalDuration && !uncapped) {
					time = totalDuration;
				}
				this._startTime = (this._paused ? this._pauseTime : tl._time) - (!this._reversed ? time : totalDuration - time) / this._timeScale;
				if (!tl._dirty) {
					//for performance improvement. If the parent's cache is already dirty, it already took care of marking the ancestors as dirty too, so skip the function call here.
					this._uncache(false);
				}
				//in case any of the ancestor timelines had completed but should now be enabled, we should reset their totalTime() which will also ensure that they're lined up properly and enabled. Skip for animations that are on the root (wasteful). Example: a TimelineLite.exportRoot() is performed when there's a paused tween on the root, the export will not complete until that tween is unpaused, but imagine a child gets restarted later, after all [unpaused] tweens have completed. The startTime of that child would get pushed out, but one of the ancestors may have completed.
				if (tl._timeline) {
					while (tl._timeline) {
						if (tl._timeline._time !== (tl._startTime + tl._totalTime) / tl._timeScale) {
							tl.totalTime(tl._totalTime, true);
						}
						tl = tl._timeline;
					}
				}
			}
			if (this._gc) {
				this._enabled(true, false);
			}
			if (this._totalTime !== time || this._duration === 0) {
				if (_lazyTweens.length) {
					_lazyRender();
				}
				this.render(time, suppressEvents, false);
				if (_lazyTweens.length) {
					//in case rendering caused any tweens to lazy-init, we should render them because typically when someone calls seek() or time() or progress(), they expect an immediate render.
					_lazyRender();
				}
			}
		}
		return this;
	};

	p.progress = p.totalProgress = function (value, suppressEvents) {
		var duration = this.duration();
		return !arguments.length ? duration ? this._time / duration : this.ratio : this.totalTime(duration * value, suppressEvents);
	};

	p.startTime = function (value) {
		if (!arguments.length) {
			return this._startTime;
		}
		if (value !== this._startTime) {
			this._startTime = value;
			if (this.timeline) if (this.timeline._sortChildren) {
				this.timeline.add(this, value - this._delay); //ensures that any necessary re-sequencing of Animations in the timeline occurs to make sure the rendering order is correct.
			}
		}
		return this;
	};

	p.endTime = function (includeRepeats) {
		return this._startTime + (includeRepeats != false ? this.totalDuration() : this.duration()) / this._timeScale;
	};

	p.timeScale = function (value) {
		if (!arguments.length) {
			return this._timeScale;
		}
		var pauseTime, t;
		value = value || _tinyNum; //can't allow zero because it'll throw the math off
		if (this._timeline && this._timeline.smoothChildTiming) {
			pauseTime = this._pauseTime;
			t = pauseTime || pauseTime === 0 ? pauseTime : this._timeline.totalTime();
			this._startTime = t - (t - this._startTime) * this._timeScale / value;
		}
		this._timeScale = value;
		t = this.timeline;
		while (t && t.timeline) {
			//must update the duration/totalDuration of all ancestor timelines immediately in case in the middle of a render loop, one tween alters another tween's timeScale which shoves its startTime before 0, forcing the parent timeline to shift around and shiftChildren() which could affect that next tween's render (startTime). Doesn't matter for the root timeline though.
			t._dirty = true;
			t.totalDuration();
			t = t.timeline;
		}
		return this;
	};

	p.reversed = function (value) {
		if (!arguments.length) {
			return this._reversed;
		}
		if (value != this._reversed) {
			this._reversed = value;
			this.totalTime(this._timeline && !this._timeline.smoothChildTiming ? this.totalDuration() - this._totalTime : this._totalTime, true);
		}
		return this;
	};

	p.paused = function (value) {
		if (!arguments.length) {
			return this._paused;
		}
		var tl = this._timeline,
		    raw,
		    elapsed;
		if (value != this._paused) if (tl) {
			if (!_tickerActive && !value) {
				_ticker.wake();
			}
			raw = tl.rawTime();
			elapsed = raw - this._pauseTime;
			if (!value && tl.smoothChildTiming) {
				this._startTime += elapsed;
				this._uncache(false);
			}
			this._pauseTime = value ? raw : null;
			this._paused = value;
			this._active = this.isActive();
			if (!value && elapsed !== 0 && this._initted && this.duration()) {
				raw = tl.smoothChildTiming ? this._totalTime : (raw - this._startTime) / this._timeScale;
				this.render(raw, raw === this._totalTime, true); //in case the target's properties changed via some other tween or manual update by the user, we should force a render.
			}
		}
		if (this._gc && !value) {
			this._enabled(true, false);
		}
		return this;
	};

	/*
  * ----------------------------------------------------------------
  * SimpleTimeline
  * ----------------------------------------------------------------
  */
	var SimpleTimeline = _class("core.SimpleTimeline", function (vars) {
		Animation.call(this, 0, vars);
		this.autoRemoveChildren = this.smoothChildTiming = true;
	});

	p = SimpleTimeline.prototype = new Animation();
	p.constructor = SimpleTimeline;
	p.kill()._gc = false;
	p._first = p._last = p._recent = null;
	p._sortChildren = false;

	p.add = p.insert = function (child, position, align, stagger) {
		var prevTween, st;
		child._startTime = Number(position || 0) + child._delay;
		if (child._paused) if (this !== child._timeline) {
			//we only adjust the _pauseTime if it wasn't in this timeline already. Remember, sometimes a tween will be inserted again into the same timeline when its startTime is changed so that the tweens in the TimelineLite/Max are re-ordered properly in the linked list (so everything renders in the proper order).
			child._pauseTime = child._startTime + (this.rawTime() - child._startTime) / child._timeScale;
		}
		if (child.timeline) {
			child.timeline._remove(child, true); //removes from existing timeline so that it can be properly added to this one.
		}
		child.timeline = child._timeline = this;
		if (child._gc) {
			child._enabled(true, true);
		}
		prevTween = this._last;
		if (this._sortChildren) {
			st = child._startTime;
			while (prevTween && prevTween._startTime > st) {
				prevTween = prevTween._prev;
			}
		}
		if (prevTween) {
			child._next = prevTween._next;
			prevTween._next = child;
		} else {
			child._next = this._first;
			this._first = child;
		}
		if (child._next) {
			child._next._prev = child;
		} else {
			this._last = child;
		}
		child._prev = prevTween;
		this._recent = child;
		if (this._timeline) {
			this._uncache(true);
		}
		return this;
	};

	p._remove = function (tween, skipDisable) {
		if (tween.timeline === this) {
			if (!skipDisable) {
				tween._enabled(false, true);
			}

			if (tween._prev) {
				tween._prev._next = tween._next;
			} else if (this._first === tween) {
				this._first = tween._next;
			}
			if (tween._next) {
				tween._next._prev = tween._prev;
			} else if (this._last === tween) {
				this._last = tween._prev;
			}
			tween._next = tween._prev = tween.timeline = null;
			if (tween === this._recent) {
				this._recent = this._last;
			}

			if (this._timeline) {
				this._uncache(true);
			}
		}
		return this;
	};

	p.render = function (time, suppressEvents, force) {
		var tween = this._first,
		    next;
		this._totalTime = this._time = this._rawPrevTime = time;
		while (tween) {
			next = tween._next; //record it here because the value could change after rendering...
			if (tween._active || time >= tween._startTime && !tween._paused && !tween._gc) {
				if (!tween._reversed) {
					tween.render((time - tween._startTime) * tween._timeScale, suppressEvents, force);
				} else {
					tween.render((!tween._dirty ? tween._totalDuration : tween.totalDuration()) - (time - tween._startTime) * tween._timeScale, suppressEvents, force);
				}
			}
			tween = next;
		}
	};

	p.rawTime = function () {
		if (!_tickerActive) {
			_ticker.wake();
		}
		return this._totalTime;
	};

	/*
  * ----------------------------------------------------------------
  * TweenLite
  * ----------------------------------------------------------------
  */
	var TweenLite = _class("TweenLite", function (target, duration, vars) {
		Animation.call(this, duration, vars);
		this.render = TweenLite.prototype.render; //speed optimization (avoid prototype lookup on this "hot" method)

		if (target == null) {
			throw "Cannot tween a null target.";
		}

		this.target = target = typeof target !== "string" ? target : TweenLite.selector(target) || target;

		var isSelector = target.jquery || target.length && target !== window && target[0] && (target[0] === window || target[0].nodeType && target[0].style && !target.nodeType),
		    overwrite = this.vars.overwrite,
		    i,
		    targ,
		    targets;

		this._overwrite = overwrite = overwrite == null ? _overwriteLookup[TweenLite.defaultOverwrite] : typeof overwrite === "number" ? overwrite >> 0 : _overwriteLookup[overwrite];

		if ((isSelector || target instanceof Array || target.push && _isArray(target)) && typeof target[0] !== "number") {
			this._targets = targets = _slice(target); //don't use Array.prototype.slice.call(target, 0) because that doesn't work in IE8 with a NodeList that's returned by querySelectorAll()
			this._propLookup = [];
			this._siblings = [];
			for (i = 0; i < targets.length; i++) {
				targ = targets[i];
				if (!targ) {
					targets.splice(i--, 1);
					continue;
				} else if (typeof targ === "string") {
					targ = targets[i--] = TweenLite.selector(targ); //in case it's an array of strings
					if (typeof targ === "string") {
						targets.splice(i + 1, 1); //to avoid an endless loop (can't imagine why the selector would return a string, but just in case)
					}
					continue;
				} else if (targ.length && targ !== window && targ[0] && (targ[0] === window || targ[0].nodeType && targ[0].style && !targ.nodeType)) {
					//in case the user is passing in an array of selector objects (like jQuery objects), we need to check one more level and pull things out if necessary. Also note that <select> elements pass all the criteria regarding length and the first child having style, so we must also check to ensure the target isn't an HTML node itself.
					targets.splice(i--, 1);
					this._targets = targets = targets.concat(_slice(targ));
					continue;
				}
				this._siblings[i] = _register(targ, this, false);
				if (overwrite === 1) if (this._siblings[i].length > 1) {
					_applyOverwrite(targ, this, null, 1, this._siblings[i]);
				}
			}
		} else {
			this._propLookup = {};
			this._siblings = _register(target, this, false);
			if (overwrite === 1) if (this._siblings.length > 1) {
				_applyOverwrite(target, this, null, 1, this._siblings);
			}
		}
		if (this.vars.immediateRender || duration === 0 && this._delay === 0 && this.vars.immediateRender !== false) {
			this._time = -_tinyNum; //forces a render without having to set the render() "force" parameter to true because we want to allow lazying by default (using the "force" parameter always forces an immediate full render)
			this.render(Math.min(0, -this._delay)); //in case delay is negative
		}
	}, true),
	    _isSelector = function _isSelector(v) {
		return v && v.length && v !== window && v[0] && (v[0] === window || v[0].nodeType && v[0].style && !v.nodeType); //we cannot check "nodeType" if the target is window from within an iframe, otherwise it will trigger a security error in some browsers like Firefox.
	},
	    _autoCSS = function _autoCSS(vars, target) {
		var css = {},
		    p;
		for (p in vars) {
			if (!_reservedProps[p] && (!(p in target) || p === "transform" || p === "x" || p === "y" || p === "width" || p === "height" || p === "className" || p === "border") && (!_plugins[p] || _plugins[p] && _plugins[p]._autoCSS)) {
				//note: <img> elements contain read-only "x" and "y" properties. We should also prioritize editing css width/height rather than the element's properties.
				css[p] = vars[p];
				delete vars[p];
			}
		}
		vars.css = css;
	};

	p = TweenLite.prototype = new Animation();
	p.constructor = TweenLite;
	p.kill()._gc = false;

	//----TweenLite defaults, overwrite management, and root updates ----------------------------------------------------

	p.ratio = 0;
	p._firstPT = p._targets = p._overwrittenProps = p._startAt = null;
	p._notifyPluginsOfEnabled = p._lazy = false;

	TweenLite.version = "1.20.3";
	TweenLite.defaultEase = p._ease = new Ease(null, null, 1, 1);
	TweenLite.defaultOverwrite = "auto";
	TweenLite.ticker = _ticker;
	TweenLite.autoSleep = 120;
	TweenLite.lagSmoothing = function (threshold, adjustedLag) {
		_ticker.lagSmoothing(threshold, adjustedLag);
	};

	TweenLite.selector = window.$ || window.jQuery || function (e) {
		var selector = window.$ || window.jQuery;
		if (selector) {
			TweenLite.selector = selector;
			return selector(e);
		}
		return typeof _doc === "undefined" ? e : _doc.querySelectorAll ? _doc.querySelectorAll(e) : _doc.getElementById(e.charAt(0) === "#" ? e.substr(1) : e);
	};

	var _lazyTweens = [],
	    _lazyLookup = {},
	    _numbersExp = /(?:(-|-=|\+=)?\d*\.?\d*(?:e[\-+]?\d+)?)[0-9]/ig,
	    _relExp = /[\+-]=-?[\.\d]/,

	//_nonNumbersExp = /(?:([\-+](?!(\d|=)))|[^\d\-+=e]|(e(?![\-+][\d])))+/ig,
	_setRatio = function _setRatio(v) {
		var pt = this._firstPT,
		    min = 0.000001,
		    val;
		while (pt) {
			val = !pt.blob ? pt.c * v + pt.s : v === 1 && this.end != null ? this.end : v ? this.join("") : this.start;
			if (pt.m) {
				val = pt.m(val, this._target || pt.t);
			} else if (val < min) if (val > -min && !pt.blob) {
				//prevents issues with converting very small numbers to strings in the browser
				val = 0;
			}
			if (!pt.f) {
				pt.t[pt.p] = val;
			} else if (pt.fp) {
				pt.t[pt.p](pt.fp, val);
			} else {
				pt.t[pt.p](val);
			}
			pt = pt._next;
		}
	},

	//compares two strings (start/end), finds the numbers that are different and spits back an array representing the whole value but with the changing values isolated as elements. For example, "rgb(0,0,0)" and "rgb(100,50,0)" would become ["rgb(", 0, ",", 50, ",0)"]. Notice it merges the parts that are identical (performance optimization). The array also has a linked list of PropTweens attached starting with _firstPT that contain the tweening data (t, p, s, c, f, etc.). It also stores the starting value as a "start" property so that we can revert to it if/when necessary, like when a tween rewinds fully. If the quantity of numbers differs between the start and end, it will always prioritize the end value(s). The pt parameter is optional - it's for a PropTween that will be appended to the end of the linked list and is typically for actually setting the value after all of the elements have been updated (with array.join("")).
	_blobDif = function _blobDif(start, end, filter, pt) {
		var a = [],
		    charIndex = 0,
		    s = "",
		    color = 0,
		    startNums,
		    endNums,
		    num,
		    i,
		    l,
		    nonNumbers,
		    currentNum;
		a.start = start;
		a.end = end;
		start = a[0] = start + ""; //ensure values are strings
		end = a[1] = end + "";
		if (filter) {
			filter(a); //pass an array with the starting and ending values and let the filter do whatever it needs to the values.
			start = a[0];
			end = a[1];
		}
		a.length = 0;
		startNums = start.match(_numbersExp) || [];
		endNums = end.match(_numbersExp) || [];
		if (pt) {
			pt._next = null;
			pt.blob = 1;
			a._firstPT = a._applyPT = pt; //apply last in the linked list (which means inserting it first)
		}
		l = endNums.length;
		for (i = 0; i < l; i++) {
			currentNum = endNums[i];
			nonNumbers = end.substr(charIndex, end.indexOf(currentNum, charIndex) - charIndex);
			s += nonNumbers || !i ? nonNumbers : ","; //note: SVG spec allows omission of comma/space when a negative sign is wedged between two numbers, like 2.5-5.3 instead of 2.5,-5.3 but when tweening, the negative value may switch to positive, so we insert the comma just in case.
			charIndex += nonNumbers.length;
			if (color) {
				//sense rgba() values and round them.
				color = (color + 1) % 5;
			} else if (nonNumbers.substr(-5) === "rgba(") {
				color = 1;
			}
			if (currentNum === startNums[i] || startNums.length <= i) {
				s += currentNum;
			} else {
				if (s) {
					a.push(s);
					s = "";
				}
				num = parseFloat(startNums[i]);
				a.push(num);
				a._firstPT = { _next: a._firstPT, t: a, p: a.length - 1, s: num, c: (currentNum.charAt(1) === "=" ? parseInt(currentNum.charAt(0) + "1", 10) * parseFloat(currentNum.substr(2)) : parseFloat(currentNum) - num) || 0, f: 0, m: color && color < 4 ? Math.round : 0 };
				//note: we don't set _prev because we'll never need to remove individual PropTweens from this list.
			}
			charIndex += currentNum.length;
		}
		s += end.substr(charIndex);
		if (s) {
			a.push(s);
		}
		a.setRatio = _setRatio;
		if (_relExp.test(end)) {
			//if the end string contains relative values, delete it so that on the final render (in _setRatio()), we don't actually set it to the string with += or -= characters (forces it to use the calculated value).
			a.end = null;
		}
		return a;
	},

	//note: "funcParam" is only necessary for function-based getters/setters that require an extra parameter like getAttribute("width") and setAttribute("width", value). In this example, funcParam would be "width". Used by AttrPlugin for example.
	_addPropTween = function _addPropTween(target, prop, start, end, overwriteProp, mod, funcParam, stringFilter, index) {
		if (typeof end === "function") {
			end = end(index || 0, target);
		}
		var type = _typeof(target[prop]),
		    getterName = type !== "function" ? "" : prop.indexOf("set") || typeof target["get" + prop.substr(3)] !== "function" ? prop : "get" + prop.substr(3),
		    s = start !== "get" ? start : !getterName ? target[prop] : funcParam ? target[getterName](funcParam) : target[getterName](),
		    isRelative = typeof end === "string" && end.charAt(1) === "=",
		    pt = { t: target, p: prop, s: s, f: type === "function", pg: 0, n: overwriteProp || prop, m: !mod ? 0 : typeof mod === "function" ? mod : Math.round, pr: 0, c: isRelative ? parseInt(end.charAt(0) + "1", 10) * parseFloat(end.substr(2)) : parseFloat(end) - s || 0 },
		    blob;

		if (typeof s !== "number" || typeof end !== "number" && !isRelative) {
			if (funcParam || isNaN(s) || !isRelative && isNaN(end) || typeof s === "boolean" || typeof end === "boolean") {
				//a blob (string that has multiple numbers in it)
				pt.fp = funcParam;
				blob = _blobDif(s, isRelative ? parseFloat(pt.s) + pt.c : end, stringFilter || TweenLite.defaultStringFilter, pt);
				pt = { t: blob, p: "setRatio", s: 0, c: 1, f: 2, pg: 0, n: overwriteProp || prop, pr: 0, m: 0 }; //"2" indicates it's a Blob property tween. Needed for RoundPropsPlugin for example.
			} else {
				pt.s = parseFloat(s);
				if (!isRelative) {
					pt.c = parseFloat(end) - pt.s || 0;
				}
			}
		}
		if (pt.c) {
			//only add it to the linked list if there's a change.
			if (pt._next = this._firstPT) {
				pt._next._prev = pt;
			}
			this._firstPT = pt;
			return pt;
		}
	},
	    _internals = TweenLite._internals = { isArray: _isArray, isSelector: _isSelector, lazyTweens: _lazyTweens, blobDif: _blobDif },
	    //gives us a way to expose certain private values to other GreenSock classes without contaminating tha main TweenLite object.
	_plugins = TweenLite._plugins = {},
	    _tweenLookup = _internals.tweenLookup = {},
	    _tweenLookupNum = 0,
	    _reservedProps = _internals.reservedProps = { ease: 1, delay: 1, overwrite: 1, onComplete: 1, onCompleteParams: 1, onCompleteScope: 1, useFrames: 1, runBackwards: 1, startAt: 1, onUpdate: 1, onUpdateParams: 1, onUpdateScope: 1, onStart: 1, onStartParams: 1, onStartScope: 1, onReverseComplete: 1, onReverseCompleteParams: 1, onReverseCompleteScope: 1, onRepeat: 1, onRepeatParams: 1, onRepeatScope: 1, easeParams: 1, yoyo: 1, immediateRender: 1, repeat: 1, repeatDelay: 1, data: 1, paused: 1, reversed: 1, autoCSS: 1, lazy: 1, onOverwrite: 1, callbackScope: 1, stringFilter: 1, id: 1, yoyoEase: 1 },
	    _overwriteLookup = { none: 0, all: 1, auto: 2, concurrent: 3, allOnStart: 4, preexisting: 5, "true": 1, "false": 0 },
	    _rootFramesTimeline = Animation._rootFramesTimeline = new SimpleTimeline(),
	    _rootTimeline = Animation._rootTimeline = new SimpleTimeline(),
	    _nextGCFrame = 30,
	    _lazyRender = _internals.lazyRender = function () {
		var i = _lazyTweens.length,
		    tween;
		_lazyLookup = {};
		while (--i > -1) {
			tween = _lazyTweens[i];
			if (tween && tween._lazy !== false) {
				tween.render(tween._lazy[0], tween._lazy[1], true);
				tween._lazy = false;
			}
		}
		_lazyTweens.length = 0;
	};

	_rootTimeline._startTime = _ticker.time;
	_rootFramesTimeline._startTime = _ticker.frame;
	_rootTimeline._active = _rootFramesTimeline._active = true;
	setTimeout(_lazyRender, 1); //on some mobile devices, there isn't a "tick" before code runs which means any lazy renders wouldn't run before the next official "tick".

	Animation._updateRoot = TweenLite.render = function () {
		var i, a, p;
		if (_lazyTweens.length) {
			//if code is run outside of the requestAnimationFrame loop, there may be tweens queued AFTER the engine refreshed, so we need to ensure any pending renders occur before we refresh again.
			_lazyRender();
		}
		_rootTimeline.render((_ticker.time - _rootTimeline._startTime) * _rootTimeline._timeScale, false, false);
		_rootFramesTimeline.render((_ticker.frame - _rootFramesTimeline._startTime) * _rootFramesTimeline._timeScale, false, false);
		if (_lazyTweens.length) {
			_lazyRender();
		}
		if (_ticker.frame >= _nextGCFrame) {
			//dump garbage every 120 frames or whatever the user sets TweenLite.autoSleep to
			_nextGCFrame = _ticker.frame + (parseInt(TweenLite.autoSleep, 10) || 120);
			for (p in _tweenLookup) {
				a = _tweenLookup[p].tweens;
				i = a.length;
				while (--i > -1) {
					if (a[i]._gc) {
						a.splice(i, 1);
					}
				}
				if (a.length === 0) {
					delete _tweenLookup[p];
				}
			}
			//if there are no more tweens in the root timelines, or if they're all paused, make the _timer sleep to reduce load on the CPU slightly
			p = _rootTimeline._first;
			if (!p || p._paused) if (TweenLite.autoSleep && !_rootFramesTimeline._first && _ticker._listeners.tick.length === 1) {
				while (p && p._paused) {
					p = p._next;
				}
				if (!p) {
					_ticker.sleep();
				}
			}
		}
	};

	_ticker.addEventListener("tick", Animation._updateRoot);

	var _register = function _register(target, tween, scrub) {
		var id = target._gsTweenID,
		    a,
		    i;
		if (!_tweenLookup[id || (target._gsTweenID = id = "t" + _tweenLookupNum++)]) {
			_tweenLookup[id] = { target: target, tweens: [] };
		}
		if (tween) {
			a = _tweenLookup[id].tweens;
			a[i = a.length] = tween;
			if (scrub) {
				while (--i > -1) {
					if (a[i] === tween) {
						a.splice(i, 1);
					}
				}
			}
		}
		return _tweenLookup[id].tweens;
	},
	    _onOverwrite = function _onOverwrite(overwrittenTween, overwritingTween, target, killedProps) {
		var func = overwrittenTween.vars.onOverwrite,
		    r1,
		    r2;
		if (func) {
			r1 = func(overwrittenTween, overwritingTween, target, killedProps);
		}
		func = TweenLite.onOverwrite;
		if (func) {
			r2 = func(overwrittenTween, overwritingTween, target, killedProps);
		}
		return r1 !== false && r2 !== false;
	},
	    _applyOverwrite = function _applyOverwrite(target, tween, props, mode, siblings) {
		var i, changed, curTween, l;
		if (mode === 1 || mode >= 4) {
			l = siblings.length;
			for (i = 0; i < l; i++) {
				if ((curTween = siblings[i]) !== tween) {
					if (!curTween._gc) {
						if (curTween._kill(null, target, tween)) {
							changed = true;
						}
					}
				} else if (mode === 5) {
					break;
				}
			}
			return changed;
		}
		//NOTE: Add 0.0000000001 to overcome floating point errors that can cause the startTime to be VERY slightly off (when a tween's time() is set for example)
		var startTime = tween._startTime + _tinyNum,
		    overlaps = [],
		    oCount = 0,
		    zeroDur = tween._duration === 0,
		    globalStart;
		i = siblings.length;
		while (--i > -1) {
			if ((curTween = siblings[i]) === tween || curTween._gc || curTween._paused) {
				//ignore
			} else if (curTween._timeline !== tween._timeline) {
				globalStart = globalStart || _checkOverlap(tween, 0, zeroDur);
				if (_checkOverlap(curTween, globalStart, zeroDur) === 0) {
					overlaps[oCount++] = curTween;
				}
			} else if (curTween._startTime <= startTime) if (curTween._startTime + curTween.totalDuration() / curTween._timeScale > startTime) if (!((zeroDur || !curTween._initted) && startTime - curTween._startTime <= 0.0000000002)) {
				overlaps[oCount++] = curTween;
			}
		}

		i = oCount;
		while (--i > -1) {
			curTween = overlaps[i];
			if (mode === 2) if (curTween._kill(props, target, tween)) {
				changed = true;
			}
			if (mode !== 2 || !curTween._firstPT && curTween._initted) {
				if (mode !== 2 && !_onOverwrite(curTween, tween)) {
					continue;
				}
				if (curTween._enabled(false, false)) {
					//if all property tweens have been overwritten, kill the tween.
					changed = true;
				}
			}
		}
		return changed;
	},
	    _checkOverlap = function _checkOverlap(tween, reference, zeroDur) {
		var tl = tween._timeline,
		    ts = tl._timeScale,
		    t = tween._startTime;
		while (tl._timeline) {
			t += tl._startTime;
			ts *= tl._timeScale;
			if (tl._paused) {
				return -100;
			}
			tl = tl._timeline;
		}
		t /= ts;
		return t > reference ? t - reference : zeroDur && t === reference || !tween._initted && t - reference < 2 * _tinyNum ? _tinyNum : (t += tween.totalDuration() / tween._timeScale / ts) > reference + _tinyNum ? 0 : t - reference - _tinyNum;
	};

	//---- TweenLite instance methods -----------------------------------------------------------------------------

	p._init = function () {
		var v = this.vars,
		    op = this._overwrittenProps,
		    dur = this._duration,
		    immediate = !!v.immediateRender,
		    ease = v.ease,
		    i,
		    initPlugins,
		    pt,
		    p,
		    startVars,
		    l;
		if (v.startAt) {
			if (this._startAt) {
				this._startAt.render(-1, true); //if we've run a startAt previously (when the tween instantiated), we should revert it so that the values re-instantiate correctly particularly for relative tweens. Without this, a TweenLite.fromTo(obj, 1, {x:"+=100"}, {x:"-=100"}), for example, would actually jump to +=200 because the startAt would run twice, doubling the relative change.
				this._startAt.kill();
			}
			startVars = {};
			for (p in v.startAt) {
				//copy the properties/values into a new object to avoid collisions, like var to = {x:0}, from = {x:500}; timeline.fromTo(e, 1, from, to).fromTo(e, 1, to, from);
				startVars[p] = v.startAt[p];
			}
			startVars.data = "isStart";
			startVars.overwrite = false;
			startVars.immediateRender = true;
			startVars.lazy = immediate && v.lazy !== false;
			startVars.startAt = startVars.delay = null; //no nesting of startAt objects allowed (otherwise it could cause an infinite loop).
			startVars.onUpdate = v.onUpdate;
			startVars.onUpdateParams = v.onUpdateParams;
			startVars.onUpdateScope = v.onUpdateScope || v.callbackScope || this;
			this._startAt = TweenLite.to(this.target, 0, startVars);
			if (immediate) {
				if (this._time > 0) {
					this._startAt = null; //tweens that render immediately (like most from() and fromTo() tweens) shouldn't revert when their parent timeline's playhead goes backward past the startTime because the initial render could have happened anytime and it shouldn't be directly correlated to this tween's startTime. Imagine setting up a complex animation where the beginning states of various objects are rendered immediately but the tween doesn't happen for quite some time - if we revert to the starting values as soon as the playhead goes backward past the tween's startTime, it will throw things off visually. Reversion should only happen in TimelineLite/Max instances where immediateRender was false (which is the default in the convenience methods like from()).
				} else if (dur !== 0) {
					return; //we skip initialization here so that overwriting doesn't occur until the tween actually begins. Otherwise, if you create several immediateRender:true tweens of the same target/properties to drop into a TimelineLite or TimelineMax, the last one created would overwrite the first ones because they didn't get placed into the timeline yet before the first render occurs and kicks in overwriting.
				}
			}
		} else if (v.runBackwards && dur !== 0) {
			//from() tweens must be handled uniquely: their beginning values must be rendered but we don't want overwriting to occur yet (when time is still 0). Wait until the tween actually begins before doing all the routines like overwriting. At that time, we should render at the END of the tween to ensure that things initialize correctly (remember, from() tweens go backwards)
			if (this._startAt) {
				this._startAt.render(-1, true);
				this._startAt.kill();
				this._startAt = null;
			} else {
				if (this._time !== 0) {
					//in rare cases (like if a from() tween runs and then is invalidate()-ed), immediateRender could be true but the initial forced-render gets skipped, so there's no need to force the render in this context when the _time is greater than 0
					immediate = false;
				}
				pt = {};
				for (p in v) {
					//copy props into a new object and skip any reserved props, otherwise onComplete or onUpdate or onStart could fire. We should, however, permit autoCSS to go through.
					if (!_reservedProps[p] || p === "autoCSS") {
						pt[p] = v[p];
					}
				}
				pt.overwrite = 0;
				pt.data = "isFromStart"; //we tag the tween with as "isFromStart" so that if [inside a plugin] we need to only do something at the very END of a tween, we have a way of identifying this tween as merely the one that's setting the beginning values for a "from()" tween. For example, clearProps in CSSPlugin should only get applied at the very END of a tween and without this tag, from(...{height:100, clearProps:"height", delay:1}) would wipe the height at the beginning of the tween and after 1 second, it'd kick back in.
				pt.lazy = immediate && v.lazy !== false;
				pt.immediateRender = immediate; //zero-duration tweens render immediately by default, but if we're not specifically instructed to render this tween immediately, we should skip this and merely _init() to record the starting values (rendering them immediately would push them to completion which is wasteful in that case - we'd have to render(-1) immediately after)
				this._startAt = TweenLite.to(this.target, 0, pt);
				if (!immediate) {
					this._startAt._init(); //ensures that the initial values are recorded
					this._startAt._enabled(false); //no need to have the tween render on the next cycle. Disable it because we'll always manually control the renders of the _startAt tween.
					if (this.vars.immediateRender) {
						this._startAt = null;
					}
				} else if (this._time === 0) {
					return;
				}
			}
		}
		this._ease = ease = !ease ? TweenLite.defaultEase : ease instanceof Ease ? ease : typeof ease === "function" ? new Ease(ease, v.easeParams) : _easeMap[ease] || TweenLite.defaultEase;
		if (v.easeParams instanceof Array && ease.config) {
			this._ease = ease.config.apply(ease, v.easeParams);
		}
		this._easeType = this._ease._type;
		this._easePower = this._ease._power;
		this._firstPT = null;

		if (this._targets) {
			l = this._targets.length;
			for (i = 0; i < l; i++) {
				if (this._initProps(this._targets[i], this._propLookup[i] = {}, this._siblings[i], op ? op[i] : null, i)) {
					initPlugins = true;
				}
			}
		} else {
			initPlugins = this._initProps(this.target, this._propLookup, this._siblings, op, 0);
		}

		if (initPlugins) {
			TweenLite._onPluginEvent("_onInitAllProps", this); //reorders the array in order of priority. Uses a static TweenPlugin method in order to minimize file size in TweenLite
		}
		if (op) if (!this._firstPT) if (typeof this.target !== "function") {
			//if all tweening properties have been overwritten, kill the tween. If the target is a function, it's probably a delayedCall so let it live.
			this._enabled(false, false);
		}
		if (v.runBackwards) {
			pt = this._firstPT;
			while (pt) {
				pt.s += pt.c;
				pt.c = -pt.c;
				pt = pt._next;
			}
		}
		this._onUpdate = v.onUpdate;
		this._initted = true;
	};

	p._initProps = function (target, propLookup, siblings, overwrittenProps, index) {
		var p, i, initPlugins, plugin, pt, v;
		if (target == null) {
			return false;
		}

		if (_lazyLookup[target._gsTweenID]) {
			_lazyRender(); //if other tweens of the same target have recently initted but haven't rendered yet, we've got to force the render so that the starting values are correct (imagine populating a timeline with a bunch of sequential tweens and then jumping to the end)
		}

		if (!this.vars.css) if (target.style) if (target !== window && target.nodeType) if (_plugins.css) if (this.vars.autoCSS !== false) {
			//it's so common to use TweenLite/Max to animate the css of DOM elements, we assume that if the target is a DOM element, that's what is intended (a convenience so that users don't have to wrap things in css:{}, although we still recommend it for a slight performance boost and better specificity). Note: we cannot check "nodeType" on the window inside an iframe.
			_autoCSS(this.vars, target);
		}
		for (p in this.vars) {
			v = this.vars[p];
			if (_reservedProps[p]) {
				if (v) if (v instanceof Array || v.push && _isArray(v)) if (v.join("").indexOf("{self}") !== -1) {
					this.vars[p] = v = this._swapSelfInParams(v, this);
				}
			} else if (_plugins[p] && (plugin = new _plugins[p]())._onInitTween(target, this.vars[p], this, index)) {

				//t - target 		[object]
				//p - property 		[string]
				//s - start			[number]
				//c - change		[number]
				//f - isFunction	[boolean]
				//n - name			[string]
				//pg - isPlugin 	[boolean]
				//pr - priority		[number]
				//m - mod           [function | 0]
				this._firstPT = pt = { _next: this._firstPT, t: plugin, p: "setRatio", s: 0, c: 1, f: 1, n: p, pg: 1, pr: plugin._priority, m: 0 };
				i = plugin._overwriteProps.length;
				while (--i > -1) {
					propLookup[plugin._overwriteProps[i]] = this._firstPT;
				}
				if (plugin._priority || plugin._onInitAllProps) {
					initPlugins = true;
				}
				if (plugin._onDisable || plugin._onEnable) {
					this._notifyPluginsOfEnabled = true;
				}
				if (pt._next) {
					pt._next._prev = pt;
				}
			} else {
				propLookup[p] = _addPropTween.call(this, target, p, "get", v, p, 0, null, this.vars.stringFilter, index);
			}
		}

		if (overwrittenProps) if (this._kill(overwrittenProps, target)) {
			//another tween may have tried to overwrite properties of this tween before init() was called (like if two tweens start at the same time, the one created second will run first)
			return this._initProps(target, propLookup, siblings, overwrittenProps, index);
		}
		if (this._overwrite > 1) if (this._firstPT) if (siblings.length > 1) if (_applyOverwrite(target, this, propLookup, this._overwrite, siblings)) {
			this._kill(propLookup, target);
			return this._initProps(target, propLookup, siblings, overwrittenProps, index);
		}
		if (this._firstPT) if (this.vars.lazy !== false && this._duration || this.vars.lazy && !this._duration) {
			//zero duration tweens don't lazy render by default; everything else does.
			_lazyLookup[target._gsTweenID] = true;
		}
		return initPlugins;
	};

	p.render = function (time, suppressEvents, force) {
		var prevTime = this._time,
		    duration = this._duration,
		    prevRawPrevTime = this._rawPrevTime,
		    isComplete,
		    callback,
		    pt,
		    rawPrevTime;
		if (time >= duration - 0.0000001 && time >= 0) {
			//to work around occasional floating point math artifacts.
			this._totalTime = this._time = duration;
			this.ratio = this._ease._calcEnd ? this._ease.getRatio(1) : 1;
			if (!this._reversed) {
				isComplete = true;
				callback = "onComplete";
				force = force || this._timeline.autoRemoveChildren; //otherwise, if the animation is unpaused/activated after it's already finished, it doesn't get removed from the parent timeline.
			}
			if (duration === 0) if (this._initted || !this.vars.lazy || force) {
				//zero-duration tweens are tricky because we must discern the momentum/direction of time in order to determine whether the starting values should be rendered or the ending values. If the "playhead" of its timeline goes past the zero-duration tween in the forward direction or lands directly on it, the end values should be rendered, but if the timeline's "playhead" moves past it in the backward direction (from a postitive time to a negative time), the starting values must be rendered.
				if (this._startTime === this._timeline._duration) {
					//if a zero-duration tween is at the VERY end of a timeline and that timeline renders at its end, it will typically add a tiny bit of cushion to the render time to prevent rounding errors from getting in the way of tweens rendering their VERY end. If we then reverse() that timeline, the zero-duration tween will trigger its onReverseComplete even though technically the playhead didn't pass over it again. It's a very specific edge case we must accommodate.
					time = 0;
				}
				if (prevRawPrevTime < 0 || time <= 0 && time >= -0.0000001 || prevRawPrevTime === _tinyNum && this.data !== "isPause") if (prevRawPrevTime !== time) {
					//note: when this.data is "isPause", it's a callback added by addPause() on a timeline that we should not be triggered when LEAVING its exact start time. In other words, tl.addPause(1).play(1) shouldn't pause.
					force = true;
					if (prevRawPrevTime > _tinyNum) {
						callback = "onReverseComplete";
					}
				}
				this._rawPrevTime = rawPrevTime = !suppressEvents || time || prevRawPrevTime === time ? time : _tinyNum; //when the playhead arrives at EXACTLY time 0 (right on top) of a zero-duration tween, we need to discern if events are suppressed so that when the playhead moves again (next time), it'll trigger the callback. If events are NOT suppressed, obviously the callback would be triggered in this render. Basically, the callback should fire either when the playhead ARRIVES or LEAVES this exact spot, not both. Imagine doing a timeline.seek(0) and there's a callback that sits at 0. Since events are suppressed on that seek() by default, nothing will fire, but when the playhead moves off of that position, the callback should fire. This behavior is what people intuitively expect. We set the _rawPrevTime to be a precise tiny number to indicate this scenario rather than using another property/variable which would increase memory usage. This technique is less readable, but more efficient.
			}
		} else if (time < 0.0000001) {
			//to work around occasional floating point math artifacts, round super small values to 0.
			this._totalTime = this._time = 0;
			this.ratio = this._ease._calcEnd ? this._ease.getRatio(0) : 0;
			if (prevTime !== 0 || duration === 0 && prevRawPrevTime > 0) {
				callback = "onReverseComplete";
				isComplete = this._reversed;
			}
			if (time < 0) {
				this._active = false;
				if (duration === 0) if (this._initted || !this.vars.lazy || force) {
					//zero-duration tweens are tricky because we must discern the momentum/direction of time in order to determine whether the starting values should be rendered or the ending values. If the "playhead" of its timeline goes past the zero-duration tween in the forward direction or lands directly on it, the end values should be rendered, but if the timeline's "playhead" moves past it in the backward direction (from a postitive time to a negative time), the starting values must be rendered.
					if (prevRawPrevTime >= 0 && !(prevRawPrevTime === _tinyNum && this.data === "isPause")) {
						force = true;
					}
					this._rawPrevTime = rawPrevTime = !suppressEvents || time || prevRawPrevTime === time ? time : _tinyNum; //when the playhead arrives at EXACTLY time 0 (right on top) of a zero-duration tween, we need to discern if events are suppressed so that when the playhead moves again (next time), it'll trigger the callback. If events are NOT suppressed, obviously the callback would be triggered in this render. Basically, the callback should fire either when the playhead ARRIVES or LEAVES this exact spot, not both. Imagine doing a timeline.seek(0) and there's a callback that sits at 0. Since events are suppressed on that seek() by default, nothing will fire, but when the playhead moves off of that position, the callback should fire. This behavior is what people intuitively expect. We set the _rawPrevTime to be a precise tiny number to indicate this scenario rather than using another property/variable which would increase memory usage. This technique is less readable, but more efficient.
				}
			}
			if (!this._initted || this._startAt && this._startAt.progress()) {
				//if we render the very beginning (time == 0) of a fromTo(), we must force the render (normal tweens wouldn't need to render at a time of 0 when the prevTime was also 0). This is also mandatory to make sure overwriting kicks in immediately. Also, we check progress() because if startAt has already rendered at its end, we should force a render at its beginning. Otherwise, if you put the playhead directly on top of where a fromTo({immediateRender:false}) starts, and then move it backwards, the from() won't revert its values.
				force = true;
			}
		} else {
			this._totalTime = this._time = time;

			if (this._easeType) {
				var r = time / duration,
				    type = this._easeType,
				    pow = this._easePower;
				if (type === 1 || type === 3 && r >= 0.5) {
					r = 1 - r;
				}
				if (type === 3) {
					r *= 2;
				}
				if (pow === 1) {
					r *= r;
				} else if (pow === 2) {
					r *= r * r;
				} else if (pow === 3) {
					r *= r * r * r;
				} else if (pow === 4) {
					r *= r * r * r * r;
				}

				if (type === 1) {
					this.ratio = 1 - r;
				} else if (type === 2) {
					this.ratio = r;
				} else if (time / duration < 0.5) {
					this.ratio = r / 2;
				} else {
					this.ratio = 1 - r / 2;
				}
			} else {
				this.ratio = this._ease.getRatio(time / duration);
			}
		}

		if (this._time === prevTime && !force) {
			return;
		} else if (!this._initted) {
			this._init();
			if (!this._initted || this._gc) {
				//immediateRender tweens typically won't initialize until the playhead advances (_time is greater than 0) in order to ensure that overwriting occurs properly. Also, if all of the tweening properties have been overwritten (which would cause _gc to be true, as set in _init()), we shouldn't continue otherwise an onStart callback could be called for example.
				return;
			} else if (!force && this._firstPT && (this.vars.lazy !== false && this._duration || this.vars.lazy && !this._duration)) {
				this._time = this._totalTime = prevTime;
				this._rawPrevTime = prevRawPrevTime;
				_lazyTweens.push(this);
				this._lazy = [time, suppressEvents];
				return;
			}
			//_ease is initially set to defaultEase, so now that init() has run, _ease is set properly and we need to recalculate the ratio. Overall this is faster than using conditional logic earlier in the method to avoid having to set ratio twice because we only init() once but renderTime() gets called VERY frequently.
			if (this._time && !isComplete) {
				this.ratio = this._ease.getRatio(this._time / duration);
			} else if (isComplete && this._ease._calcEnd) {
				this.ratio = this._ease.getRatio(this._time === 0 ? 0 : 1);
			}
		}
		if (this._lazy !== false) {
			//in case a lazy render is pending, we should flush it because the new render is occurring now (imagine a lazy tween instantiating and then immediately the user calls tween.seek(tween.duration()), skipping to the end - the end render would be forced, and then if we didn't flush the lazy render, it'd fire AFTER the seek(), rendering it at the wrong time.
			this._lazy = false;
		}
		if (!this._active) if (!this._paused && this._time !== prevTime && time >= 0) {
			this._active = true; //so that if the user renders a tween (as opposed to the timeline rendering it), the timeline is forced to re-render and align it with the proper time/frame on the next rendering cycle. Maybe the tween already finished but the user manually re-renders it as halfway done.
		}
		if (prevTime === 0) {
			if (this._startAt) {
				if (time >= 0) {
					this._startAt.render(time, true, force);
				} else if (!callback) {
					callback = "_dummyGS"; //if no callback is defined, use a dummy value just so that the condition at the end evaluates as true because _startAt should render AFTER the normal render loop when the time is negative. We could handle this in a more intuitive way, of course, but the render loop is the MOST important thing to optimize, so this technique allows us to avoid adding extra conditional logic in a high-frequency area.
				}
			}
			if (this.vars.onStart) if (this._time !== 0 || duration === 0) if (!suppressEvents) {
				this._callback("onStart");
			}
		}
		pt = this._firstPT;
		while (pt) {
			if (pt.f) {
				pt.t[pt.p](pt.c * this.ratio + pt.s);
			} else {
				pt.t[pt.p] = pt.c * this.ratio + pt.s;
			}
			pt = pt._next;
		}

		if (this._onUpdate) {
			if (time < 0) if (this._startAt && time !== -0.0001) {
				//if the tween is positioned at the VERY beginning (_startTime 0) of its parent timeline, it's illegal for the playhead to go back further, so we should not render the recorded startAt values.
				this._startAt.render(time, true, force); //note: for performance reasons, we tuck this conditional logic inside less traveled areas (most tweens don't have an onUpdate). We'd just have it at the end before the onComplete, but the values should be updated before any onUpdate is called, so we ALSO put it here and then if it's not called, we do so later near the onComplete.
			}
			if (!suppressEvents) if (this._time !== prevTime || isComplete || force) {
				this._callback("onUpdate");
			}
		}
		if (callback) if (!this._gc || force) {
			//check _gc because there's a chance that kill() could be called in an onUpdate
			if (time < 0 && this._startAt && !this._onUpdate && time !== -0.0001) {
				//-0.0001 is a special value that we use when looping back to the beginning of a repeated TimelineMax, in which case we shouldn't render the _startAt values.
				this._startAt.render(time, true, force);
			}
			if (isComplete) {
				if (this._timeline.autoRemoveChildren) {
					this._enabled(false, false);
				}
				this._active = false;
			}
			if (!suppressEvents && this.vars[callback]) {
				this._callback(callback);
			}
			if (duration === 0 && this._rawPrevTime === _tinyNum && rawPrevTime !== _tinyNum) {
				//the onComplete or onReverseComplete could trigger movement of the playhead and for zero-duration tweens (which must discern direction) that land directly back on their start time, we don't want to fire again on the next render. Think of several addPause()'s in a timeline that forces the playhead to a certain spot, but what if it's already paused and another tween is tweening the "time" of the timeline? Each time it moves [forward] past that spot, it would move back, and since suppressEvents is true, it'd reset _rawPrevTime to _tinyNum so that when it begins again, the callback would fire (so ultimately it could bounce back and forth during that tween). Again, this is a very uncommon scenario, but possible nonetheless.
				this._rawPrevTime = 0;
			}
		}
	};

	p._kill = function (vars, target, overwritingTween) {
		if (vars === "all") {
			vars = null;
		}
		if (vars == null) if (target == null || target === this.target) {
			this._lazy = false;
			return this._enabled(false, false);
		}
		target = typeof target !== "string" ? target || this._targets || this.target : TweenLite.selector(target) || target;
		var simultaneousOverwrite = overwritingTween && this._time && overwritingTween._startTime === this._startTime && this._timeline === overwritingTween._timeline,
		    i,
		    overwrittenProps,
		    p,
		    pt,
		    propLookup,
		    changed,
		    killProps,
		    record,
		    killed;
		if ((_isArray(target) || _isSelector(target)) && typeof target[0] !== "number") {
			i = target.length;
			while (--i > -1) {
				if (this._kill(vars, target[i], overwritingTween)) {
					changed = true;
				}
			}
		} else {
			if (this._targets) {
				i = this._targets.length;
				while (--i > -1) {
					if (target === this._targets[i]) {
						propLookup = this._propLookup[i] || {};
						this._overwrittenProps = this._overwrittenProps || [];
						overwrittenProps = this._overwrittenProps[i] = vars ? this._overwrittenProps[i] || {} : "all";
						break;
					}
				}
			} else if (target !== this.target) {
				return false;
			} else {
				propLookup = this._propLookup;
				overwrittenProps = this._overwrittenProps = vars ? this._overwrittenProps || {} : "all";
			}

			if (propLookup) {
				killProps = vars || propLookup;
				record = vars !== overwrittenProps && overwrittenProps !== "all" && vars !== propLookup && ((typeof vars === "undefined" ? "undefined" : _typeof(vars)) !== "object" || !vars._tempKill); //_tempKill is a super-secret way to delete a particular tweening property but NOT have it remembered as an official overwritten property (like in BezierPlugin)
				if (overwritingTween && (TweenLite.onOverwrite || this.vars.onOverwrite)) {
					for (p in killProps) {
						if (propLookup[p]) {
							if (!killed) {
								killed = [];
							}
							killed.push(p);
						}
					}
					if ((killed || !vars) && !_onOverwrite(this, overwritingTween, target, killed)) {
						//if the onOverwrite returned false, that means the user wants to override the overwriting (cancel it).
						return false;
					}
				}

				for (p in killProps) {
					if (pt = propLookup[p]) {
						if (simultaneousOverwrite) {
							//if another tween overwrites this one and they both start at exactly the same time, yet this tween has already rendered once (for example, at 0.001) because it's first in the queue, we should revert the values to where they were at 0 so that the starting values aren't contaminated on the overwriting tween.
							if (pt.f) {
								pt.t[pt.p](pt.s);
							} else {
								pt.t[pt.p] = pt.s;
							}
							changed = true;
						}
						if (pt.pg && pt.t._kill(killProps)) {
							changed = true; //some plugins need to be notified so they can perform cleanup tasks first
						}
						if (!pt.pg || pt.t._overwriteProps.length === 0) {
							if (pt._prev) {
								pt._prev._next = pt._next;
							} else if (pt === this._firstPT) {
								this._firstPT = pt._next;
							}
							if (pt._next) {
								pt._next._prev = pt._prev;
							}
							pt._next = pt._prev = null;
						}
						delete propLookup[p];
					}
					if (record) {
						overwrittenProps[p] = 1;
					}
				}
				if (!this._firstPT && this._initted) {
					//if all tweening properties are killed, kill the tween. Without this line, if there's a tween with multiple targets and then you killTweensOf() each target individually, the tween would technically still remain active and fire its onComplete even though there aren't any more properties tweening.
					this._enabled(false, false);
				}
			}
		}
		return changed;
	};

	p.invalidate = function () {
		if (this._notifyPluginsOfEnabled) {
			TweenLite._onPluginEvent("_onDisable", this);
		}
		this._firstPT = this._overwrittenProps = this._startAt = this._onUpdate = null;
		this._notifyPluginsOfEnabled = this._active = this._lazy = false;
		this._propLookup = this._targets ? {} : [];
		Animation.prototype.invalidate.call(this);
		if (this.vars.immediateRender) {
			this._time = -_tinyNum; //forces a render without having to set the render() "force" parameter to true because we want to allow lazying by default (using the "force" parameter always forces an immediate full render)
			this.render(Math.min(0, -this._delay)); //in case delay is negative.
		}
		return this;
	};

	p._enabled = function (enabled, ignoreTimeline) {
		if (!_tickerActive) {
			_ticker.wake();
		}
		if (enabled && this._gc) {
			var targets = this._targets,
			    i;
			if (targets) {
				i = targets.length;
				while (--i > -1) {
					this._siblings[i] = _register(targets[i], this, true);
				}
			} else {
				this._siblings = _register(this.target, this, true);
			}
		}
		Animation.prototype._enabled.call(this, enabled, ignoreTimeline);
		if (this._notifyPluginsOfEnabled) if (this._firstPT) {
			return TweenLite._onPluginEvent(enabled ? "_onEnable" : "_onDisable", this);
		}
		return false;
	};

	//----TweenLite static methods -----------------------------------------------------

	TweenLite.to = function (target, duration, vars) {
		return new TweenLite(target, duration, vars);
	};

	TweenLite.from = function (target, duration, vars) {
		vars.runBackwards = true;
		vars.immediateRender = vars.immediateRender != false;
		return new TweenLite(target, duration, vars);
	};

	TweenLite.fromTo = function (target, duration, fromVars, toVars) {
		toVars.startAt = fromVars;
		toVars.immediateRender = toVars.immediateRender != false && fromVars.immediateRender != false;
		return new TweenLite(target, duration, toVars);
	};

	TweenLite.delayedCall = function (delay, callback, params, scope, useFrames) {
		return new TweenLite(callback, 0, { delay: delay, onComplete: callback, onCompleteParams: params, callbackScope: scope, onReverseComplete: callback, onReverseCompleteParams: params, immediateRender: false, lazy: false, useFrames: useFrames, overwrite: 0 });
	};

	TweenLite.set = function (target, vars) {
		return new TweenLite(target, 0, vars);
	};

	TweenLite.getTweensOf = function (target, onlyActive) {
		if (target == null) {
			return [];
		}
		target = typeof target !== "string" ? target : TweenLite.selector(target) || target;
		var i, a, j, t;
		if ((_isArray(target) || _isSelector(target)) && typeof target[0] !== "number") {
			i = target.length;
			a = [];
			while (--i > -1) {
				a = a.concat(TweenLite.getTweensOf(target[i], onlyActive));
			}
			i = a.length;
			//now get rid of any duplicates (tweens of arrays of objects could cause duplicates)
			while (--i > -1) {
				t = a[i];
				j = i;
				while (--j > -1) {
					if (t === a[j]) {
						a.splice(i, 1);
					}
				}
			}
		} else if (target._gsTweenID) {
			a = _register(target).concat();
			i = a.length;
			while (--i > -1) {
				if (a[i]._gc || onlyActive && !a[i].isActive()) {
					a.splice(i, 1);
				}
			}
		}
		return a || [];
	};

	TweenLite.killTweensOf = TweenLite.killDelayedCallsTo = function (target, onlyActive, vars) {
		if ((typeof onlyActive === "undefined" ? "undefined" : _typeof(onlyActive)) === "object") {
			vars = onlyActive; //for backwards compatibility (before "onlyActive" parameter was inserted)
			onlyActive = false;
		}
		var a = TweenLite.getTweensOf(target, onlyActive),
		    i = a.length;
		while (--i > -1) {
			a[i]._kill(vars, target);
		}
	};

	/*
  * ----------------------------------------------------------------
  * TweenPlugin   (could easily be split out as a separate file/class, but included for ease of use (so that people don't need to include another script call before loading plugins which is easy to forget)
  * ----------------------------------------------------------------
  */
	var TweenPlugin = _class("plugins.TweenPlugin", function (props, priority) {
		this._overwriteProps = (props || "").split(",");
		this._propName = this._overwriteProps[0];
		this._priority = priority || 0;
		this._super = TweenPlugin.prototype;
	}, true);

	p = TweenPlugin.prototype;
	TweenPlugin.version = "1.19.0";
	TweenPlugin.API = 2;
	p._firstPT = null;
	p._addTween = _addPropTween;
	p.setRatio = _setRatio;

	p._kill = function (lookup) {
		var a = this._overwriteProps,
		    pt = this._firstPT,
		    i;
		if (lookup[this._propName] != null) {
			this._overwriteProps = [];
		} else {
			i = a.length;
			while (--i > -1) {
				if (lookup[a[i]] != null) {
					a.splice(i, 1);
				}
			}
		}
		while (pt) {
			if (lookup[pt.n] != null) {
				if (pt._next) {
					pt._next._prev = pt._prev;
				}
				if (pt._prev) {
					pt._prev._next = pt._next;
					pt._prev = null;
				} else if (this._firstPT === pt) {
					this._firstPT = pt._next;
				}
			}
			pt = pt._next;
		}
		return false;
	};

	p._mod = p._roundProps = function (lookup) {
		var pt = this._firstPT,
		    val;
		while (pt) {
			val = lookup[this._propName] || pt.n != null && lookup[pt.n.split(this._propName + "_").join("")];
			if (val && typeof val === "function") {
				//some properties that are very plugin-specific add a prefix named after the _propName plus an underscore, so we need to ignore that extra stuff here.
				if (pt.f === 2) {
					pt.t._applyPT.m = val;
				} else {
					pt.m = val;
				}
			}
			pt = pt._next;
		}
	};

	TweenLite._onPluginEvent = function (type, tween) {
		var pt = tween._firstPT,
		    changed,
		    pt2,
		    first,
		    last,
		    next;
		if (type === "_onInitAllProps") {
			//sorts the PropTween linked list in order of priority because some plugins need to render earlier/later than others, like MotionBlurPlugin applies its effects after all x/y/alpha tweens have rendered on each frame.
			while (pt) {
				next = pt._next;
				pt2 = first;
				while (pt2 && pt2.pr > pt.pr) {
					pt2 = pt2._next;
				}
				if (pt._prev = pt2 ? pt2._prev : last) {
					pt._prev._next = pt;
				} else {
					first = pt;
				}
				if (pt._next = pt2) {
					pt2._prev = pt;
				} else {
					last = pt;
				}
				pt = next;
			}
			pt = tween._firstPT = first;
		}
		while (pt) {
			if (pt.pg) if (typeof pt.t[type] === "function") if (pt.t[type]()) {
				changed = true;
			}
			pt = pt._next;
		}
		return changed;
	};

	TweenPlugin.activate = function (plugins) {
		var i = plugins.length;
		while (--i > -1) {
			if (plugins[i].API === TweenPlugin.API) {
				_plugins[new plugins[i]()._propName] = plugins[i];
			}
		}
		return true;
	};

	//provides a more concise way to define plugins that have no dependencies besides TweenPlugin and TweenLite, wrapping common boilerplate stuff into one function (added in 1.9.0). You don't NEED to use this to define a plugin - the old way still works and can be useful in certain (rare) situations.
	_gsDefine.plugin = function (config) {
		if (!config || !config.propName || !config.init || !config.API) {
			throw "illegal plugin definition.";
		}
		var propName = config.propName,
		    priority = config.priority || 0,
		    overwriteProps = config.overwriteProps,
		    map = { init: "_onInitTween", set: "setRatio", kill: "_kill", round: "_mod", mod: "_mod", initAll: "_onInitAllProps" },
		    Plugin = _class("plugins." + propName.charAt(0).toUpperCase() + propName.substr(1) + "Plugin", function () {
			TweenPlugin.call(this, propName, priority);
			this._overwriteProps = overwriteProps || [];
		}, config.global === true),
		    p = Plugin.prototype = new TweenPlugin(propName),
		    prop;
		p.constructor = Plugin;
		Plugin.API = config.API;
		for (prop in map) {
			if (typeof config[prop] === "function") {
				p[map[prop]] = config[prop];
			}
		}
		Plugin.version = config.version;
		TweenPlugin.activate([Plugin]);
		return Plugin;
	};

	//now run through all the dependencies discovered and if any are missing, log that to the console as a warning. This is why it's best to have TweenLite load last - it can check all the dependencies for you.
	a = window._gsQueue;
	if (a) {
		for (i = 0; i < a.length; i++) {
			a[i]();
		}
		for (p in _defLookup) {
			if (!_defLookup[p].func) {
				window.console.log("GSAP encountered missing dependency: " + p);
			}
		}
	}

	_tickerActive = false; //ensures that the first official animation forces a ticker.tick() to update the time when it is instantiated
})(typeof module !== "undefined" && module.exports && typeof global !== "undefined" ? global : undefined || window, "TweenLite");
/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(2)))

/***/ }),
/* 1 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.default = undefined;

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _dom = __webpack_require__(55);

function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { step("next", value); }, function (err) { step("throw", err); }); } } return step("next"); }); }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Component = function () {
    function Component(element) {
        _classCallCheck(this, Component);

        this.element = element;
        this.element['__giant_component__'] = this;
        this._ref = {};
        this.ref = {};
    }

    _createClass(Component, [{
        key: 'require',
        value: function () {
            var _ref = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee() {
                return regeneratorRuntime.wrap(function _callee$(_context) {
                    while (1) {
                        switch (_context.prev = _context.next) {
                            case 0:
                            case 'end':
                                return _context.stop();
                        }
                    }
                }, _callee, this);
            }));

            function require() {
                return _ref.apply(this, arguments);
            }

            return require;
        }()
    }, {
        key: '_load',
        value: function _load() {
            this.require().then(this.prepare.bind(this));
        }
    }, {
        key: 'prepare',
        value: function prepare() {
            console.warn('Component ' + this._name + ' does not have "prepare" method.');
        }
    }, {
        key: 'destroy',
        value: function destroy() {
            //console.warn('Destroy method: override me');
        }
    }, {
        key: 'is',
        value: function is(state) {
            return this.element.classList.contains(state);
        }
    }, {
        key: 'getElement',
        value: function getElement() {
            return this.element;
        }
    }, {
        key: 'getRef',
        value: function getRef(ref) {
            var prefixed = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : false;

            return '[data-ref="' + (prefixed ? this._name + ':' : '') + ref + '"]';
        }
    }, {
        key: '_getRefElements',
        value: function _getRefElements() {
            var _this = this;

            var items = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;

            if (items == null) {
                items = {};

                this.element.querySelectorAll('[data-ref]').forEach(function (item) {
                    var name = item.getAttribute('data-ref');
                    var multiple = false;

                    if (items[name] != null) {
                        return true;
                    }

                    if (name.includes('[]')) {
                        multiple = true;
                        name = name.replace('[]', '');
                    }

                    if (name.split(':').length > 1) {
                        if (name.split(':')[0] == _this._name) {
                            if (multiple) {
                                items[name.split(':')[1]] = [];
                            } else {
                                items[name.split(':')[1]] = null;
                            }
                        }
                    } else {
                        if (multiple) {
                            items[name] = [];
                        } else {
                            items[name] = null;
                        }
                    }
                });
            }

            Object.keys(items).forEach(function (key) {
                if (Array.isArray(items[key])) {
                    var elements = (0, _dom.queryAll)(_this.getRef(key + '[]', true), _this.element);
                    if (elements.length === 0) {
                        elements = (0, _dom.queryAll)(_this.getRef(key.slice(0, -1), true), _this.element);
                        if (elements.length === 0) {
                            elements = (0, _dom.queryAll)(_this.getRef(key + '[]'), _this.element);
                            if (elements.length === 0) {
                                elements = (0, _dom.queryAll)(_this.getRef(key.slice(0, -1)), _this.element);
                            }
                        }
                    }
                    _this._ref[key] = elements;
                } else if (!items[key]) {
                    var element = (0, _dom.query)(_this.getRef(key, true), _this.element);
                    if (!element) {
                        element = (0, _dom.query)(_this.getRef(key), _this.element);
                    }
                    _this._ref[key] = element;
                } else {
                    _this._ref[key] = items[key];
                }
            });

            return this._ref;
        }
    }, {
        key: 'ref',
        get: function get() {
            return this._ref;
        },
        set: function set(items) {
            if (Object.keys(items).length == 0) {
                this._ref = this._getRefElements();
            } else {
                this._ref = {};
                this._ref = this._getRefElements(items);
            }

            return this._ref;
        }
    }], [{
        key: 'getFromElement',
        value: function getFromElement(element) {
            return element['__giant_component__'];
        }
    }]);

    return Component;
}();

exports.default = Component;

/***/ }),
/* 2 */
/***/ (function(module, exports) {

var g;

// This works in non-strict mode
g = (function() {
	return this;
})();

try {
	// This works if eval is allowed (see CSP)
	g = g || Function("return this")() || (1,eval)("this");
} catch(e) {
	// This works if the window reference is available
	if(typeof window === "object")
		g = window;
}

// g can still be undefined, but nothing to do about it...
// We return undefined, instead of nothing here, so it's
// easier to handle this case. if(!global) { ...}

module.exports = g;


/***/ }),
/* 3 */
/***/ (function(module, exports, __webpack_require__) {

var debounce = __webpack_require__(60),
    isObject = __webpack_require__(5);

/** Error message constants. */
var FUNC_ERROR_TEXT = 'Expected a function';

/**
 * Creates a throttled function that only invokes `func` at most once per
 * every `wait` milliseconds. The throttled function comes with a `cancel`
 * method to cancel delayed `func` invocations and a `flush` method to
 * immediately invoke them. Provide `options` to indicate whether `func`
 * should be invoked on the leading and/or trailing edge of the `wait`
 * timeout. The `func` is invoked with the last arguments provided to the
 * throttled function. Subsequent calls to the throttled function return the
 * result of the last `func` invocation.
 *
 * **Note:** If `leading` and `trailing` options are `true`, `func` is
 * invoked on the trailing edge of the timeout only if the throttled function
 * is invoked more than once during the `wait` timeout.
 *
 * If `wait` is `0` and `leading` is `false`, `func` invocation is deferred
 * until to the next tick, similar to `setTimeout` with a timeout of `0`.
 *
 * See [David Corbacho's article](https://css-tricks.com/debouncing-throttling-explained-examples/)
 * for details over the differences between `_.throttle` and `_.debounce`.
 *
 * @static
 * @memberOf _
 * @since 0.1.0
 * @category Function
 * @param {Function} func The function to throttle.
 * @param {number} [wait=0] The number of milliseconds to throttle invocations to.
 * @param {Object} [options={}] The options object.
 * @param {boolean} [options.leading=true]
 *  Specify invoking on the leading edge of the timeout.
 * @param {boolean} [options.trailing=true]
 *  Specify invoking on the trailing edge of the timeout.
 * @returns {Function} Returns the new throttled function.
 * @example
 *
 * // Avoid excessively updating the position while scrolling.
 * jQuery(window).on('scroll', _.throttle(updatePosition, 100));
 *
 * // Invoke `renewToken` when the click event is fired, but not more than once every 5 minutes.
 * var throttled = _.throttle(renewToken, 300000, { 'trailing': false });
 * jQuery(element).on('click', throttled);
 *
 * // Cancel the trailing throttled invocation.
 * jQuery(window).on('popstate', throttled.cancel);
 */
function throttle(func, wait, options) {
  var leading = true,
      trailing = true;

  if (typeof func != 'function') {
    throw new TypeError(FUNC_ERROR_TEXT);
  }
  if (isObject(options)) {
    leading = 'leading' in options ? !!options.leading : leading;
    trailing = 'trailing' in options ? !!options.trailing : trailing;
  }
  return debounce(func, wait, {
    'leading': leading,
    'maxWait': wait,
    'trailing': trailing
  });
}

module.exports = throttle;


/***/ }),
/* 4 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/* WEBPACK VAR INJECTION */(function(global) {var __WEBPACK_AMD_DEFINE_FACTORY__, __WEBPACK_AMD_DEFINE_ARRAY__, __WEBPACK_AMD_DEFINE_RESULT__;

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

/*!
 * VERSION: 1.20.3
 * DATE: 2017-10-02
 * UPDATES AND DOCS AT: http://greensock.com
 *
 * @license Copyright (c) 2008-2017, GreenSock. All rights reserved.
 * This work is subject to the terms at http://greensock.com/standard-license or for
 * Club GreenSock members, the software agreement that was issued with your membership.
 * 
 * @author: Jack Doyle, jack@greensock.com
 */
var _gsScope = typeof module !== "undefined" && module.exports && typeof global !== "undefined" ? global : undefined || window; //helps ensure compatibility with AMD/RequireJS and CommonJS/Node
(_gsScope._gsQueue || (_gsScope._gsQueue = [])).push(function () {

	"use strict";

	_gsScope._gsDefine("plugins.CSSPlugin", ["plugins.TweenPlugin", "TweenLite"], function (TweenPlugin, TweenLite) {

		/** @constructor **/
		var CSSPlugin = function CSSPlugin() {
			TweenPlugin.call(this, "css");
			this._overwriteProps.length = 0;
			this.setRatio = CSSPlugin.prototype.setRatio; //speed optimization (avoid prototype lookup on this "hot" method)
		},
		    _globals = _gsScope._gsDefine.globals,
		    _hasPriority,
		    //turns true whenever a CSSPropTween instance is created that has a priority other than 0. This helps us discern whether or not we should spend the time organizing the linked list or not after a CSSPlugin's _onInitTween() method is called.
		_suffixMap,
		    //we set this in _onInitTween() each time as a way to have a persistent variable we can use in other methods like _parse() without having to pass it around as a parameter and we keep _parse() decoupled from a particular CSSPlugin instance
		_cs,
		    //computed style (we store this in a shared variable to conserve memory and make minification tighter
		_overwriteProps,
		    //alias to the currently instantiating CSSPlugin's _overwriteProps array. We use this closure in order to avoid having to pass a reference around from method to method and aid in minification.
		_specialProps = {},
		    p = CSSPlugin.prototype = new TweenPlugin("css");

		p.constructor = CSSPlugin;
		CSSPlugin.version = "1.20.3";
		CSSPlugin.API = 2;
		CSSPlugin.defaultTransformPerspective = 0;
		CSSPlugin.defaultSkewType = "compensated";
		CSSPlugin.defaultSmoothOrigin = true;
		p = "px"; //we'll reuse the "p" variable to keep file size down
		CSSPlugin.suffixMap = { top: p, right: p, bottom: p, left: p, width: p, height: p, fontSize: p, padding: p, margin: p, perspective: p, lineHeight: "" };

		var _numExp = /(?:\-|\.|\b)(\d|\.|e\-)+/g,
		    _relNumExp = /(?:\d|\-\d|\.\d|\-\.\d|\+=\d|\-=\d|\+=.\d|\-=\.\d)+/g,
		    _valuesExp = /(?:\+=|\-=|\-|\b)[\d\-\.]+[a-zA-Z0-9]*(?:%|\b)/gi,
		    //finds all the values that begin with numbers or += or -= and then a number. Includes suffixes. We use this to split complex values apart like "1px 5px 20px rgb(255,102,51)"
		_NaNExp = /(?![+-]?\d*\.?\d+|[+-]|e[+-]\d+)[^0-9]/g,
		    //also allows scientific notation and doesn't kill the leading -/+ in -= and +=
		_suffixExp = /(?:\d|\-|\+|=|#|\.)*/g,
		    _opacityExp = /opacity *= *([^)]*)/i,
		    _opacityValExp = /opacity:([^;]*)/i,
		    _alphaFilterExp = /alpha\(opacity *=.+?\)/i,
		    _rgbhslExp = /^(rgb|hsl)/,
		    _capsExp = /([A-Z])/g,
		    _camelExp = /-([a-z])/gi,
		    _urlExp = /(^(?:url\(\"|url\())|(?:(\"\))$|\)$)/gi,
		    //for pulling out urls from url(...) or url("...") strings (some browsers wrap urls in quotes, some don't when reporting things like backgroundImage)
		_camelFunc = function _camelFunc(s, g) {
			return g.toUpperCase();
		},
		    _horizExp = /(?:Left|Right|Width)/i,
		    _ieGetMatrixExp = /(M11|M12|M21|M22)=[\d\-\.e]+/gi,
		    _ieSetMatrixExp = /progid\:DXImageTransform\.Microsoft\.Matrix\(.+?\)/i,
		    _commasOutsideParenExp = /,(?=[^\)]*(?:\(|$))/gi,
		    //finds any commas that are not within parenthesis
		_complexExp = /[\s,\(]/i,
		    //for testing a string to find if it has a space, comma, or open parenthesis (clues that it's a complex value)
		_DEG2RAD = Math.PI / 180,
		    _RAD2DEG = 180 / Math.PI,
		    _forcePT = {},
		    _dummyElement = { style: {} },
		    _doc = _gsScope.document || { createElement: function createElement() {
				return _dummyElement;
			} },
		    _createElement = function _createElement(type, ns) {
			return _doc.createElementNS ? _doc.createElementNS(ns || "http://www.w3.org/1999/xhtml", type) : _doc.createElement(type);
		},
		    _tempDiv = _createElement("div"),
		    _tempImg = _createElement("img"),
		    _internals = CSSPlugin._internals = { _specialProps: _specialProps },
		    //provides a hook to a few internal methods that we need to access from inside other plugins
		_agent = (_gsScope.navigator || {}).userAgent || "",
		    _autoRound,
		    _reqSafariFix,
		    //we won't apply the Safari transform fix until we actually come across a tween that affects a transform property (to maintain best performance).

		_isSafari,
		    _isFirefox,
		    //Firefox has a bug that causes 3D transformed elements to randomly disappear unless a repaint is forced after each update on each element.
		_isSafariLT6,
		    //Safari (and Android 4 which uses a flavor of Safari) has a bug that prevents changes to "top" and "left" properties from rendering properly if changed on the same frame as a transform UNLESS we set the element's WebkitBackfaceVisibility to hidden (weird, I know). Doing this for Android 3 and earlier seems to actually cause other problems, though (fun!)
		_ieVers,
		    _supportsOpacity = function () {
			//we set _isSafari, _ieVers, _isFirefox, and _supportsOpacity all in one function here to reduce file size slightly, especially in the minified version.
			var i = _agent.indexOf("Android"),
			    a = _createElement("a");
			_isSafari = _agent.indexOf("Safari") !== -1 && _agent.indexOf("Chrome") === -1 && (i === -1 || parseFloat(_agent.substr(i + 8, 2)) > 3);
			_isSafariLT6 = _isSafari && parseFloat(_agent.substr(_agent.indexOf("Version/") + 8, 2)) < 6;
			_isFirefox = _agent.indexOf("Firefox") !== -1;
			if (/MSIE ([0-9]{1,}[\.0-9]{0,})/.exec(_agent) || /Trident\/.*rv:([0-9]{1,}[\.0-9]{0,})/.exec(_agent)) {
				_ieVers = parseFloat(RegExp.$1);
			}
			if (!a) {
				return false;
			}
			a.style.cssText = "top:1px;opacity:.55;";
			return (/^0.55/.test(a.style.opacity)
			);
		}(),
		    _getIEOpacity = function _getIEOpacity(v) {
			return _opacityExp.test(typeof v === "string" ? v : (v.currentStyle ? v.currentStyle.filter : v.style.filter) || "") ? parseFloat(RegExp.$1) / 100 : 1;
		},
		    _log = function _log(s) {
			//for logging messages, but in a way that won't throw errors in old versions of IE.
			if (_gsScope.console) {
				console.log(s);
			}
		},
		    _target,
		    //when initting a CSSPlugin, we set this variable so that we can access it from within many other functions without having to pass it around as params
		_index,
		    //when initting a CSSPlugin, we set this variable so that we can access it from within many other functions without having to pass it around as params

		_prefixCSS = "",
		    //the non-camelCase vendor prefix like "-o-", "-moz-", "-ms-", or "-webkit-"
		_prefix = "",
		    //camelCase vendor prefix like "O", "ms", "Webkit", or "Moz".

		// @private feed in a camelCase property name like "transform" and it will check to see if it is valid as-is or if it needs a vendor prefix. It returns the corrected camelCase property name (i.e. "WebkitTransform" or "MozTransform" or "transform" or null if no such property is found, like if the browser is IE8 or before, "transform" won't be found at all)
		_checkPropPrefix = function _checkPropPrefix(p, e) {
			e = e || _tempDiv;
			var s = e.style,
			    a,
			    i;
			if (s[p] !== undefined) {
				return p;
			}
			p = p.charAt(0).toUpperCase() + p.substr(1);
			a = ["O", "Moz", "ms", "Ms", "Webkit"];
			i = 5;
			while (--i > -1 && s[a[i] + p] === undefined) {}
			if (i >= 0) {
				_prefix = i === 3 ? "ms" : a[i];
				_prefixCSS = "-" + _prefix.toLowerCase() + "-";
				return _prefix + p;
			}
			return null;
		},
		    _getComputedStyle = _doc.defaultView ? _doc.defaultView.getComputedStyle : function () {},


		/**
   * @private Returns the css style for a particular property of an element. For example, to get whatever the current "left" css value for an element with an ID of "myElement", you could do:
   * var currentLeft = CSSPlugin.getStyle( document.getElementById("myElement"), "left");
   *
   * @param {!Object} t Target element whose style property you want to query
   * @param {!string} p Property name (like "left" or "top" or "marginTop", etc.)
   * @param {Object=} cs Computed style object. This just provides a way to speed processing if you're going to get several properties on the same element in quick succession - you can reuse the result of the getComputedStyle() call.
   * @param {boolean=} calc If true, the value will not be read directly from the element's "style" property (if it exists there), but instead the getComputedStyle() result will be used. This can be useful when you want to ensure that the browser itself is interpreting the value.
   * @param {string=} dflt Default value that should be returned in the place of null, "none", "auto" or "auto auto".
   * @return {?string} The current property value
   */
		_getStyle = CSSPlugin.getStyle = function (t, p, cs, calc, dflt) {
			var rv;
			if (!_supportsOpacity) if (p === "opacity") {
				//several versions of IE don't use the standard "opacity" property - they use things like filter:alpha(opacity=50), so we parse that here.
				return _getIEOpacity(t);
			}
			if (!calc && t.style[p]) {
				rv = t.style[p];
			} else if (cs = cs || _getComputedStyle(t)) {
				rv = cs[p] || cs.getPropertyValue(p) || cs.getPropertyValue(p.replace(_capsExp, "-$1").toLowerCase());
			} else if (t.currentStyle) {
				rv = t.currentStyle[p];
			}
			return dflt != null && (!rv || rv === "none" || rv === "auto" || rv === "auto auto") ? dflt : rv;
		},


		/**
   * @private Pass the target element, the property name, the numeric value, and the suffix (like "%", "em", "px", etc.) and it will spit back the equivalent pixel number.
   * @param {!Object} t Target element
   * @param {!string} p Property name (like "left", "top", "marginLeft", etc.)
   * @param {!number} v Value
   * @param {string=} sfx Suffix (like "px" or "%" or "em")
   * @param {boolean=} recurse If true, the call is a recursive one. In some browsers (like IE7/8), occasionally the value isn't accurately reported initially, but if we run the function again it will take effect.
   * @return {number} value in pixels
   */
		_convertToPixels = _internals.convertToPixels = function (t, p, v, sfx, recurse) {
			if (sfx === "px" || !sfx && p !== "lineHeight") {
				return v;
			}
			if (sfx === "auto" || !v) {
				return 0;
			}
			var horiz = _horizExp.test(p),
			    node = t,
			    style = _tempDiv.style,
			    neg = v < 0,
			    precise = v === 1,
			    pix,
			    cache,
			    time;
			if (neg) {
				v = -v;
			}
			if (precise) {
				v *= 100;
			}
			if (p === "lineHeight" && !sfx) {
				//special case of when a simple lineHeight (without a unit) is used. Set it to the value, read back the computed value, and then revert.
				cache = _getComputedStyle(t).lineHeight;
				t.style.lineHeight = v;
				pix = parseFloat(_getComputedStyle(t).lineHeight);
				t.style.lineHeight = cache;
			} else if (sfx === "%" && p.indexOf("border") !== -1) {
				pix = v / 100 * (horiz ? t.clientWidth : t.clientHeight);
			} else {
				style.cssText = "border:0 solid red;position:" + _getStyle(t, "position") + ";line-height:0;";
				if (sfx === "%" || !node.appendChild || sfx.charAt(0) === "v" || sfx === "rem") {
					node = t.parentNode || _doc.body;
					if (_getStyle(node, "display").indexOf("flex") !== -1) {
						//Edge and IE11 have a bug that causes offsetWidth to report as 0 if the container has display:flex and the child is position:relative. Switching to position: absolute solves it.
						style.position = "absolute";
					}
					cache = node._gsCache;
					time = TweenLite.ticker.frame;
					if (cache && horiz && cache.time === time) {
						//performance optimization: we record the width of elements along with the ticker frame so that we can quickly get it again on the same tick (seems relatively safe to assume it wouldn't change on the same tick)
						return cache.width * v / 100;
					}
					style[horiz ? "width" : "height"] = v + sfx;
				} else {
					style[horiz ? "borderLeftWidth" : "borderTopWidth"] = v + sfx;
				}
				node.appendChild(_tempDiv);
				pix = parseFloat(_tempDiv[horiz ? "offsetWidth" : "offsetHeight"]);
				node.removeChild(_tempDiv);
				if (horiz && sfx === "%" && CSSPlugin.cacheWidths !== false) {
					cache = node._gsCache = node._gsCache || {};
					cache.time = time;
					cache.width = pix / v * 100;
				}
				if (pix === 0 && !recurse) {
					pix = _convertToPixels(t, p, v, sfx, true);
				}
			}
			if (precise) {
				pix /= 100;
			}
			return neg ? -pix : pix;
		},
		    _calculateOffset = _internals.calculateOffset = function (t, p, cs) {
			//for figuring out "top" or "left" in px when it's "auto". We need to factor in margin with the offsetLeft/offsetTop
			if (_getStyle(t, "position", cs) !== "absolute") {
				return 0;
			}
			var dim = p === "left" ? "Left" : "Top",
			    v = _getStyle(t, "margin" + dim, cs);
			return t["offset" + dim] - (_convertToPixels(t, p, parseFloat(v), v.replace(_suffixExp, "")) || 0);
		},


		// @private returns at object containing ALL of the style properties in camelCase and their associated values.
		_getAllStyles = function _getAllStyles(t, cs) {
			var s = {},
			    i,
			    tr,
			    p;
			if (cs = cs || _getComputedStyle(t, null)) {
				if (i = cs.length) {
					while (--i > -1) {
						p = cs[i];
						if (p.indexOf("-transform") === -1 || _transformPropCSS === p) {
							//Some webkit browsers duplicate transform values, one non-prefixed and one prefixed ("transform" and "WebkitTransform"), so we must weed out the extra one here.
							s[p.replace(_camelExp, _camelFunc)] = cs.getPropertyValue(p);
						}
					}
				} else {
					//some browsers behave differently - cs.length is always 0, so we must do a for...in loop.
					for (i in cs) {
						if (i.indexOf("Transform") === -1 || _transformProp === i) {
							//Some webkit browsers duplicate transform values, one non-prefixed and one prefixed ("transform" and "WebkitTransform"), so we must weed out the extra one here.
							s[i] = cs[i];
						}
					}
				}
			} else if (cs = t.currentStyle || t.style) {
				for (i in cs) {
					if (typeof i === "string" && s[i] === undefined) {
						s[i.replace(_camelExp, _camelFunc)] = cs[i];
					}
				}
			}
			if (!_supportsOpacity) {
				s.opacity = _getIEOpacity(t);
			}
			tr = _getTransform(t, cs, false);
			s.rotation = tr.rotation;
			s.skewX = tr.skewX;
			s.scaleX = tr.scaleX;
			s.scaleY = tr.scaleY;
			s.x = tr.x;
			s.y = tr.y;
			if (_supports3D) {
				s.z = tr.z;
				s.rotationX = tr.rotationX;
				s.rotationY = tr.rotationY;
				s.scaleZ = tr.scaleZ;
			}
			if (s.filters) {
				delete s.filters;
			}
			return s;
		},


		// @private analyzes two style objects (as returned by _getAllStyles()) and only looks for differences between them that contain tweenable values (like a number or color). It returns an object with a "difs" property which refers to an object containing only those isolated properties and values for tweening, and a "firstMPT" property which refers to the first MiniPropTween instance in a linked list that recorded all the starting values of the different properties so that we can revert to them at the end or beginning of the tween - we don't want the cascading to get messed up. The forceLookup parameter is an optional generic object with properties that should be forced into the results - this is necessary for className tweens that are overwriting others because imagine a scenario where a rollover/rollout adds/removes a class and the user swipes the mouse over the target SUPER fast, thus nothing actually changed yet and the subsequent comparison of the properties would indicate they match (especially when px rounding is taken into consideration), thus no tweening is necessary even though it SHOULD tween and remove those properties after the tween (otherwise the inline styles will contaminate things). See the className SpecialProp code for details.
		_cssDif = function _cssDif(t, s1, s2, vars, forceLookup) {
			var difs = {},
			    style = t.style,
			    val,
			    p,
			    mpt;
			for (p in s2) {
				if (p !== "cssText") if (p !== "length") if (isNaN(p)) if (s1[p] !== (val = s2[p]) || forceLookup && forceLookup[p]) if (p.indexOf("Origin") === -1) if (typeof val === "number" || typeof val === "string") {
					difs[p] = val === "auto" && (p === "left" || p === "top") ? _calculateOffset(t, p) : (val === "" || val === "auto" || val === "none") && typeof s1[p] === "string" && s1[p].replace(_NaNExp, "") !== "" ? 0 : val; //if the ending value is defaulting ("" or "auto"), we check the starting value and if it can be parsed into a number (a string which could have a suffix too, like 700px), then we swap in 0 for "" or "auto" so that things actually tween.
					if (style[p] !== undefined) {
						//for className tweens, we must remember which properties already existed inline - the ones that didn't should be removed when the tween isn't in progress because they were only introduced to facilitate the transition between classes.
						mpt = new MiniPropTween(style, p, style[p], mpt);
					}
				}
			}
			if (vars) {
				for (p in vars) {
					//copy properties (except className)
					if (p !== "className") {
						difs[p] = vars[p];
					}
				}
			}
			return { difs: difs, firstMPT: mpt };
		},
		    _dimensions = { width: ["Left", "Right"], height: ["Top", "Bottom"] },
		    _margins = ["marginLeft", "marginRight", "marginTop", "marginBottom"],


		/**
   * @private Gets the width or height of an element
   * @param {!Object} t Target element
   * @param {!string} p Property name ("width" or "height")
   * @param {Object=} cs Computed style object (if one exists). Just a speed optimization.
   * @return {number} Dimension (in pixels)
   */
		_getDimension = function _getDimension(t, p, cs) {
			if ((t.nodeName + "").toLowerCase() === "svg") {
				//Chrome no longer supports offsetWidth/offsetHeight on SVG elements.
				return (cs || _getComputedStyle(t))[p] || 0;
			} else if (t.getCTM && _isSVG(t)) {
				return t.getBBox()[p] || 0;
			}
			var v = parseFloat(p === "width" ? t.offsetWidth : t.offsetHeight),
			    a = _dimensions[p],
			    i = a.length;
			cs = cs || _getComputedStyle(t, null);
			while (--i > -1) {
				v -= parseFloat(_getStyle(t, "padding" + a[i], cs, true)) || 0;
				v -= parseFloat(_getStyle(t, "border" + a[i] + "Width", cs, true)) || 0;
			}
			return v;
		},


		// @private Parses position-related complex strings like "top left" or "50px 10px" or "70% 20%", etc. which are used for things like transformOrigin or backgroundPosition. Optionally decorates a supplied object (recObj) with the following properties: "ox" (offsetX), "oy" (offsetY), "oxp" (if true, "ox" is a percentage not a pixel value), and "oxy" (if true, "oy" is a percentage not a pixel value)
		_parsePosition = function _parsePosition(v, recObj) {
			if (v === "contain" || v === "auto" || v === "auto auto") {
				//note: Firefox uses "auto auto" as default whereas Chrome uses "auto".
				return v + " ";
			}
			if (v == null || v === "") {
				v = "0 0";
			}
			var a = v.split(" "),
			    x = v.indexOf("left") !== -1 ? "0%" : v.indexOf("right") !== -1 ? "100%" : a[0],
			    y = v.indexOf("top") !== -1 ? "0%" : v.indexOf("bottom") !== -1 ? "100%" : a[1],
			    i;
			if (a.length > 3 && !recObj) {
				//multiple positions
				a = v.split(", ").join(",").split(",");
				v = [];
				for (i = 0; i < a.length; i++) {
					v.push(_parsePosition(a[i]));
				}
				return v.join(",");
			}
			if (y == null) {
				y = x === "center" ? "50%" : "0";
			} else if (y === "center") {
				y = "50%";
			}
			if (x === "center" || isNaN(parseFloat(x)) && (x + "").indexOf("=") === -1) {
				//remember, the user could flip-flop the values and say "bottom center" or "center bottom", etc. "center" is ambiguous because it could be used to describe horizontal or vertical, hence the isNaN(). If there's an "=" sign in the value, it's relative.
				x = "50%";
			}
			v = x + " " + y + (a.length > 2 ? " " + a[2] : "");
			if (recObj) {
				recObj.oxp = x.indexOf("%") !== -1;
				recObj.oyp = y.indexOf("%") !== -1;
				recObj.oxr = x.charAt(1) === "=";
				recObj.oyr = y.charAt(1) === "=";
				recObj.ox = parseFloat(x.replace(_NaNExp, ""));
				recObj.oy = parseFloat(y.replace(_NaNExp, ""));
				recObj.v = v;
			}
			return recObj || v;
		},


		/**
   * @private Takes an ending value (typically a string, but can be a number) and a starting value and returns the change between the two, looking for relative value indicators like += and -= and it also ignores suffixes (but make sure the ending value starts with a number or +=/-= and that the starting value is a NUMBER!)
   * @param {(number|string)} e End value which is typically a string, but could be a number
   * @param {(number|string)} b Beginning value which is typically a string but could be a number
   * @return {number} Amount of change between the beginning and ending values (relative values that have a "+=" or "-=" are recognized)
   */
		_parseChange = function _parseChange(e, b) {
			if (typeof e === "function") {
				e = e(_index, _target);
			}
			return typeof e === "string" && e.charAt(1) === "=" ? parseInt(e.charAt(0) + "1", 10) * parseFloat(e.substr(2)) : parseFloat(e) - parseFloat(b) || 0;
		},


		/**
   * @private Takes a value and a default number, checks if the value is relative, null, or numeric and spits back a normalized number accordingly. Primarily used in the _parseTransform() function.
   * @param {Object} v Value to be parsed
   * @param {!number} d Default value (which is also used for relative calculations if "+=" or "-=" is found in the first parameter)
   * @return {number} Parsed value
   */
		_parseVal = function _parseVal(v, d) {
			if (typeof v === "function") {
				v = v(_index, _target);
			}
			return v == null ? d : typeof v === "string" && v.charAt(1) === "=" ? parseInt(v.charAt(0) + "1", 10) * parseFloat(v.substr(2)) + d : parseFloat(v) || 0;
		},


		/**
   * @private Translates strings like "40deg" or "40" or 40rad" or "+=40deg" or "270_short" or "-90_cw" or "+=45_ccw" to a numeric radian angle. Of course a starting/default value must be fed in too so that relative values can be calculated properly.
   * @param {Object} v Value to be parsed
   * @param {!number} d Default value (which is also used for relative calculations if "+=" or "-=" is found in the first parameter)
   * @param {string=} p property name for directionalEnd (optional - only used when the parsed value is directional ("_short", "_cw", or "_ccw" suffix). We need a way to store the uncompensated value so that at the end of the tween, we set it to exactly what was requested with no directional compensation). Property name would be "rotation", "rotationX", or "rotationY"
   * @param {Object=} directionalEnd An object that will store the raw end values for directional angles ("_short", "_cw", or "_ccw" suffix). We need a way to store the uncompensated value so that at the end of the tween, we set it to exactly what was requested with no directional compensation.
   * @return {number} parsed angle in radians
   */
		_parseAngle = function _parseAngle(v, d, p, directionalEnd) {
			var min = 0.000001,
			    cap,
			    split,
			    dif,
			    result,
			    isRelative;
			if (typeof v === "function") {
				v = v(_index, _target);
			}
			if (v == null) {
				result = d;
			} else if (typeof v === "number") {
				result = v;
			} else {
				cap = 360;
				split = v.split("_");
				isRelative = v.charAt(1) === "=";
				dif = (isRelative ? parseInt(v.charAt(0) + "1", 10) * parseFloat(split[0].substr(2)) : parseFloat(split[0])) * (v.indexOf("rad") === -1 ? 1 : _RAD2DEG) - (isRelative ? 0 : d);
				if (split.length) {
					if (directionalEnd) {
						directionalEnd[p] = d + dif;
					}
					if (v.indexOf("short") !== -1) {
						dif = dif % cap;
						if (dif !== dif % (cap / 2)) {
							dif = dif < 0 ? dif + cap : dif - cap;
						}
					}
					if (v.indexOf("_cw") !== -1 && dif < 0) {
						dif = (dif + cap * 9999999999) % cap - (dif / cap | 0) * cap;
					} else if (v.indexOf("ccw") !== -1 && dif > 0) {
						dif = (dif - cap * 9999999999) % cap - (dif / cap | 0) * cap;
					}
				}
				result = d + dif;
			}
			if (result < min && result > -min) {
				result = 0;
			}
			return result;
		},
		    _colorLookup = { aqua: [0, 255, 255],
			lime: [0, 255, 0],
			silver: [192, 192, 192],
			black: [0, 0, 0],
			maroon: [128, 0, 0],
			teal: [0, 128, 128],
			blue: [0, 0, 255],
			navy: [0, 0, 128],
			white: [255, 255, 255],
			fuchsia: [255, 0, 255],
			olive: [128, 128, 0],
			yellow: [255, 255, 0],
			orange: [255, 165, 0],
			gray: [128, 128, 128],
			purple: [128, 0, 128],
			green: [0, 128, 0],
			red: [255, 0, 0],
			pink: [255, 192, 203],
			cyan: [0, 255, 255],
			transparent: [255, 255, 255, 0] },
		    _hue = function _hue(h, m1, m2) {
			h = h < 0 ? h + 1 : h > 1 ? h - 1 : h;
			return (h * 6 < 1 ? m1 + (m2 - m1) * h * 6 : h < 0.5 ? m2 : h * 3 < 2 ? m1 + (m2 - m1) * (2 / 3 - h) * 6 : m1) * 255 + 0.5 | 0;
		},


		/**
   * @private Parses a color (like #9F0, #FF9900, rgb(255,51,153) or hsl(108, 50%, 10%)) into an array with 3 elements for red, green, and blue or if toHSL parameter is true, it will populate the array with hue, saturation, and lightness values. If a relative value is found in an hsl() or hsla() string, it will preserve those relative prefixes and all the values in the array will be strings instead of numbers (in all other cases it will be populated with numbers).
   * @param {(string|number)} v The value the should be parsed which could be a string like #9F0 or rgb(255,102,51) or rgba(255,0,0,0.5) or it could be a number like 0xFF00CC or even a named color like red, blue, purple, etc.
   * @param {(boolean)} toHSL If true, an hsl() or hsla() value will be returned instead of rgb() or rgba()
   * @return {Array.<number>} An array containing red, green, and blue (and optionally alpha) in that order, or if the toHSL parameter was true, the array will contain hue, saturation and lightness (and optionally alpha) in that order. Always numbers unless there's a relative prefix found in an hsl() or hsla() string and toHSL is true.
   */
		_parseColor = CSSPlugin.parseColor = function (v, toHSL) {
			var a, r, g, b, h, s, l, max, min, d, wasHSL;
			if (!v) {
				a = _colorLookup.black;
			} else if (typeof v === "number") {
				a = [v >> 16, v >> 8 & 255, v & 255];
			} else {
				if (v.charAt(v.length - 1) === ",") {
					//sometimes a trailing comma is included and we should chop it off (typically from a comma-delimited list of values like a textShadow:"2px 2px 2px blue, 5px 5px 5px rgb(255,0,0)" - in this example "blue," has a trailing comma. We could strip it out inside parseComplex() but we'd need to do it to the beginning and ending values plus it wouldn't provide protection from other potential scenarios like if the user passes in a similar value.
					v = v.substr(0, v.length - 1);
				}
				if (_colorLookup[v]) {
					a = _colorLookup[v];
				} else if (v.charAt(0) === "#") {
					if (v.length === 4) {
						//for shorthand like #9F0
						r = v.charAt(1);
						g = v.charAt(2);
						b = v.charAt(3);
						v = "#" + r + r + g + g + b + b;
					}
					v = parseInt(v.substr(1), 16);
					a = [v >> 16, v >> 8 & 255, v & 255];
				} else if (v.substr(0, 3) === "hsl") {
					a = wasHSL = v.match(_numExp);
					if (!toHSL) {
						h = Number(a[0]) % 360 / 360;
						s = Number(a[1]) / 100;
						l = Number(a[2]) / 100;
						g = l <= 0.5 ? l * (s + 1) : l + s - l * s;
						r = l * 2 - g;
						if (a.length > 3) {
							a[3] = Number(a[3]);
						}
						a[0] = _hue(h + 1 / 3, r, g);
						a[1] = _hue(h, r, g);
						a[2] = _hue(h - 1 / 3, r, g);
					} else if (v.indexOf("=") !== -1) {
						//if relative values are found, just return the raw strings with the relative prefixes in place.
						return v.match(_relNumExp);
					}
				} else {
					a = v.match(_numExp) || _colorLookup.transparent;
				}
				a[0] = Number(a[0]);
				a[1] = Number(a[1]);
				a[2] = Number(a[2]);
				if (a.length > 3) {
					a[3] = Number(a[3]);
				}
			}
			if (toHSL && !wasHSL) {
				r = a[0] / 255;
				g = a[1] / 255;
				b = a[2] / 255;
				max = Math.max(r, g, b);
				min = Math.min(r, g, b);
				l = (max + min) / 2;
				if (max === min) {
					h = s = 0;
				} else {
					d = max - min;
					s = l > 0.5 ? d / (2 - max - min) : d / (max + min);
					h = max === r ? (g - b) / d + (g < b ? 6 : 0) : max === g ? (b - r) / d + 2 : (r - g) / d + 4;
					h *= 60;
				}
				a[0] = h + 0.5 | 0;
				a[1] = s * 100 + 0.5 | 0;
				a[2] = l * 100 + 0.5 | 0;
			}
			return a;
		},
		    _formatColors = function _formatColors(s, toHSL) {
			var colors = s.match(_colorExp) || [],
			    charIndex = 0,
			    parsed = "",
			    i,
			    color,
			    temp;
			if (!colors.length) {
				return s;
			}
			for (i = 0; i < colors.length; i++) {
				color = colors[i];
				temp = s.substr(charIndex, s.indexOf(color, charIndex) - charIndex);
				charIndex += temp.length + color.length;
				color = _parseColor(color, toHSL);
				if (color.length === 3) {
					color.push(1);
				}
				parsed += temp + (toHSL ? "hsla(" + color[0] + "," + color[1] + "%," + color[2] + "%," + color[3] : "rgba(" + color.join(",")) + ")";
			}
			return parsed + s.substr(charIndex);
		},
		    _colorExp = "(?:\\b(?:(?:rgb|rgba|hsl|hsla)\\(.+?\\))|\\B#(?:[0-9a-f]{3}){1,2}\\b"; //we'll dynamically build this Regular Expression to conserve file size. After building it, it will be able to find rgb(), rgba(), # (hexadecimal), and named color values like red, blue, purple, etc.

		for (p in _colorLookup) {
			_colorExp += "|" + p + "\\b";
		}
		_colorExp = new RegExp(_colorExp + ")", "gi");

		CSSPlugin.colorStringFilter = function (a) {
			var combined = a[0] + " " + a[1],
			    toHSL;
			if (_colorExp.test(combined)) {
				toHSL = combined.indexOf("hsl(") !== -1 || combined.indexOf("hsla(") !== -1;
				a[0] = _formatColors(a[0], toHSL);
				a[1] = _formatColors(a[1], toHSL);
			}
			_colorExp.lastIndex = 0;
		};

		if (!TweenLite.defaultStringFilter) {
			TweenLite.defaultStringFilter = CSSPlugin.colorStringFilter;
		}

		/**
   * @private Returns a formatter function that handles taking a string (or number in some cases) and returning a consistently formatted one in terms of delimiters, quantity of values, etc. For example, we may get boxShadow values defined as "0px red" or "0px 0px 10px rgb(255,0,0)" or "0px 0px 20px 20px #F00" and we need to ensure that what we get back is described with 4 numbers and a color. This allows us to feed it into the _parseComplex() method and split the values up appropriately. The neat thing about this _getFormatter() function is that the dflt defines a pattern as well as a default, so for example, _getFormatter("0px 0px 0px 0px #777", true) not only sets the default as 0px for all distances and #777 for the color, but also sets the pattern such that 4 numbers and a color will always get returned.
   * @param {!string} dflt The default value and pattern to follow. So "0px 0px 0px 0px #777" will ensure that 4 numbers and a color will always get returned.
   * @param {boolean=} clr If true, the values should be searched for color-related data. For example, boxShadow values typically contain a color whereas borderRadius don't.
   * @param {boolean=} collapsible If true, the value is a top/left/right/bottom style one that acts like margin or padding, where if only one value is received, it's used for all 4; if 2 are received, the first is duplicated for 3rd (bottom) and the 2nd is duplicated for the 4th spot (left), etc.
   * @return {Function} formatter function
   */
		var _getFormatter = function _getFormatter(dflt, clr, collapsible, multi) {
			if (dflt == null) {
				return function (v) {
					return v;
				};
			}
			var dColor = clr ? (dflt.match(_colorExp) || [""])[0] : "",
			    dVals = dflt.split(dColor).join("").match(_valuesExp) || [],
			    pfx = dflt.substr(0, dflt.indexOf(dVals[0])),
			    sfx = dflt.charAt(dflt.length - 1) === ")" ? ")" : "",
			    delim = dflt.indexOf(" ") !== -1 ? " " : ",",
			    numVals = dVals.length,
			    dSfx = numVals > 0 ? dVals[0].replace(_numExp, "") : "",
			    _formatter2;
			if (!numVals) {
				return function (v) {
					return v;
				};
			}
			if (clr) {
				_formatter2 = function formatter(v) {
					var color, vals, i, a;
					if (typeof v === "number") {
						v += dSfx;
					} else if (multi && _commasOutsideParenExp.test(v)) {
						a = v.replace(_commasOutsideParenExp, "|").split("|");
						for (i = 0; i < a.length; i++) {
							a[i] = _formatter2(a[i]);
						}
						return a.join(",");
					}
					color = (v.match(_colorExp) || [dColor])[0];
					vals = v.split(color).join("").match(_valuesExp) || [];
					i = vals.length;
					if (numVals > i--) {
						while (++i < numVals) {
							vals[i] = collapsible ? vals[(i - 1) / 2 | 0] : dVals[i];
						}
					}
					return pfx + vals.join(delim) + delim + color + sfx + (v.indexOf("inset") !== -1 ? " inset" : "");
				};
				return _formatter2;
			}
			_formatter2 = function _formatter(v) {
				var vals, a, i;
				if (typeof v === "number") {
					v += dSfx;
				} else if (multi && _commasOutsideParenExp.test(v)) {
					a = v.replace(_commasOutsideParenExp, "|").split("|");
					for (i = 0; i < a.length; i++) {
						a[i] = _formatter2(a[i]);
					}
					return a.join(",");
				}
				vals = v.match(_valuesExp) || [];
				i = vals.length;
				if (numVals > i--) {
					while (++i < numVals) {
						vals[i] = collapsible ? vals[(i - 1) / 2 | 0] : dVals[i];
					}
				}
				return pfx + vals.join(delim) + sfx;
			};
			return _formatter2;
		},


		/**
   * @private returns a formatter function that's used for edge-related values like marginTop, marginLeft, paddingBottom, paddingRight, etc. Just pass a comma-delimited list of property names related to the edges.
   * @param {!string} props a comma-delimited list of property names in order from top to left, like "marginTop,marginRight,marginBottom,marginLeft"
   * @return {Function} a formatter function
   */
		_getEdgeParser = function _getEdgeParser(props) {
			props = props.split(",");
			return function (t, e, p, cssp, pt, plugin, vars) {
				var a = (e + "").split(" "),
				    i;
				vars = {};
				for (i = 0; i < 4; i++) {
					vars[props[i]] = a[i] = a[i] || a[(i - 1) / 2 >> 0];
				}
				return cssp.parse(t, vars, pt, plugin);
			};
		},


		// @private used when other plugins must tween values first, like BezierPlugin or ThrowPropsPlugin, etc. That plugin's setRatio() gets called first so that the values are updated, and then we loop through the MiniPropTweens which handle copying the values into their appropriate slots so that they can then be applied correctly in the main CSSPlugin setRatio() method. Remember, we typically create a proxy object that has a bunch of uniquely-named properties that we feed to the sub-plugin and it does its magic normally, and then we must interpret those values and apply them to the css because often numbers must get combined/concatenated, suffixes added, etc. to work with css, like boxShadow could have 4 values plus a color.
		_setPluginRatio = _internals._setPluginRatio = function (v) {
			this.plugin.setRatio(v);
			var d = this.data,
			    proxy = d.proxy,
			    mpt = d.firstMPT,
			    min = 0.000001,
			    val,
			    pt,
			    i,
			    str,
			    p;
			while (mpt) {
				val = proxy[mpt.v];
				if (mpt.r) {
					val = Math.round(val);
				} else if (val < min && val > -min) {
					val = 0;
				}
				mpt.t[mpt.p] = val;
				mpt = mpt._next;
			}
			if (d.autoRotate) {
				d.autoRotate.rotation = d.mod ? d.mod(proxy.rotation, this.t) : proxy.rotation; //special case for ModifyPlugin to hook into an auto-rotating bezier
			}
			//at the end, we must set the CSSPropTween's "e" (end) value dynamically here because that's what is used in the final setRatio() method. Same for "b" at the beginning.
			if (v === 1 || v === 0) {
				mpt = d.firstMPT;
				p = v === 1 ? "e" : "b";
				while (mpt) {
					pt = mpt.t;
					if (!pt.type) {
						pt[p] = pt.s + pt.xs0;
					} else if (pt.type === 1) {
						str = pt.xs0 + pt.s + pt.xs1;
						for (i = 1; i < pt.l; i++) {
							str += pt["xn" + i] + pt["xs" + (i + 1)];
						}
						pt[p] = str;
					}
					mpt = mpt._next;
				}
			}
		},


		/**
   * @private @constructor Used by a few SpecialProps to hold important values for proxies. For example, _parseToProxy() creates a MiniPropTween instance for each property that must get tweened on the proxy, and we record the original property name as well as the unique one we create for the proxy, plus whether or not the value needs to be rounded plus the original value.
   * @param {!Object} t target object whose property we're tweening (often a CSSPropTween)
   * @param {!string} p property name
   * @param {(number|string|object)} v value
   * @param {MiniPropTween=} next next MiniPropTween in the linked list
   * @param {boolean=} r if true, the tweened value should be rounded to the nearest integer
   */
		MiniPropTween = function MiniPropTween(t, p, v, next, r) {
			this.t = t;
			this.p = p;
			this.v = v;
			this.r = r;
			if (next) {
				next._prev = this;
				this._next = next;
			}
		},


		/**
   * @private Most other plugins (like BezierPlugin and ThrowPropsPlugin and others) can only tween numeric values, but CSSPlugin must accommodate special values that have a bunch of extra data (like a suffix or strings between numeric values, etc.). For example, boxShadow has values like "10px 10px 20px 30px rgb(255,0,0)" which would utterly confuse other plugins. This method allows us to split that data apart and grab only the numeric data and attach it to uniquely-named properties of a generic proxy object ({}) so that we can feed that to virtually any plugin to have the numbers tweened. However, we must also keep track of which properties from the proxy go with which CSSPropTween values and instances. So we create a linked list of MiniPropTweens. Each one records a target (the original CSSPropTween), property (like "s" or "xn1" or "xn2") that we're tweening and the unique property name that was used for the proxy (like "boxShadow_xn1" and "boxShadow_xn2") and whether or not they need to be rounded. That way, in the _setPluginRatio() method we can simply copy the values over from the proxy to the CSSPropTween instance(s). Then, when the main CSSPlugin setRatio() method runs and applies the CSSPropTween values accordingly, they're updated nicely. So the external plugin tweens the numbers, _setPluginRatio() copies them over, and setRatio() acts normally, applying css-specific values to the element.
   * This method returns an object that has the following properties:
   *  - proxy: a generic object containing the starting values for all the properties that will be tweened by the external plugin.  This is what we feed to the external _onInitTween() as the target
   *  - end: a generic object containing the ending values for all the properties that will be tweened by the external plugin. This is what we feed to the external plugin's _onInitTween() as the destination values
   *  - firstMPT: the first MiniPropTween in the linked list
   *  - pt: the first CSSPropTween in the linked list that was created when parsing. If shallow is true, this linked list will NOT attach to the one passed into the _parseToProxy() as the "pt" (4th) parameter.
   * @param {!Object} t target object to be tweened
   * @param {!(Object|string)} vars the object containing the information about the tweening values (typically the end/destination values) that should be parsed
   * @param {!CSSPlugin} cssp The CSSPlugin instance
   * @param {CSSPropTween=} pt the next CSSPropTween in the linked list
   * @param {TweenPlugin=} plugin the external TweenPlugin instance that will be handling tweening the numeric values
   * @param {boolean=} shallow if true, the resulting linked list from the parse will NOT be attached to the CSSPropTween that was passed in as the "pt" (4th) parameter.
   * @return An object containing the following properties: proxy, end, firstMPT, and pt (see above for descriptions)
   */
		_parseToProxy = _internals._parseToProxy = function (t, vars, cssp, pt, plugin, shallow) {
			var bpt = pt,
			    start = {},
			    end = {},
			    transform = cssp._transform,
			    oldForce = _forcePT,
			    i,
			    p,
			    xp,
			    mpt,
			    firstPT;
			cssp._transform = null;
			_forcePT = vars;
			pt = firstPT = cssp.parse(t, vars, pt, plugin);
			_forcePT = oldForce;
			//break off from the linked list so the new ones are isolated.
			if (shallow) {
				cssp._transform = transform;
				if (bpt) {
					bpt._prev = null;
					if (bpt._prev) {
						bpt._prev._next = null;
					}
				}
			}
			while (pt && pt !== bpt) {
				if (pt.type <= 1) {
					p = pt.p;
					end[p] = pt.s + pt.c;
					start[p] = pt.s;
					if (!shallow) {
						mpt = new MiniPropTween(pt, "s", p, mpt, pt.r);
						pt.c = 0;
					}
					if (pt.type === 1) {
						i = pt.l;
						while (--i > 0) {
							xp = "xn" + i;
							p = pt.p + "_" + xp;
							end[p] = pt.data[xp];
							start[p] = pt[xp];
							if (!shallow) {
								mpt = new MiniPropTween(pt, xp, p, mpt, pt.rxp[xp]);
							}
						}
					}
				}
				pt = pt._next;
			}
			return { proxy: start, end: end, firstMPT: mpt, pt: firstPT };
		},


		/**
   * @constructor Each property that is tweened has at least one CSSPropTween associated with it. These instances store important information like the target, property, starting value, amount of change, etc. They can also optionally have a number of "extra" strings and numeric values named xs1, xn1, xs2, xn2, xs3, xn3, etc. where "s" indicates string and "n" indicates number. These can be pieced together in a complex-value tween (type:1) that has alternating types of data like a string, number, string, number, etc. For example, boxShadow could be "5px 5px 8px rgb(102, 102, 51)". In that value, there are 6 numbers that may need to tween and then pieced back together into a string again with spaces, suffixes, etc. xs0 is special in that it stores the suffix for standard (type:0) tweens, -OR- the first string (prefix) in a complex-value (type:1) CSSPropTween -OR- it can be the non-tweening value in a type:-1 CSSPropTween. We do this to conserve memory.
   * CSSPropTweens have the following optional properties as well (not defined through the constructor):
   *  - l: Length in terms of the number of extra properties that the CSSPropTween has (default: 0). For example, for a boxShadow we may need to tween 5 numbers in which case l would be 5; Keep in mind that the start/end values for the first number that's tweened are always stored in the s and c properties to conserve memory. All additional values thereafter are stored in xn1, xn2, etc.
   *  - xfirst: The first instance of any sub-CSSPropTweens that are tweening properties of this instance. For example, we may split up a boxShadow tween so that there's a main CSSPropTween of type:1 that has various xs* and xn* values associated with the h-shadow, v-shadow, blur, color, etc. Then we spawn a CSSPropTween for each of those that has a higher priority and runs BEFORE the main CSSPropTween so that the values are all set by the time it needs to re-assemble them. The xfirst gives us an easy way to identify the first one in that chain which typically ends at the main one (because they're all prepende to the linked list)
   *  - plugin: The TweenPlugin instance that will handle the tweening of any complex values. For example, sometimes we don't want to use normal subtweens (like xfirst refers to) to tween the values - we might want ThrowPropsPlugin or BezierPlugin some other plugin to do the actual tweening, so we create a plugin instance and store a reference here. We need this reference so that if we get a request to round values or disable a tween, we can pass along that request.
   *  - data: Arbitrary data that needs to be stored with the CSSPropTween. Typically if we're going to have a plugin handle the tweening of a complex-value tween, we create a generic object that stores the END values that we're tweening to and the CSSPropTween's xs1, xs2, etc. have the starting values. We store that object as data. That way, we can simply pass that object to the plugin and use the CSSPropTween as the target.
   *  - setRatio: Only used for type:2 tweens that require custom functionality. In this case, we call the CSSPropTween's setRatio() method and pass the ratio each time the tween updates. This isn't quite as efficient as doing things directly in the CSSPlugin's setRatio() method, but it's very convenient and flexible.
   * @param {!Object} t Target object whose property will be tweened. Often a DOM element, but not always. It could be anything.
   * @param {string} p Property to tween (name). For example, to tween element.width, p would be "width".
   * @param {number} s Starting numeric value
   * @param {number} c Change in numeric value over the course of the entire tween. For example, if element.width starts at 5 and should end at 100, c would be 95.
   * @param {CSSPropTween=} next The next CSSPropTween in the linked list. If one is defined, we will define its _prev as the new instance, and the new instance's _next will be pointed at it.
   * @param {number=} type The type of CSSPropTween where -1 = a non-tweening value, 0 = a standard simple tween, 1 = a complex value (like one that has multiple numbers in a comma- or space-delimited string like border:"1px solid red"), and 2 = one that uses a custom setRatio function that does all of the work of applying the values on each update.
   * @param {string=} n Name of the property that should be used for overwriting purposes which is typically the same as p but not always. For example, we may need to create a subtween for the 2nd part of a "clip:rect(...)" tween in which case "p" might be xs1 but "n" is still "clip"
   * @param {boolean=} r If true, the value(s) should be rounded
   * @param {number=} pr Priority in the linked list order. Higher priority CSSPropTweens will be updated before lower priority ones. The default priority is 0.
   * @param {string=} b Beginning value. We store this to ensure that it is EXACTLY what it was when the tween began without any risk of interpretation issues.
   * @param {string=} e Ending value. We store this to ensure that it is EXACTLY what the user defined at the end of the tween without any risk of interpretation issues.
   */
		CSSPropTween = _internals.CSSPropTween = function (t, p, s, c, next, type, n, r, pr, b, e) {
			this.t = t; //target
			this.p = p; //property
			this.s = s; //starting value
			this.c = c; //change value
			this.n = n || p; //name that this CSSPropTween should be associated to (usually the same as p, but not always - n is what overwriting looks at)
			if (!(t instanceof CSSPropTween)) {
				_overwriteProps.push(this.n);
			}
			this.r = r; //round (boolean)
			this.type = type || 0; //0 = normal tween, -1 = non-tweening (in which case xs0 will be applied to the target's property, like tp.t[tp.p] = tp.xs0), 1 = complex-value SpecialProp, 2 = custom setRatio() that does all the work
			if (pr) {
				this.pr = pr;
				_hasPriority = true;
			}
			this.b = b === undefined ? s : b;
			this.e = e === undefined ? s + c : e;
			if (next) {
				this._next = next;
				next._prev = this;
			}
		},
		    _addNonTweeningNumericPT = function _addNonTweeningNumericPT(target, prop, start, end, next, overwriteProp) {
			//cleans up some code redundancies and helps minification. Just a fast way to add a NUMERIC non-tweening CSSPropTween
			var pt = new CSSPropTween(target, prop, start, end - start, next, -1, overwriteProp);
			pt.b = start;
			pt.e = pt.xs0 = end;
			return pt;
		},


		/**
   * Takes a target, the beginning value and ending value (as strings) and parses them into a CSSPropTween (possibly with child CSSPropTweens) that accommodates multiple numbers, colors, comma-delimited values, etc. For example:
   * sp.parseComplex(element, "boxShadow", "5px 10px 20px rgb(255,102,51)", "0px 0px 0px red", true, "0px 0px 0px rgb(0,0,0,0)", pt);
   * It will walk through the beginning and ending values (which should be in the same format with the same number and type of values) and figure out which parts are numbers, what strings separate the numeric/tweenable values, and then create the CSSPropTweens accordingly. If a plugin is defined, no child CSSPropTweens will be created. Instead, the ending values will be stored in the "data" property of the returned CSSPropTween like: {s:-5, xn1:-10, xn2:-20, xn3:255, xn4:0, xn5:0} so that it can be fed to any other plugin and it'll be plain numeric tweens but the recomposition of the complex value will be handled inside CSSPlugin's setRatio().
   * If a setRatio is defined, the type of the CSSPropTween will be set to 2 and recomposition of the values will be the responsibility of that method.
   *
   * @param {!Object} t Target whose property will be tweened
   * @param {!string} p Property that will be tweened (its name, like "left" or "backgroundColor" or "boxShadow")
   * @param {string} b Beginning value
   * @param {string} e Ending value
   * @param {boolean} clrs If true, the value could contain a color value like "rgb(255,0,0)" or "#F00" or "red". The default is false, so no colors will be recognized (a performance optimization)
   * @param {(string|number|Object)} dflt The default beginning value that should be used if no valid beginning value is defined or if the number of values inside the complex beginning and ending values don't match
   * @param {?CSSPropTween} pt CSSPropTween instance that is the current head of the linked list (we'll prepend to this).
   * @param {number=} pr Priority in the linked list order. Higher priority properties will be updated before lower priority ones. The default priority is 0.
   * @param {TweenPlugin=} plugin If a plugin should handle the tweening of extra properties, pass the plugin instance here. If one is defined, then NO subtweens will be created for any extra properties (the properties will be created - just not additional CSSPropTween instances to tween them) because the plugin is expected to do so. However, the end values WILL be populated in the "data" property, like {s:100, xn1:50, xn2:300}
   * @param {function(number)=} setRatio If values should be set in a custom function instead of being pieced together in a type:1 (complex-value) CSSPropTween, define that custom function here.
   * @return {CSSPropTween} The first CSSPropTween in the linked list which includes the new one(s) added by the parseComplex() call.
   */
		_parseComplex = CSSPlugin.parseComplex = function (t, p, b, e, clrs, dflt, pt, pr, plugin, setRatio) {
			//DEBUG: _log("parseComplex: "+p+", b: "+b+", e: "+e);
			b = b || dflt || "";
			if (typeof e === "function") {
				e = e(_index, _target);
			}
			pt = new CSSPropTween(t, p, 0, 0, pt, setRatio ? 2 : 1, null, false, pr, b, e);
			e += ""; //ensures it's a string
			if (clrs && _colorExp.test(e + b)) {
				//if colors are found, normalize the formatting to rgba() or hsla().
				e = [b, e];
				CSSPlugin.colorStringFilter(e);
				b = e[0];
				e = e[1];
			}
			var ba = b.split(", ").join(",").split(" "),
			    //beginning array
			ea = e.split(", ").join(",").split(" "),
			    //ending array
			l = ba.length,
			    autoRound = _autoRound !== false,
			    i,
			    xi,
			    ni,
			    bv,
			    ev,
			    bnums,
			    enums,
			    bn,
			    hasAlpha,
			    temp,
			    cv,
			    str,
			    useHSL;
			if (e.indexOf(",") !== -1 || b.indexOf(",") !== -1) {
				if ((e + b).indexOf("rgb") !== -1 || (e + b).indexOf("hsl") !== -1) {
					//keep rgb(), rgba(), hsl(), and hsla() values together! (remember, we're splitting on spaces)
					ba = ba.join(" ").replace(_commasOutsideParenExp, ", ").split(" ");
					ea = ea.join(" ").replace(_commasOutsideParenExp, ", ").split(" ");
				} else {
					ba = ba.join(" ").split(",").join(", ").split(" ");
					ea = ea.join(" ").split(",").join(", ").split(" ");
				}
				l = ba.length;
			}
			if (l !== ea.length) {
				//DEBUG: _log("mismatched formatting detected on " + p + " (" + b + " vs " + e + ")");
				ba = (dflt || "").split(" ");
				l = ba.length;
			}
			pt.plugin = plugin;
			pt.setRatio = setRatio;
			_colorExp.lastIndex = 0;
			for (i = 0; i < l; i++) {
				bv = ba[i];
				ev = ea[i];
				bn = parseFloat(bv);
				//if the value begins with a number (most common). It's fine if it has a suffix like px
				if (bn || bn === 0) {
					pt.appendXtra("", bn, _parseChange(ev, bn), ev.replace(_relNumExp, ""), autoRound && ev.indexOf("px") !== -1, true);

					//if the value is a color
				} else if (clrs && _colorExp.test(bv)) {
					str = ev.indexOf(")") + 1;
					str = ")" + (str ? ev.substr(str) : ""); //if there's a comma or ) at the end, retain it.
					useHSL = ev.indexOf("hsl") !== -1 && _supportsOpacity;
					temp = ev; //original string value so we can look for any prefix later.
					bv = _parseColor(bv, useHSL);
					ev = _parseColor(ev, useHSL);
					hasAlpha = bv.length + ev.length > 6;
					if (hasAlpha && !_supportsOpacity && ev[3] === 0) {
						//older versions of IE don't support rgba(), so if the destination alpha is 0, just use "transparent" for the end color
						pt["xs" + pt.l] += pt.l ? " transparent" : "transparent";
						pt.e = pt.e.split(ea[i]).join("transparent");
					} else {
						if (!_supportsOpacity) {
							//old versions of IE don't support rgba().
							hasAlpha = false;
						}
						if (useHSL) {
							pt.appendXtra(temp.substr(0, temp.indexOf("hsl")) + (hasAlpha ? "hsla(" : "hsl("), bv[0], _parseChange(ev[0], bv[0]), ",", false, true).appendXtra("", bv[1], _parseChange(ev[1], bv[1]), "%,", false).appendXtra("", bv[2], _parseChange(ev[2], bv[2]), hasAlpha ? "%," : "%" + str, false);
						} else {
							pt.appendXtra(temp.substr(0, temp.indexOf("rgb")) + (hasAlpha ? "rgba(" : "rgb("), bv[0], ev[0] - bv[0], ",", true, true).appendXtra("", bv[1], ev[1] - bv[1], ",", true).appendXtra("", bv[2], ev[2] - bv[2], hasAlpha ? "," : str, true);
						}

						if (hasAlpha) {
							bv = bv.length < 4 ? 1 : bv[3];
							pt.appendXtra("", bv, (ev.length < 4 ? 1 : ev[3]) - bv, str, false);
						}
					}
					_colorExp.lastIndex = 0; //otherwise the test() on the RegExp could move the lastIndex and taint future results.
				} else {
					bnums = bv.match(_numExp); //gets each group of numbers in the beginning value string and drops them into an array

					//if no number is found, treat it as a non-tweening value and just append the string to the current xs.
					if (!bnums) {
						pt["xs" + pt.l] += pt.l || pt["xs" + pt.l] ? " " + ev : ev;

						//loop through all the numbers that are found and construct the extra values on the pt.
					} else {
						enums = ev.match(_relNumExp); //get each group of numbers in the end value string and drop them into an array. We allow relative values too, like +=50 or -=.5
						if (!enums || enums.length !== bnums.length) {
							//DEBUG: _log("mismatched formatting detected on " + p + " (" + b + " vs " + e + ")");
							return pt;
						}
						ni = 0;
						for (xi = 0; xi < bnums.length; xi++) {
							cv = bnums[xi];
							temp = bv.indexOf(cv, ni);
							pt.appendXtra(bv.substr(ni, temp - ni), Number(cv), _parseChange(enums[xi], cv), "", autoRound && bv.substr(temp + cv.length, 2) === "px", xi === 0);
							ni = temp + cv.length;
						}
						pt["xs" + pt.l] += bv.substr(ni);
					}
				}
			}
			//if there are relative values ("+=" or "-=" prefix), we need to adjust the ending value to eliminate the prefixes and combine the values properly.
			if (e.indexOf("=") !== -1) if (pt.data) {
				str = pt.xs0 + pt.data.s;
				for (i = 1; i < pt.l; i++) {
					str += pt["xs" + i] + pt.data["xn" + i];
				}
				pt.e = str + pt["xs" + i];
			}
			if (!pt.l) {
				pt.type = -1;
				pt.xs0 = pt.e;
			}
			return pt.xfirst || pt;
		},
		    i = 9;

		p = CSSPropTween.prototype;
		p.l = p.pr = 0; //length (number of extra properties like xn1, xn2, xn3, etc.
		while (--i > 0) {
			p["xn" + i] = 0;
			p["xs" + i] = "";
		}
		p.xs0 = "";
		p._next = p._prev = p.xfirst = p.data = p.plugin = p.setRatio = p.rxp = null;

		/**
   * Appends and extra tweening value to a CSSPropTween and automatically manages any prefix and suffix strings. The first extra value is stored in the s and c of the main CSSPropTween instance, but thereafter any extras are stored in the xn1, xn2, xn3, etc. The prefixes and suffixes are stored in the xs0, xs1, xs2, etc. properties. For example, if I walk through a clip value like "rect(10px, 5px, 0px, 20px)", the values would be stored like this:
   * xs0:"rect(", s:10, xs1:"px, ", xn1:5, xs2:"px, ", xn2:0, xs3:"px, ", xn3:20, xn4:"px)"
   * And they'd all get joined together when the CSSPlugin renders (in the setRatio() method).
   * @param {string=} pfx Prefix (if any)
   * @param {!number} s Starting value
   * @param {!number} c Change in numeric value over the course of the entire tween. For example, if the start is 5 and the end is 100, the change would be 95.
   * @param {string=} sfx Suffix (if any)
   * @param {boolean=} r Round (if true).
   * @param {boolean=} pad If true, this extra value should be separated by the previous one by a space. If there is no previous extra and pad is true, it will automatically drop the space.
   * @return {CSSPropTween} returns itself so that multiple methods can be chained together.
   */
		p.appendXtra = function (pfx, s, c, sfx, r, pad) {
			var pt = this,
			    l = pt.l;
			pt["xs" + l] += pad && (l || pt["xs" + l]) ? " " + pfx : pfx || "";
			if (!c) if (l !== 0 && !pt.plugin) {
				//typically we'll combine non-changing values right into the xs to optimize performance, but we don't combine them when there's a plugin that will be tweening the values because it may depend on the values being split apart, like for a bezier, if a value doesn't change between the first and second iteration but then it does on the 3rd, we'll run into trouble because there's no xn slot for that value!
				pt["xs" + l] += s + (sfx || "");
				return pt;
			}
			pt.l++;
			pt.type = pt.setRatio ? 2 : 1;
			pt["xs" + pt.l] = sfx || "";
			if (l > 0) {
				pt.data["xn" + l] = s + c;
				pt.rxp["xn" + l] = r; //round extra property (we need to tap into this in the _parseToProxy() method)
				pt["xn" + l] = s;
				if (!pt.plugin) {
					pt.xfirst = new CSSPropTween(pt, "xn" + l, s, c, pt.xfirst || pt, 0, pt.n, r, pt.pr);
					pt.xfirst.xs0 = 0; //just to ensure that the property stays numeric which helps modern browsers speed up processing. Remember, in the setRatio() method, we do pt.t[pt.p] = val + pt.xs0 so if pt.xs0 is "" (the default), it'll cast the end value as a string. When a property is a number sometimes and a string sometimes, it prevents the compiler from locking in the data type, slowing things down slightly.
				}
				return pt;
			}
			pt.data = { s: s + c };
			pt.rxp = {};
			pt.s = s;
			pt.c = c;
			pt.r = r;
			return pt;
		};

		/**
   * @constructor A SpecialProp is basically a css property that needs to be treated in a non-standard way, like if it may contain a complex value like boxShadow:"5px 10px 15px rgb(255, 102, 51)" or if it is associated with another plugin like ThrowPropsPlugin or BezierPlugin. Every SpecialProp is associated with a particular property name like "boxShadow" or "throwProps" or "bezier" and it will intercept those values in the vars object that's passed to the CSSPlugin and handle them accordingly.
   * @param {!string} p Property name (like "boxShadow" or "throwProps")
   * @param {Object=} options An object containing any of the following configuration options:
   *                      - defaultValue: the default value
   *                      - parser: A function that should be called when the associated property name is found in the vars. This function should return a CSSPropTween instance and it should ensure that it is properly inserted into the linked list. It will receive 4 paramters: 1) The target, 2) The value defined in the vars, 3) The CSSPlugin instance (whose _firstPT should be used for the linked list), and 4) A computed style object if one was calculated (this is a speed optimization that allows retrieval of starting values quicker)
   *                      - formatter: a function that formats any value received for this special property (for example, boxShadow could take "5px 5px red" and format it to "5px 5px 0px 0px red" so that both the beginning and ending values have a common order and quantity of values.)
   *                      - prefix: if true, we'll determine whether or not this property requires a vendor prefix (like Webkit or Moz or ms or O)
   *                      - color: set this to true if the value for this SpecialProp may contain color-related values like rgb(), rgba(), etc.
   *                      - priority: priority in the linked list order. Higher priority SpecialProps will be updated before lower priority ones. The default priority is 0.
   *                      - multi: if true, the formatter should accommodate a comma-delimited list of values, like boxShadow could have multiple boxShadows listed out.
   *                      - collapsible: if true, the formatter should treat the value like it's a top/right/bottom/left value that could be collapsed, like "5px" would apply to all, "5px, 10px" would use 5px for top/bottom and 10px for right/left, etc.
   *                      - keyword: a special keyword that can [optionally] be found inside the value (like "inset" for boxShadow). This allows us to validate beginning/ending values to make sure they match (if the keyword is found in one, it'll be added to the other for consistency by default).
   */
		var SpecialProp = function SpecialProp(p, options) {
			options = options || {};
			this.p = options.prefix ? _checkPropPrefix(p) || p : p;
			_specialProps[p] = _specialProps[this.p] = this;
			this.format = options.formatter || _getFormatter(options.defaultValue, options.color, options.collapsible, options.multi);
			if (options.parser) {
				this.parse = options.parser;
			}
			this.clrs = options.color;
			this.multi = options.multi;
			this.keyword = options.keyword;
			this.dflt = options.defaultValue;
			this.pr = options.priority || 0;
		},


		//shortcut for creating a new SpecialProp that can accept multiple properties as a comma-delimited list (helps minification). dflt can be an array for multiple values (we don't do a comma-delimited list because the default value may contain commas, like rect(0px,0px,0px,0px)). We attach this method to the SpecialProp class/object instead of using a private _createSpecialProp() method so that we can tap into it externally if necessary, like from another plugin.
		_registerComplexSpecialProp = _internals._registerComplexSpecialProp = function (p, options, defaults) {
			if ((typeof options === "undefined" ? "undefined" : _typeof(options)) !== "object") {
				options = { parser: defaults }; //to make backwards compatible with older versions of BezierPlugin and ThrowPropsPlugin
			}
			var a = p.split(","),
			    d = options.defaultValue,
			    i,
			    temp;
			defaults = defaults || [d];
			for (i = 0; i < a.length; i++) {
				options.prefix = i === 0 && options.prefix;
				options.defaultValue = defaults[i] || d;
				temp = new SpecialProp(a[i], options);
			}
		},


		//creates a placeholder special prop for a plugin so that the property gets caught the first time a tween of it is attempted, and at that time it makes the plugin register itself, thus taking over for all future tweens of that property. This allows us to not mandate that things load in a particular order and it also allows us to log() an error that informs the user when they attempt to tween an external plugin-related property without loading its .js file.
		_registerPluginProp = _internals._registerPluginProp = function (p) {
			if (!_specialProps[p]) {
				var pluginName = p.charAt(0).toUpperCase() + p.substr(1) + "Plugin";
				_registerComplexSpecialProp(p, { parser: function parser(t, e, p, cssp, pt, plugin, vars) {
						var pluginClass = _globals.com.greensock.plugins[pluginName];
						if (!pluginClass) {
							_log("Error: " + pluginName + " js file not loaded.");
							return pt;
						}
						pluginClass._cssRegister();
						return _specialProps[p].parse(t, e, p, cssp, pt, plugin, vars);
					} });
			}
		};

		p = SpecialProp.prototype;

		/**
   * Alias for _parseComplex() that automatically plugs in certain values for this SpecialProp, like its property name, whether or not colors should be sensed, the default value, and priority. It also looks for any keyword that the SpecialProp defines (like "inset" for boxShadow) and ensures that the beginning and ending values have the same number of values for SpecialProps where multi is true (like boxShadow and textShadow can have a comma-delimited list)
   * @param {!Object} t target element
   * @param {(string|number|object)} b beginning value
   * @param {(string|number|object)} e ending (destination) value
   * @param {CSSPropTween=} pt next CSSPropTween in the linked list
   * @param {TweenPlugin=} plugin If another plugin will be tweening the complex value, that TweenPlugin instance goes here.
   * @param {function=} setRatio If a custom setRatio() method should be used to handle this complex value, that goes here.
   * @return {CSSPropTween=} First CSSPropTween in the linked list
   */
		p.parseComplex = function (t, b, e, pt, plugin, setRatio) {
			var kwd = this.keyword,
			    i,
			    ba,
			    ea,
			    l,
			    bi,
			    ei;
			//if this SpecialProp's value can contain a comma-delimited list of values (like boxShadow or textShadow), we must parse them in a special way, and look for a keyword (like "inset" for boxShadow) and ensure that the beginning and ending BOTH have it if the end defines it as such. We also must ensure that there are an equal number of values specified (we can't tween 1 boxShadow to 3 for example)
			if (this.multi) if (_commasOutsideParenExp.test(e) || _commasOutsideParenExp.test(b)) {
				ba = b.replace(_commasOutsideParenExp, "|").split("|");
				ea = e.replace(_commasOutsideParenExp, "|").split("|");
			} else if (kwd) {
				ba = [b];
				ea = [e];
			}
			if (ea) {
				l = ea.length > ba.length ? ea.length : ba.length;
				for (i = 0; i < l; i++) {
					b = ba[i] = ba[i] || this.dflt;
					e = ea[i] = ea[i] || this.dflt;
					if (kwd) {
						bi = b.indexOf(kwd);
						ei = e.indexOf(kwd);
						if (bi !== ei) {
							if (ei === -1) {
								//if the keyword isn't in the end value, remove it from the beginning one.
								ba[i] = ba[i].split(kwd).join("");
							} else if (bi === -1) {
								//if the keyword isn't in the beginning, add it.
								ba[i] += " " + kwd;
							}
						}
					}
				}
				b = ba.join(", ");
				e = ea.join(", ");
			}
			return _parseComplex(t, this.p, b, e, this.clrs, this.dflt, pt, this.pr, plugin, setRatio);
		};

		/**
   * Accepts a target and end value and spits back a CSSPropTween that has been inserted into the CSSPlugin's linked list and conforms with all the conventions we use internally, like type:-1, 0, 1, or 2, setting up any extra property tweens, priority, etc. For example, if we have a boxShadow SpecialProp and call:
   * this._firstPT = sp.parse(element, "5px 10px 20px rgb(2550,102,51)", "boxShadow", this);
   * It should figure out the starting value of the element's boxShadow, compare it to the provided end value and create all the necessary CSSPropTweens of the appropriate types to tween the boxShadow. The CSSPropTween that gets spit back should already be inserted into the linked list (the 4th parameter is the current head, so prepend to that).
   * @param {!Object} t Target object whose property is being tweened
   * @param {Object} e End value as provided in the vars object (typically a string, but not always - like a throwProps would be an object).
   * @param {!string} p Property name
   * @param {!CSSPlugin} cssp The CSSPlugin instance that should be associated with this tween.
   * @param {?CSSPropTween} pt The CSSPropTween that is the current head of the linked list (we'll prepend to it)
   * @param {TweenPlugin=} plugin If a plugin will be used to tween the parsed value, this is the plugin instance.
   * @param {Object=} vars Original vars object that contains the data for parsing.
   * @return {CSSPropTween} The first CSSPropTween in the linked list which includes the new one(s) added by the parse() call.
   */
		p.parse = function (t, e, p, cssp, pt, plugin, vars) {
			return this.parseComplex(t.style, this.format(_getStyle(t, this.p, _cs, false, this.dflt)), this.format(e), pt, plugin);
		};

		/**
   * Registers a special property that should be intercepted from any "css" objects defined in tweens. This allows you to handle them however you want without CSSPlugin doing it for you. The 2nd parameter should be a function that accepts 3 parameters:
   *  1) Target object whose property should be tweened (typically a DOM element)
   *  2) The end/destination value (could be a string, number, object, or whatever you want)
   *  3) The tween instance (you probably don't need to worry about this, but it can be useful for looking up information like the duration)
   *
   * Then, your function should return a function which will be called each time the tween gets rendered, passing a numeric "ratio" parameter to your function that indicates the change factor (usually between 0 and 1). For example:
   *
   * CSSPlugin.registerSpecialProp("myCustomProp", function(target, value, tween) {
   *      var start = target.style.width;
   *      return function(ratio) {
   *              target.style.width = (start + value * ratio) + "px";
   *              console.log("set width to " + target.style.width);
   *          }
   * }, 0);
   *
   * Then, when I do this tween, it will trigger my special property:
   *
   * TweenLite.to(element, 1, {css:{myCustomProp:100}});
   *
   * In the example, of course, we're just changing the width, but you can do anything you want.
   *
   * @param {!string} name Property name (or comma-delimited list of property names) that should be intercepted and handled by your function. For example, if I define "myCustomProp", then it would handle that portion of the following tween: TweenLite.to(element, 1, {css:{myCustomProp:100}})
   * @param {!function(Object, Object, Object, string):function(number)} onInitTween The function that will be called when a tween of this special property is performed. The function will receive 4 parameters: 1) Target object that should be tweened, 2) Value that was passed to the tween, 3) The tween instance itself (rarely used), and 4) The property name that's being tweened. Your function should return a function that should be called on every update of the tween. That function will receive a single parameter that is a "change factor" value (typically between 0 and 1) indicating the amount of change as a ratio. You can use this to determine how to set the values appropriately in your function.
   * @param {number=} priority Priority that helps the engine determine the order in which to set the properties (default: 0). Higher priority properties will be updated before lower priority ones.
   */
		CSSPlugin.registerSpecialProp = function (name, onInitTween, priority) {
			_registerComplexSpecialProp(name, { parser: function parser(t, e, p, cssp, pt, plugin, vars) {
					var rv = new CSSPropTween(t, p, 0, 0, pt, 2, p, false, priority);
					rv.plugin = plugin;
					rv.setRatio = onInitTween(t, e, cssp._tween, p);
					return rv;
				}, priority: priority });
		};

		//transform-related methods and properties
		CSSPlugin.useSVGTransformAttr = true; //Safari and Firefox both have some rendering bugs when applying CSS transforms to SVG elements, so default to using the "transform" attribute instead (users can override this).
		var _transformProps = "scaleX,scaleY,scaleZ,x,y,z,skewX,skewY,rotation,rotationX,rotationY,perspective,xPercent,yPercent".split(","),
		    _transformProp = _checkPropPrefix("transform"),
		    //the Javascript (camelCase) transform property, like msTransform, WebkitTransform, MozTransform, or OTransform.
		_transformPropCSS = _prefixCSS + "transform",
		    _transformOriginProp = _checkPropPrefix("transformOrigin"),
		    _supports3D = _checkPropPrefix("perspective") !== null,
		    Transform = _internals.Transform = function () {
			this.perspective = parseFloat(CSSPlugin.defaultTransformPerspective) || 0;
			this.force3D = CSSPlugin.defaultForce3D === false || !_supports3D ? false : CSSPlugin.defaultForce3D || "auto";
		},
		    _SVGElement = _gsScope.SVGElement,
		    _useSVGTransformAttr,

		//Some browsers (like Firefox and IE) don't honor transform-origin properly in SVG elements, so we need to manually adjust the matrix accordingly. We feature detect here rather than always doing the conversion for certain browsers because they may fix the problem at some point in the future.

		_createSVG = function _createSVG(type, container, attributes) {
			var element = _doc.createElementNS("http://www.w3.org/2000/svg", type),
			    reg = /([a-z])([A-Z])/g,
			    p;
			for (p in attributes) {
				element.setAttributeNS(null, p.replace(reg, "$1-$2").toLowerCase(), attributes[p]);
			}
			container.appendChild(element);
			return element;
		},
		    _docElement = _doc.documentElement || {},
		    _forceSVGTransformAttr = function () {
			//IE and Android stock don't support CSS transforms on SVG elements, so we must write them to the "transform" attribute. We populate this variable in the _parseTransform() method, and only if/when we come across an SVG element
			var force = _ieVers || /Android/i.test(_agent) && !_gsScope.chrome,
			    svg,
			    rect,
			    width;
			if (_doc.createElementNS && !force) {
				//IE8 and earlier doesn't support SVG anyway
				svg = _createSVG("svg", _docElement);
				rect = _createSVG("rect", svg, { width: 100, height: 50, x: 100 });
				width = rect.getBoundingClientRect().width;
				rect.style[_transformOriginProp] = "50% 50%";
				rect.style[_transformProp] = "scaleX(0.5)";
				force = width === rect.getBoundingClientRect().width && !(_isFirefox && _supports3D); //note: Firefox fails the test even though it does support CSS transforms in 3D. Since we can't push 3D stuff into the transform attribute, we force Firefox to pass the test here (as long as it does truly support 3D).
				_docElement.removeChild(svg);
			}
			return force;
		}(),
		    _parseSVGOrigin = function _parseSVGOrigin(e, local, decoratee, absolute, smoothOrigin, skipRecord) {
			var tm = e._gsTransform,
			    m = _getMatrix(e, true),
			    v,
			    x,
			    y,
			    xOrigin,
			    yOrigin,
			    a,
			    b,
			    c,
			    d,
			    tx,
			    ty,
			    determinant,
			    xOriginOld,
			    yOriginOld;
			if (tm) {
				xOriginOld = tm.xOrigin; //record the original values before we alter them.
				yOriginOld = tm.yOrigin;
			}
			if (!absolute || (v = absolute.split(" ")).length < 2) {
				b = e.getBBox();
				if (b.x === 0 && b.y === 0 && b.width + b.height === 0) {
					//some browsers (like Firefox) misreport the bounds if the element has zero width and height (it just assumes it's at x:0, y:0), thus we need to manually grab the position in that case.
					b = { x: parseFloat(e.hasAttribute("x") ? e.getAttribute("x") : e.hasAttribute("cx") ? e.getAttribute("cx") : 0) || 0, y: parseFloat(e.hasAttribute("y") ? e.getAttribute("y") : e.hasAttribute("cy") ? e.getAttribute("cy") : 0) || 0, width: 0, height: 0 };
				}
				local = _parsePosition(local).split(" ");
				v = [(local[0].indexOf("%") !== -1 ? parseFloat(local[0]) / 100 * b.width : parseFloat(local[0])) + b.x, (local[1].indexOf("%") !== -1 ? parseFloat(local[1]) / 100 * b.height : parseFloat(local[1])) + b.y];
			}
			decoratee.xOrigin = xOrigin = parseFloat(v[0]);
			decoratee.yOrigin = yOrigin = parseFloat(v[1]);
			if (absolute && m !== _identity2DMatrix) {
				//if svgOrigin is being set, we must invert the matrix and determine where the absolute point is, factoring in the current transforms. Otherwise, the svgOrigin would be based on the element's non-transformed position on the canvas.
				a = m[0];
				b = m[1];
				c = m[2];
				d = m[3];
				tx = m[4];
				ty = m[5];
				determinant = a * d - b * c;
				if (determinant) {
					//if it's zero (like if scaleX and scaleY are zero), skip it to avoid errors with dividing by zero.
					x = xOrigin * (d / determinant) + yOrigin * (-c / determinant) + (c * ty - d * tx) / determinant;
					y = xOrigin * (-b / determinant) + yOrigin * (a / determinant) - (a * ty - b * tx) / determinant;
					xOrigin = decoratee.xOrigin = v[0] = x;
					yOrigin = decoratee.yOrigin = v[1] = y;
				}
			}
			if (tm) {
				//avoid jump when transformOrigin is changed - adjust the x/y values accordingly
				if (skipRecord) {
					decoratee.xOffset = tm.xOffset;
					decoratee.yOffset = tm.yOffset;
					tm = decoratee;
				}
				if (smoothOrigin || smoothOrigin !== false && CSSPlugin.defaultSmoothOrigin !== false) {
					x = xOrigin - xOriginOld;
					y = yOrigin - yOriginOld;
					//originally, we simply adjusted the x and y values, but that would cause problems if, for example, you created a rotational tween part-way through an x/y tween. Managing the offset in a separate variable gives us ultimate flexibility.
					//tm.x -= x - (x * m[0] + y * m[2]);
					//tm.y -= y - (x * m[1] + y * m[3]);
					tm.xOffset += x * m[0] + y * m[2] - x;
					tm.yOffset += x * m[1] + y * m[3] - y;
				} else {
					tm.xOffset = tm.yOffset = 0;
				}
			}
			if (!skipRecord) {
				e.setAttribute("data-svg-origin", v.join(" "));
			}
		},
		    _getBBoxHack = function _getBBoxHack(swapIfPossible) {
			//works around issues in some browsers (like Firefox) that don't correctly report getBBox() on SVG elements inside a <defs> element and/or <mask>. We try creating an SVG, adding it to the documentElement and toss the element in there so that it's definitely part of the rendering tree, then grab the bbox and if it works, we actually swap out the original getBBox() method for our own that does these extra steps whenever getBBox is needed. This helps ensure that performance is optimal (only do all these extra steps when absolutely necessary...most elements don't need it).
			var svg = _createElement("svg", this.ownerSVGElement && this.ownerSVGElement.getAttribute("xmlns") || "http://www.w3.org/2000/svg"),
			    oldParent = this.parentNode,
			    oldSibling = this.nextSibling,
			    oldCSS = this.style.cssText,
			    bbox;
			_docElement.appendChild(svg);
			svg.appendChild(this);
			this.style.display = "block";
			if (swapIfPossible) {
				try {
					bbox = this.getBBox();
					this._originalGetBBox = this.getBBox;
					this.getBBox = _getBBoxHack;
				} catch (e) {}
			} else if (this._originalGetBBox) {
				bbox = this._originalGetBBox();
			}
			if (oldSibling) {
				oldParent.insertBefore(this, oldSibling);
			} else {
				oldParent.appendChild(this);
			}
			_docElement.removeChild(svg);
			this.style.cssText = oldCSS;
			return bbox;
		},
		    _getBBox = function _getBBox(e) {
			try {
				return e.getBBox(); //Firefox throws errors if you try calling getBBox() on an SVG element that's not rendered (like in a <symbol> or <defs>). https://bugzilla.mozilla.org/show_bug.cgi?id=612118
			} catch (error) {
				return _getBBoxHack.call(e, true);
			}
		},
		    _isSVG = function _isSVG(e) {
			//reports if the element is an SVG on which getBBox() actually works
			return !!(_SVGElement && e.getCTM && (!e.parentNode || e.ownerSVGElement) && _getBBox(e));
		},
		    _identity2DMatrix = [1, 0, 0, 1, 0, 0],
		    _getMatrix = function _getMatrix(e, force2D) {
			var tm = e._gsTransform || new Transform(),
			    rnd = 100000,
			    style = e.style,
			    isDefault,
			    s,
			    m,
			    n,
			    dec,
			    none;
			if (_transformProp) {
				s = _getStyle(e, _transformPropCSS, null, true);
			} else if (e.currentStyle) {
				//for older versions of IE, we need to interpret the filter portion that is in the format: progid:DXImageTransform.Microsoft.Matrix(M11=6.123233995736766e-17, M12=-1, M21=1, M22=6.123233995736766e-17, sizingMethod='auto expand') Notice that we need to swap b and c compared to a normal matrix.
				s = e.currentStyle.filter.match(_ieGetMatrixExp);
				s = s && s.length === 4 ? [s[0].substr(4), Number(s[2].substr(4)), Number(s[1].substr(4)), s[3].substr(4), tm.x || 0, tm.y || 0].join(",") : "";
			}
			isDefault = !s || s === "none" || s === "matrix(1, 0, 0, 1, 0, 0)";
			if (_transformProp && ((none = !_getComputedStyle(e) || _getComputedStyle(e).display === "none") || !e.parentNode)) {
				//note: Firefox returns null for getComputedStyle() if the element is in an iframe that has display:none. https://bugzilla.mozilla.org/show_bug.cgi?id=548397
				if (none) {
					//browsers don't report transforms accurately unless the element is in the DOM and has a display value that's not "none". Firefox and Microsoft browsers have a partial bug where they'll report transforms even if display:none BUT not any percentage-based values like translate(-50%, 8px) will be reported as if it's translate(0, 8px).
					n = style.display;
					style.display = "block";
				}
				if (!e.parentNode) {
					dec = 1; //flag
					_docElement.appendChild(e);
				}
				s = _getStyle(e, _transformPropCSS, null, true);
				isDefault = !s || s === "none" || s === "matrix(1, 0, 0, 1, 0, 0)";
				if (n) {
					style.display = n;
				} else if (none) {
					_removeProp(style, "display");
				}
				if (dec) {
					_docElement.removeChild(e);
				}
			}
			if (tm.svg || e.getCTM && _isSVG(e)) {
				if (isDefault && (style[_transformProp] + "").indexOf("matrix") !== -1) {
					//some browsers (like Chrome 40) don't correctly report transforms that are applied inline on an SVG element (they don't get included in the computed style), so we double-check here and accept matrix values
					s = style[_transformProp];
					isDefault = 0;
				}
				m = e.getAttribute("transform");
				if (isDefault && m) {
					if (m.indexOf("matrix") !== -1) {
						//just in case there's a "transform" value specified as an attribute instead of CSS style. Accept either a matrix() or simple translate() value though.
						s = m;
						isDefault = 0;
					} else if (m.indexOf("translate") !== -1) {
						s = "matrix(1,0,0,1," + m.match(/(?:\-|\b)[\d\-\.e]+\b/gi).join(",") + ")";
						isDefault = 0;
					}
				}
			}
			if (isDefault) {
				return _identity2DMatrix;
			}
			//split the matrix values out into an array (m for matrix)
			m = (s || "").match(_numExp) || [];
			i = m.length;
			while (--i > -1) {
				n = Number(m[i]);
				m[i] = (dec = n - (n |= 0)) ? (dec * rnd + (dec < 0 ? -0.5 : 0.5) | 0) / rnd + n : n; //convert strings to Numbers and round to 5 decimal places to avoid issues with tiny numbers. Roughly 20x faster than Number.toFixed(). We also must make sure to round before dividing so that values like 0.9999999999 become 1 to avoid glitches in browser rendering and interpretation of flipped/rotated 3D matrices. And don't just multiply the number by rnd, floor it, and then divide by rnd because the bitwise operations max out at a 32-bit signed integer, thus it could get clipped at a relatively low value (like 22,000.00000 for example).
			}
			return force2D && m.length > 6 ? [m[0], m[1], m[4], m[5], m[12], m[13]] : m;
		},


		/**
   * Parses the transform values for an element, returning an object with x, y, z, scaleX, scaleY, scaleZ, rotation, rotationX, rotationY, skewX, and skewY properties. Note: by default (for performance reasons), all skewing is combined into skewX and rotation but skewY still has a place in the transform object so that we can record how much of the skew is attributed to skewX vs skewY. Remember, a skewY of 10 looks the same as a rotation of 10 and skewX of -10.
   * @param {!Object} t target element
   * @param {Object=} cs computed style object (optional)
   * @param {boolean=} rec if true, the transform values will be recorded to the target element's _gsTransform object, like target._gsTransform = {x:0, y:0, z:0, scaleX:1...}
   * @param {boolean=} parse if true, we'll ignore any _gsTransform values that already exist on the element, and force a reparsing of the css (calculated style)
   * @return {object} object containing all of the transform properties/values like {x:0, y:0, z:0, scaleX:1...}
   */
		_getTransform = _internals.getTransform = function (t, cs, rec, parse) {
			if (t._gsTransform && rec && !parse) {
				return t._gsTransform; //if the element already has a _gsTransform, use that. Note: some browsers don't accurately return the calculated style for the transform (particularly for SVG), so it's almost always safest to just use the values we've already applied rather than re-parsing things.
			}
			var tm = rec ? t._gsTransform || new Transform() : new Transform(),
			    invX = tm.scaleX < 0,
			    //in order to interpret things properly, we need to know if the user applied a negative scaleX previously so that we can adjust the rotation and skewX accordingly. Otherwise, if we always interpret a flipped matrix as affecting scaleY and the user only wants to tween the scaleX on multiple sequential tweens, it would keep the negative scaleY without that being the user's intent.
			min = 0.00002,
			    rnd = 100000,
			    zOrigin = _supports3D ? parseFloat(_getStyle(t, _transformOriginProp, cs, false, "0 0 0").split(" ")[2]) || tm.zOrigin || 0 : 0,
			    defaultTransformPerspective = parseFloat(CSSPlugin.defaultTransformPerspective) || 0,
			    m,
			    i,
			    scaleX,
			    scaleY,
			    rotation,
			    skewX;

			tm.svg = !!(t.getCTM && _isSVG(t));
			if (tm.svg) {
				_parseSVGOrigin(t, _getStyle(t, _transformOriginProp, cs, false, "50% 50%") + "", tm, t.getAttribute("data-svg-origin"));
				_useSVGTransformAttr = CSSPlugin.useSVGTransformAttr || _forceSVGTransformAttr;
			}
			m = _getMatrix(t);
			if (m !== _identity2DMatrix) {

				if (m.length === 16) {
					//we'll only look at these position-related 6 variables first because if x/y/z all match, it's relatively safe to assume we don't need to re-parse everything which risks losing important rotational information (like rotationX:180 plus rotationY:180 would look the same as rotation:180 - there's no way to know for sure which direction was taken based solely on the matrix3d() values)
					var a11 = m[0],
					    a21 = m[1],
					    a31 = m[2],
					    a41 = m[3],
					    a12 = m[4],
					    a22 = m[5],
					    a32 = m[6],
					    a42 = m[7],
					    a13 = m[8],
					    a23 = m[9],
					    a33 = m[10],
					    a14 = m[12],
					    a24 = m[13],
					    a34 = m[14],
					    a43 = m[11],
					    angle = Math.atan2(a32, a33),
					    t1,
					    t2,
					    t3,
					    t4,
					    cos,
					    sin;
					//we manually compensate for non-zero z component of transformOrigin to work around bugs in Safari
					if (tm.zOrigin) {
						a34 = -tm.zOrigin;
						a14 = a13 * a34 - m[12];
						a24 = a23 * a34 - m[13];
						a34 = a33 * a34 + tm.zOrigin - m[14];
					}
					//note for possible future consolidation: rotationX: Math.atan2(a32, a33), rotationY: Math.atan2(-a31, Math.sqrt(a33 * a33 + a32 * a32)), rotation: Math.atan2(a21, a11), skew: Math.atan2(a12, a22). However, it doesn't seem to be quite as reliable as the full-on backwards rotation procedure.
					tm.rotationX = angle * _RAD2DEG;
					//rotationX
					if (angle) {
						cos = Math.cos(-angle);
						sin = Math.sin(-angle);
						t1 = a12 * cos + a13 * sin;
						t2 = a22 * cos + a23 * sin;
						t3 = a32 * cos + a33 * sin;
						a13 = a12 * -sin + a13 * cos;
						a23 = a22 * -sin + a23 * cos;
						a33 = a32 * -sin + a33 * cos;
						a43 = a42 * -sin + a43 * cos;
						a12 = t1;
						a22 = t2;
						a32 = t3;
					}
					//rotationY
					angle = Math.atan2(-a31, a33);
					tm.rotationY = angle * _RAD2DEG;
					if (angle) {
						cos = Math.cos(-angle);
						sin = Math.sin(-angle);
						t1 = a11 * cos - a13 * sin;
						t2 = a21 * cos - a23 * sin;
						t3 = a31 * cos - a33 * sin;
						a23 = a21 * sin + a23 * cos;
						a33 = a31 * sin + a33 * cos;
						a43 = a41 * sin + a43 * cos;
						a11 = t1;
						a21 = t2;
						a31 = t3;
					}
					//rotationZ
					angle = Math.atan2(a21, a11);
					tm.rotation = angle * _RAD2DEG;
					if (angle) {
						cos = Math.cos(angle);
						sin = Math.sin(angle);
						t1 = a11 * cos + a21 * sin;
						t2 = a12 * cos + a22 * sin;
						t3 = a13 * cos + a23 * sin;
						a21 = a21 * cos - a11 * sin;
						a22 = a22 * cos - a12 * sin;
						a23 = a23 * cos - a13 * sin;
						a11 = t1;
						a12 = t2;
						a13 = t3;
					}

					if (tm.rotationX && Math.abs(tm.rotationX) + Math.abs(tm.rotation) > 359.9) {
						//when rotationY is set, it will often be parsed as 180 degrees different than it should be, and rotationX and rotation both being 180 (it looks the same), so we adjust for that here.
						tm.rotationX = tm.rotation = 0;
						tm.rotationY = 180 - tm.rotationY;
					}

					//skewX
					angle = Math.atan2(a12, a22);

					//scales
					tm.scaleX = (Math.sqrt(a11 * a11 + a21 * a21 + a31 * a31) * rnd + 0.5 | 0) / rnd;
					tm.scaleY = (Math.sqrt(a22 * a22 + a32 * a32) * rnd + 0.5 | 0) / rnd;
					tm.scaleZ = (Math.sqrt(a13 * a13 + a23 * a23 + a33 * a33) * rnd + 0.5 | 0) / rnd;
					a11 /= tm.scaleX;
					a12 /= tm.scaleY;
					a21 /= tm.scaleX;
					a22 /= tm.scaleY;
					if (Math.abs(angle) > min) {
						tm.skewX = angle * _RAD2DEG;
						a12 = 0; //unskews
						if (tm.skewType !== "simple") {
							tm.scaleY *= 1 / Math.cos(angle); //by default, we compensate the scale based on the skew so that the element maintains a similar proportion when skewed, so we have to alter the scaleY here accordingly to match the default (non-adjusted) skewing that CSS does (stretching more and more as it skews).
						}
					} else {
						tm.skewX = 0;
					}

					/* //for testing purposes
     var transform = "matrix3d(",
     	comma = ",",
     	zero = "0";
     a13 /= tm.scaleZ;
     a23 /= tm.scaleZ;
     a31 /= tm.scaleX;
     a32 /= tm.scaleY;
     a33 /= tm.scaleZ;
     transform += ((a11 < min && a11 > -min) ? zero : a11) + comma + ((a21 < min && a21 > -min) ? zero : a21) + comma + ((a31 < min && a31 > -min) ? zero : a31);
     transform += comma + ((a41 < min && a41 > -min) ? zero : a41) + comma + ((a12 < min && a12 > -min) ? zero : a12) + comma + ((a22 < min && a22 > -min) ? zero : a22);
     transform += comma + ((a32 < min && a32 > -min) ? zero : a32) + comma + ((a42 < min && a42 > -min) ? zero : a42) + comma + ((a13 < min && a13 > -min) ? zero : a13);
     transform += comma + ((a23 < min && a23 > -min) ? zero : a23) + comma + ((a33 < min && a33 > -min) ? zero : a33) + comma + ((a43 < min && a43 > -min) ? zero : a43) + comma;
     transform += a14 + comma + a24 + comma + a34 + comma + (tm.perspective ? (1 + (-a34 / tm.perspective)) : 1) + ")";
     console.log(transform);
     document.querySelector(".test").style[_transformProp] = transform;
     */

					tm.perspective = a43 ? 1 / (a43 < 0 ? -a43 : a43) : 0;
					tm.x = a14;
					tm.y = a24;
					tm.z = a34;
					if (tm.svg) {
						tm.x -= tm.xOrigin - (tm.xOrigin * a11 - tm.yOrigin * a12);
						tm.y -= tm.yOrigin - (tm.yOrigin * a21 - tm.xOrigin * a22);
					}
				} else if (!_supports3D || parse || !m.length || tm.x !== m[4] || tm.y !== m[5] || !tm.rotationX && !tm.rotationY) {
					//sometimes a 6-element matrix is returned even when we performed 3D transforms, like if rotationX and rotationY are 180. In cases like this, we still need to honor the 3D transforms. If we just rely on the 2D info, it could affect how the data is interpreted, like scaleY might get set to -1 or rotation could get offset by 180 degrees. For example, do a TweenLite.to(element, 1, {css:{rotationX:180, rotationY:180}}) and then later, TweenLite.to(element, 1, {css:{rotationX:0}}) and without this conditional logic in place, it'd jump to a state of being unrotated when the 2nd tween starts. Then again, we need to honor the fact that the user COULD alter the transforms outside of CSSPlugin, like by manually applying new css, so we try to sense that by looking at x and y because if those changed, we know the changes were made outside CSSPlugin and we force a reinterpretation of the matrix values. Also, in Webkit browsers, if the element's "display" is "none", its calculated style value will always return empty, so if we've already recorded the values in the _gsTransform object, we'll just rely on those.
					var k = m.length >= 6,
					    a = k ? m[0] : 1,
					    b = m[1] || 0,
					    c = m[2] || 0,
					    d = k ? m[3] : 1;
					tm.x = m[4] || 0;
					tm.y = m[5] || 0;
					scaleX = Math.sqrt(a * a + b * b);
					scaleY = Math.sqrt(d * d + c * c);
					rotation = a || b ? Math.atan2(b, a) * _RAD2DEG : tm.rotation || 0; //note: if scaleX is 0, we cannot accurately measure rotation. Same for skewX with a scaleY of 0. Therefore, we default to the previously recorded value (or zero if that doesn't exist).
					skewX = c || d ? Math.atan2(c, d) * _RAD2DEG + rotation : tm.skewX || 0;
					tm.scaleX = scaleX;
					tm.scaleY = scaleY;
					tm.rotation = rotation;
					tm.skewX = skewX;
					if (_supports3D) {
						tm.rotationX = tm.rotationY = tm.z = 0;
						tm.perspective = defaultTransformPerspective;
						tm.scaleZ = 1;
					}
					if (tm.svg) {
						tm.x -= tm.xOrigin - (tm.xOrigin * a + tm.yOrigin * c);
						tm.y -= tm.yOrigin - (tm.xOrigin * b + tm.yOrigin * d);
					}
				}
				if (Math.abs(tm.skewX) > 90 && Math.abs(tm.skewX) < 270) {
					if (invX) {
						tm.scaleX *= -1;
						tm.skewX += tm.rotation <= 0 ? 180 : -180;
						tm.rotation += tm.rotation <= 0 ? 180 : -180;
					} else {
						tm.scaleY *= -1;
						tm.skewX += tm.skewX <= 0 ? 180 : -180;
					}
				}
				tm.zOrigin = zOrigin;
				//some browsers have a hard time with very small values like 2.4492935982947064e-16 (notice the "e-" towards the end) and would render the object slightly off. So we round to 0 in these cases. The conditional logic here is faster than calling Math.abs(). Also, browsers tend to render a SLIGHTLY rotated object in a fuzzy way, so we need to snap to exactly 0 when appropriate.
				for (i in tm) {
					if (tm[i] < min) if (tm[i] > -min) {
						tm[i] = 0;
					}
				}
			}
			//DEBUG: _log("parsed rotation of " + t.getAttribute("id")+": "+(tm.rotationX)+", "+(tm.rotationY)+", "+(tm.rotation)+", scale: "+tm.scaleX+", "+tm.scaleY+", "+tm.scaleZ+", position: "+tm.x+", "+tm.y+", "+tm.z+", perspective: "+tm.perspective+ ", origin: "+ tm.xOrigin+ ","+ tm.yOrigin);
			if (rec) {
				t._gsTransform = tm; //record to the object's _gsTransform which we use so that tweens can control individual properties independently (we need all the properties to accurately recompose the matrix in the setRatio() method)
				if (tm.svg) {
					//if we're supposed to apply transforms to the SVG element's "transform" attribute, make sure there aren't any CSS transforms applied or they'll override the attribute ones. Also clear the transform attribute if we're using CSS, just to be clean.
					if (_useSVGTransformAttr && t.style[_transformProp]) {
						TweenLite.delayedCall(0.001, function () {
							//if we apply this right away (before anything has rendered), we risk there being no transforms for a brief moment and it also interferes with adjusting the transformOrigin in a tween with immediateRender:true (it'd try reading the matrix and it wouldn't have the appropriate data in place because we just removed it).
							_removeProp(t.style, _transformProp);
						});
					} else if (!_useSVGTransformAttr && t.getAttribute("transform")) {
						TweenLite.delayedCall(0.001, function () {
							t.removeAttribute("transform");
						});
					}
				}
			}
			return tm;
		},


		//for setting 2D transforms in IE6, IE7, and IE8 (must use a "filter" to emulate the behavior of modern day browser transforms)
		_setIETransformRatio = function _setIETransformRatio(v) {
			var t = this.data,
			    //refers to the element's _gsTransform object
			ang = -t.rotation * _DEG2RAD,
			    skew = ang + t.skewX * _DEG2RAD,
			    rnd = 100000,
			    a = (Math.cos(ang) * t.scaleX * rnd | 0) / rnd,
			    b = (Math.sin(ang) * t.scaleX * rnd | 0) / rnd,
			    c = (Math.sin(skew) * -t.scaleY * rnd | 0) / rnd,
			    d = (Math.cos(skew) * t.scaleY * rnd | 0) / rnd,
			    style = this.t.style,
			    cs = this.t.currentStyle,
			    filters,
			    val;
			if (!cs) {
				return;
			}
			val = b; //just for swapping the variables an inverting them (reused "val" to avoid creating another variable in memory). IE's filter matrix uses a non-standard matrix configuration (angle goes the opposite way, and b and c are reversed and inverted)
			b = -c;
			c = -val;
			filters = cs.filter;
			style.filter = ""; //remove filters so that we can accurately measure offsetWidth/offsetHeight
			var w = this.t.offsetWidth,
			    h = this.t.offsetHeight,
			    clip = cs.position !== "absolute",
			    m = "progid:DXImageTransform.Microsoft.Matrix(M11=" + a + ", M12=" + b + ", M21=" + c + ", M22=" + d,
			    ox = t.x + w * t.xPercent / 100,
			    oy = t.y + h * t.yPercent / 100,
			    dx,
			    dy;

			//if transformOrigin is being used, adjust the offset x and y
			if (t.ox != null) {
				dx = (t.oxp ? w * t.ox * 0.01 : t.ox) - w / 2;
				dy = (t.oyp ? h * t.oy * 0.01 : t.oy) - h / 2;
				ox += dx - (dx * a + dy * b);
				oy += dy - (dx * c + dy * d);
			}

			if (!clip) {
				m += ", sizingMethod='auto expand')";
			} else {
				dx = w / 2;
				dy = h / 2;
				//translate to ensure that transformations occur around the correct origin (default is center).
				m += ", Dx=" + (dx - (dx * a + dy * b) + ox) + ", Dy=" + (dy - (dx * c + dy * d) + oy) + ")";
			}
			if (filters.indexOf("DXImageTransform.Microsoft.Matrix(") !== -1) {
				style.filter = filters.replace(_ieSetMatrixExp, m);
			} else {
				style.filter = m + " " + filters; //we must always put the transform/matrix FIRST (before alpha(opacity=xx)) to avoid an IE bug that slices part of the object when rotation is applied with alpha.
			}

			//at the end or beginning of the tween, if the matrix is normal (1, 0, 0, 1) and opacity is 100 (or doesn't exist), remove the filter to improve browser performance.
			if (v === 0 || v === 1) if (a === 1) if (b === 0) if (c === 0) if (d === 1) if (!clip || m.indexOf("Dx=0, Dy=0") !== -1) if (!_opacityExp.test(filters) || parseFloat(RegExp.$1) === 100) if (filters.indexOf("gradient(" && filters.indexOf("Alpha")) === -1) {
				style.removeAttribute("filter");
			}

			//we must set the margins AFTER applying the filter in order to avoid some bugs in IE8 that could (in rare scenarios) cause them to be ignored intermittently (vibration).
			if (!clip) {
				var mult = _ieVers < 8 ? 1 : -1,
				    //in Internet Explorer 7 and before, the box model is broken, causing the browser to treat the width/height of the actual rotated filtered image as the width/height of the box itself, but Microsoft corrected that in IE8. We must use a negative offset in IE8 on the right/bottom
				marg,
				    prop,
				    dif;
				dx = t.ieOffsetX || 0;
				dy = t.ieOffsetY || 0;
				t.ieOffsetX = Math.round((w - ((a < 0 ? -a : a) * w + (b < 0 ? -b : b) * h)) / 2 + ox);
				t.ieOffsetY = Math.round((h - ((d < 0 ? -d : d) * h + (c < 0 ? -c : c) * w)) / 2 + oy);
				for (i = 0; i < 4; i++) {
					prop = _margins[i];
					marg = cs[prop];
					//we need to get the current margin in case it is being tweened separately (we want to respect that tween's changes)
					val = marg.indexOf("px") !== -1 ? parseFloat(marg) : _convertToPixels(this.t, prop, parseFloat(marg), marg.replace(_suffixExp, "")) || 0;
					if (val !== t[prop]) {
						dif = i < 2 ? -t.ieOffsetX : -t.ieOffsetY; //if another tween is controlling a margin, we cannot only apply the difference in the ieOffsets, so we essentially zero-out the dx and dy here in that case. We record the margin(s) later so that we can keep comparing them, making this code very flexible.
					} else {
						dif = i < 2 ? dx - t.ieOffsetX : dy - t.ieOffsetY;
					}
					style[prop] = (t[prop] = Math.round(val - dif * (i === 0 || i === 2 ? 1 : mult))) + "px";
				}
			}
		},


		/* translates a super small decimal to a string WITHOUT scientific notation
  _safeDecimal = function(n) {
  	var s = (n < 0 ? -n : n) + "",
  		a = s.split("e-");
  	return (n < 0 ? "-0." : "0.") + new Array(parseInt(a[1], 10) || 0).join("0") + a[0].split(".").join("");
  },
  */

		_setTransformRatio = _internals.set3DTransformRatio = _internals.setTransformRatio = function (v) {
			var t = this.data,
			    //refers to the element's _gsTransform object
			style = this.t.style,
			    angle = t.rotation,
			    rotationX = t.rotationX,
			    rotationY = t.rotationY,
			    sx = t.scaleX,
			    sy = t.scaleY,
			    sz = t.scaleZ,
			    x = t.x,
			    y = t.y,
			    z = t.z,
			    isSVG = t.svg,
			    perspective = t.perspective,
			    force3D = t.force3D,
			    skewY = t.skewY,
			    skewX = t.skewX,
			    t1,
			    a11,
			    a12,
			    a13,
			    a21,
			    a22,
			    a23,
			    a31,
			    a32,
			    a33,
			    a41,
			    a42,
			    a43,
			    zOrigin,
			    min,
			    cos,
			    sin,
			    t2,
			    transform,
			    comma,
			    zero,
			    skew,
			    rnd;
			if (skewY) {
				//for performance reasons, we combine all skewing into the skewX and rotation values. Remember, a skewY of 10 degrees looks the same as a rotation of 10 degrees plus a skewX of 10 degrees.
				skewX += skewY;
				angle += skewY;
			}

			//check to see if we should render as 2D (and SVGs must use 2D when _useSVGTransformAttr is true)
			if (((v === 1 || v === 0) && force3D === "auto" && (this.tween._totalTime === this.tween._totalDuration || !this.tween._totalTime) || !force3D) && !z && !perspective && !rotationY && !rotationX && sz === 1 || _useSVGTransformAttr && isSVG || !_supports3D) {
				//on the final render (which could be 0 for a from tween), if there are no 3D aspects, render in 2D to free up memory and improve performance especially on mobile devices. Check the tween's totalTime/totalDuration too in order to make sure it doesn't happen between repeats if it's a repeating tween.

				//2D
				if (angle || skewX || isSVG) {
					angle *= _DEG2RAD;
					skew = skewX * _DEG2RAD;
					rnd = 100000;
					a11 = Math.cos(angle) * sx;
					a21 = Math.sin(angle) * sx;
					a12 = Math.sin(angle - skew) * -sy;
					a22 = Math.cos(angle - skew) * sy;
					if (skew && t.skewType === "simple") {
						//by default, we compensate skewing on the other axis to make it look more natural, but you can set the skewType to "simple" to use the uncompensated skewing that CSS does
						t1 = Math.tan(skew - skewY * _DEG2RAD);
						t1 = Math.sqrt(1 + t1 * t1);
						a12 *= t1;
						a22 *= t1;
						if (skewY) {
							t1 = Math.tan(skewY * _DEG2RAD);
							t1 = Math.sqrt(1 + t1 * t1);
							a11 *= t1;
							a21 *= t1;
						}
					}
					if (isSVG) {
						x += t.xOrigin - (t.xOrigin * a11 + t.yOrigin * a12) + t.xOffset;
						y += t.yOrigin - (t.xOrigin * a21 + t.yOrigin * a22) + t.yOffset;
						if (_useSVGTransformAttr && (t.xPercent || t.yPercent)) {
							//The SVG spec doesn't support percentage-based translation in the "transform" attribute, so we merge it into the matrix to simulate it.
							min = this.t.getBBox();
							x += t.xPercent * 0.01 * min.width;
							y += t.yPercent * 0.01 * min.height;
						}
						min = 0.000001;
						if (x < min) if (x > -min) {
							x = 0;
						}
						if (y < min) if (y > -min) {
							y = 0;
						}
					}
					transform = (a11 * rnd | 0) / rnd + "," + (a21 * rnd | 0) / rnd + "," + (a12 * rnd | 0) / rnd + "," + (a22 * rnd | 0) / rnd + "," + x + "," + y + ")";
					if (isSVG && _useSVGTransformAttr) {
						this.t.setAttribute("transform", "matrix(" + transform);
					} else {
						//some browsers have a hard time with very small values like 2.4492935982947064e-16 (notice the "e-" towards the end) and would render the object slightly off. So we round to 5 decimal places.
						style[_transformProp] = (t.xPercent || t.yPercent ? "translate(" + t.xPercent + "%," + t.yPercent + "%) matrix(" : "matrix(") + transform;
					}
				} else {
					style[_transformProp] = (t.xPercent || t.yPercent ? "translate(" + t.xPercent + "%," + t.yPercent + "%) matrix(" : "matrix(") + sx + ",0,0," + sy + "," + x + "," + y + ")";
				}
				return;
			}
			if (_isFirefox) {
				//Firefox has a bug (at least in v25) that causes it to render the transparent part of 32-bit PNG images as black when displayed inside an iframe and the 3D scale is very small and doesn't change sufficiently enough between renders (like if you use a Power4.easeInOut to scale from 0 to 1 where the beginning values only change a tiny amount to begin the tween before accelerating). In this case, we force the scale to be 0.00002 instead which is visually the same but works around the Firefox issue.
				min = 0.0001;
				if (sx < min && sx > -min) {
					sx = sz = 0.00002;
				}
				if (sy < min && sy > -min) {
					sy = sz = 0.00002;
				}
				if (perspective && !t.z && !t.rotationX && !t.rotationY) {
					//Firefox has a bug that causes elements to have an odd super-thin, broken/dotted black border on elements that have a perspective set but aren't utilizing 3D space (no rotationX, rotationY, or z).
					perspective = 0;
				}
			}
			if (angle || skewX) {
				angle *= _DEG2RAD;
				cos = a11 = Math.cos(angle);
				sin = a21 = Math.sin(angle);
				if (skewX) {
					angle -= skewX * _DEG2RAD;
					cos = Math.cos(angle);
					sin = Math.sin(angle);
					if (t.skewType === "simple") {
						//by default, we compensate skewing on the other axis to make it look more natural, but you can set the skewType to "simple" to use the uncompensated skewing that CSS does
						t1 = Math.tan((skewX - skewY) * _DEG2RAD);
						t1 = Math.sqrt(1 + t1 * t1);
						cos *= t1;
						sin *= t1;
						if (t.skewY) {
							t1 = Math.tan(skewY * _DEG2RAD);
							t1 = Math.sqrt(1 + t1 * t1);
							a11 *= t1;
							a21 *= t1;
						}
					}
				}
				a12 = -sin;
				a22 = cos;
			} else if (!rotationY && !rotationX && sz === 1 && !perspective && !isSVG) {
				//if we're only translating and/or 2D scaling, this is faster...
				style[_transformProp] = (t.xPercent || t.yPercent ? "translate(" + t.xPercent + "%," + t.yPercent + "%) translate3d(" : "translate3d(") + x + "px," + y + "px," + z + "px)" + (sx !== 1 || sy !== 1 ? " scale(" + sx + "," + sy + ")" : "");
				return;
			} else {
				a11 = a22 = 1;
				a12 = a21 = 0;
			}
			// KEY  INDEX   AFFECTS a[row][column]
			// a11  0       rotation, rotationY, scaleX
			// a21  1       rotation, rotationY, scaleX
			// a31  2       rotationY, scaleX
			// a41  3       rotationY, scaleX
			// a12  4       rotation, skewX, rotationX, scaleY
			// a22  5       rotation, skewX, rotationX, scaleY
			// a32  6       rotationX, scaleY
			// a42  7       rotationX, scaleY
			// a13  8       rotationY, rotationX, scaleZ
			// a23  9       rotationY, rotationX, scaleZ
			// a33  10      rotationY, rotationX, scaleZ
			// a43  11      rotationY, rotationX, perspective, scaleZ
			// a14  12      x, zOrigin, svgOrigin
			// a24  13      y, zOrigin, svgOrigin
			// a34  14      z, zOrigin
			// a44  15
			// rotation: Math.atan2(a21, a11)
			// rotationY: Math.atan2(a13, a33) (or Math.atan2(a13, a11))
			// rotationX: Math.atan2(a32, a33)
			a33 = 1;
			a13 = a23 = a31 = a32 = a41 = a42 = 0;
			a43 = perspective ? -1 / perspective : 0;
			zOrigin = t.zOrigin;
			min = 0.000001; //threshold below which browsers use scientific notation which won't work.
			comma = ",";
			zero = "0";
			angle = rotationY * _DEG2RAD;
			if (angle) {
				cos = Math.cos(angle);
				sin = Math.sin(angle);
				a31 = -sin;
				a41 = a43 * -sin;
				a13 = a11 * sin;
				a23 = a21 * sin;
				a33 = cos;
				a43 *= cos;
				a11 *= cos;
				a21 *= cos;
			}
			angle = rotationX * _DEG2RAD;
			if (angle) {
				cos = Math.cos(angle);
				sin = Math.sin(angle);
				t1 = a12 * cos + a13 * sin;
				t2 = a22 * cos + a23 * sin;
				a32 = a33 * sin;
				a42 = a43 * sin;
				a13 = a12 * -sin + a13 * cos;
				a23 = a22 * -sin + a23 * cos;
				a33 = a33 * cos;
				a43 = a43 * cos;
				a12 = t1;
				a22 = t2;
			}
			if (sz !== 1) {
				a13 *= sz;
				a23 *= sz;
				a33 *= sz;
				a43 *= sz;
			}
			if (sy !== 1) {
				a12 *= sy;
				a22 *= sy;
				a32 *= sy;
				a42 *= sy;
			}
			if (sx !== 1) {
				a11 *= sx;
				a21 *= sx;
				a31 *= sx;
				a41 *= sx;
			}

			if (zOrigin || isSVG) {
				if (zOrigin) {
					x += a13 * -zOrigin;
					y += a23 * -zOrigin;
					z += a33 * -zOrigin + zOrigin;
				}
				if (isSVG) {
					//due to bugs in some browsers, we need to manage the transform-origin of SVG manually
					x += t.xOrigin - (t.xOrigin * a11 + t.yOrigin * a12) + t.xOffset;
					y += t.yOrigin - (t.xOrigin * a21 + t.yOrigin * a22) + t.yOffset;
				}
				if (x < min && x > -min) {
					x = zero;
				}
				if (y < min && y > -min) {
					y = zero;
				}
				if (z < min && z > -min) {
					z = 0; //don't use string because we calculate perspective later and need the number.
				}
			}

			//optimized way of concatenating all the values into a string. If we do it all in one shot, it's slower because of the way browsers have to create temp strings and the way it affects memory. If we do it piece-by-piece with +=, it's a bit slower too. We found that doing it in these sized chunks works best overall:
			transform = t.xPercent || t.yPercent ? "translate(" + t.xPercent + "%," + t.yPercent + "%) matrix3d(" : "matrix3d(";
			transform += (a11 < min && a11 > -min ? zero : a11) + comma + (a21 < min && a21 > -min ? zero : a21) + comma + (a31 < min && a31 > -min ? zero : a31);
			transform += comma + (a41 < min && a41 > -min ? zero : a41) + comma + (a12 < min && a12 > -min ? zero : a12) + comma + (a22 < min && a22 > -min ? zero : a22);
			if (rotationX || rotationY || sz !== 1) {
				//performance optimization (often there's no rotationX or rotationY, so we can skip these calculations)
				transform += comma + (a32 < min && a32 > -min ? zero : a32) + comma + (a42 < min && a42 > -min ? zero : a42) + comma + (a13 < min && a13 > -min ? zero : a13);
				transform += comma + (a23 < min && a23 > -min ? zero : a23) + comma + (a33 < min && a33 > -min ? zero : a33) + comma + (a43 < min && a43 > -min ? zero : a43) + comma;
			} else {
				transform += ",0,0,0,0,1,0,";
			}
			transform += x + comma + y + comma + z + comma + (perspective ? 1 + -z / perspective : 1) + ")";

			style[_transformProp] = transform;
		};

		p = Transform.prototype;
		p.x = p.y = p.z = p.skewX = p.skewY = p.rotation = p.rotationX = p.rotationY = p.zOrigin = p.xPercent = p.yPercent = p.xOffset = p.yOffset = 0;
		p.scaleX = p.scaleY = p.scaleZ = 1;

		_registerComplexSpecialProp("transform,scale,scaleX,scaleY,scaleZ,x,y,z,rotation,rotationX,rotationY,rotationZ,skewX,skewY,shortRotation,shortRotationX,shortRotationY,shortRotationZ,transformOrigin,svgOrigin,transformPerspective,directionalRotation,parseTransform,force3D,skewType,xPercent,yPercent,smoothOrigin", { parser: function parser(t, e, parsingProp, cssp, pt, plugin, vars) {
				if (cssp._lastParsedTransform === vars) {
					return pt;
				} //only need to parse the transform once, and only if the browser supports it.
				cssp._lastParsedTransform = vars;
				var scaleFunc = vars.scale && typeof vars.scale === "function" ? vars.scale : 0,
				    //if there's a function-based "scale" value, swap in the resulting numeric value temporarily. Otherwise, if it's called for both scaleX and scaleY independently, they may not match (like if the function uses Math.random()).
				swapFunc;
				if (typeof vars[parsingProp] === "function") {
					//whatever property triggers the initial parsing might be a function-based value in which case it already got called in parse(), thus we don't want to call it again in here. The most efficient way to avoid this is to temporarily swap the value directly into the vars object, and then after we do all our parsing in this function, we'll swap it back again.
					swapFunc = vars[parsingProp];
					vars[parsingProp] = e;
				}
				if (scaleFunc) {
					vars.scale = scaleFunc(_index, t);
				}
				var originalGSTransform = t._gsTransform,
				    style = t.style,
				    min = 0.000001,
				    i = _transformProps.length,
				    v = vars,
				    endRotations = {},
				    transformOriginString = "transformOrigin",
				    m1 = _getTransform(t, _cs, true, v.parseTransform),
				    orig = v.transform && (typeof v.transform === "function" ? v.transform(_index, _target) : v.transform),
				    m2,
				    copy,
				    has3D,
				    hasChange,
				    dr,
				    x,
				    y,
				    matrix,
				    p;
				m1.skewType = v.skewType || m1.skewType || CSSPlugin.defaultSkewType;
				cssp._transform = m1;
				if (orig && typeof orig === "string" && _transformProp) {
					//for values like transform:"rotate(60deg) scale(0.5, 0.8)"
					copy = _tempDiv.style; //don't use the original target because it might be SVG in which case some browsers don't report computed style correctly.
					copy[_transformProp] = orig;
					copy.display = "block"; //if display is "none", the browser often refuses to report the transform properties correctly.
					copy.position = "absolute";
					_doc.body.appendChild(_tempDiv);
					m2 = _getTransform(_tempDiv, null, false);
					if (m1.skewType === "simple") {
						//the default _getTransform() reports the skewX/scaleY as if skewType is "compensated", thus we need to adjust that here if skewType is "simple".
						m2.scaleY *= Math.cos(m2.skewX * _DEG2RAD);
					}
					if (m1.svg) {
						//if it's an SVG element, x/y part of the matrix will be affected by whatever we use as the origin and the offsets, so compensate here...
						x = m1.xOrigin;
						y = m1.yOrigin;
						m2.x -= m1.xOffset;
						m2.y -= m1.yOffset;
						if (v.transformOrigin || v.svgOrigin) {
							//if this tween is altering the origin, we must factor that in here. The actual work of recording the transformOrigin values and setting up the PropTween is done later (still inside this function) so we cannot leave the changes intact here - we only want to update the x/y accordingly.
							orig = {};
							_parseSVGOrigin(t, _parsePosition(v.transformOrigin), orig, v.svgOrigin, v.smoothOrigin, true);
							x = orig.xOrigin;
							y = orig.yOrigin;
							m2.x -= orig.xOffset - m1.xOffset;
							m2.y -= orig.yOffset - m1.yOffset;
						}
						if (x || y) {
							matrix = _getMatrix(_tempDiv, true);
							m2.x -= x - (x * matrix[0] + y * matrix[2]);
							m2.y -= y - (x * matrix[1] + y * matrix[3]);
						}
					}
					_doc.body.removeChild(_tempDiv);
					if (!m2.perspective) {
						m2.perspective = m1.perspective; //tweening to no perspective gives very unintuitive results - just keep the same perspective in that case.
					}
					if (v.xPercent != null) {
						m2.xPercent = _parseVal(v.xPercent, m1.xPercent);
					}
					if (v.yPercent != null) {
						m2.yPercent = _parseVal(v.yPercent, m1.yPercent);
					}
				} else if ((typeof v === "undefined" ? "undefined" : _typeof(v)) === "object") {
					//for values like scaleX, scaleY, rotation, x, y, skewX, and skewY or transform:{...} (object)
					m2 = { scaleX: _parseVal(v.scaleX != null ? v.scaleX : v.scale, m1.scaleX),
						scaleY: _parseVal(v.scaleY != null ? v.scaleY : v.scale, m1.scaleY),
						scaleZ: _parseVal(v.scaleZ, m1.scaleZ),
						x: _parseVal(v.x, m1.x),
						y: _parseVal(v.y, m1.y),
						z: _parseVal(v.z, m1.z),
						xPercent: _parseVal(v.xPercent, m1.xPercent),
						yPercent: _parseVal(v.yPercent, m1.yPercent),
						perspective: _parseVal(v.transformPerspective, m1.perspective) };
					dr = v.directionalRotation;
					if (dr != null) {
						if ((typeof dr === "undefined" ? "undefined" : _typeof(dr)) === "object") {
							for (copy in dr) {
								v[copy] = dr[copy];
							}
						} else {
							v.rotation = dr;
						}
					}
					if (typeof v.x === "string" && v.x.indexOf("%") !== -1) {
						m2.x = 0;
						m2.xPercent = _parseVal(v.x, m1.xPercent);
					}
					if (typeof v.y === "string" && v.y.indexOf("%") !== -1) {
						m2.y = 0;
						m2.yPercent = _parseVal(v.y, m1.yPercent);
					}

					m2.rotation = _parseAngle("rotation" in v ? v.rotation : "shortRotation" in v ? v.shortRotation + "_short" : "rotationZ" in v ? v.rotationZ : m1.rotation, m1.rotation, "rotation", endRotations);
					if (_supports3D) {
						m2.rotationX = _parseAngle("rotationX" in v ? v.rotationX : "shortRotationX" in v ? v.shortRotationX + "_short" : m1.rotationX || 0, m1.rotationX, "rotationX", endRotations);
						m2.rotationY = _parseAngle("rotationY" in v ? v.rotationY : "shortRotationY" in v ? v.shortRotationY + "_short" : m1.rotationY || 0, m1.rotationY, "rotationY", endRotations);
					}
					m2.skewX = _parseAngle(v.skewX, m1.skewX);
					m2.skewY = _parseAngle(v.skewY, m1.skewY);
				}
				if (_supports3D && v.force3D != null) {
					m1.force3D = v.force3D;
					hasChange = true;
				}

				has3D = m1.force3D || m1.z || m1.rotationX || m1.rotationY || m2.z || m2.rotationX || m2.rotationY || m2.perspective;
				if (!has3D && v.scale != null) {
					m2.scaleZ = 1; //no need to tween scaleZ.
				}

				while (--i > -1) {
					p = _transformProps[i];
					orig = m2[p] - m1[p];
					if (orig > min || orig < -min || v[p] != null || _forcePT[p] != null) {
						hasChange = true;
						pt = new CSSPropTween(m1, p, m1[p], orig, pt);
						if (p in endRotations) {
							pt.e = endRotations[p]; //directional rotations typically have compensated values during the tween, but we need to make sure they end at exactly what the user requested
						}
						pt.xs0 = 0; //ensures the value stays numeric in setRatio()
						pt.plugin = plugin;
						cssp._overwriteProps.push(pt.n);
					}
				}

				orig = v.transformOrigin;
				if (m1.svg && (orig || v.svgOrigin)) {
					x = m1.xOffset; //when we change the origin, in order to prevent things from jumping we adjust the x/y so we must record those here so that we can create PropTweens for them and flip them at the same time as the origin
					y = m1.yOffset;
					_parseSVGOrigin(t, _parsePosition(orig), m2, v.svgOrigin, v.smoothOrigin);
					pt = _addNonTweeningNumericPT(m1, "xOrigin", (originalGSTransform ? m1 : m2).xOrigin, m2.xOrigin, pt, transformOriginString); //note: if there wasn't a transformOrigin defined yet, just start with the destination one; it's wasteful otherwise, and it causes problems with fromTo() tweens. For example, TweenLite.to("#wheel", 3, {rotation:180, transformOrigin:"50% 50%", delay:1}); TweenLite.fromTo("#wheel", 3, {scale:0.5, transformOrigin:"50% 50%"}, {scale:1, delay:2}); would cause a jump when the from values revert at the beginning of the 2nd tween.
					pt = _addNonTweeningNumericPT(m1, "yOrigin", (originalGSTransform ? m1 : m2).yOrigin, m2.yOrigin, pt, transformOriginString);
					if (x !== m1.xOffset || y !== m1.yOffset) {
						pt = _addNonTweeningNumericPT(m1, "xOffset", originalGSTransform ? x : m1.xOffset, m1.xOffset, pt, transformOriginString);
						pt = _addNonTweeningNumericPT(m1, "yOffset", originalGSTransform ? y : m1.yOffset, m1.yOffset, pt, transformOriginString);
					}
					orig = "0px 0px"; //certain browsers (like firefox) completely botch transform-origin, so we must remove it to prevent it from contaminating transforms. We manage it ourselves with xOrigin and yOrigin
				}
				if (orig || _supports3D && has3D && m1.zOrigin) {
					//if anything 3D is happening and there's a transformOrigin with a z component that's non-zero, we must ensure that the transformOrigin's z-component is set to 0 so that we can manually do those calculations to get around Safari bugs. Even if the user didn't specifically define a "transformOrigin" in this particular tween (maybe they did it via css directly).
					if (_transformProp) {
						hasChange = true;
						p = _transformOriginProp;
						orig = (orig || _getStyle(t, p, _cs, false, "50% 50%")) + ""; //cast as string to avoid errors
						pt = new CSSPropTween(style, p, 0, 0, pt, -1, transformOriginString);
						pt.b = style[p];
						pt.plugin = plugin;
						if (_supports3D) {
							copy = m1.zOrigin;
							orig = orig.split(" ");
							m1.zOrigin = (orig.length > 2 && !(copy !== 0 && orig[2] === "0px") ? parseFloat(orig[2]) : copy) || 0; //Safari doesn't handle the z part of transformOrigin correctly, so we'll manually handle it in the _set3DTransformRatio() method.
							pt.xs0 = pt.e = orig[0] + " " + (orig[1] || "50%") + " 0px"; //we must define a z value of 0px specifically otherwise iOS 5 Safari will stick with the old one (if one was defined)!
							pt = new CSSPropTween(m1, "zOrigin", 0, 0, pt, -1, pt.n); //we must create a CSSPropTween for the _gsTransform.zOrigin so that it gets reset properly at the beginning if the tween runs backward (as opposed to just setting m1.zOrigin here)
							pt.b = copy;
							pt.xs0 = pt.e = m1.zOrigin;
						} else {
							pt.xs0 = pt.e = orig;
						}

						//for older versions of IE (6-8), we need to manually calculate things inside the setRatio() function. We record origin x and y (ox and oy) and whether or not the values are percentages (oxp and oyp).
					} else {
						_parsePosition(orig + "", m1);
					}
				}
				if (hasChange) {
					cssp._transformType = !(m1.svg && _useSVGTransformAttr) && (has3D || this._transformType === 3) ? 3 : 2; //quicker than calling cssp._enableTransforms();
				}
				if (swapFunc) {
					vars[parsingProp] = swapFunc;
				}
				if (scaleFunc) {
					vars.scale = scaleFunc;
				}
				return pt;
			}, prefix: true });

		_registerComplexSpecialProp("boxShadow", { defaultValue: "0px 0px 0px 0px #999", prefix: true, color: true, multi: true, keyword: "inset" });

		_registerComplexSpecialProp("borderRadius", { defaultValue: "0px", parser: function parser(t, e, p, cssp, pt, plugin) {
				e = this.format(e);
				var props = ["borderTopLeftRadius", "borderTopRightRadius", "borderBottomRightRadius", "borderBottomLeftRadius"],
				    style = t.style,
				    ea1,
				    i,
				    es2,
				    bs2,
				    bs,
				    es,
				    bn,
				    en,
				    w,
				    h,
				    esfx,
				    bsfx,
				    rel,
				    hn,
				    vn,
				    em;
				w = parseFloat(t.offsetWidth);
				h = parseFloat(t.offsetHeight);
				ea1 = e.split(" ");
				for (i = 0; i < props.length; i++) {
					//if we're dealing with percentages, we must convert things separately for the horizontal and vertical axis!
					if (this.p.indexOf("border")) {
						//older browsers used a prefix
						props[i] = _checkPropPrefix(props[i]);
					}
					bs = bs2 = _getStyle(t, props[i], _cs, false, "0px");
					if (bs.indexOf(" ") !== -1) {
						bs2 = bs.split(" ");
						bs = bs2[0];
						bs2 = bs2[1];
					}
					es = es2 = ea1[i];
					bn = parseFloat(bs);
					bsfx = bs.substr((bn + "").length);
					rel = es.charAt(1) === "=";
					if (rel) {
						en = parseInt(es.charAt(0) + "1", 10);
						es = es.substr(2);
						en *= parseFloat(es);
						esfx = es.substr((en + "").length - (en < 0 ? 1 : 0)) || "";
					} else {
						en = parseFloat(es);
						esfx = es.substr((en + "").length);
					}
					if (esfx === "") {
						esfx = _suffixMap[p] || bsfx;
					}
					if (esfx !== bsfx) {
						hn = _convertToPixels(t, "borderLeft", bn, bsfx); //horizontal number (we use a bogus "borderLeft" property just because the _convertToPixels() method searches for the keywords "Left", "Right", "Top", and "Bottom" to determine of it's a horizontal or vertical property, and we need "border" in the name so that it knows it should measure relative to the element itself, not its parent.
						vn = _convertToPixels(t, "borderTop", bn, bsfx); //vertical number
						if (esfx === "%") {
							bs = hn / w * 100 + "%";
							bs2 = vn / h * 100 + "%";
						} else if (esfx === "em") {
							em = _convertToPixels(t, "borderLeft", 1, "em");
							bs = hn / em + "em";
							bs2 = vn / em + "em";
						} else {
							bs = hn + "px";
							bs2 = vn + "px";
						}
						if (rel) {
							es = parseFloat(bs) + en + esfx;
							es2 = parseFloat(bs2) + en + esfx;
						}
					}
					pt = _parseComplex(style, props[i], bs + " " + bs2, es + " " + es2, false, "0px", pt);
				}
				return pt;
			}, prefix: true, formatter: _getFormatter("0px 0px 0px 0px", false, true) });
		_registerComplexSpecialProp("borderBottomLeftRadius,borderBottomRightRadius,borderTopLeftRadius,borderTopRightRadius", { defaultValue: "0px", parser: function parser(t, e, p, cssp, pt, plugin) {
				return _parseComplex(t.style, p, this.format(_getStyle(t, p, _cs, false, "0px 0px")), this.format(e), false, "0px", pt);
			}, prefix: true, formatter: _getFormatter("0px 0px", false, true) });
		_registerComplexSpecialProp("backgroundPosition", { defaultValue: "0 0", parser: function parser(t, e, p, cssp, pt, plugin) {
				var bp = "background-position",
				    cs = _cs || _getComputedStyle(t, null),
				    bs = this.format((cs ? _ieVers ? cs.getPropertyValue(bp + "-x") + " " + cs.getPropertyValue(bp + "-y") : cs.getPropertyValue(bp) : t.currentStyle.backgroundPositionX + " " + t.currentStyle.backgroundPositionY) || "0 0"),
				    //Internet Explorer doesn't report background-position correctly - we must query background-position-x and background-position-y and combine them (even in IE10). Before IE9, we must do the same with the currentStyle object and use camelCase
				es = this.format(e),
				    ba,
				    ea,
				    i,
				    pct,
				    overlap,
				    src;
				if (bs.indexOf("%") !== -1 !== (es.indexOf("%") !== -1) && es.split(",").length < 2) {
					src = _getStyle(t, "backgroundImage").replace(_urlExp, "");
					if (src && src !== "none") {
						ba = bs.split(" ");
						ea = es.split(" ");
						_tempImg.setAttribute("src", src); //set the temp IMG's src to the background-image so that we can measure its width/height
						i = 2;
						while (--i > -1) {
							bs = ba[i];
							pct = bs.indexOf("%") !== -1;
							if (pct !== (ea[i].indexOf("%") !== -1)) {
								overlap = i === 0 ? t.offsetWidth - _tempImg.width : t.offsetHeight - _tempImg.height;
								ba[i] = pct ? parseFloat(bs) / 100 * overlap + "px" : parseFloat(bs) / overlap * 100 + "%";
							}
						}
						bs = ba.join(" ");
					}
				}
				return this.parseComplex(t.style, bs, es, pt, plugin);
			}, formatter: _parsePosition });
		_registerComplexSpecialProp("backgroundSize", { defaultValue: "0 0", formatter: function formatter(v) {
				v += ""; //ensure it's a string
				return _parsePosition(v.indexOf(" ") === -1 ? v + " " + v : v); //if set to something like "100% 100%", Safari typically reports the computed style as just "100%" (no 2nd value), but we should ensure that there are two values, so copy the first one. Otherwise, it'd be interpreted as "100% 0" (wrong).
			} });
		_registerComplexSpecialProp("perspective", { defaultValue: "0px", prefix: true });
		_registerComplexSpecialProp("perspectiveOrigin", { defaultValue: "50% 50%", prefix: true });
		_registerComplexSpecialProp("transformStyle", { prefix: true });
		_registerComplexSpecialProp("backfaceVisibility", { prefix: true });
		_registerComplexSpecialProp("userSelect", { prefix: true });
		_registerComplexSpecialProp("margin", { parser: _getEdgeParser("marginTop,marginRight,marginBottom,marginLeft") });
		_registerComplexSpecialProp("padding", { parser: _getEdgeParser("paddingTop,paddingRight,paddingBottom,paddingLeft") });
		_registerComplexSpecialProp("clip", { defaultValue: "rect(0px,0px,0px,0px)", parser: function parser(t, e, p, cssp, pt, plugin) {
				var b, cs, delim;
				if (_ieVers < 9) {
					//IE8 and earlier don't report a "clip" value in the currentStyle - instead, the values are split apart into clipTop, clipRight, clipBottom, and clipLeft. Also, in IE7 and earlier, the values inside rect() are space-delimited, not comma-delimited.
					cs = t.currentStyle;
					delim = _ieVers < 8 ? " " : ",";
					b = "rect(" + cs.clipTop + delim + cs.clipRight + delim + cs.clipBottom + delim + cs.clipLeft + ")";
					e = this.format(e).split(",").join(delim);
				} else {
					b = this.format(_getStyle(t, this.p, _cs, false, this.dflt));
					e = this.format(e);
				}
				return this.parseComplex(t.style, b, e, pt, plugin);
			} });
		_registerComplexSpecialProp("textShadow", { defaultValue: "0px 0px 0px #999", color: true, multi: true });
		_registerComplexSpecialProp("autoRound,strictUnits", { parser: function parser(t, e, p, cssp, pt) {
				return pt;
			} }); //just so that we can ignore these properties (not tween them)
		_registerComplexSpecialProp("border", { defaultValue: "0px solid #000", parser: function parser(t, e, p, cssp, pt, plugin) {
				var bw = _getStyle(t, "borderTopWidth", _cs, false, "0px"),
				    end = this.format(e).split(" "),
				    esfx = end[0].replace(_suffixExp, "");
				if (esfx !== "px") {
					//if we're animating to a non-px value, we need to convert the beginning width to that unit.
					bw = parseFloat(bw) / _convertToPixels(t, "borderTopWidth", 1, esfx) + esfx;
				}
				return this.parseComplex(t.style, this.format(bw + " " + _getStyle(t, "borderTopStyle", _cs, false, "solid") + " " + _getStyle(t, "borderTopColor", _cs, false, "#000")), end.join(" "), pt, plugin);
			}, color: true, formatter: function formatter(v) {
				var a = v.split(" ");
				return a[0] + " " + (a[1] || "solid") + " " + (v.match(_colorExp) || ["#000"])[0];
			} });
		_registerComplexSpecialProp("borderWidth", { parser: _getEdgeParser("borderTopWidth,borderRightWidth,borderBottomWidth,borderLeftWidth") }); //Firefox doesn't pick up on borderWidth set in style sheets (only inline).
		_registerComplexSpecialProp("float,cssFloat,styleFloat", { parser: function parser(t, e, p, cssp, pt, plugin) {
				var s = t.style,
				    prop = "cssFloat" in s ? "cssFloat" : "styleFloat";
				return new CSSPropTween(s, prop, 0, 0, pt, -1, p, false, 0, s[prop], e);
			} });

		//opacity-related
		var _setIEOpacityRatio = function _setIEOpacityRatio(v) {
			var t = this.t,
			    //refers to the element's style property
			filters = t.filter || _getStyle(this.data, "filter") || "",
			    val = this.s + this.c * v | 0,
			    skip;
			if (val === 100) {
				//for older versions of IE that need to use a filter to apply opacity, we should remove the filter if opacity hits 1 in order to improve performance, but make sure there isn't a transform (matrix) or gradient in the filters.
				if (filters.indexOf("atrix(") === -1 && filters.indexOf("radient(") === -1 && filters.indexOf("oader(") === -1) {
					t.removeAttribute("filter");
					skip = !_getStyle(this.data, "filter"); //if a class is applied that has an alpha filter, it will take effect (we don't want that), so re-apply our alpha filter in that case. We must first remove it and then check.
				} else {
					t.filter = filters.replace(_alphaFilterExp, "");
					skip = true;
				}
			}
			if (!skip) {
				if (this.xn1) {
					t.filter = filters = filters || "alpha(opacity=" + val + ")"; //works around bug in IE7/8 that prevents changes to "visibility" from being applied properly if the filter is changed to a different alpha on the same frame.
				}
				if (filters.indexOf("pacity") === -1) {
					//only used if browser doesn't support the standard opacity style property (IE 7 and 8). We omit the "O" to avoid case-sensitivity issues
					if (val !== 0 || !this.xn1) {
						//bugs in IE7/8 won't render the filter properly if opacity is ADDED on the same frame/render as "visibility" changes (this.xn1 is 1 if this tween is an "autoAlpha" tween)
						t.filter = filters + " alpha(opacity=" + val + ")"; //we round the value because otherwise, bugs in IE7/8 can prevent "visibility" changes from being applied properly.
					}
				} else {
					t.filter = filters.replace(_opacityExp, "opacity=" + val);
				}
			}
		};
		_registerComplexSpecialProp("opacity,alpha,autoAlpha", { defaultValue: "1", parser: function parser(t, e, p, cssp, pt, plugin) {
				var b = parseFloat(_getStyle(t, "opacity", _cs, false, "1")),
				    style = t.style,
				    isAutoAlpha = p === "autoAlpha";
				if (typeof e === "string" && e.charAt(1) === "=") {
					e = (e.charAt(0) === "-" ? -1 : 1) * parseFloat(e.substr(2)) + b;
				}
				if (isAutoAlpha && b === 1 && _getStyle(t, "visibility", _cs) === "hidden" && e !== 0) {
					//if visibility is initially set to "hidden", we should interpret that as intent to make opacity 0 (a convenience)
					b = 0;
				}
				if (_supportsOpacity) {
					pt = new CSSPropTween(style, "opacity", b, e - b, pt);
				} else {
					pt = new CSSPropTween(style, "opacity", b * 100, (e - b) * 100, pt);
					pt.xn1 = isAutoAlpha ? 1 : 0; //we need to record whether or not this is an autoAlpha so that in the setRatio(), we know to duplicate the setting of the alpha in order to work around a bug in IE7 and IE8 that prevents changes to "visibility" from taking effect if the filter is changed to a different alpha(opacity) at the same time. Setting it to the SAME value first, then the new value works around the IE7/8 bug.
					style.zoom = 1; //helps correct an IE issue.
					pt.type = 2;
					pt.b = "alpha(opacity=" + pt.s + ")";
					pt.e = "alpha(opacity=" + (pt.s + pt.c) + ")";
					pt.data = t;
					pt.plugin = plugin;
					pt.setRatio = _setIEOpacityRatio;
				}
				if (isAutoAlpha) {
					//we have to create the "visibility" PropTween after the opacity one in the linked list so that they run in the order that works properly in IE8 and earlier
					pt = new CSSPropTween(style, "visibility", 0, 0, pt, -1, null, false, 0, b !== 0 ? "inherit" : "hidden", e === 0 ? "hidden" : "inherit");
					pt.xs0 = "inherit";
					cssp._overwriteProps.push(pt.n);
					cssp._overwriteProps.push(p);
				}
				return pt;
			} });

		var _removeProp = function _removeProp(s, p) {
			if (p) {
				if (s.removeProperty) {
					if (p.substr(0, 2) === "ms" || p.substr(0, 6) === "webkit") {
						//Microsoft and some Webkit browsers don't conform to the standard of capitalizing the first prefix character, so we adjust so that when we prefix the caps with a dash, it's correct (otherwise it'd be "ms-transform" instead of "-ms-transform" for IE9, for example)
						p = "-" + p;
					}
					s.removeProperty(p.replace(_capsExp, "-$1").toLowerCase());
				} else {
					//note: old versions of IE use "removeAttribute()" instead of "removeProperty()"
					s.removeAttribute(p);
				}
			}
		},
		    _setClassNameRatio = function _setClassNameRatio(v) {
			this.t._gsClassPT = this;
			if (v === 1 || v === 0) {
				this.t.setAttribute("class", v === 0 ? this.b : this.e);
				var mpt = this.data,
				    //first MiniPropTween
				s = this.t.style;
				while (mpt) {
					if (!mpt.v) {
						_removeProp(s, mpt.p);
					} else {
						s[mpt.p] = mpt.v;
					}
					mpt = mpt._next;
				}
				if (v === 1 && this.t._gsClassPT === this) {
					this.t._gsClassPT = null;
				}
			} else if (this.t.getAttribute("class") !== this.e) {
				this.t.setAttribute("class", this.e);
			}
		};
		_registerComplexSpecialProp("className", { parser: function parser(t, e, p, cssp, pt, plugin, vars) {
				var b = t.getAttribute("class") || "",
				    //don't use t.className because it doesn't work consistently on SVG elements; getAttribute("class") and setAttribute("class", value") is more reliable.
				cssText = t.style.cssText,
				    difData,
				    bs,
				    cnpt,
				    cnptLookup,
				    mpt;
				pt = cssp._classNamePT = new CSSPropTween(t, p, 0, 0, pt, 2);
				pt.setRatio = _setClassNameRatio;
				pt.pr = -11;
				_hasPriority = true;
				pt.b = b;
				bs = _getAllStyles(t, _cs);
				//if there's a className tween already operating on the target, force it to its end so that the necessary inline styles are removed and the class name is applied before we determine the end state (we don't want inline styles interfering that were there just for class-specific values)
				cnpt = t._gsClassPT;
				if (cnpt) {
					cnptLookup = {};
					mpt = cnpt.data; //first MiniPropTween which stores the inline styles - we need to force these so that the inline styles don't contaminate things. Otherwise, there's a small chance that a tween could start and the inline values match the destination values and they never get cleaned.
					while (mpt) {
						cnptLookup[mpt.p] = 1;
						mpt = mpt._next;
					}
					cnpt.setRatio(1);
				}
				t._gsClassPT = pt;
				pt.e = e.charAt(1) !== "=" ? e : b.replace(new RegExp("(?:\\s|^)" + e.substr(2) + "(?![\\w-])"), "") + (e.charAt(0) === "+" ? " " + e.substr(2) : "");
				t.setAttribute("class", pt.e);
				difData = _cssDif(t, bs, _getAllStyles(t), vars, cnptLookup);
				t.setAttribute("class", b);
				pt.data = difData.firstMPT;
				t.style.cssText = cssText; //we recorded cssText before we swapped classes and ran _getAllStyles() because in cases when a className tween is overwritten, we remove all the related tweening properties from that class change (otherwise class-specific stuff can't override properties we've directly set on the target's style object due to specificity).
				pt = pt.xfirst = cssp.parse(t, difData.difs, pt, plugin); //we record the CSSPropTween as the xfirst so that we can handle overwriting propertly (if "className" gets overwritten, we must kill all the properties associated with the className part of the tween, so we can loop through from xfirst to the pt itself)
				return pt;
			} });

		var _setClearPropsRatio = function _setClearPropsRatio(v) {
			if (v === 1 || v === 0) if (this.data._totalTime === this.data._totalDuration && this.data.data !== "isFromStart") {
				//this.data refers to the tween. Only clear at the END of the tween (remember, from() tweens make the ratio go from 1 to 0, so we can't just check that and if the tween is the zero-duration one that's created internally to render the starting values in a from() tween, ignore that because otherwise, for example, from(...{height:100, clearProps:"height", delay:1}) would wipe the height at the beginning of the tween and after 1 second, it'd kick back in).
				var s = this.t.style,
				    transformParse = _specialProps.transform.parse,
				    a,
				    p,
				    i,
				    clearTransform,
				    transform;
				if (this.e === "all") {
					s.cssText = "";
					clearTransform = true;
				} else {
					a = this.e.split(" ").join("").split(",");
					i = a.length;
					while (--i > -1) {
						p = a[i];
						if (_specialProps[p]) {
							if (_specialProps[p].parse === transformParse) {
								clearTransform = true;
							} else {
								p = p === "transformOrigin" ? _transformOriginProp : _specialProps[p].p; //ensures that special properties use the proper browser-specific property name, like "scaleX" might be "-webkit-transform" or "boxShadow" might be "-moz-box-shadow"
							}
						}
						_removeProp(s, p);
					}
				}
				if (clearTransform) {
					_removeProp(s, _transformProp);
					transform = this.t._gsTransform;
					if (transform) {
						if (transform.svg) {
							this.t.removeAttribute("data-svg-origin");
							this.t.removeAttribute("transform");
						}
						delete this.t._gsTransform;
					}
				}
			}
		};
		_registerComplexSpecialProp("clearProps", { parser: function parser(t, e, p, cssp, pt) {
				pt = new CSSPropTween(t, p, 0, 0, pt, 2);
				pt.setRatio = _setClearPropsRatio;
				pt.e = e;
				pt.pr = -10;
				pt.data = cssp._tween;
				_hasPriority = true;
				return pt;
			} });

		p = "bezier,throwProps,physicsProps,physics2D".split(",");
		i = p.length;
		while (i--) {
			_registerPluginProp(p[i]);
		}

		p = CSSPlugin.prototype;
		p._firstPT = p._lastParsedTransform = p._transform = null;

		//gets called when the tween renders for the first time. This kicks everything off, recording start/end values, etc.
		p._onInitTween = function (target, vars, tween, index) {
			if (!target.nodeType) {
				//css is only for dom elements
				return false;
			}
			this._target = _target = target;
			this._tween = tween;
			this._vars = vars;
			_index = index;
			_autoRound = vars.autoRound;
			_hasPriority = false;
			_suffixMap = vars.suffixMap || CSSPlugin.suffixMap;
			_cs = _getComputedStyle(target, "");
			_overwriteProps = this._overwriteProps;
			var style = target.style,
			    v,
			    pt,
			    pt2,
			    first,
			    last,
			    next,
			    zIndex,
			    tpt,
			    threeD;
			if (_reqSafariFix) if (style.zIndex === "") {
				v = _getStyle(target, "zIndex", _cs);
				if (v === "auto" || v === "") {
					//corrects a bug in [non-Android] Safari that prevents it from repainting elements in their new positions if they don't have a zIndex set. We also can't just apply this inside _parseTransform() because anything that's moved in any way (like using "left" or "top" instead of transforms like "x" and "y") can be affected, so it is best to ensure that anything that's tweening has a z-index. Setting "WebkitPerspective" to a non-zero value worked too except that on iOS Safari things would flicker randomly. Plus zIndex is less memory-intensive.
					this._addLazySet(style, "zIndex", 0);
				}
			}

			if (typeof vars === "string") {
				first = style.cssText;
				v = _getAllStyles(target, _cs);
				style.cssText = first + ";" + vars;
				v = _cssDif(target, v, _getAllStyles(target)).difs;
				if (!_supportsOpacity && _opacityValExp.test(vars)) {
					v.opacity = parseFloat(RegExp.$1);
				}
				vars = v;
				style.cssText = first;
			}

			if (vars.className) {
				//className tweens will combine any differences they find in the css with the vars that are passed in, so {className:"myClass", scale:0.5, left:20} would work.
				this._firstPT = pt = _specialProps.className.parse(target, vars.className, "className", this, null, null, vars);
			} else {
				this._firstPT = pt = this.parse(target, vars, null);
			}

			if (this._transformType) {
				threeD = this._transformType === 3;
				if (!_transformProp) {
					style.zoom = 1; //helps correct an IE issue.
				} else if (_isSafari) {
					_reqSafariFix = true;
					//if zIndex isn't set, iOS Safari doesn't repaint things correctly sometimes (seemingly at random).
					if (style.zIndex === "") {
						zIndex = _getStyle(target, "zIndex", _cs);
						if (zIndex === "auto" || zIndex === "") {
							this._addLazySet(style, "zIndex", 0);
						}
					}
					//Setting WebkitBackfaceVisibility corrects 3 bugs:
					// 1) [non-Android] Safari skips rendering changes to "top" and "left" that are made on the same frame/render as a transform update.
					// 2) iOS Safari sometimes neglects to repaint elements in their new positions. Setting "WebkitPerspective" to a non-zero value worked too except that on iOS Safari things would flicker randomly.
					// 3) Safari sometimes displayed odd artifacts when tweening the transform (or WebkitTransform) property, like ghosts of the edges of the element remained. Definitely a browser bug.
					//Note: we allow the user to override the auto-setting by defining WebkitBackfaceVisibility in the vars of the tween.
					if (_isSafariLT6) {
						this._addLazySet(style, "WebkitBackfaceVisibility", this._vars.WebkitBackfaceVisibility || (threeD ? "visible" : "hidden"));
					}
				}
				pt2 = pt;
				while (pt2 && pt2._next) {
					pt2 = pt2._next;
				}
				tpt = new CSSPropTween(target, "transform", 0, 0, null, 2);
				this._linkCSSP(tpt, null, pt2);
				tpt.setRatio = _transformProp ? _setTransformRatio : _setIETransformRatio;
				tpt.data = this._transform || _getTransform(target, _cs, true);
				tpt.tween = tween;
				tpt.pr = -1; //ensures that the transforms get applied after the components are updated.
				_overwriteProps.pop(); //we don't want to force the overwrite of all "transform" tweens of the target - we only care about individual transform properties like scaleX, rotation, etc. The CSSPropTween constructor automatically adds the property to _overwriteProps which is why we need to pop() here.
			}

			if (_hasPriority) {
				//reorders the linked list in order of pr (priority)
				while (pt) {
					next = pt._next;
					pt2 = first;
					while (pt2 && pt2.pr > pt.pr) {
						pt2 = pt2._next;
					}
					if (pt._prev = pt2 ? pt2._prev : last) {
						pt._prev._next = pt;
					} else {
						first = pt;
					}
					if (pt._next = pt2) {
						pt2._prev = pt;
					} else {
						last = pt;
					}
					pt = next;
				}
				this._firstPT = first;
			}
			return true;
		};

		p.parse = function (target, vars, pt, plugin) {
			var style = target.style,
			    p,
			    sp,
			    bn,
			    en,
			    bs,
			    es,
			    bsfx,
			    esfx,
			    isStr,
			    rel;
			for (p in vars) {
				es = vars[p]; //ending value string
				if (typeof es === "function") {
					es = es(_index, _target);
				}
				sp = _specialProps[p]; //SpecialProp lookup.
				if (sp) {
					pt = sp.parse(target, es, p, this, pt, plugin, vars);
				} else if (p.substr(0, 2) === "--") {
					//for tweening CSS variables (which always start with "--"). To maximize performance and simplicity, we bypass CSSPlugin altogether and just add a normal property tween to the tween instance itself.
					this._tween._propLookup[p] = this._addTween.call(this._tween, target.style, "setProperty", _getComputedStyle(target).getPropertyValue(p) + "", es + "", p, false, p);
					continue;
				} else {
					bs = _getStyle(target, p, _cs) + "";
					isStr = typeof es === "string";
					if (p === "color" || p === "fill" || p === "stroke" || p.indexOf("Color") !== -1 || isStr && _rgbhslExp.test(es)) {
						//Opera uses background: to define color sometimes in addition to backgroundColor:
						if (!isStr) {
							es = _parseColor(es);
							es = (es.length > 3 ? "rgba(" : "rgb(") + es.join(",") + ")";
						}
						pt = _parseComplex(style, p, bs, es, true, "transparent", pt, 0, plugin);
					} else if (isStr && _complexExp.test(es)) {
						pt = _parseComplex(style, p, bs, es, true, null, pt, 0, plugin);
					} else {
						bn = parseFloat(bs);
						bsfx = bn || bn === 0 ? bs.substr((bn + "").length) : ""; //remember, bs could be non-numeric like "normal" for fontWeight, so we should default to a blank suffix in that case.

						if (bs === "" || bs === "auto") {
							if (p === "width" || p === "height") {
								bn = _getDimension(target, p, _cs);
								bsfx = "px";
							} else if (p === "left" || p === "top") {
								bn = _calculateOffset(target, p, _cs);
								bsfx = "px";
							} else {
								bn = p !== "opacity" ? 0 : 1;
								bsfx = "";
							}
						}

						rel = isStr && es.charAt(1) === "=";
						if (rel) {
							en = parseInt(es.charAt(0) + "1", 10);
							es = es.substr(2);
							en *= parseFloat(es);
							esfx = es.replace(_suffixExp, "");
						} else {
							en = parseFloat(es);
							esfx = isStr ? es.replace(_suffixExp, "") : "";
						}

						if (esfx === "") {
							esfx = p in _suffixMap ? _suffixMap[p] : bsfx; //populate the end suffix, prioritizing the map, then if none is found, use the beginning suffix.
						}

						es = en || en === 0 ? (rel ? en + bn : en) + esfx : vars[p]; //ensures that any += or -= prefixes are taken care of. Record the end value before normalizing the suffix because we always want to end the tween on exactly what they intended even if it doesn't match the beginning value's suffix.
						//if the beginning/ending suffixes don't match, normalize them...
						if (bsfx !== esfx) if (esfx !== "" || p === "lineHeight") if (en || en === 0) if (bn) {
							//note: if the beginning value (bn) is 0, we don't need to convert units!
							bn = _convertToPixels(target, p, bn, bsfx);
							if (esfx === "%") {
								bn /= _convertToPixels(target, p, 100, "%") / 100;
								if (vars.strictUnits !== true) {
									//some browsers report only "px" values instead of allowing "%" with getComputedStyle(), so we assume that if we're tweening to a %, we should start there too unless strictUnits:true is defined. This approach is particularly useful for responsive designs that use from() tweens.
									bs = bn + "%";
								}
							} else if (esfx === "em" || esfx === "rem" || esfx === "vw" || esfx === "vh") {
								bn /= _convertToPixels(target, p, 1, esfx);

								//otherwise convert to pixels.
							} else if (esfx !== "px") {
								en = _convertToPixels(target, p, en, esfx);
								esfx = "px"; //we don't use bsfx after this, so we don't need to set it to px too.
							}
							if (rel) if (en || en === 0) {
								es = en + bn + esfx; //the changes we made affect relative calculations, so adjust the end value here.
							}
						}

						if (rel) {
							en += bn;
						}

						if ((bn || bn === 0) && (en || en === 0)) {
							//faster than isNaN(). Also, previously we required en !== bn but that doesn't really gain much performance and it prevents _parseToProxy() from working properly if beginning and ending values match but need to get tweened by an external plugin anyway. For example, a bezier tween where the target starts at left:0 and has these points: [{left:50},{left:0}] wouldn't work properly because when parsing the last point, it'd match the first (current) one and a non-tweening CSSPropTween would be recorded when we actually need a normal tween (type:0) so that things get updated during the tween properly.
							pt = new CSSPropTween(style, p, bn, en - bn, pt, 0, p, _autoRound !== false && (esfx === "px" || p === "zIndex"), 0, bs, es);
							pt.xs0 = esfx;
							//DEBUG: _log("tween "+p+" from "+pt.b+" ("+bn+esfx+") to "+pt.e+" with suffix: "+pt.xs0);
						} else if (style[p] === undefined || !es && (es + "" === "NaN" || es == null)) {
							_log("invalid " + p + " tween value: " + vars[p]);
						} else {
							pt = new CSSPropTween(style, p, en || bn || 0, 0, pt, -1, p, false, 0, bs, es);
							pt.xs0 = es === "none" && (p === "display" || p.indexOf("Style") !== -1) ? bs : es; //intermediate value should typically be set immediately (end value) except for "display" or things like borderTopStyle, borderBottomStyle, etc. which should use the beginning value during the tween.
							//DEBUG: _log("non-tweening value "+p+": "+pt.xs0);
						}
					}
				}
				if (plugin) if (pt && !pt.plugin) {
					pt.plugin = plugin;
				}
			}
			return pt;
		};

		//gets called every time the tween updates, passing the new ratio (typically a value between 0 and 1, but not always (for example, if an Elastic.easeOut is used, the value can jump above 1 mid-tween). It will always start and 0 and end at 1.
		p.setRatio = function (v) {
			var pt = this._firstPT,
			    min = 0.000001,
			    val,
			    str,
			    i;
			//at the end of the tween, we set the values to exactly what we received in order to make sure non-tweening values (like "position" or "float" or whatever) are set and so that if the beginning/ending suffixes (units) didn't match and we normalized to px, the value that the user passed in is used here. We check to see if the tween is at its beginning in case it's a from() tween in which case the ratio will actually go from 1 to 0 over the course of the tween (backwards).
			if (v === 1 && (this._tween._time === this._tween._duration || this._tween._time === 0)) {
				while (pt) {
					if (pt.type !== 2) {
						if (pt.r && pt.type !== -1) {
							val = Math.round(pt.s + pt.c);
							if (!pt.type) {
								pt.t[pt.p] = val + pt.xs0;
							} else if (pt.type === 1) {
								//complex value (one that typically has multiple numbers inside a string, like "rect(5px,10px,20px,25px)"
								i = pt.l;
								str = pt.xs0 + val + pt.xs1;
								for (i = 1; i < pt.l; i++) {
									str += pt["xn" + i] + pt["xs" + (i + 1)];
								}
								pt.t[pt.p] = str;
							}
						} else {
							pt.t[pt.p] = pt.e;
						}
					} else {
						pt.setRatio(v);
					}
					pt = pt._next;
				}
			} else if (v || !(this._tween._time === this._tween._duration || this._tween._time === 0) || this._tween._rawPrevTime === -0.000001) {
				while (pt) {
					val = pt.c * v + pt.s;
					if (pt.r) {
						val = Math.round(val);
					} else if (val < min) if (val > -min) {
						val = 0;
					}
					if (!pt.type) {
						pt.t[pt.p] = val + pt.xs0;
					} else if (pt.type === 1) {
						//complex value (one that typically has multiple numbers inside a string, like "rect(5px,10px,20px,25px)"
						i = pt.l;
						if (i === 2) {
							pt.t[pt.p] = pt.xs0 + val + pt.xs1 + pt.xn1 + pt.xs2;
						} else if (i === 3) {
							pt.t[pt.p] = pt.xs0 + val + pt.xs1 + pt.xn1 + pt.xs2 + pt.xn2 + pt.xs3;
						} else if (i === 4) {
							pt.t[pt.p] = pt.xs0 + val + pt.xs1 + pt.xn1 + pt.xs2 + pt.xn2 + pt.xs3 + pt.xn3 + pt.xs4;
						} else if (i === 5) {
							pt.t[pt.p] = pt.xs0 + val + pt.xs1 + pt.xn1 + pt.xs2 + pt.xn2 + pt.xs3 + pt.xn3 + pt.xs4 + pt.xn4 + pt.xs5;
						} else {
							str = pt.xs0 + val + pt.xs1;
							for (i = 1; i < pt.l; i++) {
								str += pt["xn" + i] + pt["xs" + (i + 1)];
							}
							pt.t[pt.p] = str;
						}
					} else if (pt.type === -1) {
						//non-tweening value
						pt.t[pt.p] = pt.xs0;
					} else if (pt.setRatio) {
						//custom setRatio() for things like SpecialProps, external plugins, etc.
						pt.setRatio(v);
					}
					pt = pt._next;
				}

				//if the tween is reversed all the way back to the beginning, we need to restore the original values which may have different units (like % instead of px or em or whatever).
			} else {
				while (pt) {
					if (pt.type !== 2) {
						pt.t[pt.p] = pt.b;
					} else {
						pt.setRatio(v);
					}
					pt = pt._next;
				}
			}
		};

		/**
   * @private
   * Forces rendering of the target's transforms (rotation, scale, etc.) whenever the CSSPlugin's setRatio() is called.
   * Basically, this tells the CSSPlugin to create a CSSPropTween (type 2) after instantiation that runs last in the linked
   * list and calls the appropriate (3D or 2D) rendering function. We separate this into its own method so that we can call
   * it from other plugins like BezierPlugin if, for example, it needs to apply an autoRotation and this CSSPlugin
   * doesn't have any transform-related properties of its own. You can call this method as many times as you
   * want and it won't create duplicate CSSPropTweens.
   *
   * @param {boolean} threeD if true, it should apply 3D tweens (otherwise, just 2D ones are fine and typically faster)
   */
		p._enableTransforms = function (threeD) {
			this._transform = this._transform || _getTransform(this._target, _cs, true); //ensures that the element has a _gsTransform property with the appropriate values.
			this._transformType = !(this._transform.svg && _useSVGTransformAttr) && (threeD || this._transformType === 3) ? 3 : 2;
		};

		var lazySet = function lazySet(v) {
			this.t[this.p] = this.e;
			this.data._linkCSSP(this, this._next, null, true); //we purposefully keep this._next even though it'd make sense to null it, but this is a performance optimization, as this happens during the while (pt) {} loop in setRatio() at the bottom of which it sets pt = pt._next, so if we null it, the linked list will be broken in that loop.
		};
		/** @private Gives us a way to set a value on the first render (and only the first render). **/
		p._addLazySet = function (t, p, v) {
			var pt = this._firstPT = new CSSPropTween(t, p, 0, 0, this._firstPT, 2);
			pt.e = v;
			pt.setRatio = lazySet;
			pt.data = this;
		};

		/** @private **/
		p._linkCSSP = function (pt, next, prev, remove) {
			if (pt) {
				if (next) {
					next._prev = pt;
				}
				if (pt._next) {
					pt._next._prev = pt._prev;
				}
				if (pt._prev) {
					pt._prev._next = pt._next;
				} else if (this._firstPT === pt) {
					this._firstPT = pt._next;
					remove = true; //just to prevent resetting this._firstPT 5 lines down in case pt._next is null. (optimized for speed)
				}
				if (prev) {
					prev._next = pt;
				} else if (!remove && this._firstPT === null) {
					this._firstPT = pt;
				}
				pt._next = next;
				pt._prev = prev;
			}
			return pt;
		};

		p._mod = function (lookup) {
			var pt = this._firstPT;
			while (pt) {
				if (typeof lookup[pt.p] === "function" && lookup[pt.p] === Math.round) {
					//only gets called by RoundPropsPlugin (ModifyPlugin manages all the rendering internally for CSSPlugin properties that need modification). Remember, we handle rounding a bit differently in this plugin for performance reasons, leveraging "r" as an indicator that the value should be rounded internally..
					pt.r = 1;
				}
				pt = pt._next;
			}
		};

		//we need to make sure that if alpha or autoAlpha is killed, opacity is too. And autoAlpha affects the "visibility" property.
		p._kill = function (lookup) {
			var copy = lookup,
			    pt,
			    p,
			    xfirst;
			if (lookup.autoAlpha || lookup.alpha) {
				copy = {};
				for (p in lookup) {
					//copy the lookup so that we're not changing the original which may be passed elsewhere.
					copy[p] = lookup[p];
				}
				copy.opacity = 1;
				if (copy.autoAlpha) {
					copy.visibility = 1;
				}
			}
			if (lookup.className && (pt = this._classNamePT)) {
				//for className tweens, we need to kill any associated CSSPropTweens too; a linked list starts at the className's "xfirst".
				xfirst = pt.xfirst;
				if (xfirst && xfirst._prev) {
					this._linkCSSP(xfirst._prev, pt._next, xfirst._prev._prev); //break off the prev
				} else if (xfirst === this._firstPT) {
					this._firstPT = pt._next;
				}
				if (pt._next) {
					this._linkCSSP(pt._next, pt._next._next, xfirst._prev);
				}
				this._classNamePT = null;
			}
			pt = this._firstPT;
			while (pt) {
				if (pt.plugin && pt.plugin !== p && pt.plugin._kill) {
					//for plugins that are registered with CSSPlugin, we should notify them of the kill.
					pt.plugin._kill(lookup);
					p = pt.plugin;
				}
				pt = pt._next;
			}
			return TweenPlugin.prototype._kill.call(this, copy);
		};

		//used by cascadeTo() for gathering all the style properties of each child element into an array for comparison.
		var _getChildStyles = function _getChildStyles(e, props, targets) {
			var children, i, child, type;
			if (e.slice) {
				i = e.length;
				while (--i > -1) {
					_getChildStyles(e[i], props, targets);
				}
				return;
			}
			children = e.childNodes;
			i = children.length;
			while (--i > -1) {
				child = children[i];
				type = child.type;
				if (child.style) {
					props.push(_getAllStyles(child));
					if (targets) {
						targets.push(child);
					}
				}
				if ((type === 1 || type === 9 || type === 11) && child.childNodes.length) {
					_getChildStyles(child, props, targets);
				}
			}
		};

		/**
   * Typically only useful for className tweens that may affect child elements, this method creates a TweenLite
   * and then compares the style properties of all the target's child elements at the tween's start and end, and
   * if any are different, it also creates tweens for those and returns an array containing ALL of the resulting
   * tweens (so that you can easily add() them to a TimelineLite, for example). The reason this functionality is
   * wrapped into a separate static method of CSSPlugin instead of being integrated into all regular className tweens
   * is because it creates entirely new tweens that may have completely different targets than the original tween,
   * so if they were all lumped into the original tween instance, it would be inconsistent with the rest of the API
   * and it would create other problems. For example:
   *  - If I create a tween of elementA, that tween instance may suddenly change its target to include 50 other elements (unintuitive if I specifically defined the target I wanted)
   *  - We can't just create new independent tweens because otherwise, what happens if the original/parent tween is reversed or pause or dropped into a TimelineLite for tight control? You'd expect that tween's behavior to affect all the others.
   *  - Analyzing every style property of every child before and after the tween is an expensive operation when there are many children, so this behavior shouldn't be imposed on all className tweens by default, especially since it's probably rare that this extra functionality is needed.
   *
   * @param {Object} target object to be tweened
   * @param {number} Duration in seconds (or frames for frames-based tweens)
   * @param {Object} Object containing the end values, like {className:"newClass", ease:Linear.easeNone}
   * @return {Array} An array of TweenLite instances
   */
		CSSPlugin.cascadeTo = function (target, duration, vars) {
			var tween = TweenLite.to(target, duration, vars),
			    results = [tween],
			    b = [],
			    e = [],
			    targets = [],
			    _reservedProps = TweenLite._internals.reservedProps,
			    i,
			    difs,
			    p,
			    from;
			target = tween._targets || tween.target;
			_getChildStyles(target, b, targets);
			tween.render(duration, true, true);
			_getChildStyles(target, e);
			tween.render(0, true, true);
			tween._enabled(true);
			i = targets.length;
			while (--i > -1) {
				difs = _cssDif(targets[i], b[i], e[i]);
				if (difs.firstMPT) {
					difs = difs.difs;
					for (p in vars) {
						if (_reservedProps[p]) {
							difs[p] = vars[p];
						}
					}
					from = {};
					for (p in difs) {
						from[p] = b[i][p];
					}
					results.push(TweenLite.fromTo(targets[i], duration, from, difs));
				}
			}
			return results;
		};

		TweenPlugin.activate([CSSPlugin]);
		return CSSPlugin;
	}, true);
});if (_gsScope._gsDefine) {
	_gsScope._gsQueue.pop()();
}

//export to AMD/RequireJS and CommonJS/Node (precursor to full modular build system coming at a later date)
(function (name) {
	"use strict";

	var getGlobal = function getGlobal() {
		return (_gsScope.GreenSockGlobals || _gsScope)[name];
	};
	if (typeof module !== "undefined" && module.exports) {
		//node
		__webpack_require__(0);
		module.exports = getGlobal();
	} else if (true) {
		//AMD
		!(__WEBPACK_AMD_DEFINE_ARRAY__ = [__webpack_require__(0)], __WEBPACK_AMD_DEFINE_FACTORY__ = (getGlobal),
				__WEBPACK_AMD_DEFINE_RESULT__ = (typeof __WEBPACK_AMD_DEFINE_FACTORY__ === 'function' ?
				(__WEBPACK_AMD_DEFINE_FACTORY__.apply(exports, __WEBPACK_AMD_DEFINE_ARRAY__)) : __WEBPACK_AMD_DEFINE_FACTORY__),
				__WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));
	}
})("CSSPlugin");
/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(2)))

/***/ }),
/* 5 */
/***/ (function(module, exports) {

/**
 * Checks if `value` is the
 * [language type](http://www.ecma-international.org/ecma-262/7.0/#sec-ecmascript-language-types)
 * of `Object`. (e.g. arrays, functions, objects, regexes, `new Number(0)`, and `new String('')`)
 *
 * @static
 * @memberOf _
 * @since 0.1.0
 * @category Lang
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is an object, else `false`.
 * @example
 *
 * _.isObject({});
 * // => true
 *
 * _.isObject([1, 2, 3]);
 * // => true
 *
 * _.isObject(_.noop);
 * // => true
 *
 * _.isObject(null);
 * // => false
 */
function isObject(value) {
  var type = typeof value;
  return value != null && (type == 'object' || type == 'function');
}

module.exports = isObject;


/***/ }),
/* 6 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/* WEBPACK VAR INJECTION */(function(global) {var __WEBPACK_AMD_DEFINE_FACTORY__, __WEBPACK_AMD_DEFINE_ARRAY__, __WEBPACK_AMD_DEFINE_RESULT__;

/*!
 * VERSION: 1.15.6
 * DATE: 2017-06-19
 * UPDATES AND DOCS AT: http://greensock.com
 *
 * @license Copyright (c) 2008-2017, GreenSock. All rights reserved.
 * This work is subject to the terms at http://greensock.com/standard-license or for
 * Club GreenSock members, the software agreement that was issued with your membership.
 * 
 * @author: Jack Doyle, jack@greensock.com
 **/
var _gsScope = typeof module !== "undefined" && module.exports && typeof global !== "undefined" ? global : undefined || window; //helps ensure compatibility with AMD/RequireJS and CommonJS/Node
(_gsScope._gsQueue || (_gsScope._gsQueue = [])).push(function () {

	"use strict";

	_gsScope._gsDefine("easing.Back", ["easing.Ease"], function (Ease) {

		var w = _gsScope.GreenSockGlobals || _gsScope,
		    gs = w.com.greensock,
		    _2PI = Math.PI * 2,
		    _HALF_PI = Math.PI / 2,
		    _class = gs._class,
		    _create = function _create(n, f) {
			var C = _class("easing." + n, function () {}, true),
			    p = C.prototype = new Ease();
			p.constructor = C;
			p.getRatio = f;
			return C;
		},
		    _easeReg = Ease.register || function () {},
		    //put an empty function in place just as a safety measure in case someone loads an OLD version of TweenLite.js where Ease.register doesn't exist.
		_wrap = function _wrap(name, EaseOut, EaseIn, EaseInOut, aliases) {
			var C = _class("easing." + name, {
				easeOut: new EaseOut(),
				easeIn: new EaseIn(),
				easeInOut: new EaseInOut()
			}, true);
			_easeReg(C, name);
			return C;
		},
		    EasePoint = function EasePoint(time, value, next) {
			this.t = time;
			this.v = value;
			if (next) {
				this.next = next;
				next.prev = this;
				this.c = next.v - value;
				this.gap = next.t - time;
			}
		},


		//Back
		_createBack = function _createBack(n, f) {
			var C = _class("easing." + n, function (overshoot) {
				this._p1 = overshoot || overshoot === 0 ? overshoot : 1.70158;
				this._p2 = this._p1 * 1.525;
			}, true),
			    p = C.prototype = new Ease();
			p.constructor = C;
			p.getRatio = f;
			p.config = function (overshoot) {
				return new C(overshoot);
			};
			return C;
		},
		    Back = _wrap("Back", _createBack("BackOut", function (p) {
			return (p = p - 1) * p * ((this._p1 + 1) * p + this._p1) + 1;
		}), _createBack("BackIn", function (p) {
			return p * p * ((this._p1 + 1) * p - this._p1);
		}), _createBack("BackInOut", function (p) {
			return (p *= 2) < 1 ? 0.5 * p * p * ((this._p2 + 1) * p - this._p2) : 0.5 * ((p -= 2) * p * ((this._p2 + 1) * p + this._p2) + 2);
		})),


		//SlowMo
		SlowMo = _class("easing.SlowMo", function (linearRatio, power, yoyoMode) {
			power = power || power === 0 ? power : 0.7;
			if (linearRatio == null) {
				linearRatio = 0.7;
			} else if (linearRatio > 1) {
				linearRatio = 1;
			}
			this._p = linearRatio !== 1 ? power : 0;
			this._p1 = (1 - linearRatio) / 2;
			this._p2 = linearRatio;
			this._p3 = this._p1 + this._p2;
			this._calcEnd = yoyoMode === true;
		}, true),
		    p = SlowMo.prototype = new Ease(),
		    SteppedEase,
		    RoughEase,
		    _createElastic;

		p.constructor = SlowMo;
		p.getRatio = function (p) {
			var r = p + (0.5 - p) * this._p;
			if (p < this._p1) {
				return this._calcEnd ? 1 - (p = 1 - p / this._p1) * p : r - (p = 1 - p / this._p1) * p * p * p * r;
			} else if (p > this._p3) {
				return this._calcEnd ? p === 1 ? 0 : 1 - (p = (p - this._p3) / this._p1) * p : r + (p - r) * (p = (p - this._p3) / this._p1) * p * p * p; //added p === 1 ? 0 to avoid floating point rounding errors from affecting the final value, like 1 - 0.7 = 0.30000000000000004 instead of 0.3
			}
			return this._calcEnd ? 1 : r;
		};
		SlowMo.ease = new SlowMo(0.7, 0.7);

		p.config = SlowMo.config = function (linearRatio, power, yoyoMode) {
			return new SlowMo(linearRatio, power, yoyoMode);
		};

		//SteppedEase
		SteppedEase = _class("easing.SteppedEase", function (steps, immediateStart) {
			steps = steps || 1;
			this._p1 = 1 / steps;
			this._p2 = steps + (immediateStart ? 0 : 1);
			this._p3 = immediateStart ? 1 : 0;
		}, true);
		p = SteppedEase.prototype = new Ease();
		p.constructor = SteppedEase;
		p.getRatio = function (p) {
			if (p < 0) {
				p = 0;
			} else if (p >= 1) {
				p = 0.999999999;
			}
			return ((this._p2 * p | 0) + this._p3) * this._p1;
		};
		p.config = SteppedEase.config = function (steps, immediateStart) {
			return new SteppedEase(steps, immediateStart);
		};

		//RoughEase
		RoughEase = _class("easing.RoughEase", function (vars) {
			vars = vars || {};
			var taper = vars.taper || "none",
			    a = [],
			    cnt = 0,
			    points = (vars.points || 20) | 0,
			    i = points,
			    randomize = vars.randomize !== false,
			    clamp = vars.clamp === true,
			    template = vars.template instanceof Ease ? vars.template : null,
			    strength = typeof vars.strength === "number" ? vars.strength * 0.4 : 0.4,
			    x,
			    y,
			    bump,
			    invX,
			    obj,
			    pnt;
			while (--i > -1) {
				x = randomize ? Math.random() : 1 / points * i;
				y = template ? template.getRatio(x) : x;
				if (taper === "none") {
					bump = strength;
				} else if (taper === "out") {
					invX = 1 - x;
					bump = invX * invX * strength;
				} else if (taper === "in") {
					bump = x * x * strength;
				} else if (x < 0.5) {
					//"both" (start)
					invX = x * 2;
					bump = invX * invX * 0.5 * strength;
				} else {
					//"both" (end)
					invX = (1 - x) * 2;
					bump = invX * invX * 0.5 * strength;
				}
				if (randomize) {
					y += Math.random() * bump - bump * 0.5;
				} else if (i % 2) {
					y += bump * 0.5;
				} else {
					y -= bump * 0.5;
				}
				if (clamp) {
					if (y > 1) {
						y = 1;
					} else if (y < 0) {
						y = 0;
					}
				}
				a[cnt++] = { x: x, y: y };
			}
			a.sort(function (a, b) {
				return a.x - b.x;
			});

			pnt = new EasePoint(1, 1, null);
			i = points;
			while (--i > -1) {
				obj = a[i];
				pnt = new EasePoint(obj.x, obj.y, pnt);
			}

			this._prev = new EasePoint(0, 0, pnt.t !== 0 ? pnt : pnt.next);
		}, true);
		p = RoughEase.prototype = new Ease();
		p.constructor = RoughEase;
		p.getRatio = function (p) {
			var pnt = this._prev;
			if (p > pnt.t) {
				while (pnt.next && p >= pnt.t) {
					pnt = pnt.next;
				}
				pnt = pnt.prev;
			} else {
				while (pnt.prev && p <= pnt.t) {
					pnt = pnt.prev;
				}
			}
			this._prev = pnt;
			return pnt.v + (p - pnt.t) / pnt.gap * pnt.c;
		};
		p.config = function (vars) {
			return new RoughEase(vars);
		};
		RoughEase.ease = new RoughEase();

		//Bounce
		_wrap("Bounce", _create("BounceOut", function (p) {
			if (p < 1 / 2.75) {
				return 7.5625 * p * p;
			} else if (p < 2 / 2.75) {
				return 7.5625 * (p -= 1.5 / 2.75) * p + 0.75;
			} else if (p < 2.5 / 2.75) {
				return 7.5625 * (p -= 2.25 / 2.75) * p + 0.9375;
			}
			return 7.5625 * (p -= 2.625 / 2.75) * p + 0.984375;
		}), _create("BounceIn", function (p) {
			if ((p = 1 - p) < 1 / 2.75) {
				return 1 - 7.5625 * p * p;
			} else if (p < 2 / 2.75) {
				return 1 - (7.5625 * (p -= 1.5 / 2.75) * p + 0.75);
			} else if (p < 2.5 / 2.75) {
				return 1 - (7.5625 * (p -= 2.25 / 2.75) * p + 0.9375);
			}
			return 1 - (7.5625 * (p -= 2.625 / 2.75) * p + 0.984375);
		}), _create("BounceInOut", function (p) {
			var invert = p < 0.5;
			if (invert) {
				p = 1 - p * 2;
			} else {
				p = p * 2 - 1;
			}
			if (p < 1 / 2.75) {
				p = 7.5625 * p * p;
			} else if (p < 2 / 2.75) {
				p = 7.5625 * (p -= 1.5 / 2.75) * p + 0.75;
			} else if (p < 2.5 / 2.75) {
				p = 7.5625 * (p -= 2.25 / 2.75) * p + 0.9375;
			} else {
				p = 7.5625 * (p -= 2.625 / 2.75) * p + 0.984375;
			}
			return invert ? (1 - p) * 0.5 : p * 0.5 + 0.5;
		}));

		//CIRC
		_wrap("Circ", _create("CircOut", function (p) {
			return Math.sqrt(1 - (p = p - 1) * p);
		}), _create("CircIn", function (p) {
			return -(Math.sqrt(1 - p * p) - 1);
		}), _create("CircInOut", function (p) {
			return (p *= 2) < 1 ? -0.5 * (Math.sqrt(1 - p * p) - 1) : 0.5 * (Math.sqrt(1 - (p -= 2) * p) + 1);
		}));

		//Elastic
		_createElastic = function _createElastic(n, f, def) {
			var C = _class("easing." + n, function (amplitude, period) {
				this._p1 = amplitude >= 1 ? amplitude : 1; //note: if amplitude is < 1, we simply adjust the period for a more natural feel. Otherwise the math doesn't work right and the curve starts at 1.
				this._p2 = (period || def) / (amplitude < 1 ? amplitude : 1);
				this._p3 = this._p2 / _2PI * (Math.asin(1 / this._p1) || 0);
				this._p2 = _2PI / this._p2; //precalculate to optimize
			}, true),
			    p = C.prototype = new Ease();
			p.constructor = C;
			p.getRatio = f;
			p.config = function (amplitude, period) {
				return new C(amplitude, period);
			};
			return C;
		};
		_wrap("Elastic", _createElastic("ElasticOut", function (p) {
			return this._p1 * Math.pow(2, -10 * p) * Math.sin((p - this._p3) * this._p2) + 1;
		}, 0.3), _createElastic("ElasticIn", function (p) {
			return -(this._p1 * Math.pow(2, 10 * (p -= 1)) * Math.sin((p - this._p3) * this._p2));
		}, 0.3), _createElastic("ElasticInOut", function (p) {
			return (p *= 2) < 1 ? -0.5 * (this._p1 * Math.pow(2, 10 * (p -= 1)) * Math.sin((p - this._p3) * this._p2)) : this._p1 * Math.pow(2, -10 * (p -= 1)) * Math.sin((p - this._p3) * this._p2) * 0.5 + 1;
		}, 0.45));

		//Expo
		_wrap("Expo", _create("ExpoOut", function (p) {
			return 1 - Math.pow(2, -10 * p);
		}), _create("ExpoIn", function (p) {
			return Math.pow(2, 10 * (p - 1)) - 0.001;
		}), _create("ExpoInOut", function (p) {
			return (p *= 2) < 1 ? 0.5 * Math.pow(2, 10 * (p - 1)) : 0.5 * (2 - Math.pow(2, -10 * (p - 1)));
		}));

		//Sine
		_wrap("Sine", _create("SineOut", function (p) {
			return Math.sin(p * _HALF_PI);
		}), _create("SineIn", function (p) {
			return -Math.cos(p * _HALF_PI) + 1;
		}), _create("SineInOut", function (p) {
			return -0.5 * (Math.cos(Math.PI * p) - 1);
		}));

		_class("easing.EaseLookup", {
			find: function find(s) {
				return Ease.map[s];
			}
		}, true);

		//register the non-standard eases
		_easeReg(w.SlowMo, "SlowMo", "ease,");
		_easeReg(RoughEase, "RoughEase", "ease,");
		_easeReg(SteppedEase, "SteppedEase", "ease,");

		return Back;
	}, true);
});if (_gsScope._gsDefine) {
	_gsScope._gsQueue.pop()();
}

//export to AMD/RequireJS and CommonJS/Node (precursor to full modular build system coming at a later date)
(function () {
	"use strict";

	var getGlobal = function getGlobal() {
		return _gsScope.GreenSockGlobals || _gsScope;
	};
	if (typeof module !== "undefined" && module.exports) {
		//node
		__webpack_require__(0);
		module.exports = getGlobal();
	} else if (true) {
		//AMD
		!(__WEBPACK_AMD_DEFINE_ARRAY__ = [__webpack_require__(0)], __WEBPACK_AMD_DEFINE_FACTORY__ = (getGlobal),
				__WEBPACK_AMD_DEFINE_RESULT__ = (typeof __WEBPACK_AMD_DEFINE_FACTORY__ === 'function' ?
				(__WEBPACK_AMD_DEFINE_FACTORY__.apply(exports, __WEBPACK_AMD_DEFINE_ARRAY__)) : __WEBPACK_AMD_DEFINE_FACTORY__),
				__WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));
	}
})();
/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(2)))

/***/ }),
/* 7 */
/***/ (function(module, exports) {

// shim for using process in browser
var process = module.exports = {};

// cached from whatever global is present so that test runners that stub it
// don't break things.  But we need to wrap it in a try catch in case it is
// wrapped in strict mode code which doesn't define any globals.  It's inside a
// function because try/catches deoptimize in certain engines.

var cachedSetTimeout;
var cachedClearTimeout;

function defaultSetTimout() {
    throw new Error('setTimeout has not been defined');
}
function defaultClearTimeout () {
    throw new Error('clearTimeout has not been defined');
}
(function () {
    try {
        if (typeof setTimeout === 'function') {
            cachedSetTimeout = setTimeout;
        } else {
            cachedSetTimeout = defaultSetTimout;
        }
    } catch (e) {
        cachedSetTimeout = defaultSetTimout;
    }
    try {
        if (typeof clearTimeout === 'function') {
            cachedClearTimeout = clearTimeout;
        } else {
            cachedClearTimeout = defaultClearTimeout;
        }
    } catch (e) {
        cachedClearTimeout = defaultClearTimeout;
    }
} ())
function runTimeout(fun) {
    if (cachedSetTimeout === setTimeout) {
        //normal enviroments in sane situations
        return setTimeout(fun, 0);
    }
    // if setTimeout wasn't available but was latter defined
    if ((cachedSetTimeout === defaultSetTimout || !cachedSetTimeout) && setTimeout) {
        cachedSetTimeout = setTimeout;
        return setTimeout(fun, 0);
    }
    try {
        // when when somebody has screwed with setTimeout but no I.E. maddness
        return cachedSetTimeout(fun, 0);
    } catch(e){
        try {
            // When we are in I.E. but the script has been evaled so I.E. doesn't trust the global object when called normally
            return cachedSetTimeout.call(null, fun, 0);
        } catch(e){
            // same as above but when it's a version of I.E. that must have the global object for 'this', hopfully our context correct otherwise it will throw a global error
            return cachedSetTimeout.call(this, fun, 0);
        }
    }


}
function runClearTimeout(marker) {
    if (cachedClearTimeout === clearTimeout) {
        //normal enviroments in sane situations
        return clearTimeout(marker);
    }
    // if clearTimeout wasn't available but was latter defined
    if ((cachedClearTimeout === defaultClearTimeout || !cachedClearTimeout) && clearTimeout) {
        cachedClearTimeout = clearTimeout;
        return clearTimeout(marker);
    }
    try {
        // when when somebody has screwed with setTimeout but no I.E. maddness
        return cachedClearTimeout(marker);
    } catch (e){
        try {
            // When we are in I.E. but the script has been evaled so I.E. doesn't  trust the global object when called normally
            return cachedClearTimeout.call(null, marker);
        } catch (e){
            // same as above but when it's a version of I.E. that must have the global object for 'this', hopfully our context correct otherwise it will throw a global error.
            // Some versions of I.E. have different rules for clearTimeout vs setTimeout
            return cachedClearTimeout.call(this, marker);
        }
    }



}
var queue = [];
var draining = false;
var currentQueue;
var queueIndex = -1;

function cleanUpNextTick() {
    if (!draining || !currentQueue) {
        return;
    }
    draining = false;
    if (currentQueue.length) {
        queue = currentQueue.concat(queue);
    } else {
        queueIndex = -1;
    }
    if (queue.length) {
        drainQueue();
    }
}

function drainQueue() {
    if (draining) {
        return;
    }
    var timeout = runTimeout(cleanUpNextTick);
    draining = true;

    var len = queue.length;
    while(len) {
        currentQueue = queue;
        queue = [];
        while (++queueIndex < len) {
            if (currentQueue) {
                currentQueue[queueIndex].run();
            }
        }
        queueIndex = -1;
        len = queue.length;
    }
    currentQueue = null;
    draining = false;
    runClearTimeout(timeout);
}

process.nextTick = function (fun) {
    var args = new Array(arguments.length - 1);
    if (arguments.length > 1) {
        for (var i = 1; i < arguments.length; i++) {
            args[i - 1] = arguments[i];
        }
    }
    queue.push(new Item(fun, args));
    if (queue.length === 1 && !draining) {
        runTimeout(drainQueue);
    }
};

// v8 likes predictible objects
function Item(fun, array) {
    this.fun = fun;
    this.array = array;
}
Item.prototype.run = function () {
    this.fun.apply(null, this.array);
};
process.title = 'browser';
process.browser = true;
process.env = {};
process.argv = [];
process.version = ''; // empty string to avoid regexp issues
process.versions = {};

function noop() {}

process.on = noop;
process.addListener = noop;
process.once = noop;
process.off = noop;
process.removeListener = noop;
process.removeAllListeners = noop;
process.emit = noop;
process.prependListener = noop;
process.prependOnceListener = noop;

process.listeners = function (name) { return [] }

process.binding = function (name) {
    throw new Error('process.binding is not supported');
};

process.cwd = function () { return '/' };
process.chdir = function (dir) {
    throw new Error('process.chdir is not supported');
};
process.umask = function() { return 0; };


/***/ }),
/* 8 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Link = function () {
    function Link() {
        _classCallCheck(this, Link);

        this.link = document.createElement("a");
    }

    _createClass(Link, [{
        key: 'setPath',
        value: function setPath(href) {
            this.link.href = href;
        }
    }, {
        key: 'getPath',
        value: function getPath(href) {
            var path = this.link.pathname;
            if (path[0] != '/') {
                path = '/' + path;
            }
            return path;
        }
    }, {
        key: 'getHash',
        value: function getHash(href) {
            return this.link.hash;
        }
    }]);

    return Link;
}();

exports.default = Link;

/***/ }),
/* 9 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.default = scrollTo;
var body = document.body;

var UP = -1;
var DOWN = 1;

var friction = 0.7;
var acceleration = 0.04;

var positionY = 100;
var velocityY = 0;
var targetPositionY = 400;

var raf = null;

function getScrollTop() {
    return document.body.scrollTop || document.documentElement.scrollTop;
}

function animate() {
    var distance = update();
    render();

    if (Math.abs(distance) > 0.1) {
        raf = requestAnimationFrame(animate);
    }
}

function update() {
    var distance = targetPositionY - positionY;
    var attraction = distance * acceleration;

    applyForce(attraction);

    velocityY *= friction;
    positionY += velocityY;

    return distance;
}

function applyForce(force) {
    velocityY += force;
}

function render() {
    window.scrollTo(0, positionY);
}

window.addEventListener('mousewheel', function (event) {
    if (raf) {
        cancelAnimationFrame(raf);
        raf = null;
    }
}, {
    passive: true
});

function scrollTo(offset, callback) {
    positionY = getScrollTop();
    targetPositionY = offset;
    velocityY = 0;
    animate();
}

// WEBPACK FOOTER //
// ./services/Viewport/scrollTo.js

/***/ }),
/* 10 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.destroyInstance = exports.createInstance = undefined;
exports.loadComponents = loadComponents;
exports.removeComponents = removeComponents;

var _components = __webpack_require__(11);

var _components2 = _interopRequireDefault(_components);

var _Component = __webpack_require__(1);

var _Component2 = _interopRequireDefault(_Component);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function loadComponents() {
    var context = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : document.documentElement;

    if (!_components2.default || Object.keys(_components2.default).length === 0) {
        console.warn('App has no components');
        return;
    }

    return context.querySelectorAll('[data-component]').forEach(function (element) {
        var instance = _Component2.default.getFromElement(element);

        if (instance) {
            console.warn('Error: instance exists: \n', instance);
            return true; // continue
        }

        var componentName = element.getAttribute('data-component');

        if (typeof _components2.default[componentName] === 'function') {
            createInstance(element, componentName);
        } else {
            console.warn('Constructor for component "' + componentName + '" not found.');
        }
    });
}

function removeComponents() {
    var context = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : document.documentElement;

    context.querySelectorAll('[data-component]').forEach(function (element) {
        destroyInstance(element);
    });
}

var createInstance = exports.createInstance = function createInstance(element, componentName) {
    _components2.default[componentName].prototype._name = componentName;
    var component = new _components2.default[componentName](element);
    component._load();

    console.info('Created instance of component "' + componentName + '".');
    return component;
};

var destroyInstance = exports.destroyInstance = function destroyInstance(element) {
    var instance = _Component2.default.getFromElement(element);
    if (instance) {
        var name = instance._name;
        instance.destroy();
        element['__giant_component__'] = null;
        console.info('Removed component "' + name + '".');
    }
};

/***/ }),
/* 11 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _Cards = __webpack_require__(56);

var _Cards2 = _interopRequireDefault(_Cards);

var _Carousel = __webpack_require__(57);

var _Carousel2 = _interopRequireDefault(_Carousel);

var _DetectImageLoad = __webpack_require__(58);

var _DetectImageLoad2 = _interopRequireDefault(_DetectImageLoad);

var _Explode = __webpack_require__(59);

var _Explode2 = _interopRequireDefault(_Explode);

var _Hero = __webpack_require__(69);

var _Hero2 = _interopRequireDefault(_Hero);

var _Magnetic = __webpack_require__(70);

var _Magnetic2 = _interopRequireDefault(_Magnetic);

var _MenuOpener = __webpack_require__(71);

var _MenuOpener2 = _interopRequireDefault(_MenuOpener);

var _Nav = __webpack_require__(72);

var _Nav2 = _interopRequireDefault(_Nav);

var _Parallax = __webpack_require__(74);

var _Parallax2 = _interopRequireDefault(_Parallax);

var _Slideshow = __webpack_require__(75);

var _Slideshow2 = _interopRequireDefault(_Slideshow);

var _Youtube = __webpack_require__(76);

var _Youtube2 = _interopRequireDefault(_Youtube);

var _Question = __webpack_require__(77);

var _Question2 = _interopRequireDefault(_Question);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var Components = {
    Cards: _Cards2.default,
    Carousel: _Carousel2.default,
    DetectImageLoad: _DetectImageLoad2.default,
    Explode: _Explode2.default,
    Hero: _Hero2.default,
    //Gallery: Gallery,
    //Liquid: Liquid,
    Magnetic: _Magnetic2.default,
    MenuOpener: _MenuOpener2.default,
    Nav: _Nav2.default,
    Parallax: _Parallax2.default,
    Slideshow: _Slideshow2.default,
    Youtube: _Youtube2.default,
    Question: _Question2.default
};
// import Gallery from './Gallery';
//import Liquid from './Liquid';
exports.default = Components;

/***/ }),
/* 12 */
/***/ (function(module, exports, __webpack_require__) {

var freeGlobal = __webpack_require__(62);

/** Detect free variable `self`. */
var freeSelf = typeof self == 'object' && self && self.Object === Object && self;

/** Used as a reference to the global object. */
var root = freeGlobal || freeSelf || Function('return this')();

module.exports = root;


/***/ }),
/* 13 */
/***/ (function(module, exports, __webpack_require__) {

var root = __webpack_require__(12);

/** Built-in value references. */
var Symbol = root.Symbol;

module.exports = Symbol;


/***/ }),
/* 14 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/* WEBPACK VAR INJECTION */(function(global) {var __WEBPACK_AMD_DEFINE_FACTORY__, __WEBPACK_AMD_DEFINE_ARRAY__, __WEBPACK_AMD_DEFINE_RESULT__;

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

/*!
 * VERSION: 0.8.11
 * DATE: 2017-04-29
 * UPDATES AND DOCS AT: http://greensock.com
 *
 * @license Copyright (c) 2008-2017, GreenSock. All rights reserved.
 * MorphSVGPlugin is a Club GreenSock membership benefit; You must have a valid membership to use
 * this code without violating the terms of use. Visit http://greensock.com/club/ to sign up or get more details.
 * This work is subject to the software agreement that was issued with your membership.
 * 
 * @author: Jack Doyle, jack@greensock.com
 */
var _gsScope = typeof module !== "undefined" && module.exports && typeof global !== "undefined" ? global : undefined || window; //helps ensure compatibility with AMD/RequireJS and CommonJS/Node
(_gsScope._gsQueue || (_gsScope._gsQueue = [])).push(function () {

	"use strict";

	var _DEG2RAD = Math.PI / 180,
	    _RAD2DEG = 180 / Math.PI,
	    _svgPathExp = /[achlmqstvz]|(-?\d*\.?\d*(?:e[\-+]?\d+)?)[0-9]/ig,
	    _numbersExp = /(?:(-|-=|\+=)?\d*\.?\d*(?:e[\-+]?\d+)?)[0-9]/ig,
	    _selectorExp = /(^[#\.]|[a-y][a-z])/gi,
	    _commands = /[achlmqstvz]/ig,
	    _scientific = /[\+\-]?\d*\.?\d+e[\+\-]?\d+/ig,

	//_attrExp = /(\S+)=["']?((?:.(?!["']?\s+(?:\S+)=|[>"']))+.)["']?/gi, //finds all the attribute name/value pairs in an HTML element
	//_outerTagExp = /^<([A-Za-z0-9_\-]+)((?:\s+[A-Za-z0-9_\-]+(?:\s*=\s*(?:(?:"[^"]*")|(?:'[^']*')|[^>\s]+))?)*)\s*(\/?)>/i, //takes the outerHTML and pulls out [0] - the first tag, [1] - the tag name, and [2] - the attribute name/value pairs (space-delimited)
	//_wrappingQuotesExp = /^["']|["']$/g,
	TweenLite = _gsScope._gsDefine.globals.TweenLite,

	//_nonNumbersExp = /(?:([\-+](?!(\d|=)))|[^\d\-+=e]|(e(?![\-+][\d])))+/ig,

	_log = function _log(message) {
		if (_gsScope.console) {
			console.log(message);
		}
	},


	// translates an arc into a normalized array of cubic beziers excluding the starting x/y. The circle the arc follows will be centered at 0,0 and have a radius of 1 (hence normalized). Each bezier covers no more than 90 degrees; the arc will be divided evenly into a maximum of four curves.
	_normalizedArcToBeziers = function _normalizedArcToBeziers(angleStart, angleExtent) {
		var segments = Math.ceil(Math.abs(angleExtent) / 90),
		    l = 0,
		    a = [],
		    angleIncrement,
		    controlLength,
		    angle,
		    dx,
		    dy,
		    i;
		angleStart *= _DEG2RAD;
		angleExtent *= _DEG2RAD;
		angleIncrement = angleExtent / segments;
		controlLength = 4 / 3 * Math.sin(angleIncrement / 2) / (1 + Math.cos(angleIncrement / 2));
		for (i = 0; i < segments; i++) {
			angle = angleStart + i * angleIncrement;
			dx = Math.cos(angle);
			dy = Math.sin(angle);
			a[l++] = dx - controlLength * dy;
			a[l++] = dy + controlLength * dx;
			angle += angleIncrement;
			dx = Math.cos(angle);
			dy = Math.sin(angle);
			a[l++] = dx + controlLength * dy;
			a[l++] = dy - controlLength * dx;
			a[l++] = dx;
			a[l++] = dy;
		}
		return a;
	},


	// translates SVG arc data into an array of cubic beziers
	_arcToBeziers = function _arcToBeziers(lastX, lastY, rx, ry, angle, largeArcFlag, sweepFlag, x, y) {
		if (lastX === x && lastY === y) {
			return;
		}
		rx = Math.abs(rx);
		ry = Math.abs(ry);
		var angleRad = angle % 360 * _DEG2RAD,
		    cosAngle = Math.cos(angleRad),
		    sinAngle = Math.sin(angleRad),
		    dx2 = (lastX - x) / 2,
		    dy2 = (lastY - y) / 2,
		    x1 = cosAngle * dx2 + sinAngle * dy2,
		    y1 = -sinAngle * dx2 + cosAngle * dy2,
		    rx_sq = rx * rx,
		    ry_sq = ry * ry,
		    x1_sq = x1 * x1,
		    y1_sq = y1 * y1,
		    radiiCheck = x1_sq / rx_sq + y1_sq / ry_sq;
		if (radiiCheck > 1) {
			rx = Math.sqrt(radiiCheck) * rx;
			ry = Math.sqrt(radiiCheck) * ry;
			rx_sq = rx * rx;
			ry_sq = ry * ry;
		}
		var sign = largeArcFlag === sweepFlag ? -1 : 1,
		    sq = (rx_sq * ry_sq - rx_sq * y1_sq - ry_sq * x1_sq) / (rx_sq * y1_sq + ry_sq * x1_sq);
		if (sq < 0) {
			sq = 0;
		}
		var coef = sign * Math.sqrt(sq),
		    cx1 = coef * (rx * y1 / ry),
		    cy1 = coef * -(ry * x1 / rx),
		    sx2 = (lastX + x) / 2,
		    sy2 = (lastY + y) / 2,
		    cx = sx2 + (cosAngle * cx1 - sinAngle * cy1),
		    cy = sy2 + (sinAngle * cx1 + cosAngle * cy1),
		    ux = (x1 - cx1) / rx,
		    uy = (y1 - cy1) / ry,
		    vx = (-x1 - cx1) / rx,
		    vy = (-y1 - cy1) / ry,
		    n = Math.sqrt(ux * ux + uy * uy),
		    p = ux;
		sign = uy < 0 ? -1 : 1;
		var angleStart = sign * Math.acos(p / n) * _RAD2DEG;

		n = Math.sqrt((ux * ux + uy * uy) * (vx * vx + vy * vy));
		p = ux * vx + uy * vy;
		sign = ux * vy - uy * vx < 0 ? -1 : 1;
		var angleExtent = sign * Math.acos(p / n) * _RAD2DEG;
		if (!sweepFlag && angleExtent > 0) {
			angleExtent -= 360;
		} else if (sweepFlag && angleExtent < 0) {
			angleExtent += 360;
		}
		angleExtent %= 360;
		angleStart %= 360;

		var bezierPoints = _normalizedArcToBeziers(angleStart, angleExtent),
		    a = cosAngle * rx,
		    b = sinAngle * rx,
		    c = sinAngle * -ry,
		    d = cosAngle * ry,
		    l = bezierPoints.length - 2,
		    i,
		    px,
		    py;
		//translate all the bezier points according to the matrix...
		for (i = 0; i < l; i += 2) {
			px = bezierPoints[i];
			py = bezierPoints[i + 1];
			bezierPoints[i] = px * a + py * c + cx;
			bezierPoints[i + 1] = px * b + py * d + cy;
		}
		bezierPoints[bezierPoints.length - 2] = x; //always set the end to exactly where it's supposed to be
		bezierPoints[bezierPoints.length - 1] = y;
		return bezierPoints;
	},


	//Spits back an array of cubic Bezier segments that use absolute coordinates. Each segment starts with a "moveTo" command (x coordinate, then y) and then 2 control points (x, y, x, y), then anchor. The goal is to minimize memory and maximize speed.
	_pathDataToBezier = function _pathDataToBezier(d) {
		var a = (d + "").replace(_scientific, function (m) {
			var n = +m;return n < 0.0001 && n > -0.0001 ? 0 : n;
		}).match(_svgPathExp) || [],
		    //some authoring programs spit out very small numbers in scientific notation like "1e-5", so make sure we round that down to 0 first.
		path = [],
		    relativeX = 0,
		    relativeY = 0,
		    elements = a.length,
		    l = 2,
		    points = 0,
		    i,
		    j,
		    x,
		    y,
		    command,
		    isRelative,
		    segment,
		    startX,
		    startY,
		    difX,
		    difY,
		    beziers,
		    prevCommand;
		if (!d || !isNaN(a[0]) || isNaN(a[1])) {
			_log("ERROR: malformed path data: " + d);
			return path;
		}
		for (i = 0; i < elements; i++) {
			prevCommand = command;
			if (isNaN(a[i])) {
				command = a[i].toUpperCase();
				isRelative = command !== a[i]; //lower case means relative
			} else {
				//commands like "C" can be strung together without any new command characters between.
				i--;
			}
			x = +a[i + 1];
			y = +a[i + 2];
			if (isRelative) {
				x += relativeX;
				y += relativeY;
			}
			if (i === 0) {
				startX = x;
				startY = y;
			}

			// "M" (move)
			if (command === "M") {
				if (segment && segment.length < 8) {
					//if the path data was funky and just had a M with no actual drawing anywhere, skip it.
					path.length -= 1;
					l = 0;
				}
				relativeX = startX = x;
				relativeY = startY = y;
				segment = [x, y];
				points += l;
				l = 2;
				path.push(segment);
				i += 2;
				command = "L"; //an "M" with more than 2 values gets interpreted as "lineTo" commands ("L").

				// "C" (cubic bezier)
			} else if (command === "C") {
				if (!segment) {
					segment = [0, 0];
				}
				segment[l++] = x;
				segment[l++] = y;
				if (!isRelative) {
					relativeX = relativeY = 0;
				}
				segment[l++] = relativeX + a[i + 3] * 1; //note: "*1" is just a fast/short way to cast the value as a Number. WAAAY faster in Chrome, slightly slower in Firefox.
				segment[l++] = relativeY + a[i + 4] * 1;
				segment[l++] = relativeX = relativeX + a[i + 5] * 1;
				segment[l++] = relativeY = relativeY + a[i + 6] * 1;
				//if (y === segment[l-1] && y === segment[l-3] && x === segment[l-2] && x === segment[l-4]) { //if all the values are the same, eliminate the waste.
				//	segment.length = l = l-6;
				//}
				i += 6;

				// "S" (continuation of cubic bezier)
			} else if (command === "S") {
				if (prevCommand === "C" || prevCommand === "S") {
					difX = relativeX - segment[l - 4];
					difY = relativeY - segment[l - 3];
					segment[l++] = relativeX + difX;
					segment[l++] = relativeY + difY;
				} else {
					segment[l++] = relativeX;
					segment[l++] = relativeY;
				}
				segment[l++] = x;
				segment[l++] = y;
				if (!isRelative) {
					relativeX = relativeY = 0;
				}
				segment[l++] = relativeX = relativeX + a[i + 3] * 1;
				segment[l++] = relativeY = relativeY + a[i + 4] * 1;
				//if (y === segment[l-1] && y === segment[l-3] && x === segment[l-2] && x === segment[l-4]) { //if all the values are the same, eliminate the waste.
				//	segment.length = l = l-6;
				//}
				i += 4;

				// "Q" (quadratic bezier)
			} else if (command === "Q") {
				difX = x - relativeX;
				difY = y - relativeY;
				segment[l++] = relativeX + difX * 2 / 3;
				segment[l++] = relativeY + difY * 2 / 3;
				if (!isRelative) {
					relativeX = relativeY = 0;
				}
				relativeX = relativeX + a[i + 3] * 1;
				relativeY = relativeY + a[i + 4] * 1;
				difX = x - relativeX;
				difY = y - relativeY;
				segment[l++] = relativeX + difX * 2 / 3;
				segment[l++] = relativeY + difY * 2 / 3;
				segment[l++] = relativeX;
				segment[l++] = relativeY;

				i += 4;

				// "T" (continuation of quadratic bezier)
			} else if (command === "T") {
				difX = relativeX - segment[l - 4];
				difY = relativeY - segment[l - 3];
				segment[l++] = relativeX + difX;
				segment[l++] = relativeY + difY;
				difX = relativeX + difX * 1.5 - x;
				difY = relativeY + difY * 1.5 - y;
				segment[l++] = x + difX * 2 / 3;
				segment[l++] = y + difY * 2 / 3;
				segment[l++] = relativeX = x;
				segment[l++] = relativeY = y;

				i += 2;

				// "H" (horizontal line)
			} else if (command === "H") {
				y = relativeY;
				//if (x !== relativeX) {
				segment[l++] = relativeX + (x - relativeX) / 3;
				segment[l++] = relativeY + (y - relativeY) / 3;
				segment[l++] = relativeX + (x - relativeX) * 2 / 3;
				segment[l++] = relativeY + (y - relativeY) * 2 / 3;
				segment[l++] = relativeX = x;
				segment[l++] = y;
				//}
				i += 1;

				// "V" (horizontal line)
			} else if (command === "V") {
				y = x; //adjust values because the first (and only one) isn't x in this case, it's y.
				x = relativeX;
				if (isRelative) {
					y += relativeY - relativeX;
				}
				//if (y !== relativeY) {
				segment[l++] = x;
				segment[l++] = relativeY + (y - relativeY) / 3;
				segment[l++] = x;
				segment[l++] = relativeY + (y - relativeY) * 2 / 3;
				segment[l++] = x;
				segment[l++] = relativeY = y;
				//}
				i += 1;

				// "L" (line) or "Z" (close)
			} else if (command === "L" || command === "Z") {
				if (command === "Z") {
					x = startX;
					y = startY;
					segment.closed = true;
				}
				if (command === "L" || Math.abs(relativeX - x) > 0.5 || Math.abs(relativeY - y) > 0.5) {
					segment[l++] = relativeX + (x - relativeX) / 3;
					segment[l++] = relativeY + (y - relativeY) / 3;
					segment[l++] = relativeX + (x - relativeX) * 2 / 3;
					segment[l++] = relativeY + (y - relativeY) * 2 / 3;
					segment[l++] = x;
					segment[l++] = y;
					if (command === "L") {
						i += 2;
					}
				}
				relativeX = x;
				relativeY = y;

				// "A" (arc)
			} else if (command === "A") {
				beziers = _arcToBeziers(relativeX, relativeY, a[i + 1] * 1, a[i + 2] * 1, a[i + 3] * 1, a[i + 4] * 1, a[i + 5] * 1, (isRelative ? relativeX : 0) + a[i + 6] * 1, (isRelative ? relativeY : 0) + a[i + 7] * 1);
				if (beziers) {
					for (j = 0; j < beziers.length; j++) {
						segment[l++] = beziers[j];
					}
				}
				relativeX = segment[l - 2];
				relativeY = segment[l - 1];
				i += 7;
			} else {
				_log("Error: malformed path data: " + d);
			}
		}
		path.totalPoints = points + l;
		return path;
	},


	//adds a certain number of Beziers while maintaining the path shape (so that the start/end values can have a matching quantity of points to animate). Only pass in ONE segment of the Bezier at a time. Format: [xAnchor, yAnchor, xControlPoint1, yControlPoint1, xControlPoint2, yControlPoint2, xAnchor, yAnchor, xControlPoint1, etc...]
	_subdivideBezier = function _subdivideBezier(bezier, quantity) {
		var tally = 0,
		    max = 0.999999,
		    l = bezier.length,
		    newPointsPerSegment = quantity / ((l - 2) / 6),
		    ax,
		    ay,
		    cp1x,
		    cp1y,
		    cp2x,
		    cp2y,
		    bx,
		    by,
		    x1,
		    y1,
		    x2,
		    y2,
		    i,
		    t;
		for (i = 2; i < l; i += 6) {
			tally += newPointsPerSegment;
			while (tally > max) {
				//compare with 0.99999 instead of 1 in order to prevent rounding errors
				ax = bezier[i - 2];
				ay = bezier[i - 1];
				cp1x = bezier[i];
				cp1y = bezier[i + 1];
				cp2x = bezier[i + 2];
				cp2y = bezier[i + 3];
				bx = bezier[i + 4];
				by = bezier[i + 5];
				t = 1 / (Math.floor(tally) + 1); //progress along the bezier (value between 0 and 1)

				x1 = ax + (cp1x - ax) * t;
				x2 = cp1x + (cp2x - cp1x) * t;
				x1 += (x2 - x1) * t;
				x2 += (cp2x + (bx - cp2x) * t - x2) * t;

				y1 = ay + (cp1y - ay) * t;
				y2 = cp1y + (cp2y - cp1y) * t;
				y1 += (y2 - y1) * t;
				y2 += (cp2y + (by - cp2y) * t - y2) * t;

				bezier.splice(i, 4, ax + (cp1x - ax) * t, //first control point
				ay + (cp1y - ay) * t, x1, //second control point
				y1, x1 + (x2 - x1) * t, //new fabricated anchor on line
				y1 + (y2 - y1) * t, x2, //third control point
				y2, cp2x + (bx - cp2x) * t, //fourth control point
				cp2y + (by - cp2y) * t);
				i += 6;
				l += 6;
				tally--;
			}
		}
		return bezier;
	},
	    _bezierToPathData = function _bezierToPathData(beziers) {
		var data = "",
		    l = beziers.length,
		    rnd = 100,
		    sl,
		    s,
		    i,
		    segment;
		for (s = 0; s < l; s++) {
			segment = beziers[s];
			data += "M" + segment[0] + "," + segment[1] + " C";
			sl = segment.length;
			for (i = 2; i < sl; i++) {
				data += (segment[i++] * rnd | 0) / rnd + "," + (segment[i++] * rnd | 0) / rnd + " " + (segment[i++] * rnd | 0) / rnd + "," + (segment[i++] * rnd | 0) / rnd + " " + (segment[i++] * rnd | 0) / rnd + "," + (segment[i] * rnd | 0) / rnd + " ";
			}
			if (segment.closed) {
				data += "z";
			}
		}
		return data;
	},
	    _reverseBezier = function _reverseBezier(bezier) {
		var a = [],
		    i = bezier.length - 1,
		    l = 0;
		while (--i > -1) {
			a[l++] = bezier[i];
			a[l++] = bezier[i + 1];
			i--;
		}
		for (i = 0; i < l; i++) {
			bezier[i] = a[i];
		}
		bezier.reversed = bezier.reversed ? false : true;
	},
	    _getAverageXY = function _getAverageXY(bezier) {
		var l = bezier.length,
		    x = 0,
		    y = 0,
		    i;
		for (i = 0; i < l; i++) {
			x += bezier[i++];
			y += bezier[i];
		}
		return [x / (l / 2), y / (l / 2)];
	},
	    _getSize = function _getSize(bezier) {
		//rough estimate of the bounding box (based solely on the anchors) of a single segment. sets "size", "centerX", and "centerY" properties on the bezier array itself, and returns the size (width * height)
		var l = bezier.length,
		    xMax = bezier[0],
		    xMin = xMax,
		    yMax = bezier[1],
		    yMin = yMax,
		    x,
		    y,
		    i;
		for (i = 6; i < l; i += 6) {
			x = bezier[i];
			y = bezier[i + 1];
			if (x > xMax) {
				xMax = x;
			} else if (x < xMin) {
				xMin = x;
			}
			if (y > yMax) {
				yMax = y;
			} else if (y < yMin) {
				yMin = y;
			}
		}
		bezier.centerX = (xMax + xMin) / 2;
		bezier.centerY = (yMax + yMin) / 2;
		return bezier.size = (xMax - xMin) * (yMax - yMin);
	},
	    _getTotalSize = function _getTotalSize(bezier) {
		//rough estimate of the bounding box of the entire list of Bezier segments (based solely on the anchors). sets "size", "centerX", and "centerY" properties on the bezier array itself, and returns the size (width * height)
		var segment = bezier.length,
		    xMax = bezier[0][0],
		    xMin = xMax,
		    yMax = bezier[0][1],
		    yMin = yMax,
		    l,
		    x,
		    y,
		    i,
		    b;
		while (--segment > -1) {
			b = bezier[segment];
			l = b.length;
			for (i = 6; i < l; i += 6) {
				x = b[i];
				y = b[i + 1];
				if (x > xMax) {
					xMax = x;
				} else if (x < xMin) {
					xMin = x;
				}
				if (y > yMax) {
					yMax = y;
				} else if (y < yMin) {
					yMin = y;
				}
			}
		}
		bezier.centerX = (xMax + xMin) / 2;
		bezier.centerY = (yMax + yMin) / 2;
		return bezier.size = (xMax - xMin) * (yMax - yMin);
	},
	    _sortByComplexity = function _sortByComplexity(a, b) {
		return b.length - a.length;
	},
	    _sortBySize = function _sortBySize(a, b) {
		var sizeA = a.size || _getSize(a),
		    sizeB = b.size || _getSize(b);
		return Math.abs(sizeB - sizeA) < (sizeA + sizeB) / 20 ? b.centerX - a.centerX || b.centerY - a.centerY : sizeB - sizeA; //if the size is within 10% of each other, prioritize position from left to right, then top to bottom.
	},
	    _offsetBezier = function _offsetBezier(bezier, shapeIndex) {
		var a = bezier.slice(0),
		    l = bezier.length,
		    wrap = l - 2,
		    i,
		    index;
		shapeIndex = shapeIndex | 0;
		for (i = 0; i < l; i++) {
			index = (i + shapeIndex) % wrap;
			bezier[i++] = a[index];
			bezier[i] = a[index + 1];
		}
	},
	    _getTotalMovement = function _getTotalMovement(sb, eb, shapeIndex, offsetX, offsetY) {
		var l = sb.length,
		    d = 0,
		    wrap = l - 2,
		    index,
		    i,
		    x,
		    y;
		shapeIndex *= 6;
		for (i = 0; i < l; i += 6) {
			index = (i + shapeIndex) % wrap;
			y = sb[index] - (eb[i] - offsetX);
			x = sb[index + 1] - (eb[i + 1] - offsetY);
			d += Math.sqrt(x * x + y * y);
		}
		return d;
	},
	    _getClosestShapeIndex = function _getClosestShapeIndex(sb, eb, checkReverse) {
		//finds the index in a closed cubic bezier array that's closest to the angle provided (angle measured from the center or average x/y).
		var l = sb.length,
		    sCenter = _getAverageXY(sb),
		    //when comparing distances, adjust the coordinates as if the shapes are centered with each other.
		eCenter = _getAverageXY(eb),
		    offsetX = eCenter[0] - sCenter[0],
		    offsetY = eCenter[1] - sCenter[1],
		    min = _getTotalMovement(sb, eb, 0, offsetX, offsetY),
		    minIndex = 0,
		    copy,
		    d,
		    i;
		for (i = 6; i < l; i += 6) {
			d = _getTotalMovement(sb, eb, i / 6, offsetX, offsetY);
			if (d < min) {
				min = d;
				minIndex = i;
			}
		}
		if (checkReverse) {
			copy = sb.slice(0);
			_reverseBezier(copy);
			for (i = 6; i < l; i += 6) {
				d = _getTotalMovement(copy, eb, i / 6, offsetX, offsetY);
				if (d < min) {
					min = d;
					minIndex = -i;
				}
			}
		}
		return minIndex / 6;
	},
	    _getClosestAnchor = function _getClosestAnchor(bezier, x, y) {
		//finds the x/y of the anchor that's closest to the provided x/y coordinate (returns an array, like [x, y]). The bezier should be the top-level type that contains an array for each segment.
		var j = bezier.length,
		    closestDistance = 99999999999,
		    closestX = 0,
		    closestY = 0,
		    b,
		    dx,
		    dy,
		    d,
		    i,
		    l;
		while (--j > -1) {
			b = bezier[j];
			l = b.length;
			for (i = 0; i < l; i += 6) {
				dx = b[i] - x;
				dy = b[i + 1] - y;
				d = Math.sqrt(dx * dx + dy * dy);
				if (d < closestDistance) {
					closestDistance = d;
					closestX = b[i];
					closestY = b[i + 1];
				}
			}
		}
		return [closestX, closestY];
	},
	    _getClosestSegment = function _getClosestSegment(bezier, pool, startIndex, sortRatio, offsetX, offsetY) {
		//matches the bezier to the closest one in a pool (array) of beziers, assuming they are in order of size and we shouldn't drop more than 20% of the size, otherwise prioritizing location (total distance to the center). Extracts the segment out of the pool array and returns it.
		var l = pool.length,
		    index = 0,
		    minSize = Math.min(bezier.size || _getSize(bezier), pool[startIndex].size || _getSize(pool[startIndex])) * sortRatio,
		    //limit things based on a percentage of the size of either the bezier or the next element in the array, whichever is smaller.
		min = 999999999999,
		    cx = bezier.centerX + offsetX,
		    cy = bezier.centerY + offsetY,
		    size,
		    i,
		    dx,
		    dy,
		    d;
		for (i = startIndex; i < l; i++) {
			size = pool[i].size || _getSize(pool[i]);
			if (size < minSize) {
				break;
			}
			dx = pool[i].centerX - cx;
			dy = pool[i].centerY - cy;
			d = Math.sqrt(dx * dx + dy * dy);
			if (d < min) {
				index = i;
				min = d;
			}
		}
		d = pool[index];
		pool.splice(index, 1);
		return d;
	},
	    _equalizeSegmentQuantity = function _equalizeSegmentQuantity(start, end, shapeIndex, map) {
		//returns an array of shape indexes, 1 for each segment.
		var dif = end.length - start.length,
		    longer = dif > 0 ? end : start,
		    shorter = dif > 0 ? start : end,
		    added = 0,
		    sortMethod = map === "complexity" ? _sortByComplexity : _sortBySize,
		    sortRatio = map === "position" ? 0 : typeof map === "number" ? map : 0.8,
		    i = shorter.length,
		    shapeIndices = (typeof shapeIndex === "undefined" ? "undefined" : _typeof(shapeIndex)) === "object" && shapeIndex.push ? shapeIndex.slice(0) : [shapeIndex],
		    reverse = shapeIndices[0] === "reverse" || shapeIndices[0] < 0,
		    log = shapeIndex === "log",
		    eb,
		    sb,
		    b,
		    x,
		    y,
		    offsetX,
		    offsetY;
		if (!shorter[0]) {
			return;
		}
		if (longer.length > 1) {
			start.sort(sortMethod);
			end.sort(sortMethod);
			offsetX = longer.size || _getTotalSize(longer); //ensures centerX and centerY are defined (used below).
			offsetX = shorter.size || _getTotalSize(shorter);
			offsetX = longer.centerX - shorter.centerX;
			offsetY = longer.centerY - shorter.centerY;
			if (sortMethod === _sortBySize) {
				for (i = 0; i < shorter.length; i++) {
					longer.splice(i, 0, _getClosestSegment(shorter[i], longer, i, sortRatio, offsetX, offsetY));
				}
			}
		}
		if (dif) {
			if (dif < 0) {
				dif = -dif;
			}
			if (longer[0].length > shorter[0].length) {
				//since we use shorter[0] as the one to map the origination point of any brand new fabricated segments, do any subdividing first so that there are more points to choose from (if necessary)
				_subdivideBezier(shorter[0], (longer[0].length - shorter[0].length) / 6 | 0);
			}
			i = shorter.length;
			while (added < dif) {
				x = longer[i].size || _getSize(longer[i]); //just to ensure centerX and centerY are calculated which we use on the next line.
				b = _getClosestAnchor(shorter, longer[i].centerX, longer[i].centerY);
				x = b[0];
				y = b[1];
				shorter[i++] = [x, y, x, y, x, y, x, y];
				shorter.totalPoints += 8;
				added++;
			}
		}
		for (i = 0; i < start.length; i++) {
			eb = end[i];
			sb = start[i];
			dif = eb.length - sb.length;
			if (dif < 0) {
				_subdivideBezier(eb, -dif / 6 | 0);
			} else if (dif > 0) {
				_subdivideBezier(sb, dif / 6 | 0);
			}
			if (reverse && !sb.reversed) {
				_reverseBezier(sb);
			}
			shapeIndex = shapeIndices[i] || shapeIndices[i] === 0 ? shapeIndices[i] : "auto";
			if (shapeIndex) {
				//if start shape is closed, find the closest point to the start/end, and re-organize the bezier points accordingly so that the shape morphs in a more intuitive way.
				if (sb.closed || Math.abs(sb[0] - sb[sb.length - 2]) < 0.5 && Math.abs(sb[1] - sb[sb.length - 1]) < 0.5) {
					if (shapeIndex === "auto" || shapeIndex === "log") {
						shapeIndices[i] = shapeIndex = _getClosestShapeIndex(sb, eb, i === 0);
						if (shapeIndex < 0) {
							reverse = true;
							_reverseBezier(sb);
							shapeIndex = -shapeIndex;
						}
						_offsetBezier(sb, shapeIndex * 6);
					} else if (shapeIndex !== "reverse") {
						if (i && shapeIndex < 0) {
							//only happens if an array is passed as shapeIndex and a negative value is defined for an index beyond 0. Very rare, but helpful sometimes.
							_reverseBezier(sb);
						}
						_offsetBezier(sb, (shapeIndex < 0 ? -shapeIndex : shapeIndex) * 6);
					}
					//otherwise, if it's not a closed shape, consider reversing it if that would make the overall travel less
				} else if (!reverse && (shapeIndex === "auto" && Math.abs(eb[0] - sb[0]) + Math.abs(eb[1] - sb[1]) + Math.abs(eb[eb.length - 2] - sb[sb.length - 2]) + Math.abs(eb[eb.length - 1] - sb[sb.length - 1]) > Math.abs(eb[0] - sb[sb.length - 2]) + Math.abs(eb[1] - sb[sb.length - 1]) + Math.abs(eb[eb.length - 2] - sb[0]) + Math.abs(eb[eb.length - 1] - sb[1]) || shapeIndex % 2)) {
					_reverseBezier(sb);
					shapeIndices[i] = -1;
					reverse = true;
				} else if (shapeIndex === "auto") {
					shapeIndices[i] = 0;
				} else if (shapeIndex === "reverse") {
					shapeIndices[i] = -1;
				}
				if (sb.closed !== eb.closed) {
					//if one is closed and one isn't, don't close either one otherwise the tweening will look weird (but remember, the beginning and final states will honor the actual values, so this only affects the inbetween state)
					sb.closed = eb.closed = false;
				}
			}
		}
		if (log) {
			_log("shapeIndex:[" + shapeIndices.join(",") + "]");
		}
		return shapeIndices;
	},
	    _pathFilter = function _pathFilter(a, shapeIndex, map, precompile) {
		var start = _pathDataToBezier(a[0]),
		    end = _pathDataToBezier(a[1]);
		if (!_equalizeSegmentQuantity(start, end, shapeIndex || shapeIndex === 0 ? shapeIndex : "auto", map)) {
			return; //malformed path data or null target
		}
		a[0] = _bezierToPathData(start);
		a[1] = _bezierToPathData(end);
		if (precompile === "log" || precompile === true) {
			_log('precompile:["' + a[0] + '","' + a[1] + '"]');
		}
	},
	    _buildPathFilter = function _buildPathFilter(shapeIndex, map, precompile) {
		return map || precompile || shapeIndex || shapeIndex === 0 ? function (a) {
			_pathFilter(a, shapeIndex, map, precompile);
		} : _pathFilter;
	},
	    _offsetPoints = function _offsetPoints(text, offset) {
		if (!offset) {
			return text;
		}
		var a = text.match(_numbersExp) || [],
		    l = a.length,
		    s = "",
		    inc,
		    i,
		    j;
		if (offset === "reverse") {
			i = l - 1;
			inc = -2;
		} else {
			i = ((parseInt(offset, 10) || 0) * 2 + 1 + l * 100) % l;
			inc = 2;
		}
		for (j = 0; j < l; j += 2) {
			s += a[i - 1] + "," + a[i] + " ";
			i = (i + inc) % l;
		}
		return s;
	},

	//adds a certain number of points while maintaining the polygon/polyline shape (so that the start/end values can have a matching quantity of points to animate). Returns the revised string.
	_equalizePointQuantity = function _equalizePointQuantity(a, quantity) {
		var tally = 0,
		    x = parseFloat(a[0]),
		    y = parseFloat(a[1]),
		    s = x + "," + y + " ",
		    max = 0.999999,
		    newPointsPerSegment,
		    i,
		    l,
		    j,
		    factor,
		    nextX,
		    nextY;
		l = a.length;
		newPointsPerSegment = quantity * 0.5 / (l * 0.5 - 1);
		for (i = 0; i < l - 2; i += 2) {
			tally += newPointsPerSegment;
			nextX = parseFloat(a[i + 2]);
			nextY = parseFloat(a[i + 3]);
			if (tally > max) {
				//compare with 0.99999 instead of 1 in order to prevent rounding errors
				factor = 1 / (Math.floor(tally) + 1);
				j = 1;
				while (tally > max) {
					s += (x + (nextX - x) * factor * j).toFixed(2) + "," + (y + (nextY - y) * factor * j).toFixed(2) + " ";
					tally--;
					j++;
				}
			}
			s += nextX + "," + nextY + " ";
			x = nextX;
			y = nextY;
		}
		return s;
	},
	    _pointsFilter = function _pointsFilter(a) {
		var startNums = a[0].match(_numbersExp) || [],
		    endNums = a[1].match(_numbersExp) || [],
		    dif = endNums.length - startNums.length;
		if (dif > 0) {
			a[0] = _equalizePointQuantity(startNums, dif);
		} else {
			a[1] = _equalizePointQuantity(endNums, -dif);
		}
	},
	    _buildPointsFilter = function _buildPointsFilter(shapeIndex) {
		return !isNaN(shapeIndex) ? function (a) {
			_pointsFilter(a);
			a[1] = _offsetPoints(a[1], parseInt(shapeIndex, 10));
		} : _pointsFilter;
	},
	    _createPath = function _createPath(e, ignore) {
		var path = _gsScope.document.createElementNS("http://www.w3.org/2000/svg", "path"),
		    attr = Array.prototype.slice.call(e.attributes),
		    i = attr.length,
		    name;
		ignore = "," + ignore + ",";
		while (--i > -1) {
			name = attr[i].nodeName.toLowerCase(); //in Microsoft Edge, if you don't set the attribute with a lowercase name, it doesn't render correctly! Super weird.
			if (ignore.indexOf("," + name + ",") === -1) {
				path.setAttributeNS(null, name, attr[i].nodeValue);
			}
		}
		return path;
	},
	    _convertToPath = function _convertToPath(e, swap) {
		var type = e.tagName.toLowerCase(),
		    circ = 0.552284749831,
		    data,
		    x,
		    y,
		    r,
		    ry,
		    path,
		    rcirc,
		    rycirc,
		    points,
		    w,
		    h,
		    x2,
		    x3,
		    x4,
		    x5,
		    x6,
		    y2,
		    y3,
		    y4,
		    y5,
		    y6;
		if (type === "path" || !e.getBBox) {
			return e;
		}
		path = _createPath(e, "x,y,width,height,cx,cy,rx,ry,r,x1,x2,y1,y2,points");
		if (type === "rect") {
			r = +e.getAttribute("rx") || 0;
			ry = +e.getAttribute("ry") || 0;
			x = +e.getAttribute("x") || 0;
			y = +e.getAttribute("y") || 0;
			w = (+e.getAttribute("width") || 0) - r * 2;
			h = (+e.getAttribute("height") || 0) - ry * 2;
			if (r || ry) {
				//if there are rounded corners, render cubic beziers
				x2 = x + r * (1 - circ);
				x3 = x + r;
				x4 = x3 + w;
				x5 = x4 + r * circ;
				x6 = x4 + r;
				y2 = y + ry * (1 - circ);
				y3 = y + ry;
				y4 = y3 + h;
				y5 = y4 + ry * circ;
				y6 = y4 + ry;
				data = "M" + x6 + "," + y3 + " V" + y4 + " C" + [x6, y5, x5, y6, x4, y6, x4 - (x4 - x3) / 3, y6, x3 + (x4 - x3) / 3, y6, x3, y6, x2, y6, x, y5, x, y4, x, y4 - (y4 - y3) / 3, x, y3 + (y4 - y3) / 3, x, y3, x, y2, x2, y, x3, y, x3 + (x4 - x3) / 3, y, x4 - (x4 - x3) / 3, y, x4, y, x5, y, x6, y2, x6, y3].join(",") + "z";
			} else {
				data = "M" + (x + w) + "," + y + " v" + h + " h" + -w + " v" + -h + " h" + w + "z";
			}
		} else if (type === "circle" || type === "ellipse") {
			if (type === "circle") {
				r = ry = +e.getAttribute("r") || 0;
				rycirc = r * circ;
			} else {
				r = +e.getAttribute("rx") || 0;
				ry = +e.getAttribute("ry") || 0;
				rycirc = ry * circ;
			}
			x = +e.getAttribute("cx") || 0;
			y = +e.getAttribute("cy") || 0;
			rcirc = r * circ;
			data = "M" + (x + r) + "," + y + " C" + [x + r, y + rycirc, x + rcirc, y + ry, x, y + ry, x - rcirc, y + ry, x - r, y + rycirc, x - r, y, x - r, y - rycirc, x - rcirc, y - ry, x, y - ry, x + rcirc, y - ry, x + r, y - rycirc, x + r, y].join(",") + "z";
		} else if (type === "line") {
			data = "M" + (e.getAttribute("x1") || 0) + "," + (e.getAttribute("y1") || 0) + " L" + (e.getAttribute("x2") || 0) + "," + (e.getAttribute("y2") || 0);
		} else if (type === "polyline" || type === "polygon") {
			points = (e.getAttribute("points") + "").match(_numbersExp) || [];
			x = points.shift();
			y = points.shift();
			data = "M" + x + "," + y + " L" + points.join(",");
			if (type === "polygon") {
				data += "," + x + "," + y + "z";
			}
		}
		path.setAttribute("d", data);
		if (swap && e.parentNode) {
			e.parentNode.insertBefore(path, e);
			e.parentNode.removeChild(e);
		}

		return path;
	},
	    _parseShape = function _parseShape(shape, forcePath, target) {
		var isString = typeof shape === "string",
		    e,
		    type;
		if (!isString || _selectorExp.test(shape) || (shape.match(_numbersExp) || []).length < 3) {
			e = isString ? TweenLite.selector(shape) : shape && shape[0] ? shape : [shape]; //allow array-like objects like jQuery objects.
			if (e && e[0]) {
				e = e[0];
				type = e.nodeName.toUpperCase();
				if (forcePath && type !== "PATH") {
					//if we were passed an element (or selector text for an element) that isn't a path, convert it.
					e = _convertToPath(e, false);
					type = "PATH";
				}
				shape = e.getAttribute(type === "PATH" ? "d" : "points") || "";
				if (e === target) {
					//if the shape matches the target element, the user wants to revert to the original which should have been stored in the data-original attribute
					shape = e.getAttributeNS(null, "data-original") || shape;
				}
			} else {
				_log("WARNING: invalid morph to: " + shape);
				shape = false;
			}
		}
		return shape;
	},
	    _morphMessage = "Use MorphSVGPlugin.convertToPath(elementOrSelectorText) to convert to a path before morphing.",
	    MorphSVGPlugin = _gsScope._gsDefine.plugin({
		propName: "morphSVG",
		API: 2,
		global: true,
		version: "0.8.11",

		//called when the tween renders for the first time. This is where initial values should be recorded and any setup routines should run.
		init: function init(target, value, tween, index) {
			var type, p, pt, shape, isPoly;
			if (typeof target.setAttribute !== "function") {
				return false;
			}
			if (typeof value === "function") {
				value = value(index, target);
			}
			type = target.nodeName.toUpperCase();
			isPoly = type === "POLYLINE" || type === "POLYGON";
			if (type !== "PATH" && !isPoly) {
				_log("WARNING: cannot morph a <" + type + "> SVG element. " + _morphMessage);
				return false;
			}
			p = type === "PATH" ? "d" : "points";
			if (typeof value === "string" || value.getBBox || value[0]) {
				value = { shape: value };
			}
			shape = _parseShape(value.shape || value.d || value.points || "", p === "d", target);
			if (isPoly && _commands.test(shape)) {
				_log("WARNING: a <" + type + "> cannot accept path data. " + _morphMessage);
				return false;
			}
			if (shape) {
				this._target = target;
				if (!target.getAttributeNS(null, "data-original")) {
					target.setAttributeNS(null, "data-original", target.getAttribute(p)); //record the original state in a data-original attribute so that we can revert to it later.
				}
				pt = this._addTween(target, "setAttribute", target.getAttribute(p) + "", shape + "", "morphSVG", false, p, _typeof(value.precompile) === "object" ? function (a) {
					a[0] = value.precompile[0];a[1] = value.precompile[1];
				} : p === "d" ? _buildPathFilter(value.shapeIndex, value.map || MorphSVGPlugin.defaultMap, value.precompile) : _buildPointsFilter(value.shapeIndex));
				if (pt) {
					this._overwriteProps.push("morphSVG");
					pt.end = shape;
					pt.endProp = p;
				}
			}
			return true;
		},

		set: function set(ratio) {
			var pt;
			this._super.setRatio.call(this, ratio);
			if (ratio === 1) {
				pt = this._firstPT;
				while (pt) {
					if (pt.end) {
						this._target.setAttribute(pt.endProp, pt.end); //make sure the end value is exactly as specified (in case we had to add fabricated points during the tween)
					}
					pt = pt._next;
				}
			}
		}

	});

	MorphSVGPlugin.pathFilter = _pathFilter;
	MorphSVGPlugin.pointsFilter = _pointsFilter;
	MorphSVGPlugin.subdivideRawBezier = _subdivideBezier;
	MorphSVGPlugin.defaultMap = "size";
	MorphSVGPlugin.pathDataToRawBezier = function (data) {
		return _pathDataToBezier(_parseShape(data, true));
	};
	MorphSVGPlugin.equalizeSegmentQuantity = _equalizeSegmentQuantity;

	MorphSVGPlugin.convertToPath = function (targets, swap) {
		if (typeof targets === "string") {
			targets = TweenLite.selector(targets);
		}
		var a = !targets || targets.length === 0 ? [] : targets.length && targets[0] && targets[0].nodeType ? Array.prototype.slice.call(targets, 0) : [targets],
		    i = a.length;
		while (--i > -1) {
			a[i] = _convertToPath(a[i], swap !== false);
		}
		return a;
	};

	MorphSVGPlugin.pathDataToBezier = function (data, vars) {
		//converts SVG path data into an array of {x, y} objects that can be plugged directly into a bezier tween. You can optionally pass in a 2D matrix like [a, b, c, d, tx, ty] containing numbers that should transform each point.
		var bezier = _pathDataToBezier(_parseShape(data, true))[0] || [],
		    prefix = 0,
		    a,
		    i,
		    l,
		    matrix,
		    offsetX,
		    offsetY,
		    bbox,
		    e;
		vars = vars || {};
		e = vars.align || vars.relative;
		matrix = vars.matrix || [1, 0, 0, 1, 0, 0];
		offsetX = vars.offsetX || 0;
		offsetY = vars.offsetY || 0;
		if (e === "relative" || e === true) {
			offsetX -= bezier[0] * matrix[0] + bezier[1] * matrix[2];
			offsetY -= bezier[0] * matrix[1] + bezier[1] * matrix[3];
			prefix = "+=";
		} else {
			offsetX += matrix[4];
			offsetY += matrix[5];
			if (e) {
				e = typeof e === "string" ? TweenLite.selector(e) : e && e[0] ? e : [e]; //allow array-like objects like jQuery objects.
				if (e && e[0]) {
					bbox = e[0].getBBox() || { x: 0, y: 0 };
					offsetX -= bbox.x;
					offsetY -= bbox.y;
				}
			}
		}
		a = [];
		l = bezier.length;
		if (matrix && matrix.join(",") !== "1,0,0,1,0,0") {
			for (i = 0; i < l; i += 2) {
				a.push({ x: prefix + (bezier[i] * matrix[0] + bezier[i + 1] * matrix[2] + offsetX), y: prefix + (bezier[i] * matrix[1] + bezier[i + 1] * matrix[3] + offsetY) });
			}
		} else {
			for (i = 0; i < l; i += 2) {
				a.push({ x: prefix + (bezier[i] + offsetX), y: prefix + (bezier[i + 1] + offsetY) });
			}
		}
		return a;
	};
});if (_gsScope._gsDefine) {
	_gsScope._gsQueue.pop()();
}
//export to AMD/RequireJS and CommonJS/Node (precursor to full modular build system coming at a later date)
(function (name) {
	"use strict";

	var getGlobal = function getGlobal() {
		return (_gsScope.GreenSockGlobals || _gsScope)[name];
	};
	if (typeof module !== "undefined" && module.exports) {
		//node
		__webpack_require__(0);
		module.exports = getGlobal();
	} else if (true) {
		//AMD
		!(__WEBPACK_AMD_DEFINE_ARRAY__ = [__webpack_require__(0)], __WEBPACK_AMD_DEFINE_FACTORY__ = (getGlobal),
				__WEBPACK_AMD_DEFINE_RESULT__ = (typeof __WEBPACK_AMD_DEFINE_FACTORY__ === 'function' ?
				(__WEBPACK_AMD_DEFINE_FACTORY__.apply(exports, __WEBPACK_AMD_DEFINE_ARRAY__)) : __WEBPACK_AMD_DEFINE_FACTORY__),
				__WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));
	}
})("MorphSVGPlugin");
/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(2)))

/***/ }),
/* 15 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/* WEBPACK VAR INJECTION */(function(global) {var __WEBPACK_AMD_DEFINE_FACTORY__, __WEBPACK_AMD_DEFINE_ARRAY__, __WEBPACK_AMD_DEFINE_RESULT__;

/*!
 * VERSION: 0.2.2
 * DATE: 2017-06-19
 * UPDATES AND DOCS AT: http://greensock.com
 *
 * @license Copyright (c) 2008-2017, GreenSock. All rights reserved.
 * This work is subject to the terms at http://greensock.com/standard-license or for
 * Club GreenSock members, the software agreement that was issued with your membership.
 *
 * @author: Jack Doyle, jack@greensock.com
 **/
var _gsScope = typeof module !== "undefined" && module.exports && typeof global !== "undefined" ? global : undefined || window; //helps ensure compatibility with AMD/RequireJS and CommonJS/Node
(_gsScope._gsQueue || (_gsScope._gsQueue = [])).push(function () {

	"use strict";

	_gsScope._gsDefine("easing.CustomEase", ["easing.Ease"], function (Ease) {

		var _numbersExp = /(?:(-|-=|\+=)?\d*\.?\d*(?:e[\-+]?\d+)?)[0-9]/ig,
		    _svgPathExp = /[achlmqstvz]|(-?\d*\.?\d*(?:e[\-+]?\d+)?)[0-9]/ig,
		    _scientific = /[\+\-]?\d*\.?\d+e[\+\-]?\d+/ig,
		    _needsParsingExp = /[cLlsS]/g,
		    _bezierError = "CustomEase only accepts Cubic Bezier data.",
		    _bezierToPoints = function _bezierToPoints(x1, y1, x2, y2, x3, y3, x4, y4, threshold, points, index) {
			var x12 = (x1 + x2) / 2,
			    y12 = (y1 + y2) / 2,
			    x23 = (x2 + x3) / 2,
			    y23 = (y2 + y3) / 2,
			    x34 = (x3 + x4) / 2,
			    y34 = (y3 + y4) / 2,
			    x123 = (x12 + x23) / 2,
			    y123 = (y12 + y23) / 2,
			    x234 = (x23 + x34) / 2,
			    y234 = (y23 + y34) / 2,
			    x1234 = (x123 + x234) / 2,
			    y1234 = (y123 + y234) / 2,
			    dx = x4 - x1,
			    dy = y4 - y1,
			    d2 = Math.abs((x2 - x4) * dy - (y2 - y4) * dx),
			    d3 = Math.abs((x3 - x4) * dy - (y3 - y4) * dx),
			    length;
			if (!points) {
				points = [{ x: x1, y: y1 }, { x: x4, y: y4 }];
				index = 1;
			}
			points.splice(index || points.length - 1, 0, { x: x1234, y: y1234 });
			if ((d2 + d3) * (d2 + d3) > threshold * (dx * dx + dy * dy)) {
				length = points.length;
				_bezierToPoints(x1, y1, x12, y12, x123, y123, x1234, y1234, threshold, points, index);
				_bezierToPoints(x1234, y1234, x234, y234, x34, y34, x4, y4, threshold, points, index + 1 + (points.length - length));
			}
			return points;
		},
		    _pathDataToBezier = function _pathDataToBezier(d) {
			var a = (d + "").replace(_scientific, function (m) {
				var n = +m;
				return n < 0.0001 && n > -0.0001 ? 0 : n;
			}).match(_svgPathExp) || [],
			    //some authoring programs spit out very small numbers in scientific notation like "1e-5", so make sure we round that down to 0 first.
			path = [],
			    relativeX = 0,
			    relativeY = 0,
			    elements = a.length,
			    l = 2,
			    i,
			    x,
			    y,
			    command,
			    isRelative,
			    segment,
			    startX,
			    startY,
			    prevCommand,
			    difX,
			    difY;
			for (i = 0; i < elements; i++) {
				prevCommand = command;
				if (isNaN(a[i])) {
					command = a[i].toUpperCase();
					isRelative = command !== a[i]; //lower case means relative
				} else {
					//commands like "C" can be strung together without any new command characters between.
					i--;
				}
				x = +a[i + 1];
				y = +a[i + 2];
				if (isRelative) {
					x += relativeX;
					y += relativeY;
				}
				if (!i) {
					startX = x;
					startY = y;
				}
				if (command === "M") {
					if (segment && segment.length < 8) {
						//if the path data was funky and just had a M with no actual drawing anywhere, skip it.
						path.length -= 1;
						l = 0;
					}
					relativeX = startX = x;
					relativeY = startY = y;
					segment = [x, y];
					l = 2;
					path.push(segment);
					i += 2;
					command = "L"; //an "M" with more than 2 values gets interpreted as "lineTo" commands ("L").
				} else if (command === "C") {
					if (!segment) {
						segment = [0, 0];
					}
					segment[l++] = x;
					segment[l++] = y;
					if (!isRelative) {
						relativeX = relativeY = 0;
					}
					segment[l++] = relativeX + a[i + 3] * 1; //note: "*1" is just a fast/short way to cast the value as a Number. WAAAY faster in Chrome, slightly slower in Firefox.
					segment[l++] = relativeY + a[i + 4] * 1;
					segment[l++] = relativeX = relativeX + a[i + 5] * 1;
					segment[l++] = relativeY = relativeY + a[i + 6] * 1;
					i += 6;
				} else if (command === "S") {
					if (prevCommand === "C" || prevCommand === "S") {
						difX = relativeX - segment[l - 4];
						difY = relativeY - segment[l - 3];
						segment[l++] = relativeX + difX;
						segment[l++] = relativeY + difY;
					} else {
						segment[l++] = relativeX;
						segment[l++] = relativeY;
					}
					segment[l++] = x;
					segment[l++] = y;
					if (!isRelative) {
						relativeX = relativeY = 0;
					}
					segment[l++] = relativeX = relativeX + a[i + 3] * 1;
					segment[l++] = relativeY = relativeY + a[i + 4] * 1;
					i += 4;
				} else if (command === "L" || command === "Z") {
					if (command === "Z") {
						x = startX;
						y = startY;
						segment.closed = true;
					}
					if (command === "L" || Math.abs(relativeX - x) > 0.5 || Math.abs(relativeY - y) > 0.5) {
						segment[l++] = relativeX + (x - relativeX) / 3;
						segment[l++] = relativeY + (y - relativeY) / 3;
						segment[l++] = relativeX + (x - relativeX) * 2 / 3;
						segment[l++] = relativeY + (y - relativeY) * 2 / 3;
						segment[l++] = x;
						segment[l++] = y;
						if (command === "L") {
							i += 2;
						}
					}
					relativeX = x;
					relativeY = y;
				} else {
					throw _bezierError;
				}
			}
			return path[0];
		},
		    _findMinimum = function _findMinimum(values) {
			var l = values.length,
			    min = 999999999999,
			    i;
			for (i = 1; i < l; i += 6) {
				if (+values[i] < min) {
					min = +values[i];
				}
			}
			return min;
		},
		    _normalize = function _normalize(values, height, originY) {
			//takes all the points and translates/scales them so that the x starts at 0 and ends at 1.
			if (!originY && originY !== 0) {
				originY = Math.max(+values[values.length - 1], +values[1]);
			}
			var tx = +values[0] * -1,
			    ty = -originY,
			    l = values.length,
			    sx = 1 / (+values[l - 2] + tx),
			    sy = -height || (Math.abs(+values[l - 1] - +values[1]) < 0.01 * (+values[l - 2] - +values[0]) ? _findMinimum(values) + ty : +values[l - 1] + ty),
			    i;
			if (sy) {
				//typically y ends at 1 (so that the end values are reached)
				sy = 1 / sy;
			} else {
				//in case the ease returns to its beginning value, scale everything proportionally
				sy = -sx;
			}
			for (i = 0; i < l; i += 2) {
				values[i] = (+values[i] + tx) * sx;
				values[i + 1] = (+values[i + 1] + ty) * sy;
			}
		},
		    _getRatio = function _getRatio(p) {
			var point = this.lookup[p * this.l | 0] || this.lookup[this.l - 1];
			if (point.nx < p) {
				point = point.n;
			}
			return point.y + (p - point.x) / point.cx * point.cy;
		},
		    CustomEase = function CustomEase(id, data, config) {
			this._calcEnd = true;
			this.id = id;
			if (id) {
				Ease.map[id] = this;
			}
			this.getRatio = _getRatio; //speed optimization, faster lookups.
			this.setData(data, config);
		},
		    p = CustomEase.prototype = new Ease();

		p.constructor = CustomEase;

		p.setData = function (data, config) {
			data = data || "0,0,1,1";
			var values = data.match(_numbersExp),
			    closest = 1,
			    points = [],
			    l,
			    a1,
			    a2,
			    i,
			    inc,
			    j,
			    point,
			    prevPoint,
			    p,
			    precision;
			config = config || {};
			precision = config.precision || 1;
			this.data = data;
			this.lookup = [];
			this.points = points;
			this.fast = precision <= 1;
			if (_needsParsingExp.test(data) || data.indexOf("M") !== -1 && data.indexOf("C") === -1) {
				values = _pathDataToBezier(data);
			}
			l = values.length;
			if (l === 4) {
				values.unshift(0, 0);
				values.push(1, 1);
				l = 8;
			} else if ((l - 2) % 6) {
				throw _bezierError;
			}
			if (+values[0] !== 0 || +values[l - 2] !== 1) {
				_normalize(values, config.height, config.originY);
			}

			this.rawBezier = values;

			for (i = 2; i < l; i += 6) {
				a1 = { x: +values[i - 2], y: +values[i - 1] };
				a2 = { x: +values[i + 4], y: +values[i + 5] };
				points.push(a1, a2);
				_bezierToPoints(a1.x, a1.y, +values[i], +values[i + 1], +values[i + 2], +values[i + 3], a2.x, a2.y, 1 / (precision * 200000), points, points.length - 1);
			}
			l = points.length;
			for (i = 0; i < l; i++) {
				point = points[i];
				prevPoint = points[i - 1] || point;
				if (point.x > prevPoint.x || prevPoint.y !== point.y && prevPoint.x === point.x || point === prevPoint) {
					//if a point goes BACKWARD in time or is a duplicate, just drop it.
					prevPoint.cx = point.x - prevPoint.x; //change in x between this point and the next point (performance optimization)
					prevPoint.cy = point.y - prevPoint.y;
					prevPoint.n = point;
					prevPoint.nx = point.x; //next point's x value (performance optimization, making lookups faster in getRatio()). Remember, the lookup will always land on a spot where it's either this point or the very next one (never beyond that)
					if (this.fast && i > 1 && Math.abs(prevPoint.cy / prevPoint.cx - points[i - 2].cy / points[i - 2].cx) > 2) {
						//if there's a sudden change in direction, prioritize accuracy over speed. Like a bounce ease - you don't want to risk the sampling chunks landing on each side of the bounce anchor and having it clipped off.
						this.fast = false;
					}
					if (prevPoint.cx < closest) {
						if (!prevPoint.cx) {
							prevPoint.cx = 0.001; //avoids math problems in getRatio() (dividing by zero)
							if (i === l - 1) {
								//in case the final segment goes vertical RIGHT at the end, make sure we end at the end.
								prevPoint.x -= 0.001;
								closest = Math.min(closest, 0.001);
								this.fast = false;
							}
						} else {
							closest = prevPoint.cx;
						}
					}
				} else {
					points.splice(i--, 1);
					l--;
				}
			}
			l = 1 / closest + 1 | 0;
			this.l = l; //record for speed optimization
			inc = 1 / l;
			j = 0;
			point = points[0];
			if (this.fast) {
				for (i = 0; i < l; i++) {
					//for fastest lookups, we just sample along the path at equal x (time) distance. Uses more memory and is slightly less accurate for anchors that don't land on the sampling points, but for the vast majority of eases it's excellent (and fast).
					p = i * inc;
					if (point.nx < p) {
						point = points[++j];
					}
					a1 = point.y + (p - point.x) / point.cx * point.cy;
					this.lookup[i] = { x: p, cx: inc, y: a1, cy: 0, nx: 9 };
					if (i) {
						this.lookup[i - 1].cy = a1 - this.lookup[i - 1].y;
					}
				}
				this.lookup[l - 1].cy = points[points.length - 1].y - a1;
			} else {
				//this option is more accurate, ensuring that EVERY anchor is hit perfectly. Clipping across a bounce, for example, would never happen.
				for (i = 0; i < l; i++) {
					//build a lookup table based on the smallest distance so that we can instantly find the appropriate point (well, it'll either be that point or the very next one). We'll look up based on the linear progress. So it's it's 0.5 and the lookup table has 100 elements, it'd be like lookup[Math.floor(0.5 * 100)]
					if (point.nx < i * inc) {
						point = points[++j];
					}
					this.lookup[i] = point;
				}

				if (j < points.length - 1) {
					this.lookup[i - 1] = points[points.length - 2];
				}
			}
			this._calcEnd = points[points.length - 1].y !== 1 || points[0].y !== 0; //ensures that we don't run into floating point errors. As long as we're starting at 0 and ending at 1, tell GSAP to skip the final calculation and use 0/1 as the factor.
			return this;
		};

		p.getRatio = _getRatio;

		p.getSVGData = function (config) {
			return CustomEase.getSVGData(this, config);
		};

		CustomEase.create = function (id, data, config) {
			return new CustomEase(id, data, config);
		};

		CustomEase.version = "0.2.2";

		CustomEase.bezierToPoints = _bezierToPoints;
		CustomEase.get = function (id) {
			return Ease.map[id];
		};
		CustomEase.getSVGData = function (ease, config) {
			config = config || {};
			var rnd = 1000,
			    width = config.width || 100,
			    height = config.height || 100,
			    x = config.x || 0,
			    y = (config.y || 0) + height,
			    e = config.path,
			    a,
			    slope,
			    i,
			    inc,
			    tx,
			    ty,
			    precision,
			    threshold,
			    prevX,
			    prevY;
			if (config.invert) {
				height = -height;
				y = 0;
			}
			ease = ease.getRatio ? ease : Ease.map[ease] || console.log("No ease found: ", ease);
			if (!ease.rawBezier) {
				a = ["M" + x + "," + y];
				precision = Math.max(5, (config.precision || 1) * 200);
				inc = 1 / precision;
				precision += 2;
				threshold = 5 / precision;
				prevX = ((x + inc * width) * rnd | 0) / rnd;
				prevY = ((y + ease.getRatio(inc) * -height) * rnd | 0) / rnd;
				slope = (prevY - y) / (prevX - x);
				for (i = 2; i < precision; i++) {
					tx = ((x + i * inc * width) * rnd | 0) / rnd;
					ty = ((y + ease.getRatio(i * inc) * -height) * rnd | 0) / rnd;
					if (Math.abs((ty - prevY) / (tx - prevX) - slope) > threshold || i === precision - 1) {
						//only add points when the slope changes beyond the threshold
						a.push(prevX + "," + prevY);
						slope = (ty - prevY) / (tx - prevX);
					}
					prevX = tx;
					prevY = ty;
				}
			} else {
				a = [];
				precision = ease.rawBezier.length;
				for (i = 0; i < precision; i += 2) {
					a.push(((x + ease.rawBezier[i] * width) * rnd | 0) / rnd + "," + ((y + ease.rawBezier[i + 1] * -height) * rnd | 0) / rnd);
				}
				a[0] = "M" + a[0];
				a[1] = "C" + a[1];
			}
			if (e) {
				(typeof e === "string" ? document.querySelector(e) : e).setAttribute("d", a.join(" "));
			}
			return a.join(" ");
		};

		return CustomEase;
	}, true);
});if (_gsScope._gsDefine) {
	_gsScope._gsQueue.pop()();
}

//export to AMD/RequireJS and CommonJS/Node (precursor to full modular build system coming at a later date)
(function (name) {
	"use strict";

	var getGlobal = function getGlobal() {
		return (_gsScope.GreenSockGlobals || _gsScope)[name];
	};
	if (typeof module !== "undefined" && module.exports) {
		//node
		__webpack_require__(0);
		module.exports = getGlobal();
	} else if (true) {
		//AMD
		!(__WEBPACK_AMD_DEFINE_ARRAY__ = [__webpack_require__(0)], __WEBPACK_AMD_DEFINE_FACTORY__ = (getGlobal),
				__WEBPACK_AMD_DEFINE_RESULT__ = (typeof __WEBPACK_AMD_DEFINE_FACTORY__ === 'function' ?
				(__WEBPACK_AMD_DEFINE_FACTORY__.apply(exports, __WEBPACK_AMD_DEFINE_ARRAY__)) : __WEBPACK_AMD_DEFINE_FACTORY__),
				__WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));
	}
})("CustomEase");
/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(2)))

/***/ }),
/* 16 */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(17);
module.exports = __webpack_require__(18);


/***/ }),
/* 17 */
/***/ (function(module, exports, __webpack_require__) {

/* WEBPACK VAR INJECTION */(function(global, process) {/**
 * Copyright (c) 2014, Facebook, Inc.
 * All rights reserved.
 *
 * This source code is licensed under the BSD-style license found in the
 * https://raw.github.com/facebook/regenerator/master/LICENSE file. An
 * additional grant of patent rights can be found in the PATENTS file in
 * the same directory.
 */

!(function(global) {
  "use strict";

  var hasOwn = Object.prototype.hasOwnProperty;
  var undefined; // More compressible than void 0.
  var iteratorSymbol =
    typeof Symbol === "function" && Symbol.iterator || "@@iterator";

  var inModule = typeof module === "object";
  var runtime = global.regeneratorRuntime;
  if (runtime) {
    if (inModule) {
      // If regeneratorRuntime is defined globally and we're in a module,
      // make the exports object identical to regeneratorRuntime.
      module.exports = runtime;
    }
    // Don't bother evaluating the rest of this file if the runtime was
    // already defined globally.
    return;
  }

  // Define the runtime globally (as expected by generated code) as either
  // module.exports (if we're in a module) or a new, empty object.
  runtime = global.regeneratorRuntime = inModule ? module.exports : {};

  function wrap(innerFn, outerFn, self, tryLocsList) {
    // If outerFn provided, then outerFn.prototype instanceof Generator.
    var generator = Object.create((outerFn || Generator).prototype);
    var context = new Context(tryLocsList || []);

    // The ._invoke method unifies the implementations of the .next,
    // .throw, and .return methods.
    generator._invoke = makeInvokeMethod(innerFn, self, context);

    return generator;
  }
  runtime.wrap = wrap;

  // Try/catch helper to minimize deoptimizations. Returns a completion
  // record like context.tryEntries[i].completion. This interface could
  // have been (and was previously) designed to take a closure to be
  // invoked without arguments, but in all the cases we care about we
  // already have an existing method we want to call, so there's no need
  // to create a new function object. We can even get away with assuming
  // the method takes exactly one argument, since that happens to be true
  // in every case, so we don't have to touch the arguments object. The
  // only additional allocation required is the completion record, which
  // has a stable shape and so hopefully should be cheap to allocate.
  function tryCatch(fn, obj, arg) {
    try {
      return { type: "normal", arg: fn.call(obj, arg) };
    } catch (err) {
      return { type: "throw", arg: err };
    }
  }

  var GenStateSuspendedStart = "suspendedStart";
  var GenStateSuspendedYield = "suspendedYield";
  var GenStateExecuting = "executing";
  var GenStateCompleted = "completed";

  // Returning this object from the innerFn has the same effect as
  // breaking out of the dispatch switch statement.
  var ContinueSentinel = {};

  // Dummy constructor functions that we use as the .constructor and
  // .constructor.prototype properties for functions that return Generator
  // objects. For full spec compliance, you may wish to configure your
  // minifier not to mangle the names of these two functions.
  function Generator() {}
  function GeneratorFunction() {}
  function GeneratorFunctionPrototype() {}

  var Gp = GeneratorFunctionPrototype.prototype = Generator.prototype;
  GeneratorFunction.prototype = Gp.constructor = GeneratorFunctionPrototype;
  GeneratorFunctionPrototype.constructor = GeneratorFunction;
  GeneratorFunction.displayName = "GeneratorFunction";

  // Helper for defining the .next, .throw, and .return methods of the
  // Iterator interface in terms of a single ._invoke method.
  function defineIteratorMethods(prototype) {
    ["next", "throw", "return"].forEach(function(method) {
      prototype[method] = function(arg) {
        return this._invoke(method, arg);
      };
    });
  }

  runtime.isGeneratorFunction = function(genFun) {
    var ctor = typeof genFun === "function" && genFun.constructor;
    return ctor
      ? ctor === GeneratorFunction ||
        // For the native GeneratorFunction constructor, the best we can
        // do is to check its .name property.
        (ctor.displayName || ctor.name) === "GeneratorFunction"
      : false;
  };

  runtime.mark = function(genFun) {
    if (Object.setPrototypeOf) {
      Object.setPrototypeOf(genFun, GeneratorFunctionPrototype);
    } else {
      genFun.__proto__ = GeneratorFunctionPrototype;
    }
    genFun.prototype = Object.create(Gp);
    return genFun;
  };

  // Within the body of any async function, `await x` is transformed to
  // `yield regeneratorRuntime.awrap(x)`, so that the runtime can test
  // `value instanceof AwaitArgument` to determine if the yielded value is
  // meant to be awaited. Some may consider the name of this method too
  // cutesy, but they are curmudgeons.
  runtime.awrap = function(arg) {
    return new AwaitArgument(arg);
  };

  function AwaitArgument(arg) {
    this.arg = arg;
  }

  function AsyncIterator(generator) {
    // This invoke function is written in a style that assumes some
    // calling function (or Promise) will handle exceptions.
    function invoke(method, arg) {
      var result = generator[method](arg);
      var value = result.value;
      return value instanceof AwaitArgument
        ? Promise.resolve(value.arg).then(invokeNext, invokeThrow)
        : Promise.resolve(value).then(function(unwrapped) {
            // When a yielded Promise is resolved, its final value becomes
            // the .value of the Promise<{value,done}> result for the
            // current iteration. If the Promise is rejected, however, the
            // result for this iteration will be rejected with the same
            // reason. Note that rejections of yielded Promises are not
            // thrown back into the generator function, as is the case
            // when an awaited Promise is rejected. This difference in
            // behavior between yield and await is important, because it
            // allows the consumer to decide what to do with the yielded
            // rejection (swallow it and continue, manually .throw it back
            // into the generator, abandon iteration, whatever). With
            // await, by contrast, there is no opportunity to examine the
            // rejection reason outside the generator function, so the
            // only option is to throw it from the await expression, and
            // let the generator function handle the exception.
            result.value = unwrapped;
            return result;
          });
    }

    if (typeof process === "object" && process.domain) {
      invoke = process.domain.bind(invoke);
    }

    var invokeNext = invoke.bind(generator, "next");
    var invokeThrow = invoke.bind(generator, "throw");
    var invokeReturn = invoke.bind(generator, "return");
    var previousPromise;

    function enqueue(method, arg) {
      function callInvokeWithMethodAndArg() {
        return invoke(method, arg);
      }

      return previousPromise =
        // If enqueue has been called before, then we want to wait until
        // all previous Promises have been resolved before calling invoke,
        // so that results are always delivered in the correct order. If
        // enqueue has not been called before, then it is important to
        // call invoke immediately, without waiting on a callback to fire,
        // so that the async generator function has the opportunity to do
        // any necessary setup in a predictable way. This predictability
        // is why the Promise constructor synchronously invokes its
        // executor callback, and why async functions synchronously
        // execute code before the first await. Since we implement simple
        // async functions in terms of async generators, it is especially
        // important to get this right, even though it requires care.
        previousPromise ? previousPromise.then(
          callInvokeWithMethodAndArg,
          // Avoid propagating failures to Promises returned by later
          // invocations of the iterator.
          callInvokeWithMethodAndArg
        ) : new Promise(function (resolve) {
          resolve(callInvokeWithMethodAndArg());
        });
    }

    // Define the unified helper method that is used to implement .next,
    // .throw, and .return (see defineIteratorMethods).
    this._invoke = enqueue;
  }

  defineIteratorMethods(AsyncIterator.prototype);

  // Note that simple async functions are implemented on top of
  // AsyncIterator objects; they just return a Promise for the value of
  // the final result produced by the iterator.
  runtime.async = function(innerFn, outerFn, self, tryLocsList) {
    var iter = new AsyncIterator(
      wrap(innerFn, outerFn, self, tryLocsList)
    );

    return runtime.isGeneratorFunction(outerFn)
      ? iter // If outerFn is a generator, return the full iterator.
      : iter.next().then(function(result) {
          return result.done ? result.value : iter.next();
        });
  };

  function makeInvokeMethod(innerFn, self, context) {
    var state = GenStateSuspendedStart;

    return function invoke(method, arg) {
      if (state === GenStateExecuting) {
        throw new Error("Generator is already running");
      }

      if (state === GenStateCompleted) {
        if (method === "throw") {
          throw arg;
        }

        // Be forgiving, per 25.3.3.3.3 of the spec:
        // https://people.mozilla.org/~jorendorff/es6-draft.html#sec-generatorresume
        return doneResult();
      }

      while (true) {
        var delegate = context.delegate;
        if (delegate) {
          if (method === "return" ||
              (method === "throw" && delegate.iterator[method] === undefined)) {
            // A return or throw (when the delegate iterator has no throw
            // method) always terminates the yield* loop.
            context.delegate = null;

            // If the delegate iterator has a return method, give it a
            // chance to clean up.
            var returnMethod = delegate.iterator["return"];
            if (returnMethod) {
              var record = tryCatch(returnMethod, delegate.iterator, arg);
              if (record.type === "throw") {
                // If the return method threw an exception, let that
                // exception prevail over the original return or throw.
                method = "throw";
                arg = record.arg;
                continue;
              }
            }

            if (method === "return") {
              // Continue with the outer return, now that the delegate
              // iterator has been terminated.
              continue;
            }
          }

          var record = tryCatch(
            delegate.iterator[method],
            delegate.iterator,
            arg
          );

          if (record.type === "throw") {
            context.delegate = null;

            // Like returning generator.throw(uncaught), but without the
            // overhead of an extra function call.
            method = "throw";
            arg = record.arg;
            continue;
          }

          // Delegate generator ran and handled its own exceptions so
          // regardless of what the method was, we continue as if it is
          // "next" with an undefined arg.
          method = "next";
          arg = undefined;

          var info = record.arg;
          if (info.done) {
            context[delegate.resultName] = info.value;
            context.next = delegate.nextLoc;
          } else {
            state = GenStateSuspendedYield;
            return info;
          }

          context.delegate = null;
        }

        if (method === "next") {
          context._sent = arg;

          if (state === GenStateSuspendedYield) {
            context.sent = arg;
          } else {
            context.sent = undefined;
          }
        } else if (method === "throw") {
          if (state === GenStateSuspendedStart) {
            state = GenStateCompleted;
            throw arg;
          }

          if (context.dispatchException(arg)) {
            // If the dispatched exception was caught by a catch block,
            // then let that catch block handle the exception normally.
            method = "next";
            arg = undefined;
          }

        } else if (method === "return") {
          context.abrupt("return", arg);
        }

        state = GenStateExecuting;

        var record = tryCatch(innerFn, self, context);
        if (record.type === "normal") {
          // If an exception is thrown from innerFn, we leave state ===
          // GenStateExecuting and loop back for another invocation.
          state = context.done
            ? GenStateCompleted
            : GenStateSuspendedYield;

          var info = {
            value: record.arg,
            done: context.done
          };

          if (record.arg === ContinueSentinel) {
            if (context.delegate && method === "next") {
              // Deliberately forget the last sent value so that we don't
              // accidentally pass it on to the delegate.
              arg = undefined;
            }
          } else {
            return info;
          }

        } else if (record.type === "throw") {
          state = GenStateCompleted;
          // Dispatch the exception by looping back around to the
          // context.dispatchException(arg) call above.
          method = "throw";
          arg = record.arg;
        }
      }
    };
  }

  // Define Generator.prototype.{next,throw,return} in terms of the
  // unified ._invoke helper method.
  defineIteratorMethods(Gp);

  Gp[iteratorSymbol] = function() {
    return this;
  };

  Gp.toString = function() {
    return "[object Generator]";
  };

  function pushTryEntry(locs) {
    var entry = { tryLoc: locs[0] };

    if (1 in locs) {
      entry.catchLoc = locs[1];
    }

    if (2 in locs) {
      entry.finallyLoc = locs[2];
      entry.afterLoc = locs[3];
    }

    this.tryEntries.push(entry);
  }

  function resetTryEntry(entry) {
    var record = entry.completion || {};
    record.type = "normal";
    delete record.arg;
    entry.completion = record;
  }

  function Context(tryLocsList) {
    // The root entry object (effectively a try statement without a catch
    // or a finally block) gives us a place to store values thrown from
    // locations where there is no enclosing try statement.
    this.tryEntries = [{ tryLoc: "root" }];
    tryLocsList.forEach(pushTryEntry, this);
    this.reset(true);
  }

  runtime.keys = function(object) {
    var keys = [];
    for (var key in object) {
      keys.push(key);
    }
    keys.reverse();

    // Rather than returning an object with a next method, we keep
    // things simple and return the next function itself.
    return function next() {
      while (keys.length) {
        var key = keys.pop();
        if (key in object) {
          next.value = key;
          next.done = false;
          return next;
        }
      }

      // To avoid creating an additional object, we just hang the .value
      // and .done properties off the next function object itself. This
      // also ensures that the minifier will not anonymize the function.
      next.done = true;
      return next;
    };
  };

  function values(iterable) {
    if (iterable) {
      var iteratorMethod = iterable[iteratorSymbol];
      if (iteratorMethod) {
        return iteratorMethod.call(iterable);
      }

      if (typeof iterable.next === "function") {
        return iterable;
      }

      if (!isNaN(iterable.length)) {
        var i = -1, next = function next() {
          while (++i < iterable.length) {
            if (hasOwn.call(iterable, i)) {
              next.value = iterable[i];
              next.done = false;
              return next;
            }
          }

          next.value = undefined;
          next.done = true;

          return next;
        };

        return next.next = next;
      }
    }

    // Return an iterator with no values.
    return { next: doneResult };
  }
  runtime.values = values;

  function doneResult() {
    return { value: undefined, done: true };
  }

  Context.prototype = {
    constructor: Context,

    reset: function(skipTempReset) {
      this.prev = 0;
      this.next = 0;
      this.sent = undefined;
      this.done = false;
      this.delegate = null;

      this.tryEntries.forEach(resetTryEntry);

      if (!skipTempReset) {
        for (var name in this) {
          // Not sure about the optimal order of these conditions:
          if (name.charAt(0) === "t" &&
              hasOwn.call(this, name) &&
              !isNaN(+name.slice(1))) {
            this[name] = undefined;
          }
        }
      }
    },

    stop: function() {
      this.done = true;

      var rootEntry = this.tryEntries[0];
      var rootRecord = rootEntry.completion;
      if (rootRecord.type === "throw") {
        throw rootRecord.arg;
      }

      return this.rval;
    },

    dispatchException: function(exception) {
      if (this.done) {
        throw exception;
      }

      var context = this;
      function handle(loc, caught) {
        record.type = "throw";
        record.arg = exception;
        context.next = loc;
        return !!caught;
      }

      for (var i = this.tryEntries.length - 1; i >= 0; --i) {
        var entry = this.tryEntries[i];
        var record = entry.completion;

        if (entry.tryLoc === "root") {
          // Exception thrown outside of any try block that could handle
          // it, so set the completion value of the entire function to
          // throw the exception.
          return handle("end");
        }

        if (entry.tryLoc <= this.prev) {
          var hasCatch = hasOwn.call(entry, "catchLoc");
          var hasFinally = hasOwn.call(entry, "finallyLoc");

          if (hasCatch && hasFinally) {
            if (this.prev < entry.catchLoc) {
              return handle(entry.catchLoc, true);
            } else if (this.prev < entry.finallyLoc) {
              return handle(entry.finallyLoc);
            }

          } else if (hasCatch) {
            if (this.prev < entry.catchLoc) {
              return handle(entry.catchLoc, true);
            }

          } else if (hasFinally) {
            if (this.prev < entry.finallyLoc) {
              return handle(entry.finallyLoc);
            }

          } else {
            throw new Error("try statement without catch or finally");
          }
        }
      }
    },

    abrupt: function(type, arg) {
      for (var i = this.tryEntries.length - 1; i >= 0; --i) {
        var entry = this.tryEntries[i];
        if (entry.tryLoc <= this.prev &&
            hasOwn.call(entry, "finallyLoc") &&
            this.prev < entry.finallyLoc) {
          var finallyEntry = entry;
          break;
        }
      }

      if (finallyEntry &&
          (type === "break" ||
           type === "continue") &&
          finallyEntry.tryLoc <= arg &&
          arg <= finallyEntry.finallyLoc) {
        // Ignore the finally entry if control is not jumping to a
        // location outside the try/catch block.
        finallyEntry = null;
      }

      var record = finallyEntry ? finallyEntry.completion : {};
      record.type = type;
      record.arg = arg;

      if (finallyEntry) {
        this.next = finallyEntry.finallyLoc;
      } else {
        this.complete(record);
      }

      return ContinueSentinel;
    },

    complete: function(record, afterLoc) {
      if (record.type === "throw") {
        throw record.arg;
      }

      if (record.type === "break" ||
          record.type === "continue") {
        this.next = record.arg;
      } else if (record.type === "return") {
        this.rval = record.arg;
        this.next = "end";
      } else if (record.type === "normal" && afterLoc) {
        this.next = afterLoc;
      }
    },

    finish: function(finallyLoc) {
      for (var i = this.tryEntries.length - 1; i >= 0; --i) {
        var entry = this.tryEntries[i];
        if (entry.finallyLoc === finallyLoc) {
          this.complete(entry.completion, entry.afterLoc);
          resetTryEntry(entry);
          return ContinueSentinel;
        }
      }
    },

    "catch": function(tryLoc) {
      for (var i = this.tryEntries.length - 1; i >= 0; --i) {
        var entry = this.tryEntries[i];
        if (entry.tryLoc === tryLoc) {
          var record = entry.completion;
          if (record.type === "throw") {
            var thrown = record.arg;
            resetTryEntry(entry);
          }
          return thrown;
        }
      }

      // The context.catch method must only be called with a location
      // argument that corresponds to a known catch block.
      throw new Error("illegal catch attempt");
    },

    delegateYield: function(iterable, resultName, nextLoc) {
      this.delegate = {
        iterator: values(iterable),
        resultName: resultName,
        nextLoc: nextLoc
      };

      return ContinueSentinel;
    }
  };
})(
  // Among the various tricks for obtaining a reference to the global
  // object, this seems to be the most reliable technique that does not
  // use indirect eval (which violates Content Security Policy).
  typeof global === "object" ? global :
  typeof window === "object" ? window :
  typeof self === "object" ? self : this
);

/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(2), __webpack_require__(7)))

/***/ }),
/* 18 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


__webpack_require__(19);

__webpack_require__(20);

__webpack_require__(21);

__webpack_require__(22);

__webpack_require__(23);

__webpack_require__(24);

__webpack_require__(25);

__webpack_require__(28);

__webpack_require__(29);

var _fastclick = __webpack_require__(78);

var _fastclick2 = _interopRequireDefault(_fastclick);

var _components = __webpack_require__(11);

var _components2 = _interopRequireDefault(_components);

var _componentUtils = __webpack_require__(10);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var basepath = document.documentElement.getAttribute('data-basepath');
__webpack_require__.p = basepath ? basepath + '/assets/frontend/js/' : '/assets/frontend/js/';
//import './modules/path-data-polyfill';

//import './modules/hidpi-canvas-polyfill';

// GTM swup
document.addEventListener('swup:pageView', function (event) {
    if (typeof dataLayer !== 'undefined') {
        dataLayer.push({
            'event': 'VirtualPageview',
            'virtualPageURL': window.location.href,
            'virtualPageTitle': document.title
        });
    } else {
        console.warn('GTM is not loaded.');
    }
});

window.app = {
    components: _components2.default
};

_fastclick2.default.attach(document.body);
(0, _componentUtils.loadComponents)();

document.documentElement.classList.add('is-ready');

/***/ }),
/* 19 */
/***/ (function(module, exports) {

// Console-polyfill. MIT license.
// https://github.com/paulmillr/console-polyfill
// Make it safe to do console.log() always.
(function(global) {
  'use strict';
  if (!global.console) {
    global.console = {};
  }
  var con = global.console;
  var prop, method;
  var dummy = function() {};
  var properties = ['memory'];
  var methods = ('assert,clear,count,debug,dir,dirxml,error,exception,group,' +
     'groupCollapsed,groupEnd,info,log,markTimeline,profile,profiles,profileEnd,' +
     'show,table,time,timeEnd,timeline,timelineEnd,timeStamp,trace,warn').split(',');
  while (prop = properties.pop()) if (!con[prop]) con[prop] = {};
  while (method = methods.pop()) if (!con[method]) con[method] = dummy;
  // Using `this` for web workers & supports Browserify / Webpack.
})(typeof window === 'undefined' ? this : window);


/***/ }),
/* 20 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

/*! modernizr 3.5.0 (Custom Build) | MIT *
 * https://modernizr.com/download/?-backgroundblendmode-cssmask-svgclippaths-setclasses !*/
!function (e, n, t) {
  function r(e, n) {
    return (typeof e === "undefined" ? "undefined" : _typeof(e)) === n;
  }function o() {
    var e, n, t, o, s, i, a;for (var l in S) {
      if (S.hasOwnProperty(l)) {
        if (e = [], n = S[l], n.name && (e.push(n.name.toLowerCase()), n.options && n.options.aliases && n.options.aliases.length)) for (t = 0; t < n.options.aliases.length; t++) {
          e.push(n.options.aliases[t].toLowerCase());
        }for (o = r(n.fn, "function") ? n.fn() : n.fn, s = 0; s < e.length; s++) {
          i = e[s], a = i.split("."), 1 === a.length ? Modernizr[a[0]] = o : (!Modernizr[a[0]] || Modernizr[a[0]] instanceof Boolean || (Modernizr[a[0]] = new Boolean(Modernizr[a[0]])), Modernizr[a[0]][a[1]] = o), C.push((o ? "" : "no-") + a.join("-"));
        }
      }
    }
  }function s(e) {
    var n = x.className,
        t = Modernizr._config.classPrefix || "";if (_ && (n = n.baseVal), Modernizr._config.enableJSClass) {
      var r = new RegExp("(^|\\s)" + t + "no-js(\\s|$)");n = n.replace(r, "$1" + t + "js$2");
    }Modernizr._config.enableClasses && (n += " " + t + e.join(" " + t), _ ? x.className.baseVal = n : x.className = n);
  }function i(e) {
    return e.replace(/([a-z])-([a-z])/g, function (e, n, t) {
      return n + t.toUpperCase();
    }).replace(/^-/, "");
  }function a(e, n) {
    return !!~("" + e).indexOf(n);
  }function l() {
    return "function" != typeof n.createElement ? n.createElement(arguments[0]) : _ ? n.createElementNS.call(n, "http://www.w3.org/2000/svg", arguments[0]) : n.createElement.apply(n, arguments);
  }function f(e, n) {
    return function () {
      return e.apply(n, arguments);
    };
  }function u(e, n, t) {
    var o;for (var s in e) {
      if (e[s] in n) return t === !1 ? e[s] : (o = n[e[s]], r(o, "function") ? f(o, t || n) : o);
    }return !1;
  }function p(e) {
    return e.replace(/([A-Z])/g, function (e, n) {
      return "-" + n.toLowerCase();
    }).replace(/^ms-/, "-ms-");
  }function c(n, t, r) {
    var o;if ("getComputedStyle" in e) {
      o = getComputedStyle.call(e, n, t);var s = e.console;if (null !== o) r && (o = o.getPropertyValue(r));else if (s) {
        var i = s.error ? "error" : "log";s[i].call(s, "getComputedStyle returning null, its possible modernizr test results are inaccurate");
      }
    } else o = !t && n.currentStyle && n.currentStyle[r];return o;
  }function d() {
    var e = n.body;return e || (e = l(_ ? "svg" : "body"), e.fake = !0), e;
  }function m(e, t, r, o) {
    var s,
        i,
        a,
        f,
        u = "modernizr",
        p = l("div"),
        c = d();if (parseInt(r, 10)) for (; r--;) {
      a = l("div"), a.id = o ? o[r] : u + (r + 1), p.appendChild(a);
    }return s = l("style"), s.type = "text/css", s.id = "s" + u, (c.fake ? c : p).appendChild(s), c.appendChild(p), s.styleSheet ? s.styleSheet.cssText = e : s.appendChild(n.createTextNode(e)), p.id = u, c.fake && (c.style.background = "", c.style.overflow = "hidden", f = x.style.overflow, x.style.overflow = "hidden", x.appendChild(c)), i = t(p, e), c.fake ? (c.parentNode.removeChild(c), x.style.overflow = f, x.offsetHeight) : p.parentNode.removeChild(p), !!i;
  }function v(n, r) {
    var o = n.length;if ("CSS" in e && "supports" in e.CSS) {
      for (; o--;) {
        if (e.CSS.supports(p(n[o]), r)) return !0;
      }return !1;
    }if ("CSSSupportsRule" in e) {
      for (var s = []; o--;) {
        s.push("(" + p(n[o]) + ":" + r + ")");
      }return s = s.join(" or "), m("@supports (" + s + ") { #modernizr { position: absolute; } }", function (e) {
        return "absolute" == c(e, null, "position");
      });
    }return t;
  }function g(e, n, o, s) {
    function f() {
      p && (delete T.style, delete T.modElem);
    }if (s = r(s, "undefined") ? !1 : s, !r(o, "undefined")) {
      var u = v(e, o);if (!r(u, "undefined")) return u;
    }for (var p, c, d, m, g, h = ["modernizr", "tspan", "samp"]; !T.style && h.length;) {
      p = !0, T.modElem = l(h.shift()), T.style = T.modElem.style;
    }for (d = e.length, c = 0; d > c; c++) {
      if (m = e[c], g = T.style[m], a(m, "-") && (m = i(m)), T.style[m] !== t) {
        if (s || r(o, "undefined")) return f(), "pfx" == n ? m : !0;try {
          T.style[m] = o;
        } catch (y) {}if (T.style[m] != g) return f(), "pfx" == n ? m : !0;
      }
    }return f(), !1;
  }function h(e, n, t, o, s) {
    var i = e.charAt(0).toUpperCase() + e.slice(1),
        a = (e + " " + P.join(i + " ") + i).split(" ");return r(n, "string") || r(n, "undefined") ? g(a, n, o, s) : (a = (e + " " + k.join(i + " ") + i).split(" "), u(a, n, t));
  }function y(e, n, r) {
    return h(e, t, t, n, r);
  }var C = [],
      S = [],
      w = { _version: "3.5.0", _config: { classPrefix: "", enableClasses: !0, enableJSClass: !0, usePrefixes: !0 }, _q: [], on: function on(e, n) {
      var t = this;setTimeout(function () {
        n(t[e]);
      }, 0);
    }, addTest: function addTest(e, n, t) {
      S.push({ name: e, fn: n, options: t });
    }, addAsyncTest: function addAsyncTest(e) {
      S.push({ name: null, fn: e });
    } },
      Modernizr = function Modernizr() {};Modernizr.prototype = w, Modernizr = new Modernizr();var x = n.documentElement,
      _ = "svg" === x.nodeName.toLowerCase(),
      b = {}.toString;Modernizr.addTest("svgclippaths", function () {
    return !!n.createElementNS && /SVGClipPath/.test(b.call(n.createElementNS("http://www.w3.org/2000/svg", "clipPath")));
  });var E = "Moz O ms Webkit",
      P = w._config.usePrefixes ? E.split(" ") : [];w._cssomPrefixes = P;var N = function N(n) {
    var r,
        o = prefixes.length,
        s = e.CSSRule;if ("undefined" == typeof s) return t;if (!n) return !1;if (n = n.replace(/^@/, ""), r = n.replace(/-/g, "_").toUpperCase() + "_RULE", r in s) return "@" + n;for (var i = 0; o > i; i++) {
      var a = prefixes[i],
          l = a.toUpperCase() + "_" + r;if (l in s) return "@-" + a.toLowerCase() + "-" + n;
    }return !1;
  };w.atRule = N;var k = w._config.usePrefixes ? E.toLowerCase().split(" ") : [];w._domPrefixes = k;var z = { elem: l("modernizr") };Modernizr._q.push(function () {
    delete z.elem;
  });var T = { style: z.elem.style };Modernizr._q.unshift(function () {
    delete T.style;
  }), w.testAllProps = h;var j = w.prefixed = function (e, n, t) {
    return 0 === e.indexOf("@") ? N(e) : (-1 != e.indexOf("-") && (e = i(e)), n ? h(e, n, t) : h(e, "pfx"));
  };Modernizr.addTest("backgroundblendmode", j("backgroundBlendMode", "text")), w.testAllProps = y, Modernizr.addTest("cssmask", y("maskRepeat", "repeat-x", !0)), o(), s(C), delete w.addTest, delete w.addAsyncTest;for (var L = 0; L < Modernizr._q.length; L++) {
    Modernizr._q[L]();
  }e.Modernizr = Modernizr;
}(window, document);

/***/ }),
/* 21 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


document.addEventListener('swup:clickLink', function (event) {
    if (window.location.pathname == '/' || window.location.pathname == '/25let' || window.location.pathname == '/25let/') {
        window.savedScrollRestorationAmount = window.pageYOffset;
    }
});

document.addEventListener('swup:popState', function (event) {
    setTimeout(function () {
        if (window.location.pathname == '/' || window.location.pathname == '/25let' || window.location.pathname == '/25let/') {
            if (window.savedScrollRestorationAmount != null) {
                window.scrollTo(0, window.savedScrollRestorationAmount);
            }
        }
    }, 100);
});

/***/ }),
/* 22 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


if (!Object.prototype.forEach) {
    Object.defineProperties(Object.prototype, {
        'forEach': {
            value: function value(callback) {
                if (this == null) {
                    throw new TypeError('Not an object');
                }
                var obj = this;
                for (var key in obj) {
                    if (obj.hasOwnProperty(key)) {
                        callback.call(obj, obj[key], key, obj);
                    }
                }
            },
            writable: true
        }
    });
}

/***/ }),
/* 23 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


/**
 * Add dataset support to elements
 * No globals, no overriding prototype with non-standard methods,
 *   handles CamelCase properly, attempts to use standard
 *   Object.defineProperty() (and Function bind()) methods,
 *   falls back to native implementation when existing
 * Inspired by http://code.eligrey.com/html5/dataset/
 *   (via https://github.com/adalgiso/html5-dataset/blob/master/html5-dataset.js )
 * Depends on Function.bind and Object.defineProperty/Object.getOwnPropertyDescriptor (polyfills below)
 * All code below is Licensed under the X11/MIT License
 */

// Inspired by https://developer.mozilla.org/en-US/docs/JavaScript/Reference/Global_Objects/Function/bind#Compatibility
if (!Function.prototype.bind) {
    Function.prototype.bind = function (oThis) {
        'use strict';

        if (typeof this !== "function") {
            // closest thing possible to the ECMAScript 5 internal IsCallable function
            throw new TypeError("Function.prototype.bind - what is trying to be bound is not callable");
        }

        var aArgs = Array.prototype.slice.call(arguments, 1),
            fToBind = this,
            FNOP = function FNOP() {},
            fBound = function fBound() {
            return fToBind.apply(this instanceof FNOP && oThis ? this : oThis, aArgs.concat(Array.prototype.slice.call(arguments)));
        };

        FNOP.prototype = this.prototype;
        fBound.prototype = new FNOP();

        return fBound;
    };
}

/*
 * Xccessors Standard: Cross-browser ECMAScript 5 accessors
 * http://purl.eligrey.com/github/Xccessors
 *
 * 2010-06-21
 *
 * By Eli Grey, http://eligrey.com
 *
 * A shim that partially implements Object.defineProperty,
 * Object.getOwnPropertyDescriptor, and Object.defineProperties in browsers that have
 * legacy __(define|lookup)[GS]etter__ support.
 *
 * Licensed under the X11/MIT License
 *   See LICENSE.md
*/

// Removed a few JSLint options as Notepad++ JSLint validator complaining and
//   made comply with JSLint; also moved 'use strict' inside function
/*jslint white: true, undef: true, plusplus: true,
  bitwise: true, regexp: true, newcap: true, maxlen: 90 */

/*! @source http://purl.eligrey.com/github/Xccessors/blob/master/xccessors-standard.js*/

(function () {
    'use strict';

    var ObjectProto = Object.prototype,
        defineGetter = ObjectProto.__defineGetter__,
        defineSetter = ObjectProto.__defineSetter__,
        lookupGetter = ObjectProto.__lookupGetter__,
        lookupSetter = ObjectProto.__lookupSetter__,
        hasOwnProp = ObjectProto.hasOwnProperty;

    if (defineGetter && defineSetter && lookupGetter && lookupSetter) {

        if (!Object.defineProperty) {
            Object.defineProperty = function (obj, prop, descriptor) {
                if (arguments.length < 3) {
                    // all arguments required
                    throw new TypeError("Arguments not optional");
                }

                prop += ""; // convert prop to string

                if (hasOwnProp.call(descriptor, "value")) {
                    if (!lookupGetter.call(obj, prop) && !lookupSetter.call(obj, prop)) {
                        // data property defined and no pre-existing accessors
                        obj[prop] = descriptor.value;
                    }

                    if (hasOwnProp.call(descriptor, "get") || hasOwnProp.call(descriptor, "set")) {
                        // descriptor has a value prop but accessor already exists
                        throw new TypeError("Cannot specify an accessor and a value");
                    }
                }

                // can't switch off these features in ECMAScript 3
                // so throw a TypeError if any are false
                if (!(descriptor.writable && descriptor.enumerable && descriptor.configurable)) {
                    throw new TypeError("This implementation of Object.defineProperty does not support" + " false for configurable, enumerable, or writable.");
                }

                if (descriptor.get) {
                    defineGetter.call(obj, prop, descriptor.get);
                }
                if (descriptor.set) {
                    defineSetter.call(obj, prop, descriptor.set);
                }

                return obj;
            };
        }

        if (!Object.getOwnPropertyDescriptor) {
            Object.getOwnPropertyDescriptor = function (obj, prop) {
                if (arguments.length < 2) {
                    // all arguments required
                    throw new TypeError("Arguments not optional.");
                }

                prop += ""; // convert prop to string

                var descriptor = {
                    configurable: true,
                    enumerable: true,
                    writable: true
                },
                    getter = lookupGetter.call(obj, prop),
                    setter = lookupSetter.call(obj, prop);

                if (!hasOwnProp.call(obj, prop)) {
                    // property doesn't exist or is inherited
                    return descriptor;
                }
                if (!getter && !setter) {
                    // not an accessor so return prop
                    descriptor.value = obj[prop];
                    return descriptor;
                }

                // there is an accessor, remove descriptor.writable;
                // populate descriptor.get and descriptor.set (IE's behavior)
                delete descriptor.writable;
                descriptor.get = descriptor.set = undefined;

                if (getter) {
                    descriptor.get = getter;
                }
                if (setter) {
                    descriptor.set = setter;
                }

                return descriptor;
            };
        }

        if (!Object.defineProperties) {
            Object.defineProperties = function (obj, props) {
                var prop;
                for (prop in props) {
                    if (hasOwnProp.call(props, prop)) {
                        Object.defineProperty(obj, prop, props[prop]);
                    }
                }
            };
        }
    }
})();

// Begin dataset code

if (!document.documentElement.dataset && (
// FF is empty while IE gives empty object
!Object.getOwnPropertyDescriptor(Element.prototype, 'dataset') || !Object.getOwnPropertyDescriptor(Element.prototype, 'dataset').get)) {
    var propDescriptor = {
        enumerable: true,
        get: function get() {
            'use strict';

            var i,
                that = this,
                HTML5_DOMStringMap,
                attrVal,
                attrName,
                propName,
                attribute,
                attributes = this.attributes,
                attsLength = attributes.length,
                toUpperCase = function toUpperCase(n0) {
                return n0.charAt(1).toUpperCase();
            },
                getter = function getter() {
                return this;
            },
                setter = function setter(attrName, value) {
                return typeof value !== 'undefined' ? this.setAttribute(attrName, value) : this.removeAttribute(attrName);
            };
            try {
                // Simulate DOMStringMap w/accessor support
                // Test setting accessor on normal object
                ({}).__defineGetter__('test', function () {});
                HTML5_DOMStringMap = {};
            } catch (e1) {
                // Use a DOM object for IE8
                HTML5_DOMStringMap = document.createElement('div');
            }
            for (i = 0; i < attsLength; i++) {
                attribute = attributes[i];
                // Fix: This test really should allow any XML Name without
                //         colons (and non-uppercase for XHTML)
                if (attribute && attribute.name && /^data-\w[\w\-]*$/.test(attribute.name)) {
                    attrVal = attribute.value;
                    attrName = attribute.name;
                    // Change to CamelCase
                    propName = attrName.substr(5).replace(/-./g, toUpperCase);
                    try {
                        Object.defineProperty(HTML5_DOMStringMap, propName, {
                            enumerable: this.enumerable,
                            get: getter.bind(attrVal || ''),
                            set: setter.bind(that, attrName)
                        });
                    } catch (e2) {
                        // if accessors are not working
                        HTML5_DOMStringMap[propName] = attrVal;
                    }
                }
            }
            return HTML5_DOMStringMap;
        }
    };
    try {
        // FF enumerates over element's dataset, but not
        //   Element.prototype.dataset; IE9 iterates over both
        Object.defineProperty(Element.prototype, 'dataset', propDescriptor);
    } catch (e) {
        propDescriptor.enumerable = false; // IE8 does not allow setting to true
        Object.defineProperty(Element.prototype, 'dataset', propDescriptor);
    }
}

/***/ }),
/* 24 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


if (!String.prototype.includes) {
    String.prototype.includes = function (search, start) {
        'use strict';

        if (typeof start !== 'number') {
            start = 0;
        }

        if (start + search.length > this.length) {
            return false;
        } else {
            return this.indexOf(search, start) !== -1;
        }
    };
}

/***/ }),
/* 25 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/* WEBPACK VAR INJECTION */(function(setImmediate) {var __WEBPACK_AMD_DEFINE_FACTORY__, __WEBPACK_AMD_DEFINE_RESULT__;

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

(function (global, factory) {
    ( false ? 'undefined' : _typeof(exports)) === 'object' && typeof module !== 'undefined' ? factory() :  true ? !(__WEBPACK_AMD_DEFINE_FACTORY__ = (factory),
				__WEBPACK_AMD_DEFINE_RESULT__ = (typeof __WEBPACK_AMD_DEFINE_FACTORY__ === 'function' ?
				(__WEBPACK_AMD_DEFINE_FACTORY__.call(exports, __webpack_require__, exports, module)) :
				__WEBPACK_AMD_DEFINE_FACTORY__),
				__WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__)) : factory();
})(undefined, function () {
    'use strict';

    // Store setTimeout reference so promise-polyfill will be unaffected by
    // other code modifying setTimeout (like sinon.useFakeTimers())

    var setTimeoutFunc = setTimeout;

    function noop() {}

    // Polyfill for Function.prototype.bind
    function bind(fn, thisArg) {
        return function () {
            fn.apply(thisArg, arguments);
        };
    }

    function Promise(fn) {
        if (!(this instanceof Promise)) throw new TypeError('Promises must be constructed via new');
        if (typeof fn !== 'function') throw new TypeError('not a function');
        this._state = 0;
        this._handled = false;
        this._value = undefined;
        this._deferreds = [];

        doResolve(fn, this);
    }

    function handle(self, deferred) {
        while (self._state === 3) {
            self = self._value;
        }
        if (self._state === 0) {
            self._deferreds.push(deferred);
            return;
        }
        self._handled = true;
        Promise._immediateFn(function () {
            var cb = self._state === 1 ? deferred.onFulfilled : deferred.onRejected;
            if (cb === null) {
                (self._state === 1 ? resolve : reject)(deferred.promise, self._value);
                return;
            }
            var ret;
            try {
                ret = cb(self._value);
            } catch (e) {
                reject(deferred.promise, e);
                return;
            }
            resolve(deferred.promise, ret);
        });
    }

    function resolve(self, newValue) {
        try {
            // Promise Resolution Procedure: https://github.com/promises-aplus/promises-spec#the-promise-resolution-procedure
            if (newValue === self) throw new TypeError('A promise cannot be resolved with itself.');
            if (newValue && ((typeof newValue === 'undefined' ? 'undefined' : _typeof(newValue)) === 'object' || typeof newValue === 'function')) {
                var then = newValue.then;
                if (newValue instanceof Promise) {
                    self._state = 3;
                    self._value = newValue;
                    finale(self);
                    return;
                } else if (typeof then === 'function') {
                    doResolve(bind(then, newValue), self);
                    return;
                }
            }
            self._state = 1;
            self._value = newValue;
            finale(self);
        } catch (e) {
            reject(self, e);
        }
    }

    function reject(self, newValue) {
        self._state = 2;
        self._value = newValue;
        finale(self);
    }

    function finale(self) {
        if (self._state === 2 && self._deferreds.length === 0) {
            Promise._immediateFn(function () {
                if (!self._handled) {
                    Promise._unhandledRejectionFn(self._value);
                }
            });
        }

        for (var i = 0, len = self._deferreds.length; i < len; i++) {
            handle(self, self._deferreds[i]);
        }
        self._deferreds = null;
    }

    function Handler(onFulfilled, onRejected, promise) {
        this.onFulfilled = typeof onFulfilled === 'function' ? onFulfilled : null;
        this.onRejected = typeof onRejected === 'function' ? onRejected : null;
        this.promise = promise;
    }

    /**
     * Take a potentially misbehaving resolver function and make sure
     * onFulfilled and onRejected are only called once.
     *
     * Makes no guarantees about asynchrony.
     */
    function doResolve(fn, self) {
        var done = false;
        try {
            fn(function (value) {
                if (done) return;
                done = true;
                resolve(self, value);
            }, function (reason) {
                if (done) return;
                done = true;
                reject(self, reason);
            });
        } catch (ex) {
            if (done) return;
            done = true;
            reject(self, ex);
        }
    }

    Promise.prototype['catch'] = function (onRejected) {
        return this.then(null, onRejected);
    };

    Promise.prototype.then = function (onFulfilled, onRejected) {
        var prom = new this.constructor(noop);

        handle(this, new Handler(onFulfilled, onRejected, prom));
        return prom;
    };

    Promise.all = function (arr) {
        return new Promise(function (resolve, reject) {
            if (!arr || typeof arr.length === 'undefined') throw new TypeError('Promise.all accepts an array');
            var args = Array.prototype.slice.call(arr);
            if (args.length === 0) return resolve([]);
            var remaining = args.length;

            function res(i, val) {
                try {
                    if (val && ((typeof val === 'undefined' ? 'undefined' : _typeof(val)) === 'object' || typeof val === 'function')) {
                        var then = val.then;
                        if (typeof then === 'function') {
                            then.call(val, function (val) {
                                res(i, val);
                            }, reject);
                            return;
                        }
                    }
                    args[i] = val;
                    if (--remaining === 0) {
                        resolve(args);
                    }
                } catch (ex) {
                    reject(ex);
                }
            }

            for (var i = 0; i < args.length; i++) {
                res(i, args[i]);
            }
        });
    };

    Promise.resolve = function (value) {
        if (value && (typeof value === 'undefined' ? 'undefined' : _typeof(value)) === 'object' && value.constructor === Promise) {
            return value;
        }

        return new Promise(function (resolve) {
            resolve(value);
        });
    };

    Promise.reject = function (value) {
        return new Promise(function (resolve, reject) {
            reject(value);
        });
    };

    Promise.race = function (values) {
        return new Promise(function (resolve, reject) {
            for (var i = 0, len = values.length; i < len; i++) {
                values[i].then(resolve, reject);
            }
        });
    };

    // Use polyfill for setImmediate for performance gains
    Promise._immediateFn = typeof setImmediate === 'function' && function (fn) {
        setImmediate(fn);
    } || function (fn) {
        setTimeoutFunc(fn, 0);
    };

    Promise._unhandledRejectionFn = function _unhandledRejectionFn(err) {
        if (typeof console !== 'undefined' && console) {
            console.warn('Possible Unhandled Promise Rejection:', err); // eslint-disable-line no-console
        }
    };

    Promise.toString = function () {
        return '[native code]';
    };

    var global = function () {
        // the only reliable means to get the global object is
        // `Function('return this')()`
        // However, this causes CSP violations in Chrome apps.
        if (typeof self !== 'undefined') {
            return self;
        }
        if (typeof window !== 'undefined') {
            return window;
        }
        if (typeof global !== 'undefined') {
            return global;
        }
        throw new Error('unable to locate global object');
    }();

    if (!global.Promise) {
        global.Promise = Promise;
    }
});
/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(26).setImmediate))

/***/ }),
/* 26 */
/***/ (function(module, exports, __webpack_require__) {

var apply = Function.prototype.apply;

// DOM APIs, for completeness

exports.setTimeout = function() {
  return new Timeout(apply.call(setTimeout, window, arguments), clearTimeout);
};
exports.setInterval = function() {
  return new Timeout(apply.call(setInterval, window, arguments), clearInterval);
};
exports.clearTimeout =
exports.clearInterval = function(timeout) {
  if (timeout) {
    timeout.close();
  }
};

function Timeout(id, clearFn) {
  this._id = id;
  this._clearFn = clearFn;
}
Timeout.prototype.unref = Timeout.prototype.ref = function() {};
Timeout.prototype.close = function() {
  this._clearFn.call(window, this._id);
};

// Does not start the time, just sets up the members needed.
exports.enroll = function(item, msecs) {
  clearTimeout(item._idleTimeoutId);
  item._idleTimeout = msecs;
};

exports.unenroll = function(item) {
  clearTimeout(item._idleTimeoutId);
  item._idleTimeout = -1;
};

exports._unrefActive = exports.active = function(item) {
  clearTimeout(item._idleTimeoutId);

  var msecs = item._idleTimeout;
  if (msecs >= 0) {
    item._idleTimeoutId = setTimeout(function onTimeout() {
      if (item._onTimeout)
        item._onTimeout();
    }, msecs);
  }
};

// setimmediate attaches itself to the global object
__webpack_require__(27);
exports.setImmediate = setImmediate;
exports.clearImmediate = clearImmediate;


/***/ }),
/* 27 */
/***/ (function(module, exports, __webpack_require__) {

/* WEBPACK VAR INJECTION */(function(global, process) {(function (global, undefined) {
    "use strict";

    if (global.setImmediate) {
        return;
    }

    var nextHandle = 1; // Spec says greater than zero
    var tasksByHandle = {};
    var currentlyRunningATask = false;
    var doc = global.document;
    var registerImmediate;

    function setImmediate(callback) {
      // Callback can either be a function or a string
      if (typeof callback !== "function") {
        callback = new Function("" + callback);
      }
      // Copy function arguments
      var args = new Array(arguments.length - 1);
      for (var i = 0; i < args.length; i++) {
          args[i] = arguments[i + 1];
      }
      // Store and register the task
      var task = { callback: callback, args: args };
      tasksByHandle[nextHandle] = task;
      registerImmediate(nextHandle);
      return nextHandle++;
    }

    function clearImmediate(handle) {
        delete tasksByHandle[handle];
    }

    function run(task) {
        var callback = task.callback;
        var args = task.args;
        switch (args.length) {
        case 0:
            callback();
            break;
        case 1:
            callback(args[0]);
            break;
        case 2:
            callback(args[0], args[1]);
            break;
        case 3:
            callback(args[0], args[1], args[2]);
            break;
        default:
            callback.apply(undefined, args);
            break;
        }
    }

    function runIfPresent(handle) {
        // From the spec: "Wait until any invocations of this algorithm started before this one have completed."
        // So if we're currently running a task, we'll need to delay this invocation.
        if (currentlyRunningATask) {
            // Delay by doing a setTimeout. setImmediate was tried instead, but in Firefox 7 it generated a
            // "too much recursion" error.
            setTimeout(runIfPresent, 0, handle);
        } else {
            var task = tasksByHandle[handle];
            if (task) {
                currentlyRunningATask = true;
                try {
                    run(task);
                } finally {
                    clearImmediate(handle);
                    currentlyRunningATask = false;
                }
            }
        }
    }

    function installNextTickImplementation() {
        registerImmediate = function(handle) {
            process.nextTick(function () { runIfPresent(handle); });
        };
    }

    function canUsePostMessage() {
        // The test against `importScripts` prevents this implementation from being installed inside a web worker,
        // where `global.postMessage` means something completely different and can't be used for this purpose.
        if (global.postMessage && !global.importScripts) {
            var postMessageIsAsynchronous = true;
            var oldOnMessage = global.onmessage;
            global.onmessage = function() {
                postMessageIsAsynchronous = false;
            };
            global.postMessage("", "*");
            global.onmessage = oldOnMessage;
            return postMessageIsAsynchronous;
        }
    }

    function installPostMessageImplementation() {
        // Installs an event handler on `global` for the `message` event: see
        // * https://developer.mozilla.org/en/DOM/window.postMessage
        // * http://www.whatwg.org/specs/web-apps/current-work/multipage/comms.html#crossDocumentMessages

        var messagePrefix = "setImmediate$" + Math.random() + "$";
        var onGlobalMessage = function(event) {
            if (event.source === global &&
                typeof event.data === "string" &&
                event.data.indexOf(messagePrefix) === 0) {
                runIfPresent(+event.data.slice(messagePrefix.length));
            }
        };

        if (global.addEventListener) {
            global.addEventListener("message", onGlobalMessage, false);
        } else {
            global.attachEvent("onmessage", onGlobalMessage);
        }

        registerImmediate = function(handle) {
            global.postMessage(messagePrefix + handle, "*");
        };
    }

    function installMessageChannelImplementation() {
        var channel = new MessageChannel();
        channel.port1.onmessage = function(event) {
            var handle = event.data;
            runIfPresent(handle);
        };

        registerImmediate = function(handle) {
            channel.port2.postMessage(handle);
        };
    }

    function installReadyStateChangeImplementation() {
        var html = doc.documentElement;
        registerImmediate = function(handle) {
            // Create a <script> element; its readystatechange event will be fired asynchronously once it is inserted
            // into the document. Do so, thus queuing up the task. Remember to clean up once it's been called.
            var script = doc.createElement("script");
            script.onreadystatechange = function () {
                runIfPresent(handle);
                script.onreadystatechange = null;
                html.removeChild(script);
                script = null;
            };
            html.appendChild(script);
        };
    }

    function installSetTimeoutImplementation() {
        registerImmediate = function(handle) {
            setTimeout(runIfPresent, 0, handle);
        };
    }

    // If supported, we should attach to the prototype of global, since that is where setTimeout et al. live.
    var attachTo = Object.getPrototypeOf && Object.getPrototypeOf(global);
    attachTo = attachTo && attachTo.setTimeout ? attachTo : global;

    // Don't get fooled by e.g. browserify environments.
    if ({}.toString.call(global.process) === "[object process]") {
        // For Node.js before 0.9
        installNextTickImplementation();

    } else if (canUsePostMessage()) {
        // For non-IE10 modern browsers
        installPostMessageImplementation();

    } else if (global.MessageChannel) {
        // For web workers, where supported
        installMessageChannelImplementation();

    } else if (doc && "onreadystatechange" in doc.createElement("script")) {
        // For IE 6–8
        installReadyStateChangeImplementation();

    } else {
        // For older browsers
        installSetTimeoutImplementation();
    }

    attachTo.setImmediate = setImmediate;
    attachTo.clearImmediate = clearImmediate;
}(typeof self === "undefined" ? typeof global === "undefined" ? this : global : self));

/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(2), __webpack_require__(7)))

/***/ }),
/* 28 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


(function () {

    if (typeof window.CustomEvent === "function") return false;

    function CustomEvent(event, params) {
        params = params || { bubbles: false, cancelable: false, detail: undefined };
        var evt = document.createEvent('CustomEvent');
        evt.initCustomEvent(event, params.bubbles, params.cancelable, params.detail);
        return evt;
    }

    CustomEvent.prototype = window.Event.prototype;

    window.CustomEvent = CustomEvent;
})();

/***/ }),
/* 29 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _swupjs = __webpack_require__(30);

var _swupjs2 = _interopRequireDefault(_swupjs);

var _Component = __webpack_require__(1);

var _Component2 = _interopRequireDefault(_Component);

var _scrollTo = __webpack_require__(9);

var _scrollTo2 = _interopRequireDefault(_scrollTo);

var _componentUtils = __webpack_require__(10);

var _TweenLite = __webpack_require__(0);

var _TweenLite2 = _interopRequireDefault(_TweenLite);

__webpack_require__(4);

__webpack_require__(6);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var CustomSwupjs = function (_Swupjs) {
    _inherits(CustomSwupjs, _Swupjs);

    function CustomSwupjs(setOptions) {
        _classCallCheck(this, CustomSwupjs);

        var _this = _possibleConstructorReturn(this, (CustomSwupjs.__proto__ || Object.getPrototypeOf(CustomSwupjs)).call(this, setOptions));

        _this.getPage = function (location, callback) {
            var request = new XMLHttpRequest();

            request.onreadystatechange = function () {
                if (request.readyState === 4) {
                    if (request.status !== 500) {
                        callback(request.responseText, request);
                    } else {
                        callback(null, request);
                    }
                }
            };

            request.open("GET", location, true);
            request.setRequestHeader("X-Requested-With", "XMLHttpRequest");
            request.setRequestHeader("accept", "application/json");
            request.send(null);
            return request;
        };

        _this.getDataFromHtml = function (c) {
            var j = JSON.parse(c);

            var fakeDom = document.createElement('div');
            var fakeDomLogo = document.createElement('div');
            fakeDom.innerHTML = j.snippets['snippet--content'];
            fakeDomLogo.innerHTML = j.snippets['snippet--logo'];
            fakeDom.querySelector('#swup').dataset.swup = '0';
            fakeDomLogo.querySelector('#logo').dataset.swup = '1';

            var json = {
                title: j.snippets['snippet--title'],
                pageClass: j.snippets['snippet--pageClass'],
                originalContent: null,
                blocks: [fakeDom.innerHTML, fakeDomLogo.innerHTML]
            };
            return json;
        };

        _this.markSwupElements = function (element) {
            var _this2 = this;

            var blocks = 0;

            for (var i = 0; i < this.options.elements.length; i++) {
                if (element.querySelector(this.options.elements[i]) == null) {
                    console.warn('Element ' + this.options.elements[i] + ' is not in current page.');
                } else {
                    [].forEach.call(document.body.querySelectorAll(this.options.elements[i]), function (item, index) {
                        element.querySelectorAll(_this2.options.elements[i])[index].dataset.swup = blocks;
                        element.querySelectorAll(_this2.options.elements[i])[index].setAttribute('data-swup', blocks);
                        blocks++;
                    });
                }
            }
        };

        _this.scrollTo = function (element, to, duration) {
            if (duration == 0) {
                window.scrollTo(0, 0);
            } else {
                (0, _scrollTo2.default)(to);
            }
        };

        // this.popStateHandler = (event) => {
        //     var link = new Link()
        //     link.setPath(event.state ? event.state.url : window.location.pathname)
        //     if (link.getHash() != '') {
        //         this.scrollToElement = link.getHash()
        //     } else {
        //         console.log('!!!!!!!!!!prevent')
        //         event.preventDefault()
        //     }
        //     this.triggerEvent('popState')
        //     // hot fix for scroll restoration
        //     setTimeout(() => {
        //         this.loadPage(link.getAddress(), event)
        //     }, 10)
        // }
        return _this;
    }

    return CustomSwupjs;
}(_swupjs2.default);

var xhrElement = document.querySelector('#swup');
var fadeElement = document.querySelector('#fade');
var menu = void 0;

setTimeout(function () {
    var menuElement = document.querySelector('[data-component="Nav"]');
    menu = _Component2.default.getFromElement(menuElement);
}, 10);

// fade out
function fadeOut(next) {
    fadeElement.style.opacity = 1;

    _TweenLite2.default.to(fadeElement, .5, {
        opacity: 0,
        onComplete: next
    });
}

// fade in
function fadeIn(next) {
    fadeElement.style.opacity = 0;

    _TweenLite2.default.to(fadeElement, .5, {
        opacity: 1,
        onComplete: next
    });
}

var animations = {
    '*': {
        in: fadeIn,
        out: fadeOut
    },
    'homepage>*': {
        in: fadeIn,
        out: function out(next) {
            _TweenLite2.default.to(document.getElementById('slideshow-bg'), 0.5, {
                transform: "scale(2)",
                opacity: 0
            });
            fadeOut(next);
        }
    }
};

var options = {
    debugMode: true,
    animations: animations,
    preload: true,
    elements: ['#swup', '#logo']
};
var swupjs = new CustomSwupjs(options);

document.addEventListener('swup:pageView', function (event) {
    swupjs.markSwupElements(document.documentElement);
});
swupjs.markSwupElements(document.documentElement);

document.addEventListener('swup:clickLink', function (event) {
    menu.close();
});

document.addEventListener('swup:contentReplaced', function (event) {
    xhrElement = document.querySelector('#swup');
    fadeElement = document.querySelector('#fade');
    //fadeElement.style.opacity = 0;

    setTimeout(function () {
        document.querySelectorAll('[data-swup]').forEach(function (element) {
            (0, _componentUtils.loadComponents)(element);
        });
    }, 10);
    setTimeout(function () {
        (0, _componentUtils.loadComponents)(document.getElementById('footer-nav'));
    }, 100);
});

document.addEventListener('swup:animationOutDone', function (event) {
    document.querySelectorAll('[data-swup]').forEach(function (element) {
        (0, _componentUtils.removeComponents)(element);
    });
    (0, _componentUtils.removeComponents)(document.getElementById('footer-nav'));
});

document.addEventListener('swup:animationInDone', function (event) {
    // remove "to-{page}" classes
    document.documentElement.classList.remove('is-changing');
    document.documentElement.classList.remove('is-rendering');
});

/***/ }),
/* 30 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _swup = __webpack_require__(31);

var _swup2 = _interopRequireDefault(_swup);

var _loadPage = __webpack_require__(51);

var _loadPage2 = _interopRequireDefault(_loadPage);

var _renderPage = __webpack_require__(52);

var _renderPage2 = _interopRequireDefault(_renderPage);

var _getAnimation = __webpack_require__(53);

var _getAnimation2 = _interopRequireDefault(_getAnimation);

var _createAnimationPromise = __webpack_require__(54);

var _createAnimationPromise2 = _interopRequireDefault(_createAnimationPromise);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

// modules


var Swupjs = function (_Swup) {
    _inherits(Swupjs, _Swup);

    function Swupjs(setOptions) {
        _classCallCheck(this, Swupjs);

        var defaults = {
            animations: {
                '*': {
                    out: function out(next) {
                        next();
                    },
                    in: function _in(next) {
                        next();
                    }
                }
            }
        };

        var options = _extends({}, defaults, setOptions);

        var _this = _possibleConstructorReturn(this, (Swupjs.__proto__ || Object.getPrototypeOf(Swupjs)).call(this, options));

        _this.loadPage = _loadPage2.default;
        _this.renderPage = _renderPage2.default;
        _this.getAnimation = _getAnimation2.default;
        _this.createAnimationPromise = _createAnimationPromise2.default;


        _this.animations = options.animations;
        return _this;
    }

    /**
     * make modules accessible in instance
     */


    return Swupjs;
}(_swup2.default);

exports.default = Swupjs;

/***/ }),
/* 31 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

// helpers


// modules


var _delegate = __webpack_require__(32);

var _delegate2 = _interopRequireDefault(_delegate);

var _detectie = __webpack_require__(34);

var _detectie2 = _interopRequireDefault(_detectie);

var _Cache = __webpack_require__(35);

var _Cache2 = _interopRequireDefault(_Cache);

var _Link = __webpack_require__(8);

var _Link2 = _interopRequireDefault(_Link);

var _transitionEnd = __webpack_require__(36);

var _transitionEnd2 = _interopRequireDefault(_transitionEnd);

var _request = __webpack_require__(37);

var _request2 = _interopRequireDefault(_request);

var _getDataFromHtml = __webpack_require__(38);

var _getDataFromHtml2 = _interopRequireDefault(_getDataFromHtml);

var _loadPage = __webpack_require__(39);

var _loadPage2 = _interopRequireDefault(_loadPage);

var _renderPage = __webpack_require__(40);

var _renderPage2 = _interopRequireDefault(_renderPage);

var _goBack = __webpack_require__(41);

var _goBack2 = _interopRequireDefault(_goBack);

var _createState = __webpack_require__(42);

var _createState2 = _interopRequireDefault(_createState);

var _triggerEvent = __webpack_require__(43);

var _triggerEvent2 = _interopRequireDefault(_triggerEvent);

var _getUrl = __webpack_require__(44);

var _getUrl2 = _interopRequireDefault(_getUrl);

var _scrollTo = __webpack_require__(45);

var _scrollTo2 = _interopRequireDefault(_scrollTo);

var _classify = __webpack_require__(46);

var _classify2 = _interopRequireDefault(_classify);

var _doScrolling = __webpack_require__(47);

var _doScrolling2 = _interopRequireDefault(_doScrolling);

var _markSwupElements = __webpack_require__(48);

var _markSwupElements2 = _interopRequireDefault(_markSwupElements);

var _updateTransition = __webpack_require__(49);

var _updateTransition2 = _interopRequireDefault(_updateTransition);

var _preloadPages = __webpack_require__(50);

var _preloadPages2 = _interopRequireDefault(_preloadPages);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Swup = function () {
    function Swup(setOptions) {
        _classCallCheck(this, Swup);

        // default options
        var defaults = {
            cache: true,
            animationSelector: '[class^="a-"]',
            elements: ['#swup'],
            pageClassPrefix: '',
            debugMode: false,
            scroll: true,
            preload: true,
            support: true,
            disableIE: false,

            animateScrollToAnchor: false,
            animateScrollOnMobile: false,
            doScrollingRightAway: false,
            scrollDuration: 0,

            LINK_SELECTOR: 'a[href^="/"]:not([data-no-swup]), a[href^="#"]:not([data-no-swup]), a[xlink\\:href]'

            /**
             * current transition object
             */
        };this.transition = {};

        var options = _extends({}, defaults, setOptions);

        /**
         * helper variables
         */
        // mobile detection variable
        this.mobile = false;
        // id of element to scroll to after render
        this.scrollToElement = null;
        // promise used for preload, so no new loading of the same page starts while page is loading
        this.preloadPromise = null;
        // save options
        this.options = options;

        /**
         * make modules accessible in instance
         */
        this.getUrl = _getUrl2.default;
        this.cache = new _Cache2.default();
        this.link = new _Link2.default();
        this.transitionEndEvent = (0, _transitionEnd2.default)();
        this.getDataFromHtml = _getDataFromHtml2.default;
        this.getPage = _request2.default;
        this.scrollTo = _scrollTo2.default;
        this.loadPage = _loadPage2.default;
        this.renderPage = _renderPage2.default;
        this.goBack = _goBack2.default;
        this.createState = _createState2.default;
        this.triggerEvent = _triggerEvent2.default;
        this.classify = _classify2.default;
        this.doScrolling = _doScrolling2.default;
        this.markSwupElements = _markSwupElements2.default;
        this.updateTransition = _updateTransition2.default;
        this.preloadPages = _preloadPages2.default;
        this.detectie = _detectie2.default;
        this.enable = this.enable;
        this.destroy = this.destroy;

        /**
         * detect mobile devices
         */
        if (this.options.scroll) {
            if (window.innerWidth <= 767) {
                this.mobile = true;
            }
        }

        // attach instance to window in debug mode
        if (this.options.debugMode) {
            window.swup = this;
        }

        this.getUrl();
        this.enable();
    }

    _createClass(Swup, [{
        key: 'enable',
        value: function enable() {
            /**
             * support check
             */
            if (this.options.support) {
                // check pushState support
                if (!('pushState' in window.history)) {
                    console.warn('pushState is not supported');
                    return;
                }
                // check transitionEnd support
                if ((0, _transitionEnd2.default)()) {
                    this.transitionEndEvent = (0, _transitionEnd2.default)();
                } else {
                    console.warn('transitionEnd detection is not supported');
                    return;
                }
                // check Promise support
                if (typeof Promise === "undefined" || Promise.toString().indexOf("[native code]") === -1) {
                    console.warn('Promise is not supported');
                    return;
                }
            }
            /**
             * disable IE
             */
            if (this.options.disableIE && this.detectie()) {
                return;
            }

            // variable to keep event listeners from "delegate"
            this.delegatedListeners = {};

            /**
             * link click handler
             */
            this.delegatedListeners.click = (0, _delegate2.default)(document, this.options.LINK_SELECTOR, 'click', this.linkClickHandler.bind(this));

            /**
             * link mouseover handler (preload)
             */
            this.delegatedListeners.mouseover = (0, _delegate2.default)(document.body, this.options.LINK_SELECTOR, 'mouseover', this.linkMouseoverHandler.bind(this));

            /**
             * popstate handler
             */
            window.addEventListener('popstate', this.popStateHandler.bind(this));

            /**
             * initial save to cache
             */
            var page = this.getDataFromHtml(document.documentElement.innerHTML);
            page.url = this.currentUrl;
            if (this.options.cache) {
                this.cache.cacheUrl(page, this.options.debugMode);
            }

            /**
             * mark swup blocks in html
             */
            this.markSwupElements(document.documentElement);

            /**
             * trigger enabled event
             */
            this.triggerEvent('enabled');
            document.documentElement.classList.add('swup-enabled');

            /**
             * trigger page view event
             */
            this.triggerEvent('pageView');

            /**
             * preload pages if possible
             */
            this.preloadPages();
        }
    }, {
        key: 'destroy',
        value: function destroy() {
            // remove delegated listeners
            this.delegatedListeners.click.destroy();
            this.delegatedListeners.mouseover.destroy();

            // remove popstate listener
            window.removeEventListener('popstate', this.popStateHandler.bind(this));

            // empty cache
            this.cache.empty();

            // remove swup data atributes from blocks
            document.querySelectorAll('[data-swup]').forEach(function (element) {
                delete element.dataset.swup;
            });

            this.triggerEvent('disabled');
            document.documentElement.classList.remove('swup-enabled');
        }
    }, {
        key: 'linkClickHandler',
        value: function linkClickHandler(event) {
            // no control key pressed
            if (!event.metaKey) {
                this.triggerEvent('clickLink');
                var link = new _Link2.default();
                event.preventDefault();
                link.setPath(event.delegateTarget.href);
                if (link.getPath() == this.currentUrl || link.getPath() == '') {
                    if (link.getHash() != '') {
                        this.triggerEvent('samePageWithHash');
                        var element = document.querySelector(link.getHash());
                        if (element != null) {
                            if (this.options.scroll) {
                                this.scrollTo(document.body, element.offsetTop, 320);
                            }
                            history.replaceState(undefined, undefined, link.getHash());
                        } else {
                            console.warn('Element for offset not found (' + link.getHash() + ')');
                        }
                    } else {
                        this.triggerEvent('samePage');
                        if (this.options.scroll) {
                            this.scrollTo(document.body, 0, 320);
                        }
                    }
                } else {
                    if (link.getHash() != '') {
                        this.scrollToElement = link.getHash();
                    }
                    // custom class fro dynamic pages
                    var swupClass = event.delegateTarget.dataset.swupClass;
                    if (swupClass != null) {
                        this.updateTransition(window.location.pathname, link.getPath(), event.delegateTarget.dataset.swupClass);
                        document.documentElement.classList.add('to-' + swupClass);
                    } else {
                        this.updateTransition(window.location.pathname, link.getPath());
                    }
                    this.loadPage(link.getPath(), false);
                }
            } else {
                this.triggerEvent('openPageInNewTab');
            }
        }
    }, {
        key: 'linkMouseoverHandler',
        value: function linkMouseoverHandler(event) {
            var _this = this;

            this.triggerEvent('hoverLink');
            if (this.options.preload) {
                var link = new _Link2.default();
                link.setPath(event.delegateTarget.href);
                if (link.getPath() != this.currentUrl && !this.cache.exists(link.getPath()) && this.preloadPromise == null) {
                    this.preloadPromise = new Promise(function (resolve) {
                        _this.getPage(link.getPath(), function (response) {
                            if (response === null) {
                                console.warn('Server error.');
                                _this.triggerEvent('serverError');
                            } else {
                                // get json data
                                var page = _this.getDataFromHtml(response);
                                page.url = link.getPath();
                                _this.cache.cacheUrl(page, _this.options.debugMode);
                                _this.triggerEvent('pagePreloaded');
                            }
                            resolve();
                            _this.preloadPromise = null;
                        });
                    });
                    this.preloadPromise.route = link.getPath();
                }
            }
        }
    }, {
        key: 'popStateHandler',
        value: function popStateHandler(event) {
            var link = new _Link2.default();
            link.setPath(event.state ? event.state.url : window.location.pathname);
            if (link.getHash() != '') {
                this.scrollToElement = link.getHash();
            } else {
                event.preventDefault();
            }
            this.triggerEvent('popState');
            this.loadPage(link.getPath(), event);
        }
    }]);

    return Swup;
}();

exports.default = Swup;

/***/ }),
/* 32 */
/***/ (function(module, exports, __webpack_require__) {

var closest = __webpack_require__(33);

/**
 * Delegates event to a selector.
 *
 * @param {Element} element
 * @param {String} selector
 * @param {String} type
 * @param {Function} callback
 * @param {Boolean} useCapture
 * @return {Object}
 */
function _delegate(element, selector, type, callback, useCapture) {
    var listenerFn = listener.apply(this, arguments);

    element.addEventListener(type, listenerFn, useCapture);

    return {
        destroy: function() {
            element.removeEventListener(type, listenerFn, useCapture);
        }
    }
}

/**
 * Delegates event to a selector.
 *
 * @param {Element|String|Array} [elements]
 * @param {String} selector
 * @param {String} type
 * @param {Function} callback
 * @param {Boolean} useCapture
 * @return {Object}
 */
function delegate(elements, selector, type, callback, useCapture) {
    // Handle the regular Element usage
    if (typeof elements.addEventListener === 'function') {
        return _delegate.apply(null, arguments);
    }

    // Handle Element-less usage, it defaults to global delegation
    if (typeof type === 'function') {
        // Use `document` as the first parameter, then apply arguments
        // This is a short way to .unshift `arguments` without running into deoptimizations
        return _delegate.bind(null, document).apply(null, arguments);
    }

    // Handle Selector-based usage
    if (typeof elements === 'string') {
        elements = document.querySelectorAll(elements);
    }

    // Handle Array-like based usage
    return Array.prototype.map.call(elements, function (element) {
        return _delegate(element, selector, type, callback, useCapture);
    });
}

/**
 * Finds closest match and invokes callback.
 *
 * @param {Element} element
 * @param {String} selector
 * @param {String} type
 * @param {Function} callback
 * @return {Function}
 */
function listener(element, selector, type, callback) {
    return function(e) {
        e.delegateTarget = closest(e.target, selector);

        if (e.delegateTarget) {
            callback.call(element, e);
        }
    }
}

module.exports = delegate;


/***/ }),
/* 33 */
/***/ (function(module, exports) {

var DOCUMENT_NODE_TYPE = 9;

/**
 * A polyfill for Element.matches()
 */
if (typeof Element !== 'undefined' && !Element.prototype.matches) {
    var proto = Element.prototype;

    proto.matches = proto.matchesSelector ||
                    proto.mozMatchesSelector ||
                    proto.msMatchesSelector ||
                    proto.oMatchesSelector ||
                    proto.webkitMatchesSelector;
}

/**
 * Finds the closest parent that matches a selector.
 *
 * @param {Element} element
 * @param {String} selector
 * @return {Function}
 */
function closest (element, selector) {
    while (element && element.nodeType !== DOCUMENT_NODE_TYPE) {
        if (typeof element.matches === 'function' &&
            element.matches(selector)) {
          return element;
        }
        element = element.parentNode;
    }
}

module.exports = closest;


/***/ }),
/* 34 */
/***/ (function(module, exports) {

/**
 * detect IE
 * returns version of IE or false, if browser is not Internet Explorer
 */
var detectie = function() {
    var ua = window.navigator.userAgent;

    var msie = ua.indexOf('MSIE ');
    if (msie > 0) {
        // IE 10 or older => return version number
        return parseInt(ua.substring(msie + 5, ua.indexOf('.', msie)), 10);
    }

    var trident = ua.indexOf('Trident/');
    if (trident > 0) {
        // IE 11 => return version number
        var rv = ua.indexOf('rv:');
        return parseInt(ua.substring(rv + 3, ua.indexOf('.', rv)), 10);
    }

    var edge = ua.indexOf('Edge/');
    if (edge > 0) {
       // IE 12 => return version number
       return parseInt(ua.substring(edge + 5, ua.indexOf('.', edge)), 10);
    }
    // other browser
    return false;
}

module.exports = detectie;

/***/ }),
/* 35 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Cache = function () {
    function Cache() {
        _classCallCheck(this, Cache);

        this.pages = {};
        this.count = 0;
        this.last = null;
    }

    _createClass(Cache, [{
        key: 'cacheUrl',
        value: function cacheUrl(page, displayCache) {
            this.count++;
            if (page.url in this.pages === false) {
                this.pages[page.url] = page;
            }
            this.last = this.pages[page.url];
            if (displayCache) {
                this.displayCache();
            }
        }
    }, {
        key: 'getPage',
        value: function getPage(url) {
            return this.pages[url];
        }
    }, {
        key: 'displayCache',
        value: function displayCache() {
            console.groupCollapsed('Cache (' + Object.keys(this.pages).length + ')');
            for (var key in this.pages) {
                console.log(this.pages[key]);
            }
            console.groupEnd();
        }
    }, {
        key: 'exists',
        value: function exists(url) {
            if (url in this.pages) return true;
            return false;
        }
    }, {
        key: 'empty',
        value: function empty(showLog) {
            this.pages = {};
            this.count = 0;
            this.last = null;
            if (showLog) {
                console.log('Cache cleared');
            }
        }
    }]);

    return Cache;
}();

exports.default = Cache;

/***/ }),
/* 36 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


module.exports = function transitionEnd() {
    var el = document.createElement('div');

    var transEndEventNames = {
        WebkitTransition: 'webkitTransitionEnd',
        MozTransition: 'transitionend',
        OTransition: 'oTransitionEnd otransitionend',
        transition: 'transitionend'
    };

    for (var name in transEndEventNames) {
        if (el.style[name] !== undefined) {
            return transEndEventNames[name];
        }
    }

    return false;
};

/***/ }),
/* 37 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


module.exports = function (location, callback) {
    var request = new XMLHttpRequest();

    request.onreadystatechange = function () {
        if (request.readyState === 4) {
            if (request.status !== 500) {
                callback(request.responseText, request);
            } else {
                callback(null, request);
            }
        }
    };

    request.open("GET", location, true);
    request.setRequestHeader("X-Requested-With", "XMLHttpRequest");
    request.send(null);
    return request;
};

/***/ }),
/* 38 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


module.exports = function (html) {
    var _this = this;

    var content = html.replace('<body', '<div id="swupBody"').replace('</body>', '</div>');
    var fakeDom = document.createElement('div');
    fakeDom.innerHTML = content;
    var blocks = [];

    for (var i = 0; i < this.options.elements.length; i++) {
        if (fakeDom.querySelector(this.options.elements[i]) == null) {
            console.warn('Element ' + this.options.elements[i] + ' is not found cached page.');
        } else {
            [].forEach.call(document.body.querySelectorAll(this.options.elements[i]), function (item, index) {
                fakeDom.querySelectorAll(_this.options.elements[i])[index].dataset.swup = blocks.length;
                blocks.push(fakeDom.querySelectorAll(_this.options.elements[i])[index].outerHTML);
            });
        }
    }

    var json = {
        title: fakeDom.querySelector('title').innerText,
        pageClass: fakeDom.querySelector('#swupBody').className,
        originalContent: html,
        blocks: blocks
    };
    return json;
};

/***/ }),
/* 39 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var forEach = Array.prototype.forEach;


module.exports = function (url, popstate) {
    var _this = this;

    var finalPage = null;

    // scrolling
    if (this.options.doScrollingRightAway && !this.scrollToElement) {
        this.doScrolling(popstate);
    }

    var animationPromises = [];

    if (!popstate) {
        // start animation
        document.documentElement.classList.add('is-changing');
        document.documentElement.classList.add('is-leaving');
        document.documentElement.classList.add('is-animating');
        document.documentElement.classList.add('to-' + this.classify(url));

        // detect animation end
        var animatedElements = document.querySelectorAll(this.options.animationSelector);
        forEach.call(animatedElements, function (element) {
            var promise = new Promise(function (resolve) {
                element.addEventListener(_this.transitionEndEvent, resolve);
            });
            animationPromises.push(promise);
        });

        Promise.all(animationPromises).then(function () {
            _this.triggerEvent('animationOutDone');
        });

        // create pop element with or without anchor
        if (this.scrollToElement != null) {
            var pop = url + this.scrollToElement;
        } else {
            var pop = url;
        }
        this.createState(pop);
    } else {
        // proceed without animating
        this.triggerEvent('animationSkipped');
    }

    if (this.cache.exists(url)) {
        var xhrPromise = new Promise(function (resolve) {
            resolve();
        });
        this.triggerEvent('pageRetrievedFromCache');
    } else {
        if (!this.preloadPromise || this.preloadPromise.route != url) {
            var xhrPromise = new Promise(function (resolve) {
                _this.getPage(url, function (response) {
                    if (response === null) {
                        console.warn('Server error.');
                        _this.triggerEvent('serverError');
                        _this.goBack();
                    } else {
                        // get json data
                        var page = _this.getDataFromHtml(response);
                        page.url = url;
                        // render page
                        _this.cache.cacheUrl(page, _this.options.debugMode);
                        _this.triggerEvent('pageLoaded');
                    }
                    resolve();
                });
            });
        } else {
            var xhrPromise = this.preloadPromise;
        }
    }

    Promise.all(animationPromises.concat([xhrPromise])).then(function () {
        finalPage = _this.cache.getPage(url);
        if (!_this.options.cache) {
            _this.cache.empty(_this.options.debugMode);
        }
        _this.renderPage(finalPage, popstate);
        _this.preloadPromise = null;
    });
};

/***/ }),
/* 40 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var forEach = Array.prototype.forEach;


module.exports = function (page, popstate) {
    var _this = this;

    document.documentElement.classList.remove('is-leaving');

    // only add for non-popstate transitions
    if (!popstate) {
        document.documentElement.classList.add('is-rendering');
    }

    // replace blocks
    for (var i = 0; i < page.blocks.length; i++) {
        document.body.querySelector('[data-swup="' + i + '"]').outerHTML = page.blocks[i];
    }

    // set title
    document.title = page.title;

    this.triggerEvent('contentReplaced');
    this.triggerEvent('pageView');
    setTimeout(function () {
        document.documentElement.classList.remove('is-animating');
    }, 10);

    // handle classes after render
    if (this.options.pageClassPrefix !== false) {
        document.body.className.split(' ').forEach(function (className) {
            // empty string for page class
            if (className != "" && className.includes(_this.options.pageClassPrefix)) {
                document.body.classList.remove(className);
            }
        });
    }

    // empty string for page class
    if (page.pageClass != "") {
        page.pageClass.split(' ').forEach(function (className) {
            document.body.classList.add(className);
        });
    }

    // scrolling
    if (!this.options.doScrollingRightAway || this.scrollToElement) {
        this.doScrolling(popstate);
    }

    // detect animation end
    var animatedElements = document.querySelectorAll(this.options.animationSelector);
    var promises = [];
    forEach.call(animatedElements, function (element) {
        var promise = new Promise(function (resolve) {
            element.addEventListener(_this.transitionEndEvent, resolve);
        });
        promises.push(promise);
    });

    //preload pages if possible
    this.preloadPages();

    Promise.all(promises).then(function () {
        _this.triggerEvent('animationInDone');
        // remove "to-{page}" classes
        document.documentElement.classList.forEach(function (classItem) {
            if (classItem.startsWith('to-')) {
                document.documentElement.classList.remove(classItem);
            }
        });
        document.documentElement.classList.remove('is-changing');
        document.documentElement.classList.remove('is-rendering');
    });

    // update current url
    this.getUrl();
};

/***/ }),
/* 41 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


module.exports = function (page, popstate) {
    setTimeout(function () {
        document.body.classList.remove('is-changing');
        history.back();
    }, 100);
};

/***/ }),
/* 42 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


module.exports = function (url) {
    window.history.pushState({
        url: url || window.location.href.split(window.location.hostname)[1],
        random: Math.random()
    }, document.getElementsByTagName('title')[0].innerText, url || window.location.href.split(window.location.hostname)[1]);
};

/***/ }),
/* 43 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


module.exports = function (eventName) {
    if (this.options.debugMode) {
        console.log('%cswup:' + '%c' + eventName, 'color: #343434', 'color: #009ACD');
    }
    var event = new CustomEvent('swup:' + eventName, { detail: eventName });
    document.dispatchEvent(event);
};

/***/ }),
/* 44 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


module.exports = function () {
    this.currentUrl = window.location.pathname + window.location.search;
};

/***/ }),
/* 45 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


module.exports = function (element, to, duration) {
    var _this = this;

    this.triggerEvent('scrollStart');
    var start = (window.pageYOffset || document.documentElement.scrollTop) - (document.documentElement.clientTop || 0),
        change = to - start,
        increment = 20;

    var animateScroll = function animateScroll(elapsedTime) {
        elapsedTime += increment;
        var position = easeInOut(elapsedTime, start, change, duration);
        window.scrollTo(0, position);
        if (elapsedTime < duration) {
            setTimeout(function () {
                animateScroll(elapsedTime);
            }, increment);
        } else {
            _this.triggerEvent('scrollDone');
        }
    };

    animateScroll(0);

    function easeInOut(currentTime, start, change, duration) {
        currentTime /= duration / 2;
        if (currentTime < 1) {
            return change / 2 * currentTime * currentTime + start;
        }
        currentTime -= 1;
        return -change / 2 * (currentTime * (currentTime - 2) - 1) + start;
    }
};

/***/ }),
/* 46 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


module.exports = function (text) {
    var output = text.toString().toLowerCase().replace(/\s+/g, '-') // Replace spaces with -
    .replace(/\//g, '-') // Replace / with -
    .replace(/[^\w\-]+/g, '') // Remove all non-word chars
    .replace(/\-\-+/g, '-') // Replace multiple - with single -
    .replace(/^-+/, '') // Trim - from start of text
    .replace(/-+$/, ''); // Trim - from end of text
    if (output[0] == "/") output = output.splice(1);
    if (output == '') output = 'homepage';
    return output;
};

/***/ }),
/* 47 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


module.exports = function (popstate) {
    if (this.options.scroll && !popstate) {
        if (this.scrollToElement != null) {
            var self = this;

            var element = document.querySelector(self.scrollToElement);
            if (element != null) {
                if (self.animateScrollToAnchor) {
                    self.scrollTo(document.body, element.offsetTop, this.options.scrollDuration);
                } else {
                    self.scrollTo(document.body, element.offsetTop, 20);
                }
            } else {
                console.warn("Element for offset not found (" + self.scrollToElement + ")");
            }
            self.scrollToElement = null;
        } else {
            if (this.mobile && !this.options.animateScrollOnMobile) {
                this.scrollTo(document.body, 0, 0);
            } else if (this.mobile && this.options.animateScrollOnMobile) {
                this.scrollTo(document.body, 0, this.options.scrollDuration);
            } else {
                this.scrollTo(document.body, 0, this.options.scrollDuration);
            }
        }
    }
};

/***/ }),
/* 48 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


module.exports = function (element) {
    var _this = this;

    var blocks = 0;

    for (var i = 0; i < this.options.elements.length; i++) {
        if (element.querySelector(this.options.elements[i]) == null) {
            console.warn("Element " + this.options.elements[i] + " is not in current page.");
        } else {
            [].forEach.call(document.body.querySelectorAll(this.options.elements[i]), function (item, index) {
                element.querySelectorAll(_this.options.elements[i])[index].dataset.swup = blocks;
                blocks++;
            });
        }
    }
};

/***/ }),
/* 49 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


module.exports = function (from, to, custom) {

    // homepage case
    if (from == "/") {
        from = "/homepage";
    }
    if (to == "/") {
        to = "/homepage";
    }

    // transition routes
    this.transition = {
        from: from.replace('/', ''),
        to: to.replace('/', '')
    };

    if (custom) {
        this.transition.custom = custom;
    }
};

/***/ }),
/* 50 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _Link = __webpack_require__(8);

var _Link2 = _interopRequireDefault(_Link);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

module.exports = function (eventName) {
    var _this = this;

    if (this.options.preload) {
        var preload = function preload(pathname) {
            var link = new _Link2.default();
            link.setPath(pathname);
            if (link.getPath() != _this.currentUrl && !_this.cache.exists(link.getPath()) && _this.preloadPromise == null) {
                _this.getPage(link.getPath(), function (response) {
                    if (response === null) {
                        console.warn('Server error.');
                        _this.triggerEvent('serverError');
                    } else {
                        // get json data
                        var page = _this.getDataFromHtml(response);
                        page.url = link.getPath();
                        _this.cache.cacheUrl(page, _this.options.debugMode);
                        _this.triggerEvent('pagePreloaded');
                    }
                });
            }
        };

        document.querySelectorAll('[data-swup-preload]').forEach(function (element) {
            preload(element.href);
        });
    }
};

/***/ }),
/* 51 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


module.exports = function (url, popstate) {
    var _this = this;

    var finalPage = null;

    // scrolling
    if (this.options.doScrollingRightAway && !this.scrollToElement) {
        this.doScrolling(popstate);
    }

    var animationPromises = [];

    if (!popstate) {
        // start animation
        document.documentElement.classList.add('is-changing');
        document.documentElement.classList.add('is-leaving');
        document.documentElement.classList.add('to-' + this.classify(url));

        // animation promise
        var animationPromise = this.createAnimationPromise(this.getAnimation(this.transition, this.animations, 'out'));
        animationPromises.push(animationPromise);

        Promise.all(animationPromises).then(function () {
            _this.triggerEvent('animationOutDone');
        });

        // create pop element with or without anchor
        if (this.scrollToElement != null) {
            var pop = url + this.scrollToElement;
        } else {
            var pop = url;
        }
        this.createState(pop);
    } else {
        // proceed without animating
        this.triggerEvent('animationSkipped');
    }

    if (this.cache.exists(url)) {
        var xhrPromise = new Promise(function (resolve) {
            resolve();
        });
        this.triggerEvent('pageRetrievedFromCache');
    } else {
        if (!this.preloadPromise || this.preloadPromise.route != url) {
            var xhrPromise = new Promise(function (resolve) {
                _this.getPage(url, function (response) {
                    if (response === null) {
                        console.warn('Server error.');
                        _this.triggerEvent('serverError');
                        _this.goBack();
                    } else {
                        // get json data
                        var page = _this.getDataFromHtml(response);
                        page.url = url;
                        // render page
                        _this.cache.cacheUrl(page, _this.options.debugMode);
                        _this.triggerEvent('pageLoaded');
                    }
                    resolve();
                });
            });
        } else {
            var xhrPromise = this.preloadPromise;
        }
    }

    Promise.all(animationPromises.concat([xhrPromise])).then(function () {
        finalPage = _this.cache.getPage(url);
        if (!_this.options.cache) {
            _this.cache.empty(_this.options.debugMode);
        }
        _this.renderPage(finalPage, popstate);
        _this.preloadPromise = null;
    });
};

/***/ }),
/* 52 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var forEach = Array.prototype.forEach;


module.exports = function (page, popstate) {
    var _this = this;

    document.documentElement.classList.remove('is-leaving');
    if (!popstate) {
        document.documentElement.classList.add('is-rendering');
    }

    // replace blocks
    for (var i = 0; i < page.blocks.length; i++) {
        document.body.querySelector('[data-swup="' + i + '"]').outerHTML = page.blocks[i];
    }

    // set title
    document.title = page.title;

    this.triggerEvent('contentReplaced');
    this.triggerEvent('pageView');

    // handle classes after render
    if (this.options.pageClassPrefix !== false) {
        document.body.className.split(' ').forEach(function (className) {
            // empty string for page class
            if (className != "" && className.includes(_this.options.pageClassPrefix)) {
                document.body.classList.remove(className);
            }
        });
    }

    // empty string for page class
    if (page.pageClass != "") {
        page.pageClass.split(' ').forEach(function (className) {
            document.body.classList.add(className);
        });
    }

    // scrolling
    if (!this.options.doScrollingRightAway || this.scrollToElement) {
        this.doScrolling(popstate);
    }

    // detect animation end
    var animationPromises = [];
    if (!popstate) {
        var animationPromise = this.createAnimationPromise(this.getAnimation(this.transition, this.animations, 'in'));
        animationPromises.push(animationPromise);
    }

    Promise.all(animationPromises).then(function () {
        _this.triggerEvent('animationInDone');
        // remove "to-{page}" classes
        document.documentElement.classList.forEach(function (classItem) {
            if (classItem.startsWith('to-')) {
                document.documentElement.classList.remove(classItem);
            }
        });
        document.documentElement.classList.remove('is-changing');
        document.documentElement.classList.remove('is-rendering');
    });

    // update current url
    this.getUrl();
};

/***/ }),
/* 53 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


module.exports = function (transition, animations, type) {

    var animation = null;
    var animationName = null;
    var topRating = 0;

    Object.keys(animations).forEach(function (item) {
        var rating = 0;
        if (item.includes('>')) {
            var route = item.split('>');
            var from = route[0];
            var to = route[1];

            // TO equals to TO
            if (to == transition.to || to == "*") {
                rating++;
            }

            // equals to CUSTOM animation
            if (to == transition.custom) {
                rating = rating + 2;
            }

            // FROM equals or is ANY
            if (from == transition.from || from == "*") {
                rating++;
            }
        }

        // set new final animation
        if (rating > topRating) {
            topRating = rating;
            animationName = item;
            animation = animations[item];
        }
    });

    if (animation == null || topRating == 1) {
        animation = animations['*'];
        animationName = '*';
    }
    this.triggerEvent('pageAnimation:' + animationName);

    return animation[type];
};

/***/ }),
/* 54 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


module.exports = function (fn) {
    return new Promise(function (resolve) {
        fn(resolve);
    });
};

/***/ }),
/* 55 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.query = query;
exports.queryAll = queryAll;
exports.toggleClass = toggleClass;
exports.removeClass = removeClass;
exports.addClass = addClass;
exports.triggerEvent = triggerEvent;
function query(selector) {
    var context = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : document;

    if (typeof selector !== 'string') {
        return selector;
    }

    return context.querySelector(selector);
}

function queryAll(selector) {
    var context = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : document;

    if (typeof selector !== 'string') {
        return selector;
    }

    return Array.prototype.slice.call(context.querySelectorAll(selector));
}

function toggleClass(element, className) {
    var condition = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : null;

    if (condition === null) {
        if (element.classList.contains(className)) {
            element.classList.remove(className);
        } else {
            element.classList.add(className);
        }
    } else {
        if (condition) {
            element.classList.add(className);
        } else {
            element.classList.remove(className);
        }
    }
}

function removeClass(nodes, className) {
    if (Array.isArray(nodes)) {
        nodes.forEach(function (node) {
            return node.classList.remove(className);
        });
    } else {
        nodes.classList.remove(className);
    }

    return nodes;
}

function addClass(nodes, className) {
    if (Array.isArray(nodes)) {
        nodes.forEach(function (node) {
            return node.classList.add(className);
        });
    } else {
        nodes.classList.add(className);
    }

    return nodes;
}

function triggerEvent(element, eventType) {
    var params = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : null;
    var options = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : {
        bubbles: true,
        cancelable: true,
        detail: null
    };

    options.detail = params;
    var event = new CustomEvent(eventType, options);
    element.dispatchEvent(event);
}

// WEBPACK FOOTER //
// ./utils/dom/index.js

/***/ }),
/* 56 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.default = undefined;

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _Component2 = __webpack_require__(1);

var _Component3 = _interopRequireDefault(_Component2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Carousel = function (_Component) {
    _inherits(Carousel, _Component);

    function Carousel($element) {
        _classCallCheck(this, Carousel);

        var _this = _possibleConstructorReturn(this, (Carousel.__proto__ || Object.getPrototypeOf(Carousel)).call(this, $element));

        _this.ref = {};
        _this.ref.item = _this.element.querySelectorAll('[data-ref="item"]');
        _this.ref.navItem = _this.element.querySelectorAll('[data-ref="navItem"]');
        _this.ref.arrowTop = _this.element.querySelector('[data-ref="arrowTop"]');
        _this.ref.arrowBottom = _this.element.querySelector('[data-ref="arrowBottom"]');

        _this.id = 0;
        _this.count = _this.ref.item.length;

        var options = {};
        if (_this.element.dataset.options != null) {
            options = JSON.parse(_this.element.dataset.options);
        }

        var defaults = {};

        _this.options = _extends({}, defaults, options);
        return _this;
    }

    _createClass(Carousel, [{
        key: 'prepare',
        value: function prepare() {
            var _this2 = this;

            // previous
            this.ref.arrowTop.addEventListener('click', function (event) {
                event.preventDefault();
                _this2.previous();
            });

            // next
            this.ref.arrowBottom.addEventListener('click', function (event) {
                event.preventDefault();
                _this2.next();
            });

            this.ref.navItem.forEach(function (element, i) {
                element.addEventListener('click', function (event) {
                    event.preventDefault();

                    _this2.ref.navItem.forEach(function (item, i) {
                        item.classList.remove('is-active');
                    });
                    element.classList.add('is-active');

                    var index = _this2.count - i - 1;

                    _this2.ref.item.forEach(function (item, subIndex) {
                        item.classList.remove('is-active');
                        item.classList.remove('is-before');
                        item.classList.remove('is-after');
                        item.classList.remove('is-displayed');

                        if (subIndex < index) {
                            item.classList.add('is-after');
                            if (subIndex == index - 2 || subIndex == index - 1) {
                                item.classList.add('is-displayed');
                            }
                        } else if (subIndex == index) {
                            item.classList.add('is-active');
                        } else {
                            item.classList.add('is-before');
                        }
                    });
                });
            });

            this.ref.navItem[0].click();
        }
    }, {
        key: 'next',
        value: function next() {
            var _this3 = this;

            this.ref.navItem.forEach(function (element, i) {
                if (element.classList.contains('is-active')) {
                    if (_this3.ref.navItem[parseInt(i) + 1] != null) {
                        setTimeout(function () {
                            _this3.ref.navItem[parseInt(i) + 1].click();
                            return false;
                        }, 10);
                    }
                }
            });
        }
    }, {
        key: 'previous',
        value: function previous() {
            var _this4 = this;

            this.ref.navItem.forEach(function (element, i) {
                if (element.classList.contains('is-active')) {
                    if (_this4.ref.navItem[i - 1] != null) {
                        _this4.ref.navItem[i - 1].click();
                        return false;
                    }
                }
            });
        }
    }]);

    return Carousel;
}(_Component3.default);

exports.default = Carousel;

/***/ }),
/* 57 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.default = undefined;

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _Component2 = __webpack_require__(1);

var _Component3 = _interopRequireDefault(_Component2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { step("next", value); }, function (err) { step("throw", err); }); } } return step("next"); }); }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Carousel = function (_Component) {
    _inherits(Carousel, _Component);

    function Carousel($element) {
        _classCallCheck(this, Carousel);

        var _this = _possibleConstructorReturn(this, (Carousel.__proto__ || Object.getPrototypeOf(Carousel)).call(this, $element));

        var options = {};
        if (_this.element.dataset.options != null) {
            options = JSON.parse(_this.element.dataset.options);
        }

        var defaults = {
            // options, defaults listed

            accessibility: true,
            // enable keyboard navigation, pressing left & right keys

            adaptiveHeight: false,
            // set carousel height to the selected slide

            autoPlay: false,
            // advances to the next cell
            // if true, default is 3 seconds
            // or set time between advances in milliseconds
            // i.e. `autoPlay: 1000` will advance every 1 second

            cellAlign: 'center',
            // alignment of cells, 'center', 'left', or 'right'
            // or a decimal 0-1, 0 is beginning (left) of container, 1 is end (right)

            cellSelector: '[data-ref="item"]',
            // specify selector for cell elements

            contain: true,
            // will contain cells to container
            // so no excess scroll at beginning or end
            // has no effect if wrapAround is enabled

            draggable: true,
            // enables dragging & flicking

            dragThreshold: 15,
            // number of pixels a user must scroll horizontally to start dragging
            // increase to allow more room for vertical scroll for touch devices
            // default: 3

            freeScroll: false,
            // enables content to be freely scrolled and flicked
            // without aligning cells

            friction: 0.3,
            // smaller number = easier to flick farther

            groupCells: false,
            // group cells together in slides

            initialIndex: 0,
            // zero-based index of the initial selected cell

            lazyLoad: false,
            // enable lazy-loading images
            // set img data-flickity-lazyload="src.jpg"
            // set to number to load images adjacent cells

            percentPosition: false,
            // sets positioning in percent values, rather than pixels
            // Enable if items have percent widths
            // Disable if items have pixel widths, like images

            prevNextButtons: false,
            // creates and enables buttons to click to previous & next cells

            pageDots: false,
            // create and enable page dots

            resize: true,
            // listens to window resize events to adjust size & positions

            rightToLeft: false,
            // enables right-to-left layout

            setGallerySize: true,
            // sets the height of gallery
            // disable if gallery already has height set with CSS

            watchCSS: false,
            // watches the content of :after of the element
            // activates if #element:after { content: 'flickity' }

            wrapAround: true,
            // at end of cells, wraps-around to first for infinite scrolling


            // custom
            customArrows: false
        };

        _this.options = _extends({}, defaults, options);
        return _this;
    }

    _createClass(Carousel, [{
        key: 'require',
        value: function () {
            var _ref = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee() {
                return regeneratorRuntime.wrap(function _callee$(_context) {
                    while (1) {
                        switch (_context.prev = _context.next) {
                            case 0:
                                _context.next = 2;
                                return __webpack_require__.e/* import() */(0).then(__webpack_require__.bind(null, 79));

                            case 2:
                                this.flickity = _context.sent;

                            case 3:
                            case 'end':
                                return _context.stop();
                        }
                    }
                }, _callee, this);
            }));

            function require() {
                return _ref.apply(this, arguments);
            }

            return require;
        }()
    }, {
        key: 'prepare',
        value: function prepare() {
            var _this2 = this;

            if (this.ref.container == null) {
                this.ref.container = this.element;
            }

            this.flky = new this.flickity(this.ref.container, this.options);

            var prevIndex = null;
            this.flky.on('select', function (event) {
                _this2.element.classList.remove('flickity-index-' + prevIndex);
                _this2.element.classList.add('flickity-index-' + _this2.flky.selectedIndex);
                prevIndex = _this2.flky.selectedIndex;
            });

            // custom
            if (this.options.customArrows) {
                this.arrows();
            }

            // custom
            if (this.options.goTo) {
                this.goTo();
            }
        }

        // general custom arrows

    }, {
        key: 'arrows',
        value: function arrows() {
            var _this3 = this;

            // previous
            this.ref.arrowLeft.addEventListener('click', function (event) {
                event.preventDefault();
                _this3.flky.previous();
            });

            // next
            this.ref.arrowRight.addEventListener('click', function (event) {
                event.preventDefault();
                _this3.flky.next();
            });
        }
    }, {
        key: 'goTo',
        value: function goTo() {
            var _this4 = this;

            this.ref.nav.forEach(function (item, index) {
                item.addEventListener('click', function (event) {
                    event.preventDefault();
                    _this4.flky.select(index);
                });
            });
        }
    }]);

    return Carousel;
}(_Component3.default);

exports.default = Carousel;

/***/ }),
/* 58 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.default = undefined;

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _Component2 = __webpack_require__(1);

var _Component3 = _interopRequireDefault(_Component2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var DetectImageLoad = function (_Component) {
    _inherits(DetectImageLoad, _Component);

    function DetectImageLoad(element) {
        _classCallCheck(this, DetectImageLoad);

        return _possibleConstructorReturn(this, (DetectImageLoad.__proto__ || Object.getPrototypeOf(DetectImageLoad)).call(this, element));
    }

    _createClass(DetectImageLoad, [{
        key: "prepare",
        value: function prepare() {
            var _this2 = this;

            var promises = [];

            this.ref.image.forEach(function (item) {
                var img = document.createElement("img");
                if (item.tagName == "IMG") {
                    img.src = item.getAttribute('src');
                } else {
                    var src = window.getComputedStyle(item, null).getPropertyValue('background-image');
                    src = src.replace('url("', '');
                    src = src.replace('")', '');
                    src = src.replace('url(', '');
                    src = src.replace(')', '');
                    img.src = src;
                }
                var promise = new Promise(function (resolve) {
                    img.onload = function () {
                        resolve();
                    };
                });
                promises.push(promise);
            });

            Promise.all(promises).then(function () {
                _this2.element.classList.add('is-loaded');
            });
        }
    }]);

    return DetectImageLoad;
}(_Component3.default);

exports.default = DetectImageLoad;

/***/ }),
/* 59 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.default = undefined;

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _Component2 = __webpack_require__(1);

var _Component3 = _interopRequireDefault(_Component2);

var _throttle = __webpack_require__(3);

var _throttle2 = _interopRequireDefault(_throttle);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Explode = function (_Component) {
    _inherits(Explode, _Component);

    function Explode($element) {
        _classCallCheck(this, Explode);

        var _this = _possibleConstructorReturn(this, (Explode.__proto__ || Object.getPrototypeOf(Explode)).call(this, $element));

        _this.previousIndex = 0;

        // options
        var options = {};
        if (_this.element.dataset.options != null) {
            options = JSON.parse(_this.element.dataset.options);
        }
        var defaults = {
            offset: 200,
            selector: 'section'
        };

        _this.options = _extends({}, defaults, options);

        _this.sections = _this.element.querySelectorAll(_this.options.selector);
        _this.resize();
        return _this;
    }

    _createClass(Explode, [{
        key: 'prepare',
        value: function prepare() {
            this.scrollHandler = (0, _throttle2.default)(this.scroll.bind(this));
            this.resizeHandler = (0, _throttle2.default)(this.resize.bind(this));
            window.addEventListener('scroll', this.scrollHandler.bind(this));
            window.addEventListener('resize', this.resizeHandler.bind(this));
            this.scroll();
        }
    }, {
        key: 'destroy',
        value: function destroy() {
            window.removeEventListener('scroll', this.scrollHandler.bind(this));
            window.removeEventListener('resize', this.resizeHandler.bind(this));
        }
    }, {
        key: 'resize',
        value: function resize(event) {
            var _this2 = this;

            var body = document.body,
                html = document.documentElement;

            // window/document
            this.windowHeight = window.innerHeight;

            //items
            this.borders = [];
            this.sections.forEach(function (item) {
                var rect = item.getBoundingClientRect();
                _this2.borders.push({
                    top: rect.top + window.pageYOffset,
                    bottom: rect.top + window.pageYOffset + item.offsetHeight
                });
            });
        }
    }, {
        key: 'scroll',
        value: function scroll() {
            var _this3 = this;

            this.borders.forEach(function (item, index) {
                // is in viewport
                if (item.top < window.pageYOffset + _this3.windowHeight - _this3.options.offset && item.bottom > window.pageYOffset + _this3.options.offset) {
                    _this3.sections[index].classList.add('is-in-viewport');
                    _this3.sections[index].classList.add('has-displayed');
                } else {
                    _this3.sections[index].classList.remove('is-in-viewport');
                }
            });
        }
    }]);

    return Explode;
}(_Component3.default);

exports.default = Explode;

/***/ }),
/* 60 */
/***/ (function(module, exports, __webpack_require__) {

var isObject = __webpack_require__(5),
    now = __webpack_require__(61),
    toNumber = __webpack_require__(63);

/** Error message constants. */
var FUNC_ERROR_TEXT = 'Expected a function';

/* Built-in method references for those with the same name as other `lodash` methods. */
var nativeMax = Math.max,
    nativeMin = Math.min;

/**
 * Creates a debounced function that delays invoking `func` until after `wait`
 * milliseconds have elapsed since the last time the debounced function was
 * invoked. The debounced function comes with a `cancel` method to cancel
 * delayed `func` invocations and a `flush` method to immediately invoke them.
 * Provide `options` to indicate whether `func` should be invoked on the
 * leading and/or trailing edge of the `wait` timeout. The `func` is invoked
 * with the last arguments provided to the debounced function. Subsequent
 * calls to the debounced function return the result of the last `func`
 * invocation.
 *
 * **Note:** If `leading` and `trailing` options are `true`, `func` is
 * invoked on the trailing edge of the timeout only if the debounced function
 * is invoked more than once during the `wait` timeout.
 *
 * If `wait` is `0` and `leading` is `false`, `func` invocation is deferred
 * until to the next tick, similar to `setTimeout` with a timeout of `0`.
 *
 * See [David Corbacho's article](https://css-tricks.com/debouncing-throttling-explained-examples/)
 * for details over the differences between `_.debounce` and `_.throttle`.
 *
 * @static
 * @memberOf _
 * @since 0.1.0
 * @category Function
 * @param {Function} func The function to debounce.
 * @param {number} [wait=0] The number of milliseconds to delay.
 * @param {Object} [options={}] The options object.
 * @param {boolean} [options.leading=false]
 *  Specify invoking on the leading edge of the timeout.
 * @param {number} [options.maxWait]
 *  The maximum time `func` is allowed to be delayed before it's invoked.
 * @param {boolean} [options.trailing=true]
 *  Specify invoking on the trailing edge of the timeout.
 * @returns {Function} Returns the new debounced function.
 * @example
 *
 * // Avoid costly calculations while the window size is in flux.
 * jQuery(window).on('resize', _.debounce(calculateLayout, 150));
 *
 * // Invoke `sendMail` when clicked, debouncing subsequent calls.
 * jQuery(element).on('click', _.debounce(sendMail, 300, {
 *   'leading': true,
 *   'trailing': false
 * }));
 *
 * // Ensure `batchLog` is invoked once after 1 second of debounced calls.
 * var debounced = _.debounce(batchLog, 250, { 'maxWait': 1000 });
 * var source = new EventSource('/stream');
 * jQuery(source).on('message', debounced);
 *
 * // Cancel the trailing debounced invocation.
 * jQuery(window).on('popstate', debounced.cancel);
 */
function debounce(func, wait, options) {
  var lastArgs,
      lastThis,
      maxWait,
      result,
      timerId,
      lastCallTime,
      lastInvokeTime = 0,
      leading = false,
      maxing = false,
      trailing = true;

  if (typeof func != 'function') {
    throw new TypeError(FUNC_ERROR_TEXT);
  }
  wait = toNumber(wait) || 0;
  if (isObject(options)) {
    leading = !!options.leading;
    maxing = 'maxWait' in options;
    maxWait = maxing ? nativeMax(toNumber(options.maxWait) || 0, wait) : maxWait;
    trailing = 'trailing' in options ? !!options.trailing : trailing;
  }

  function invokeFunc(time) {
    var args = lastArgs,
        thisArg = lastThis;

    lastArgs = lastThis = undefined;
    lastInvokeTime = time;
    result = func.apply(thisArg, args);
    return result;
  }

  function leadingEdge(time) {
    // Reset any `maxWait` timer.
    lastInvokeTime = time;
    // Start the timer for the trailing edge.
    timerId = setTimeout(timerExpired, wait);
    // Invoke the leading edge.
    return leading ? invokeFunc(time) : result;
  }

  function remainingWait(time) {
    var timeSinceLastCall = time - lastCallTime,
        timeSinceLastInvoke = time - lastInvokeTime,
        result = wait - timeSinceLastCall;

    return maxing ? nativeMin(result, maxWait - timeSinceLastInvoke) : result;
  }

  function shouldInvoke(time) {
    var timeSinceLastCall = time - lastCallTime,
        timeSinceLastInvoke = time - lastInvokeTime;

    // Either this is the first call, activity has stopped and we're at the
    // trailing edge, the system time has gone backwards and we're treating
    // it as the trailing edge, or we've hit the `maxWait` limit.
    return (lastCallTime === undefined || (timeSinceLastCall >= wait) ||
      (timeSinceLastCall < 0) || (maxing && timeSinceLastInvoke >= maxWait));
  }

  function timerExpired() {
    var time = now();
    if (shouldInvoke(time)) {
      return trailingEdge(time);
    }
    // Restart the timer.
    timerId = setTimeout(timerExpired, remainingWait(time));
  }

  function trailingEdge(time) {
    timerId = undefined;

    // Only invoke if we have `lastArgs` which means `func` has been
    // debounced at least once.
    if (trailing && lastArgs) {
      return invokeFunc(time);
    }
    lastArgs = lastThis = undefined;
    return result;
  }

  function cancel() {
    if (timerId !== undefined) {
      clearTimeout(timerId);
    }
    lastInvokeTime = 0;
    lastArgs = lastCallTime = lastThis = timerId = undefined;
  }

  function flush() {
    return timerId === undefined ? result : trailingEdge(now());
  }

  function debounced() {
    var time = now(),
        isInvoking = shouldInvoke(time);

    lastArgs = arguments;
    lastThis = this;
    lastCallTime = time;

    if (isInvoking) {
      if (timerId === undefined) {
        return leadingEdge(lastCallTime);
      }
      if (maxing) {
        // Handle invocations in a tight loop.
        timerId = setTimeout(timerExpired, wait);
        return invokeFunc(lastCallTime);
      }
    }
    if (timerId === undefined) {
      timerId = setTimeout(timerExpired, wait);
    }
    return result;
  }
  debounced.cancel = cancel;
  debounced.flush = flush;
  return debounced;
}

module.exports = debounce;


/***/ }),
/* 61 */
/***/ (function(module, exports, __webpack_require__) {

var root = __webpack_require__(12);

/**
 * Gets the timestamp of the number of milliseconds that have elapsed since
 * the Unix epoch (1 January 1970 00:00:00 UTC).
 *
 * @static
 * @memberOf _
 * @since 2.4.0
 * @category Date
 * @returns {number} Returns the timestamp.
 * @example
 *
 * _.defer(function(stamp) {
 *   console.log(_.now() - stamp);
 * }, _.now());
 * // => Logs the number of milliseconds it took for the deferred invocation.
 */
var now = function() {
  return root.Date.now();
};

module.exports = now;


/***/ }),
/* 62 */
/***/ (function(module, exports, __webpack_require__) {

/* WEBPACK VAR INJECTION */(function(global) {/** Detect free variable `global` from Node.js. */
var freeGlobal = typeof global == 'object' && global && global.Object === Object && global;

module.exports = freeGlobal;

/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(2)))

/***/ }),
/* 63 */
/***/ (function(module, exports, __webpack_require__) {

var isObject = __webpack_require__(5),
    isSymbol = __webpack_require__(64);

/** Used as references for various `Number` constants. */
var NAN = 0 / 0;

/** Used to match leading and trailing whitespace. */
var reTrim = /^\s+|\s+$/g;

/** Used to detect bad signed hexadecimal string values. */
var reIsBadHex = /^[-+]0x[0-9a-f]+$/i;

/** Used to detect binary string values. */
var reIsBinary = /^0b[01]+$/i;

/** Used to detect octal string values. */
var reIsOctal = /^0o[0-7]+$/i;

/** Built-in method references without a dependency on `root`. */
var freeParseInt = parseInt;

/**
 * Converts `value` to a number.
 *
 * @static
 * @memberOf _
 * @since 4.0.0
 * @category Lang
 * @param {*} value The value to process.
 * @returns {number} Returns the number.
 * @example
 *
 * _.toNumber(3.2);
 * // => 3.2
 *
 * _.toNumber(Number.MIN_VALUE);
 * // => 5e-324
 *
 * _.toNumber(Infinity);
 * // => Infinity
 *
 * _.toNumber('3.2');
 * // => 3.2
 */
function toNumber(value) {
  if (typeof value == 'number') {
    return value;
  }
  if (isSymbol(value)) {
    return NAN;
  }
  if (isObject(value)) {
    var other = typeof value.valueOf == 'function' ? value.valueOf() : value;
    value = isObject(other) ? (other + '') : other;
  }
  if (typeof value != 'string') {
    return value === 0 ? value : +value;
  }
  value = value.replace(reTrim, '');
  var isBinary = reIsBinary.test(value);
  return (isBinary || reIsOctal.test(value))
    ? freeParseInt(value.slice(2), isBinary ? 2 : 8)
    : (reIsBadHex.test(value) ? NAN : +value);
}

module.exports = toNumber;


/***/ }),
/* 64 */
/***/ (function(module, exports, __webpack_require__) {

var baseGetTag = __webpack_require__(65),
    isObjectLike = __webpack_require__(68);

/** `Object#toString` result references. */
var symbolTag = '[object Symbol]';

/**
 * Checks if `value` is classified as a `Symbol` primitive or object.
 *
 * @static
 * @memberOf _
 * @since 4.0.0
 * @category Lang
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is a symbol, else `false`.
 * @example
 *
 * _.isSymbol(Symbol.iterator);
 * // => true
 *
 * _.isSymbol('abc');
 * // => false
 */
function isSymbol(value) {
  return typeof value == 'symbol' ||
    (isObjectLike(value) && baseGetTag(value) == symbolTag);
}

module.exports = isSymbol;


/***/ }),
/* 65 */
/***/ (function(module, exports, __webpack_require__) {

var Symbol = __webpack_require__(13),
    getRawTag = __webpack_require__(66),
    objectToString = __webpack_require__(67);

/** `Object#toString` result references. */
var nullTag = '[object Null]',
    undefinedTag = '[object Undefined]';

/** Built-in value references. */
var symToStringTag = Symbol ? Symbol.toStringTag : undefined;

/**
 * The base implementation of `getTag` without fallbacks for buggy environments.
 *
 * @private
 * @param {*} value The value to query.
 * @returns {string} Returns the `toStringTag`.
 */
function baseGetTag(value) {
  if (value == null) {
    return value === undefined ? undefinedTag : nullTag;
  }
  return (symToStringTag && symToStringTag in Object(value))
    ? getRawTag(value)
    : objectToString(value);
}

module.exports = baseGetTag;


/***/ }),
/* 66 */
/***/ (function(module, exports, __webpack_require__) {

var Symbol = __webpack_require__(13);

/** Used for built-in method references. */
var objectProto = Object.prototype;

/** Used to check objects for own properties. */
var hasOwnProperty = objectProto.hasOwnProperty;

/**
 * Used to resolve the
 * [`toStringTag`](http://ecma-international.org/ecma-262/7.0/#sec-object.prototype.tostring)
 * of values.
 */
var nativeObjectToString = objectProto.toString;

/** Built-in value references. */
var symToStringTag = Symbol ? Symbol.toStringTag : undefined;

/**
 * A specialized version of `baseGetTag` which ignores `Symbol.toStringTag` values.
 *
 * @private
 * @param {*} value The value to query.
 * @returns {string} Returns the raw `toStringTag`.
 */
function getRawTag(value) {
  var isOwn = hasOwnProperty.call(value, symToStringTag),
      tag = value[symToStringTag];

  try {
    value[symToStringTag] = undefined;
    var unmasked = true;
  } catch (e) {}

  var result = nativeObjectToString.call(value);
  if (unmasked) {
    if (isOwn) {
      value[symToStringTag] = tag;
    } else {
      delete value[symToStringTag];
    }
  }
  return result;
}

module.exports = getRawTag;


/***/ }),
/* 67 */
/***/ (function(module, exports) {

/** Used for built-in method references. */
var objectProto = Object.prototype;

/**
 * Used to resolve the
 * [`toStringTag`](http://ecma-international.org/ecma-262/7.0/#sec-object.prototype.tostring)
 * of values.
 */
var nativeObjectToString = objectProto.toString;

/**
 * Converts `value` to a string using `Object.prototype.toString`.
 *
 * @private
 * @param {*} value The value to convert.
 * @returns {string} Returns the converted string.
 */
function objectToString(value) {
  return nativeObjectToString.call(value);
}

module.exports = objectToString;


/***/ }),
/* 68 */
/***/ (function(module, exports) {

/**
 * Checks if `value` is object-like. A value is object-like if it's not `null`
 * and has a `typeof` result of "object".
 *
 * @static
 * @memberOf _
 * @since 4.0.0
 * @category Lang
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is object-like, else `false`.
 * @example
 *
 * _.isObjectLike({});
 * // => true
 *
 * _.isObjectLike([1, 2, 3]);
 * // => true
 *
 * _.isObjectLike(_.noop);
 * // => false
 *
 * _.isObjectLike(null);
 * // => false
 */
function isObjectLike(value) {
  return value != null && typeof value == 'object';
}

module.exports = isObjectLike;


/***/ }),
/* 69 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.default = undefined;

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _Component2 = __webpack_require__(1);

var _Component3 = _interopRequireDefault(_Component2);

var _TweenLite = __webpack_require__(0);

var _TweenLite2 = _interopRequireDefault(_TweenLite);

__webpack_require__(14);

__webpack_require__(15);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Slideshow = function (_Component) {
    _inherits(Slideshow, _Component);

    function Slideshow(element) {
        _classCallCheck(this, Slideshow);

        var _this = _possibleConstructorReturn(this, (Slideshow.__proto__ || Object.getPrototypeOf(Slideshow)).call(this, element));

        _this.ease = CustomEase.create("custom", "M0,0,C0.128,0.572,0.257,0.926,0.512,1,0.672,1.046,0.838,1,1,1");
        return _this;
    }

    _createClass(Slideshow, [{
        key: 'prepare',
        value: function prepare() {
            if (window.innerWidth >= 1200) {
                this.originalPath = this.ref.background.getAttribute('d');
                this.smallPath = "M1004.3-301.4c-88.3,28.3,38.7,67.1-32,109.8c-71.4,42.8-80.7-47.5-129.1,16.8c-35.5,46.5-59.6-10.3-66.3,29.2 " + "c-6.6,39.2,8.1,1.1,22,36.2c28,70.9,46.5-40.8,89.4-13.3c43,29.1-11.5,40.9,38,41.4c152.4,53.6,4.3-23.3,110-81.6 " + "c139.4-72.7-32.6,37.7,93.3,44.3c62.6,2.4-6.3-88.3,57-108.3c63.6-20.3,119.5,91.7,167.4,7c26.1-43.4-59.3,9.2-64.9-57.7 " + "c-7-66.5-79.7,45.8-123.7-12c-43.9-57.8,99.7-68.2-23.2-76C1022.6-374.2,1163.6-354.8,1004.3-301.4z";

                this.ref.background.setAttribute('d', this.smallPath);
                this.ref.clip.setAttribute('d', this.smallPath);

                this.element.style.opacity = "1";

                _TweenLite2.default.to(this.ref.background, 1, {
                    morphSVG: this.originalPath,
                    delay: 0.1,
                    ease: this.ease
                });
                _TweenLite2.default.to(this.ref.clip, 1, {
                    morphSVG: this.originalPath,
                    delay: 0.1,
                    ease: this.ease
                });
            }
        }
    }]);

    return Slideshow;
}(_Component3.default);

exports.default = Slideshow;

/***/ }),
/* 70 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.default = undefined;

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _Component2 = __webpack_require__(1);

var _Component3 = _interopRequireDefault(_Component2);

var _TweenLite = __webpack_require__(0);

var _TweenLite2 = _interopRequireDefault(_TweenLite);

__webpack_require__(4);

__webpack_require__(6);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Magnetic = function (_Component) {
    _inherits(Magnetic, _Component);

    function Magnetic(element) {
        _classCallCheck(this, Magnetic);

        var _this = _possibleConstructorReturn(this, (Magnetic.__proto__ || Object.getPrototypeOf(Magnetic)).call(this, element));

        _this.getRect();

        _this.ref = {};
        _this.ref.area = _this.element.querySelector('[data-ref="area"]');
        _this.ref.content = _this.element.querySelector('[data-ref="content"]');
        return _this;
    }

    _createClass(Magnetic, [{
        key: 'prepare',
        value: function prepare() {
            var _this2 = this;

            if (window.innerWidth >= 1200) {
                window.addEventListener('resize', this.getRect.bind(this));

                this.ref.area.addEventListener('mousemove', function (event) {
                    var x = event.clientX - _this2.rect.left - _this2.element.offsetWidth / 2;
                    var y = event.clientY - _this2.rect.top - _this2.element.offsetHeight / 2;

                    _TweenLite2.default.to(_this2.ref.content, .6, {
                        transform: 'translate3d(' + x + 'px, ' + y + 'px, 0)'
                    });
                });

                this.ref.area.addEventListener('mouseleave', this.reset.bind(this));
                this.ref.area.addEventListener('click', function (event) {
                    _this2.ref.content.querySelector('a, button').click();
                });
            }
        }
    }, {
        key: 'destroy',
        value: function destroy() {
            if (window.innerWidth >= 1200) {
                window.removeEventListener('resize', this.getRect.bind(this));
            }
        }
    }, {
        key: 'getRect',
        value: function getRect() {
            this.rect = this.element.getBoundingClientRect();
        }
    }, {
        key: 'reset',
        value: function reset() {
            _TweenLite2.default.to(this.ref.content, 0.8, {
                transform: 'translate3d(0, 0, 0)',
                ease: Back.easeOut.config(1.2)
            });
        }
    }]);

    return Magnetic;
}(_Component3.default);

exports.default = Magnetic;

/***/ }),
/* 71 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.default = undefined;

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _Component2 = __webpack_require__(1);

var _Component3 = _interopRequireDefault(_Component2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var MenuOpener = function (_Component) {
    _inherits(MenuOpener, _Component);

    function MenuOpener($element) {
        _classCallCheck(this, MenuOpener);

        return _possibleConstructorReturn(this, (MenuOpener.__proto__ || Object.getPrototypeOf(MenuOpener)).call(this, $element));
    }

    _createClass(MenuOpener, [{
        key: 'prepare',
        value: function prepare() {
            var _this2 = this;

            setTimeout(function () {
                var nav = document.querySelector('[data-component="Nav"]');
                var menu = _Component3.default.getFromElement(nav);

                _this2.element.addEventListener('click', function (event) {
                    if (document.documentElement.classList.contains('menu-is-opened')) {
                        menu.close();
                    } else {
                        menu.open();
                    }
                });

                document.addEventListener('keydown', function (event) {
                    if (event.keyCode == 27 && document.documentElement.classList.contains('menu-is-opened')) {
                        menu.close();
                    }
                });
            }, 100);
        }
    }]);

    return MenuOpener;
}(_Component3.default);

exports.default = MenuOpener;

/***/ }),
/* 72 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.default = undefined;

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _Component2 = __webpack_require__(1);

var _Component3 = _interopRequireDefault(_Component2);

var _TweenLite = __webpack_require__(0);

var _TweenLite2 = _interopRequireDefault(_TweenLite);

var _throttle = __webpack_require__(3);

var _throttle2 = _interopRequireDefault(_throttle);

var _config = __webpack_require__(73);

__webpack_require__(4);

__webpack_require__(6);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Nav = function (_Component) {
    _inherits(Nav, _Component);

    function Nav(element) {
        _classCallCheck(this, Nav);

        var _this = _possibleConstructorReturn(this, (Nav.__proto__ || Object.getPrototypeOf(Nav)).call(this, element));

        _this.resize();
        setTimeout(function () {
            _this.resize();
        }, 100);

        var options = {};
        if (_this.element.dataset.options != null) {
            options = JSON.parse(_this.element.dataset.options);
        }
        var defaults = {
            activeNext: false
        };

        _this.options = _extends({}, defaults, options);

        _this.originalPath = _this.ref.path.getAttribute('d');
        _this.innerWidth = _this.ref.scrollable.offsetWidth;
        _this.mobile = true;
        return _this;
    }

    _createClass(Nav, [{
        key: 'prepare',
        value: function prepare() {
            var _this2 = this;

            if (window.innerWidth >= 1200) {
                this.mouseMoveHandler = (0, _throttle2.default)(this.mouseMove);
                this.resizeHandler = (0, _throttle2.default)(this.resize);

                this.element.addEventListener('mousemove', this.mouseMoveHandler.bind(this));
                window.addEventListener('resize', this.resizeHandler.bind(this));

                this.ref.item.forEach(function (item) {
                    item.addEventListener('mouseover', _this2.showPath.bind(_this2));
                });
                this.ref.item.forEach(function (item) {
                    item.addEventListener('mouseleave', _this2.hidePath.bind(_this2));
                });
            }

            if (window.innerWidth < 1200) {
                this.menuScrollHandler = (0, _throttle2.default)(this.menuScroll);

                this.ref.item.forEach(function (item) {
                    _this2.showPath({ currentTarget: item }, true);
                });
                this.ref.menu.addEventListener('scroll', this.menuScroll.bind(this));

                // mobile next to active in viewport
                if (this.options.activeNext) {
                    setTimeout(function () {
                        var path = window.location.pathname;
                        var scrollAmount = 0;
                        _this2.ref.item.forEach(function (item, index) {
                            //console.log(path == item.parentNode.pathname && index+1 != this.ref.item.length);
                            if (path == item.parentNode.pathname && index + 1 != _this2.ref.item.length) {
                                scrollAmount = _this2.ref.item[index + 1].offsetLeft + _this2.ref.item[0].offsetWidth / 2 - _this2.windowWidth / 2;
                            }
                        });
                        _this2.ref.menu.scrollLeft = scrollAmount;
                    }, 100);
                }
            }
        }
    }, {
        key: 'open',
        value: function open() {
            var _this3 = this;

            document.documentElement.classList.add('menu-is-opened');
            if (!this.mobile) {
                setTimeout(function () {
                    _this3.goTo(0.9);
                }, 500);
            }
        }
    }, {
        key: 'close',
        value: function close() {
            document.documentElement.classList.remove('menu-is-opened');
            if (!this.mobile) {
                this.goTo(0.1);
            }
        }
    }, {
        key: 'destroy',
        value: function destroy() {
            if (window.innerWidth >= 1200) {
                this.element.removeEventListener('mousemove', this.mouseMoveHandler.bind(this));
                window.removeEventListener('resize', this.resizeHandler.bind(this));
            }
            if (window.innerWidth < 1200) {
                this.ref.menu.removeEventListener('scroll', this.menuScroll.bind(this));
            }
        }
    }, {
        key: 'mouseMove',
        value: function mouseMove(event) {
            this.percent = event.clientX / this.windowWidth;
            this.goTo((this.percent - 0.5) * 1.3 + 0.5);
        }
    }, {
        key: 'resize',
        value: function resize(event) {
            this.windowWidth = window.innerWidth;
        }
    }, {
        key: 'showPath',
        value: function showPath(event) {
            var halfOnly = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : false;

            if (!event.currentTarget.classList.contains('is-active')) {
                var parent = event.currentTarget.parentNode.parentNode;

                var path = event.currentTarget.querySelector('[data-ref="path"]');
                var finalPath = event.currentTarget.querySelector('[data-ref="finalPath"]');

                event.currentTarget.classList.add('is-active');
                _TweenLite2.default.to(path, 0.7, {
                    morphSVG: finalPath.getAttribute('d'),
                    transform: halfOnly == true ? "translate3d(25px, 25px, 0) scale(.5)" : "translate3d(-300px, -450px, 0) scale(2.7)"
                });
                _TweenLite2.default.to(path, 0.1, {
                    opacity: "1"
                });
                _TweenLite2.default.set(path, {
                    opacity: "1",
                    delay: .3
                });
            }
        }
    }, {
        key: 'hidePath',
        value: function hidePath(event) {
            if (event.currentTarget.classList.contains('is-active')) {
                var path = event.currentTarget.querySelector('[data-ref="path"]');

                event.currentTarget.classList.remove('is-active');
                _TweenLite2.default.to(path, 0.7, {
                    morphSVG: this.originalPath,
                    transform: "translate3d(60px, 60px, 0) scale(1)"
                });
                _TweenLite2.default.to(path, 0.1, {
                    opacity: "0",
                    delay: 0.6
                });
            }
        }
    }, {
        key: 'menuScroll',
        value: function menuScroll(event) {
            var percent = event.currentTarget.scrollLeft / (this.innerWidth - this.windowWidth);
            var amountScrollbar = Math.round((-50 + 180) * percent * 100) / 100;

            _TweenLite2.default.to(this.ref.scrollbar, .8, {
                transform: 'translate3d(' + amountScrollbar + 'px, 0, 0)',
                ease: Back.easeOut.config(1.1)
            });
        }
    }, {
        key: 'goTo',
        value: function goTo(percent) {
            var amount = Math.round((-this.innerWidth + this.windowWidth) * percent * 100) / 100;
            var amountScrollbar = Math.round((-50 + 180) * percent * 100) / 100;

            _TweenLite2.default.to(this.ref.scrollable, .8, {
                transform: 'translate3d(' + amount + 'px, 0, 0)',
                ease: Back.easeOut.config(1.1)
            });
            _TweenLite2.default.to(this.ref.scrollbar, .8, {
                transform: 'translate3d(' + amountScrollbar + 'px, 0, 0)',
                ease: Back.easeOut.config(1.1)
            });
        }
    }]);

    return Nav;
}(_Component3.default);

exports.default = Nav;

/***/ }),
/* 73 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
var viewports = {
    small: [320, 539],
    smallWide: [540, 767],
    medium: [768, 1023],
    large: [1024, 1199],
    xlarge: [1200, 1399],
    xxlarge: [1400, 1919],
    xxxlarge: [1920]
};

var queries = exports.queries = Object.keys(viewports).reduce(function (queries, key) {
    var viewport = viewports[key].map(function (value) {
        return value / 16;
    });

    queries[key + "Up"] = "screen and (min-width: " + viewport[0] + "em)";

    if (viewport.length === 2) {
        queries[key + "Only"] = "screen and (min-width: " + viewport[0] + "em) and (max-width: " + viewport[1] + "em)";
        queries[key + "Max"] = "screen and (max-width: " + viewport[1] + "em)";
    }

    return queries;
}, {});

/***/ }),
/* 74 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.default = undefined;

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _Component2 = __webpack_require__(1);

var _Component3 = _interopRequireDefault(_Component2);

var _throttle = __webpack_require__(3);

var _throttle2 = _interopRequireDefault(_throttle);

var _TweenLite = __webpack_require__(0);

var _TweenLite2 = _interopRequireDefault(_TweenLite);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Parallax = function (_Component) {
    _inherits(Parallax, _Component);

    function Parallax($element) {
        _classCallCheck(this, Parallax);

        var _this = _possibleConstructorReturn(this, (Parallax.__proto__ || Object.getPrototypeOf(Parallax)).call(this, $element));

        var options = {};
        if (_this.element.dataset.options != null) {
            options = JSON.parse(_this.element.dataset.options);
        }
        var defaults = {
            move: 500
        };

        _this.options = _extends({}, defaults, options);
        _this.resize();
        return _this;
    }

    _createClass(Parallax, [{
        key: 'prepare',
        value: function prepare() {
            this.scrollHandler = (0, _throttle2.default)(this.scroll.bind(this));
            this.resizeHandler = (0, _throttle2.default)(this.resize.bind(this));

            if (window.innerWidth >= 1200) {
                window.addEventListener('scroll', this.scrollHandler, { passive: true });
                window.addEventListener('resize', this.resizeHandler);
                this.scrollHandler();
            }
        }
    }, {
        key: 'destroy',
        value: function destroy() {
            if (window.innerWidth >= 1200) {
                window.removeEventListener('scroll', this.scrollHandler);
                window.removeEventListener('resize', this.resizeHandler);
            }
        }
    }, {
        key: 'resize',
        value: function resize(event) {
            this.windowHeight = window.innerHeight;
            var rect = this.element.getBoundingClientRect();
            this.top = rect.top + window.scrollY;
            this.topFull = this.top - this.options.move;
            this.bottom = rect.top + this.element.offsetHeight + window.scrollY;
            this.bottomFull = this.bottom + this.options.move;
        }
    }, {
        key: 'scroll',
        value: function scroll() {
            var scroll = window.pageYOffset;

            if (this.topFull < scroll + this.windowHeight && this.bottomFull > scroll) {
                var amount = Math.round(((this.top - scroll) / this.windowHeight - 0.5) * this.options.move / 2);
                //this.element.style.transform = `translate3d(0, ${amount}px, 0)`;
                _TweenLite2.default.set(this.element, {
                    transform: 'translate3d(0, ' + amount + 'px, 0)'
                });
            }
        }
    }]);

    return Parallax;
}(_Component3.default);

exports.default = Parallax;

/***/ }),
/* 75 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.default = undefined;

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _Component2 = __webpack_require__(1);

var _Component3 = _interopRequireDefault(_Component2);

var _TweenLite = __webpack_require__(0);

var _TweenLite2 = _interopRequireDefault(_TweenLite);

var _throttle = __webpack_require__(3);

var _throttle2 = _interopRequireDefault(_throttle);

var _scrollTo = __webpack_require__(9);

var _scrollTo2 = _interopRequireDefault(_scrollTo);

__webpack_require__(4);

__webpack_require__(15);

__webpack_require__(14);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Slideshow = function (_Component) {
    _inherits(Slideshow, _Component);

    function Slideshow(element) {
        _classCallCheck(this, Slideshow);

        var _this = _possibleConstructorReturn(this, (Slideshow.__proto__ || Object.getPrototypeOf(Slideshow)).call(this, element));

        _this.prev = 0;
        _this.loop = null;
        _this.indexOfSlide = 0;
        _this.indexOfShape = 1;
        _this.offset = { x: 0, y: 0 };
        _this.canUseCanvas = false;

        if (typeof Path2D !== 'undefined' && window.innerWidth >= 768 && window.navigator.userAgent.indexOf("Edge") == -1) {
            // because fucking edge does support path2D, but not really...
            _this.canUseCanvas = true;
        }

        _this.windowWidth = window.innerWidth;
        _this.windowHeight = window.innerHeight;

        if (_this.canUseCanvas) {
            _this.useCanvas();
        }
        _this.resize();

        setTimeout(function () {
            _this.resize();
        }, 20);
        _this.ease = CustomEase.create("custom", "M0,0 C0.128,0.572 0.275,1.109 0.528,1.026 0.904,0.902 0.838,1 1,1");
        //this.ease = CustomEase.create("custom", "M0,0 C0.096,0.291 0.22,1.1 0.42,1.074 0.542,1.058 0.546,0.94 0.638,0.94 0.73,0.94 0.739,1.042 0.806,1.028 0.862,1.016 0.87,0.984 0.908,0.984 0.944,0.984 0.924,1 1,1");
        return _this;
    }

    _createClass(Slideshow, [{
        key: 'prepare',
        value: function prepare() {
            this.mouseMoveHandler = (0, _throttle2.default)(this.mouseMove);
            this.resizeHandler = (0, _throttle2.default)(this.resize);
            this.scrollHandler = (0, _throttle2.default)(this.scroll);

            //this.element.addEventListener('mousemove', ::this.mouseMoveHandler);
            window.addEventListener('resize', this.resizeHandler.bind(this));
            window.addEventListener('scroll', this.scrollHandler.bind(this), { passive: true });
            document.addEventListener('keydown', this.keypressHandler.bind(this));
            if (this.canUseCanvas) {
                this.loop = setInterval(this.float.bind(this), 2000);
            };
            this.scrollHandler();
        }
    }, {
        key: 'destroy',
        value: function destroy() {
            window.removeEventListener('resize', this.resizeHandler.bind(this));
            window.removeEventListener('scroll', this.scrollHandler.bind(this));
            document.removeEventListener('keydown', this.keypressHandler.bind(this));
        }
    }, {
        key: 'mouseMove',
        value: function mouseMove(event) {
            this.percent = document.innerHeight;
        }
    }, {
        key: 'resize',
        value: function resize(event) {
            var _this2 = this;

            var body = document.body,
                html = document.documentElement;

            // window/document
            this.windowWidth = window.innerWidth;
            this.windowHeight = window.innerHeight;
            this.docHeight = Math.max(body.scrollHeight, body.offsetHeight, html.clientHeight, html.scrollHeight, html.offsetHeight);

            //items
            this.borders = [];
            this.ref.item.forEach(function (item) {
                var rect = item.getBoundingClientRect();
                _this2.borders.push(rect.top + window.pageYOffset);
            });

            if (this.canUseCanvas) {
                this.canvasResize();
            }
        }
    }, {
        key: 'getActive',
        value: function getActive() {
            var _this3 = this;

            var i = 0;
            this.borders.forEach(function (item, index) {
                if (item < window.pageYOffset + _this3.windowHeight / 2) {
                    i = index;
                    return 0;
                }
            });
            return i;
        }
    }, {
        key: 'scroll',
        value: function scroll(event) {
            var currentIndex = this.getActive();

            if (this.prev != currentIndex) {
                this.indexOfSlide = currentIndex;
                //this.resetLoop();

                // reset nav
                this.ref.navItem.forEach(function (item) {
                    item.classList.remove('is-active');
                });
                if (currentIndex != 0) {
                    this.ref.navItem[currentIndex - 1].classList.add('is-active');
                }

                // set number
                _TweenLite2.default.to(this.ref.num, .8, {
                    transform: 'translate3d(-' + currentIndex * 40 + 'px, 0, 0)'
                });

                // morph
                this.prev = currentIndex;
                this.morphTo(currentIndex);

                if (currentIndex == 0) {
                    this.element.classList.remove('is-scrolled');
                } else {
                    this.element.classList.add('is-scrolled');
                }
            }
        }
    }, {
        key: 'morphTo',
        value: function morphTo(index) {
            var position = this.getPosition(index);

            _TweenLite2.default.to(this.ref.bgPath, 2.4, {
                morphSVG: this.ref.path[index].getAttribute('d'),
                ease: this.ease,
                transform: position
            });
            _TweenLite2.default.to(this.ref.bgPath, 0.6, {
                fill: this.ref.path[index].getAttribute('fill')
            });

            _TweenLite2.default.to(this.ref.bgPath2, 2.4, {
                morphSVG: this.ref.path2[index].getAttribute('d'),
                ease: this.ease,
                transform: position
            });

            _TweenLite2.default.to(this.ref.bgPath3, 2.4, {
                morphSVG: this.ref.path3[index].getAttribute('d'),
                ease: this.ease,
                transform: position
            });

            var x = this.windowWidth / 2 - this.ref.svgs[index].getAttribute('width').replace('px', '') / 2;
            var y = this.windowHeight / 2 - this.ref.svgs[index].getAttribute('height').replace('px', '') / 2;

            _TweenLite2.default.to(this.offset, 2.4, {
                x: x,
                y: y,
                ease: this.ease
            });
        }
    }, {
        key: 'getPosition',
        value: function getPosition(i) {
            var index = "translate3d(0, 0, 0) scale(1)";
            switch (i) {
                case 0:
                    break;
                case 1:
                    index = "translate3d(-60px, 0, 0) scale(0.8)";
                    break;
                case 2:
                    index = "translate3d(-60px, 120px, 0) scale(0.7)";
                    break;
                case 3:
                    index = "translate3d(-60px, 60px, 0) scale(0.8)";
                    break;
                case 4:
                    index = "translate3d(-60px, 60px, 0) scale(0.7)";
                    break;
                case 5:
                    index = "translate3d(0, 0, 0) scale(0.7)";
                    break;
                case 6:
                    index = "translate3d(-60px, 0, 0) scale(0.8)";
                    break;
                case 7:
                    index = "translate3d(-120px, 60px, 0) scale(0.8)";
                    break;
                case 8:
                    index = "translate3d(0, 0, 0) scale(0.7)";
                    break;
                case 9:
                    index = "translate3d(0, 0, 0) scale(0.7)";
                    break;
                case 10:
                    index = "translate3d(-60px, 0, 0) scale(0.7)";
                    break;
            }

            return index;
        }
    }, {
        key: 'getCanvasPosition',
        value: function getCanvasPosition(i) {
            var index = { x: 0, y: 0 };
            switch (i) {
                case 0:
                    break;
                case 1:
                    index = { x: -500, y: 0 };
                    break;
                case 2:
                    index = { x: -60, y: 120 };
                    break;
                case 3:
                    index = { x: -60, y: 60 };
                    break;
                case 4:
                    index = { x: -60, y: 60 };
                    break;
                case 5:
                    index = { x: 0, y: 0 };
                    break;
                case 6:
                    index = { x: -60, y: 0 };
                    break;
                case 7:
                    index = { x: -120, y: 60 };
                    break;
                case 8:
                    index = { x: 0, y: 0 };
                    break;
                case 9:
                    index = { x: 0, y: 0 };
                    break;
                case 10:
                    index = { x: -60, y: 0 };
                    break;
            }

            return index;
        }
    }, {
        key: 'keypressHandler',
        value: function keypressHandler(event) {
            if (event.keyCode == 37 || event.keyCode == 38) {
                event.preventDefault();
                var index = this.getActive() - 1;
                if (index >= 0) {
                    (0, _scrollTo2.default)(this.borders[index]);
                }
            } else if (event.keyCode == 39 || event.keyCode == 40) {
                event.preventDefault();
                var _index = this.getActive() + 1;
                if (_index < this.borders.length) {
                    (0, _scrollTo2.default)(this.borders[_index]);
                }
            }
        }
    }, {
        key: 'resetLoop',
        value: function resetLoop() {
            clearInterval(this.loop);
            this.indexOfShape = 1;
            this.loop = setInterval(this.float.bind(this), 2000);
        }
    }, {
        key: 'float',
        value: function float() {
            var index1 = this.indexOfShape;
            var index2 = this.indexOfShape + 1;
            var index3 = this.indexOfShape - 1;
            if (index2 == 3) {
                index2 = 0;
            }
            if (index3 == -1) {
                index3 = 2;
            }

            var name = index1 == 0 ? 'path' : 'path' + (1 + index1);
            var name2 = index2 == 0 ? 'path' : 'path' + (1 + index2);
            var name3 = index3 == 0 ? 'path' : 'path' + (1 + index3);

            _TweenLite2.default.to(this.ref.bgPath, 3, {
                morphSVG: this.ref[name][this.indexOfSlide].getAttribute('d'),
                ease: this.ease
            });
            _TweenLite2.default.to(this.ref.bgPath2, 3, {
                morphSVG: this.ref[name2][this.indexOfSlide].getAttribute('d'),
                ease: this.ease
            });
            _TweenLite2.default.to(this.ref.bgPath3, 3, {
                morphSVG: this.ref[name3][this.indexOfSlide].getAttribute('d'),
                ease: this.ease
            });

            this.indexOfShape++;
            if (this.indexOfShape == 3) {
                this.indexOfShape = 0;
            }
        }
    }, {
        key: 'useCanvas',
        value: function useCanvas() {
            var _this4 = this;

            var canvas = this.ref.canvas = document.createElement('canvas');
            this.ref.svg.parentNode.removeChild(this.ref.svg);
            this.ref.bg.appendChild(canvas);
            var context = this.context = this.ref.canvas.getContext('2d');

            var svg = this.ref.svg;
            var pathOffsetX = this.windowWidth / 2 * .5;
            var pathOffsetY = this.windowHeight / 2 * .3;

            this.offset.x = this.windowWidth / 2 - this.ref.svgs[0].getAttribute('width').replace('px', '') / 2;
            this.offset.y = this.windowHeight / 2 - this.ref.svgs[0].getAttribute('height').replace('px', '') / 2;

            var draw = function draw(timestamp) {
                // clear canvas
                context.setTransform(1, 0, 0, 1, 0, 0);
                context.clearRect(0, 0, _this4.ref.canvas.width, _this4.ref.canvas.height);
                //this.ref.canvas.width = this.ref.canvas.width;
                //let pos = this.getCanvasPosition(this.currentIndex);
                context.translate(_this4.offset.x, _this4.offset.y);
                context.fillStyle = _this4.ref.bgPath.style.fill;
                context.strokeStyle = "rgba(24, 36, 82, 0.1)";

                // draw main shape
                var path = new Path2D(_this4.ref.bgPath.getAttribute('d'));
                context.fill(path);

                // draw other shapes
                path = new Path2D(_this4.ref.bgPath2.getAttribute('d'), _this4.offset.x, _this4.offset.y);
                context.stroke(path);

                path = new Path2D(_this4.ref.bgPath3.getAttribute('d'), _this4.offset.x, _this4.offset.y);
                context.stroke(path);

                window.requestAnimationFrame(draw);
            };
            window.requestAnimationFrame(draw);
        }
    }, {
        key: 'canvasResize',
        value: function canvasResize() {
            this.context.canvas.width = window.innerWidth;
            this.context.canvas.height = window.innerHeight;
            this.offset.x = this.windowWidth / 2 - this.ref.svgs[this.indexOfSlide].getAttribute('width').replace('px', '') / 2;
            this.offset.y = this.windowHeight / 2 - this.ref.svgs[this.indexOfSlide].getAttribute('height').replace('px', '') / 2;
        }
    }]);

    return Slideshow;
}(_Component3.default);

exports.default = Slideshow;

/***/ }),
/* 76 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.default = undefined;

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _Component2 = __webpack_require__(1);

var _Component3 = _interopRequireDefault(_Component2);

var _TweenLite = __webpack_require__(0);

var _TweenLite2 = _interopRequireDefault(_TweenLite);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Youtube = function (_Component) {
    _inherits(Youtube, _Component);

    function Youtube($element) {
        _classCallCheck(this, Youtube);

        var _this = _possibleConstructorReturn(this, (Youtube.__proto__ || Object.getPrototypeOf(Youtube)).call(this, $element));

        _this.blocked = false;

        var options = {};
        if (_this.element.dataset.options != null) {
            options = JSON.parse(_this.element.dataset.options);
        }
        var defaults = {
            move: 500
        };

        _this.options = _extends({}, defaults, options);
        return _this;
    }

    _createClass(Youtube, [{
        key: 'prepare',
        value: function prepare() {
            this.element.addEventListener('click', this.append.bind(this));
        }
    }, {
        key: 'append',
        value: function append() {
            //TweenLite.set(this.element, {
            //    transform: `translate3d(0, ${amount}px, 0)`
            //});
            if (!this.blocked) {
                this.element.classList.add('is-played');
                this.element.innerHTML += '<div class="Image-iframe"><iframe src="' + this.options.url + '&autoplay=1" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe></div>';
                this.blocked = true;
            }
        }
    }]);

    return Youtube;
}(_Component3.default);

exports.default = Youtube;

/***/ }),
/* 77 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.default = undefined;

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _Component2 = __webpack_require__(1);

var _Component3 = _interopRequireDefault(_Component2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Carousel = function (_Component) {
    _inherits(Carousel, _Component);

    function Carousel($element) {
        _classCallCheck(this, Carousel);

        var _this = _possibleConstructorReturn(this, (Carousel.__proto__ || Object.getPrototypeOf(Carousel)).call(this, $element));

        _this.done = false;
        _this.correct = _this.element.querySelector('[data-correct]');
        return _this;
    }

    _createClass(Carousel, [{
        key: 'prepare',
        value: function prepare() {
            var _this2 = this;

            this.ref.button.forEach(function (item) {
                item.removeAttribute('data-correct');
                item.addEventListener('click', _this2.handleClick.bind(_this2));
            });
        }
    }, {
        key: 'handleClick',
        value: function handleClick(event) {
            if (!this.done) {
                //<div class="Question-tag Question-tag--correct">Správně</div>

                var element = document.createElement('div');
                element.classList.add('Question-tag');
                element.classList.add('Question-tag--correct');
                element.innerHTML = "Správně";

                var errorElement = document.createElement('div');
                errorElement.classList.add('Question-tag');
                errorElement.classList.add('Question-tag--wrong');
                errorElement.innerHTML = "Špatně";

                if (event.currentTarget === this.correct) {
                    event.currentTarget.parentNode.innerHTML = element.outerHTML + event.currentTarget.parentNode.innerHTML;
                } else {
                    event.currentTarget.parentNode.innerHTML = errorElement.outerHTML + event.currentTarget.parentNode.innerHTML;
                    this.correct.parentNode.innerHTML = element.outerHTML + this.correct.parentNode.innerHTML;
                }
                this.done = true;
            }
        }
    }]);

    return Carousel;
}(_Component3.default);

exports.default = Carousel;

/***/ }),
/* 78 */
/***/ (function(module, exports, __webpack_require__) {

var __WEBPACK_AMD_DEFINE_RESULT__;;(function () {
	'use strict';

	/**
	 * @preserve FastClick: polyfill to remove click delays on browsers with touch UIs.
	 *
	 * @codingstandard ftlabs-jsv2
	 * @copyright The Financial Times Limited [All Rights Reserved]
	 * @license MIT License (see LICENSE.txt)
	 */

	/*jslint browser:true, node:true*/
	/*global define, Event, Node*/


	/**
	 * Instantiate fast-clicking listeners on the specified layer.
	 *
	 * @constructor
	 * @param {Element} layer The layer to listen on
	 * @param {Object} [options={}] The options to override the defaults
	 */
	function FastClick(layer, options) {
		var oldOnClick;

		options = options || {};

		/**
		 * Whether a click is currently being tracked.
		 *
		 * @type boolean
		 */
		this.trackingClick = false;


		/**
		 * Timestamp for when click tracking started.
		 *
		 * @type number
		 */
		this.trackingClickStart = 0;


		/**
		 * The element being tracked for a click.
		 *
		 * @type EventTarget
		 */
		this.targetElement = null;


		/**
		 * X-coordinate of touch start event.
		 *
		 * @type number
		 */
		this.touchStartX = 0;


		/**
		 * Y-coordinate of touch start event.
		 *
		 * @type number
		 */
		this.touchStartY = 0;


		/**
		 * ID of the last touch, retrieved from Touch.identifier.
		 *
		 * @type number
		 */
		this.lastTouchIdentifier = 0;


		/**
		 * Touchmove boundary, beyond which a click will be cancelled.
		 *
		 * @type number
		 */
		this.touchBoundary = options.touchBoundary || 10;


		/**
		 * The FastClick layer.
		 *
		 * @type Element
		 */
		this.layer = layer;

		/**
		 * The minimum time between tap(touchstart and touchend) events
		 *
		 * @type number
		 */
		this.tapDelay = options.tapDelay || 200;

		/**
		 * The maximum time for a tap
		 *
		 * @type number
		 */
		this.tapTimeout = options.tapTimeout || 700;

		if (FastClick.notNeeded(layer)) {
			return;
		}

		// Some old versions of Android don't have Function.prototype.bind
		function bind(method, context) {
			return function() { return method.apply(context, arguments); };
		}


		var methods = ['onMouse', 'onClick', 'onTouchStart', 'onTouchMove', 'onTouchEnd', 'onTouchCancel'];
		var context = this;
		for (var i = 0, l = methods.length; i < l; i++) {
			context[methods[i]] = bind(context[methods[i]], context);
		}

		// Set up event handlers as required
		if (deviceIsAndroid) {
			layer.addEventListener('mouseover', this.onMouse, true);
			layer.addEventListener('mousedown', this.onMouse, true);
			layer.addEventListener('mouseup', this.onMouse, true);
		}

		layer.addEventListener('click', this.onClick, true);
		layer.addEventListener('touchstart', this.onTouchStart, false);
		layer.addEventListener('touchmove', this.onTouchMove, false);
		layer.addEventListener('touchend', this.onTouchEnd, false);
		layer.addEventListener('touchcancel', this.onTouchCancel, false);

		// Hack is required for browsers that don't support Event#stopImmediatePropagation (e.g. Android 2)
		// which is how FastClick normally stops click events bubbling to callbacks registered on the FastClick
		// layer when they are cancelled.
		if (!Event.prototype.stopImmediatePropagation) {
			layer.removeEventListener = function(type, callback, capture) {
				var rmv = Node.prototype.removeEventListener;
				if (type === 'click') {
					rmv.call(layer, type, callback.hijacked || callback, capture);
				} else {
					rmv.call(layer, type, callback, capture);
				}
			};

			layer.addEventListener = function(type, callback, capture) {
				var adv = Node.prototype.addEventListener;
				if (type === 'click') {
					adv.call(layer, type, callback.hijacked || (callback.hijacked = function(event) {
						if (!event.propagationStopped) {
							callback(event);
						}
					}), capture);
				} else {
					adv.call(layer, type, callback, capture);
				}
			};
		}

		// If a handler is already declared in the element's onclick attribute, it will be fired before
		// FastClick's onClick handler. Fix this by pulling out the user-defined handler function and
		// adding it as listener.
		if (typeof layer.onclick === 'function') {

			// Android browser on at least 3.2 requires a new reference to the function in layer.onclick
			// - the old one won't work if passed to addEventListener directly.
			oldOnClick = layer.onclick;
			layer.addEventListener('click', function(event) {
				oldOnClick(event);
			}, false);
			layer.onclick = null;
		}
	}

	/**
	* Windows Phone 8.1 fakes user agent string to look like Android and iPhone.
	*
	* @type boolean
	*/
	var deviceIsWindowsPhone = navigator.userAgent.indexOf("Windows Phone") >= 0;

	/**
	 * Android requires exceptions.
	 *
	 * @type boolean
	 */
	var deviceIsAndroid = navigator.userAgent.indexOf('Android') > 0 && !deviceIsWindowsPhone;


	/**
	 * iOS requires exceptions.
	 *
	 * @type boolean
	 */
	var deviceIsIOS = /iP(ad|hone|od)/.test(navigator.userAgent) && !deviceIsWindowsPhone;


	/**
	 * iOS 4 requires an exception for select elements.
	 *
	 * @type boolean
	 */
	var deviceIsIOS4 = deviceIsIOS && (/OS 4_\d(_\d)?/).test(navigator.userAgent);


	/**
	 * iOS 6.0-7.* requires the target element to be manually derived
	 *
	 * @type boolean
	 */
	var deviceIsIOSWithBadTarget = deviceIsIOS && (/OS [6-7]_\d/).test(navigator.userAgent);

	/**
	 * BlackBerry requires exceptions.
	 *
	 * @type boolean
	 */
	var deviceIsBlackBerry10 = navigator.userAgent.indexOf('BB10') > 0;

	/**
	 * Determine whether a given element requires a native click.
	 *
	 * @param {EventTarget|Element} target Target DOM element
	 * @returns {boolean} Returns true if the element needs a native click
	 */
	FastClick.prototype.needsClick = function(target) {
		switch (target.nodeName.toLowerCase()) {

		// Don't send a synthetic click to disabled inputs (issue #62)
		case 'button':
		case 'select':
		case 'textarea':
			if (target.disabled) {
				return true;
			}

			break;
		case 'input':

			// File inputs need real clicks on iOS 6 due to a browser bug (issue #68)
			if ((deviceIsIOS && target.type === 'file') || target.disabled) {
				return true;
			}

			break;
		case 'label':
		case 'iframe': // iOS8 homescreen apps can prevent events bubbling into frames
		case 'video':
			return true;
		}

		return (/\bneedsclick\b/).test(target.className);
	};


	/**
	 * Determine whether a given element requires a call to focus to simulate click into element.
	 *
	 * @param {EventTarget|Element} target Target DOM element
	 * @returns {boolean} Returns true if the element requires a call to focus to simulate native click.
	 */
	FastClick.prototype.needsFocus = function(target) {
		switch (target.nodeName.toLowerCase()) {
		case 'textarea':
			return true;
		case 'select':
			return !deviceIsAndroid;
		case 'input':
			switch (target.type) {
			case 'button':
			case 'checkbox':
			case 'file':
			case 'image':
			case 'radio':
			case 'submit':
				return false;
			}

			// No point in attempting to focus disabled inputs
			return !target.disabled && !target.readOnly;
		default:
			return (/\bneedsfocus\b/).test(target.className);
		}
	};


	/**
	 * Send a click event to the specified element.
	 *
	 * @param {EventTarget|Element} targetElement
	 * @param {Event} event
	 */
	FastClick.prototype.sendClick = function(targetElement, event) {
		var clickEvent, touch;

		// On some Android devices activeElement needs to be blurred otherwise the synthetic click will have no effect (#24)
		if (document.activeElement && document.activeElement !== targetElement) {
			document.activeElement.blur();
		}

		touch = event.changedTouches[0];

		// Synthesise a click event, with an extra attribute so it can be tracked
		clickEvent = document.createEvent('MouseEvents');
		clickEvent.initMouseEvent(this.determineEventType(targetElement), true, true, window, 1, touch.screenX, touch.screenY, touch.clientX, touch.clientY, false, false, false, false, 0, null);
		clickEvent.forwardedTouchEvent = true;
		targetElement.dispatchEvent(clickEvent);
	};

	FastClick.prototype.determineEventType = function(targetElement) {

		//Issue #159: Android Chrome Select Box does not open with a synthetic click event
		if (deviceIsAndroid && targetElement.tagName.toLowerCase() === 'select') {
			return 'mousedown';
		}

		return 'click';
	};


	/**
	 * @param {EventTarget|Element} targetElement
	 */
	FastClick.prototype.focus = function(targetElement) {
		var length;

		// Issue #160: on iOS 7, some input elements (e.g. date datetime month) throw a vague TypeError on setSelectionRange. These elements don't have an integer value for the selectionStart and selectionEnd properties, but unfortunately that can't be used for detection because accessing the properties also throws a TypeError. Just check the type instead. Filed as Apple bug #15122724.
		if (deviceIsIOS && targetElement.setSelectionRange && targetElement.type.indexOf('date') !== 0 && targetElement.type !== 'time' && targetElement.type !== 'month') {
			length = targetElement.value.length;
			targetElement.setSelectionRange(length, length);
		} else {
			targetElement.focus();
		}
	};


	/**
	 * Check whether the given target element is a child of a scrollable layer and if so, set a flag on it.
	 *
	 * @param {EventTarget|Element} targetElement
	 */
	FastClick.prototype.updateScrollParent = function(targetElement) {
		var scrollParent, parentElement;

		scrollParent = targetElement.fastClickScrollParent;

		// Attempt to discover whether the target element is contained within a scrollable layer. Re-check if the
		// target element was moved to another parent.
		if (!scrollParent || !scrollParent.contains(targetElement)) {
			parentElement = targetElement;
			do {
				if (parentElement.scrollHeight > parentElement.offsetHeight) {
					scrollParent = parentElement;
					targetElement.fastClickScrollParent = parentElement;
					break;
				}

				parentElement = parentElement.parentElement;
			} while (parentElement);
		}

		// Always update the scroll top tracker if possible.
		if (scrollParent) {
			scrollParent.fastClickLastScrollTop = scrollParent.scrollTop;
		}
	};


	/**
	 * @param {EventTarget} targetElement
	 * @returns {Element|EventTarget}
	 */
	FastClick.prototype.getTargetElementFromEventTarget = function(eventTarget) {

		// On some older browsers (notably Safari on iOS 4.1 - see issue #56) the event target may be a text node.
		if (eventTarget.nodeType === Node.TEXT_NODE) {
			return eventTarget.parentNode;
		}

		return eventTarget;
	};


	/**
	 * On touch start, record the position and scroll offset.
	 *
	 * @param {Event} event
	 * @returns {boolean}
	 */
	FastClick.prototype.onTouchStart = function(event) {
		var targetElement, touch, selection;

		// Ignore multiple touches, otherwise pinch-to-zoom is prevented if both fingers are on the FastClick element (issue #111).
		if (event.targetTouches.length > 1) {
			return true;
		}

		targetElement = this.getTargetElementFromEventTarget(event.target);
		touch = event.targetTouches[0];

		if (deviceIsIOS) {

			// Only trusted events will deselect text on iOS (issue #49)
			selection = window.getSelection();
			if (selection.rangeCount && !selection.isCollapsed) {
				return true;
			}

			if (!deviceIsIOS4) {

				// Weird things happen on iOS when an alert or confirm dialog is opened from a click event callback (issue #23):
				// when the user next taps anywhere else on the page, new touchstart and touchend events are dispatched
				// with the same identifier as the touch event that previously triggered the click that triggered the alert.
				// Sadly, there is an issue on iOS 4 that causes some normal touch events to have the same identifier as an
				// immediately preceeding touch event (issue #52), so this fix is unavailable on that platform.
				// Issue 120: touch.identifier is 0 when Chrome dev tools 'Emulate touch events' is set with an iOS device UA string,
				// which causes all touch events to be ignored. As this block only applies to iOS, and iOS identifiers are always long,
				// random integers, it's safe to to continue if the identifier is 0 here.
				if (touch.identifier && touch.identifier === this.lastTouchIdentifier) {
					event.preventDefault();
					return false;
				}

				this.lastTouchIdentifier = touch.identifier;

				// If the target element is a child of a scrollable layer (using -webkit-overflow-scrolling: touch) and:
				// 1) the user does a fling scroll on the scrollable layer
				// 2) the user stops the fling scroll with another tap
				// then the event.target of the last 'touchend' event will be the element that was under the user's finger
				// when the fling scroll was started, causing FastClick to send a click event to that layer - unless a check
				// is made to ensure that a parent layer was not scrolled before sending a synthetic click (issue #42).
				this.updateScrollParent(targetElement);
			}
		}

		this.trackingClick = true;
		this.trackingClickStart = event.timeStamp;
		this.targetElement = targetElement;

		this.touchStartX = touch.pageX;
		this.touchStartY = touch.pageY;

		// Prevent phantom clicks on fast double-tap (issue #36)
		if ((event.timeStamp - this.lastClickTime) < this.tapDelay) {
			event.preventDefault();
		}

		return true;
	};


	/**
	 * Based on a touchmove event object, check whether the touch has moved past a boundary since it started.
	 *
	 * @param {Event} event
	 * @returns {boolean}
	 */
	FastClick.prototype.touchHasMoved = function(event) {
		var touch = event.changedTouches[0], boundary = this.touchBoundary;

		if (Math.abs(touch.pageX - this.touchStartX) > boundary || Math.abs(touch.pageY - this.touchStartY) > boundary) {
			return true;
		}

		return false;
	};


	/**
	 * Update the last position.
	 *
	 * @param {Event} event
	 * @returns {boolean}
	 */
	FastClick.prototype.onTouchMove = function(event) {
		if (!this.trackingClick) {
			return true;
		}

		// If the touch has moved, cancel the click tracking
		if (this.targetElement !== this.getTargetElementFromEventTarget(event.target) || this.touchHasMoved(event)) {
			this.trackingClick = false;
			this.targetElement = null;
		}

		return true;
	};


	/**
	 * Attempt to find the labelled control for the given label element.
	 *
	 * @param {EventTarget|HTMLLabelElement} labelElement
	 * @returns {Element|null}
	 */
	FastClick.prototype.findControl = function(labelElement) {

		// Fast path for newer browsers supporting the HTML5 control attribute
		if (labelElement.control !== undefined) {
			return labelElement.control;
		}

		// All browsers under test that support touch events also support the HTML5 htmlFor attribute
		if (labelElement.htmlFor) {
			return document.getElementById(labelElement.htmlFor);
		}

		// If no for attribute exists, attempt to retrieve the first labellable descendant element
		// the list of which is defined here: http://www.w3.org/TR/html5/forms.html#category-label
		return labelElement.querySelector('button, input:not([type=hidden]), keygen, meter, output, progress, select, textarea');
	};


	/**
	 * On touch end, determine whether to send a click event at once.
	 *
	 * @param {Event} event
	 * @returns {boolean}
	 */
	FastClick.prototype.onTouchEnd = function(event) {
		var forElement, trackingClickStart, targetTagName, scrollParent, touch, targetElement = this.targetElement;

		if (!this.trackingClick) {
			return true;
		}

		// Prevent phantom clicks on fast double-tap (issue #36)
		if ((event.timeStamp - this.lastClickTime) < this.tapDelay) {
			this.cancelNextClick = true;
			return true;
		}

		if ((event.timeStamp - this.trackingClickStart) > this.tapTimeout) {
			return true;
		}

		// Reset to prevent wrong click cancel on input (issue #156).
		this.cancelNextClick = false;

		this.lastClickTime = event.timeStamp;

		trackingClickStart = this.trackingClickStart;
		this.trackingClick = false;
		this.trackingClickStart = 0;

		// On some iOS devices, the targetElement supplied with the event is invalid if the layer
		// is performing a transition or scroll, and has to be re-detected manually. Note that
		// for this to function correctly, it must be called *after* the event target is checked!
		// See issue #57; also filed as rdar://13048589 .
		if (deviceIsIOSWithBadTarget) {
			touch = event.changedTouches[0];

			// In certain cases arguments of elementFromPoint can be negative, so prevent setting targetElement to null
			targetElement = document.elementFromPoint(touch.pageX - window.pageXOffset, touch.pageY - window.pageYOffset) || targetElement;
			targetElement.fastClickScrollParent = this.targetElement.fastClickScrollParent;
		}

		targetTagName = targetElement.tagName.toLowerCase();
		if (targetTagName === 'label') {
			forElement = this.findControl(targetElement);
			if (forElement) {
				this.focus(targetElement);
				if (deviceIsAndroid) {
					return false;
				}

				targetElement = forElement;
			}
		} else if (this.needsFocus(targetElement)) {

			// Case 1: If the touch started a while ago (best guess is 100ms based on tests for issue #36) then focus will be triggered anyway. Return early and unset the target element reference so that the subsequent click will be allowed through.
			// Case 2: Without this exception for input elements tapped when the document is contained in an iframe, then any inputted text won't be visible even though the value attribute is updated as the user types (issue #37).
			if ((event.timeStamp - trackingClickStart) > 100 || (deviceIsIOS && window.top !== window && targetTagName === 'input')) {
				this.targetElement = null;
				return false;
			}

			this.focus(targetElement);
			this.sendClick(targetElement, event);

			// Select elements need the event to go through on iOS 4, otherwise the selector menu won't open.
			// Also this breaks opening selects when VoiceOver is active on iOS6, iOS7 (and possibly others)
			if (!deviceIsIOS || targetTagName !== 'select') {
				this.targetElement = null;
				event.preventDefault();
			}

			return false;
		}

		if (deviceIsIOS && !deviceIsIOS4) {

			// Don't send a synthetic click event if the target element is contained within a parent layer that was scrolled
			// and this tap is being used to stop the scrolling (usually initiated by a fling - issue #42).
			scrollParent = targetElement.fastClickScrollParent;
			if (scrollParent && scrollParent.fastClickLastScrollTop !== scrollParent.scrollTop) {
				return true;
			}
		}

		// Prevent the actual click from going though - unless the target node is marked as requiring
		// real clicks or if it is in the whitelist in which case only non-programmatic clicks are permitted.
		if (!this.needsClick(targetElement)) {
			event.preventDefault();
			this.sendClick(targetElement, event);
		}

		return false;
	};


	/**
	 * On touch cancel, stop tracking the click.
	 *
	 * @returns {void}
	 */
	FastClick.prototype.onTouchCancel = function() {
		this.trackingClick = false;
		this.targetElement = null;
	};


	/**
	 * Determine mouse events which should be permitted.
	 *
	 * @param {Event} event
	 * @returns {boolean}
	 */
	FastClick.prototype.onMouse = function(event) {

		// If a target element was never set (because a touch event was never fired) allow the event
		if (!this.targetElement) {
			return true;
		}

		if (event.forwardedTouchEvent) {
			return true;
		}

		// Programmatically generated events targeting a specific element should be permitted
		if (!event.cancelable) {
			return true;
		}

		// Derive and check the target element to see whether the mouse event needs to be permitted;
		// unless explicitly enabled, prevent non-touch click events from triggering actions,
		// to prevent ghost/doubleclicks.
		if (!this.needsClick(this.targetElement) || this.cancelNextClick) {

			// Prevent any user-added listeners declared on FastClick element from being fired.
			if (event.stopImmediatePropagation) {
				event.stopImmediatePropagation();
			} else {

				// Part of the hack for browsers that don't support Event#stopImmediatePropagation (e.g. Android 2)
				event.propagationStopped = true;
			}

			// Cancel the event
			event.stopPropagation();
			event.preventDefault();

			return false;
		}

		// If the mouse event is permitted, return true for the action to go through.
		return true;
	};


	/**
	 * On actual clicks, determine whether this is a touch-generated click, a click action occurring
	 * naturally after a delay after a touch (which needs to be cancelled to avoid duplication), or
	 * an actual click which should be permitted.
	 *
	 * @param {Event} event
	 * @returns {boolean}
	 */
	FastClick.prototype.onClick = function(event) {
		var permitted;

		// It's possible for another FastClick-like library delivered with third-party code to fire a click event before FastClick does (issue #44). In that case, set the click-tracking flag back to false and return early. This will cause onTouchEnd to return early.
		if (this.trackingClick) {
			this.targetElement = null;
			this.trackingClick = false;
			return true;
		}

		// Very odd behaviour on iOS (issue #18): if a submit element is present inside a form and the user hits enter in the iOS simulator or clicks the Go button on the pop-up OS keyboard the a kind of 'fake' click event will be triggered with the submit-type input element as the target.
		if (event.target.type === 'submit' && event.detail === 0) {
			return true;
		}

		permitted = this.onMouse(event);

		// Only unset targetElement if the click is not permitted. This will ensure that the check for !targetElement in onMouse fails and the browser's click doesn't go through.
		if (!permitted) {
			this.targetElement = null;
		}

		// If clicks are permitted, return true for the action to go through.
		return permitted;
	};


	/**
	 * Remove all FastClick's event listeners.
	 *
	 * @returns {void}
	 */
	FastClick.prototype.destroy = function() {
		var layer = this.layer;

		if (deviceIsAndroid) {
			layer.removeEventListener('mouseover', this.onMouse, true);
			layer.removeEventListener('mousedown', this.onMouse, true);
			layer.removeEventListener('mouseup', this.onMouse, true);
		}

		layer.removeEventListener('click', this.onClick, true);
		layer.removeEventListener('touchstart', this.onTouchStart, false);
		layer.removeEventListener('touchmove', this.onTouchMove, false);
		layer.removeEventListener('touchend', this.onTouchEnd, false);
		layer.removeEventListener('touchcancel', this.onTouchCancel, false);
	};


	/**
	 * Check whether FastClick is needed.
	 *
	 * @param {Element} layer The layer to listen on
	 */
	FastClick.notNeeded = function(layer) {
		var metaViewport;
		var chromeVersion;
		var blackberryVersion;
		var firefoxVersion;

		// Devices that don't support touch don't need FastClick
		if (typeof window.ontouchstart === 'undefined') {
			return true;
		}

		// Chrome version - zero for other browsers
		chromeVersion = +(/Chrome\/([0-9]+)/.exec(navigator.userAgent) || [,0])[1];

		if (chromeVersion) {

			if (deviceIsAndroid) {
				metaViewport = document.querySelector('meta[name=viewport]');

				if (metaViewport) {
					// Chrome on Android with user-scalable="no" doesn't need FastClick (issue #89)
					if (metaViewport.content.indexOf('user-scalable=no') !== -1) {
						return true;
					}
					// Chrome 32 and above with width=device-width or less don't need FastClick
					if (chromeVersion > 31 && document.documentElement.scrollWidth <= window.outerWidth) {
						return true;
					}
				}

			// Chrome desktop doesn't need FastClick (issue #15)
			} else {
				return true;
			}
		}

		if (deviceIsBlackBerry10) {
			blackberryVersion = navigator.userAgent.match(/Version\/([0-9]*)\.([0-9]*)/);

			// BlackBerry 10.3+ does not require Fastclick library.
			// https://github.com/ftlabs/fastclick/issues/251
			if (blackberryVersion[1] >= 10 && blackberryVersion[2] >= 3) {
				metaViewport = document.querySelector('meta[name=viewport]');

				if (metaViewport) {
					// user-scalable=no eliminates click delay.
					if (metaViewport.content.indexOf('user-scalable=no') !== -1) {
						return true;
					}
					// width=device-width (or less than device-width) eliminates click delay.
					if (document.documentElement.scrollWidth <= window.outerWidth) {
						return true;
					}
				}
			}
		}

		// IE10 with -ms-touch-action: none or manipulation, which disables double-tap-to-zoom (issue #97)
		if (layer.style.msTouchAction === 'none' || layer.style.touchAction === 'manipulation') {
			return true;
		}

		// Firefox version - zero for other browsers
		firefoxVersion = +(/Firefox\/([0-9]+)/.exec(navigator.userAgent) || [,0])[1];

		if (firefoxVersion >= 27) {
			// Firefox 27+ does not have tap delay if the content is not zoomable - https://bugzilla.mozilla.org/show_bug.cgi?id=922896

			metaViewport = document.querySelector('meta[name=viewport]');
			if (metaViewport && (metaViewport.content.indexOf('user-scalable=no') !== -1 || document.documentElement.scrollWidth <= window.outerWidth)) {
				return true;
			}
		}

		// IE11: prefixed -ms-touch-action is no longer supported and it's recomended to use non-prefixed version
		// http://msdn.microsoft.com/en-us/library/windows/apps/Hh767313.aspx
		if (layer.style.touchAction === 'none' || layer.style.touchAction === 'manipulation') {
			return true;
		}

		return false;
	};


	/**
	 * Factory method for creating a FastClick object
	 *
	 * @param {Element} layer The layer to listen on
	 * @param {Object} [options={}] The options to override the defaults
	 */
	FastClick.attach = function(layer, options) {
		return new FastClick(layer, options);
	};


	if (true) {

		// AMD. Register as an anonymous module.
		!(__WEBPACK_AMD_DEFINE_RESULT__ = (function() {
			return FastClick;
		}).call(exports, __webpack_require__, exports, module),
				__WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));
	} else if (typeof module !== 'undefined' && module.exports) {
		module.exports = FastClick.attach;
		module.exports.FastClick = FastClick;
	} else {
		window.FastClick = FastClick;
	}
}());


/***/ })
/******/ ]);
//# sourceMappingURL=app.js.map